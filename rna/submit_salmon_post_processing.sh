#!/bin/bash

#$ -cwd
#$ -N salmon_post_processing
#$ -j y
#$ -S /bin/bash
#$ -l h_vmem=250G
#$ -l h_rt=2:00:00

export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/R4.3/bin:$PATH
Rscript /exports/igmm/eddie/Glioblastoma-WGS/scripts/fame_gbm/R/rna/salmon_downstream.R

