#!/bin/bash

# To run this script, do 
# qsub -t 1-n  submit_star2trinity.sh CONFIG IDS
#
#$ -N star2trinity
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=8G
#$ -pe sharedmem 8
#$ -l h_rt=72:00:00

unset MODULEPATH
. /etc/profile.d/modules.sh

CONFIG=$1
IDS=$2

PATIENT_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1`

source $CONFIG
source activate starfusion

## Build genome index
## Based on https://physiology.med.cornell.edu/faculty/skrabanek/lab/angsd/lecture_notes/STARmanual.pdf
## and https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4631051/#S8title

##cd $STAR_GENOME_DIR

##STAR \
##    --runThreadN 20 \
##    --runMode genomeGenerate \
##    --genomeDir $STAR_GENOME_DIR \
##    --genomeFastaFiles $STAR_GENOME \
##    --sjdbGTFfile $STAR_GTF \
##    --sjdbOverhang 99

## Use two-pass method to quantify expression, and generate bam files for downstream analysis.
## Run in transcriptomic output for RSEM quantification.
## Based on https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4631051/#S8title and 
## https://github.com/deweylab/RSEM#example-main

## Define fastq files
##R1=$RNA_READS/${PATIENT_ID}_1.fastq.gz
##R2=$RNA_READS/${PATIENT_ID}_2.fastq.gz

R1=$RNA_READS/${PATIENT_ID}_R1_001.fastq.gz
R2=$RNA_READS/${PATIENT_ID}_R2_001.fastq.gz

cd $STAR_RESULTS 

##STAR \
##    --runThreadN 20 \
    --genomeDir $STAR_GENOME_DIR \
    --sjdbGTFfile $STAR_GTF \
    --sjdbOverhang 99 \
    --readFilesIn $R1 $R2 \
    --readFilesCommand zcat \
    --outFileNamePrefix $PATIENT_ID \
    --twopassMode Basic \
    --quantMode TranscriptomeSAM \
    --outSAMtype BAM SortedByCoordinate \
    --outSAMunmapped Within \
    --outSAMattributes Standard \
    --chimSegmentMin 12 \
    --chimJunctionOverhangMin 8 \
    --chimOutJunctionFormat 1 \
    --alignSJDBoverhangMin 10 \
    --alignMatesGapMax 100000 \
    --alignIntronMax 100000 \
    --alignSJstitchMismatchNmax 5 -1 5 5 \
    --outSAMattrRGline ID:GRPundef \
    --chimMultimapScoreRange 3 \
    --chimScoreJunctionNonGTAG -4 \
    --chimMultimapNmax 20 \
    --chimNonchimScoreDropMin 10 \
     --peOverlapNbasesMin 12 \
     --peOverlapMMp 0.1 \
     --alignInsertionFlush Right \
     --alignSplicedMateMapLminOverLmate 0 \
     --alignSplicedMateMapLmin 30

## Generate signal files:
##if [ ! -f ${PATIENT_ID}.bw ]
##then
##echo "Generating bedgraph file..."
##STAR \
##    --runMode inputAlignmentsFromBAM \
    --inputBAMfile ${PATIENT_ID}Aligned.sortedByCoord.out.bam \
    --outWigType bedGraph \
    --outWigStrand Stranded
##fi

## Generate fusion files: 
## Based on https://github.com/STAR-Fusion/STAR-Fusion and https://github.com/FusionInspector/FusionInspector/wiki
## Need to download ready made CTAT library from that is compatible with both STAR and STAR-Fusion versions.
## Refer to https://github.com/STAR-Fusion/STAR-Fusion/wiki/STAR-Fusion-release-and-CTAT-Genome-Lib-Compatibility-Matrix
## Need samtools version 1.9
## Need cpan::assert perl module installed

if [ ! -d $STAR_FUSION_DIR/${PATIENT_ID} ]
then
mkdir -p $STAR_FUSION_DIR/${PATIENT_ID}
cp ${PATIENT_ID}Chimeric.out.junction $STAR_FUSION_DIR/${PATIENT_ID}/Chimeric.out.junction
fi

###export TRINITY_HOME=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/trinity/bin
##export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/trinity/bin:$PATH
STARFUSION=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/starfusion/bin/STAR-Fusion

cd $STAR_FUSION_DIR/${PATIENT_ID}

$STARFUSION --genome_lib_dir $STAR_FUSION_LIB \
    --chimeric_junction Chimeric.out.junction \
    --left_fq $R1 \
    --right_fq $R2 \
    --CPU 50 \
    --output_dir $STAR_FUSION_DIR/${PATIENT_ID}

##   --FusionInspector validate \
##--denovo_reconstruct \
##   --examine_coding_effect \
##   --extract_fusion_reads \









