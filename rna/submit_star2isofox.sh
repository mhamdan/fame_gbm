#!/bin/bash

# To run this script, do 
# qsub -t 1-n submit_star2isofox.sh CONFIG IDS
#
#$ -N star2isofox
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=250G
####$ -pe sharedmem 16
#$ -l h_rt=250:00:00

CONFIG=$1
IDS=$2

source $CONFIG

BATCH=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 1`
FASTQ_FILE=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 2`
SAMPLE_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 3`
SAMPLE_TYPE=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 4`

R1=${FASTQ_FILE}_R1.fastq.gz
R2=${FASTQ_FILE}_R2.fastq.gz

### Based on https://github.com/hartwigmedical/hmftools/blob/master/isofox/README.md

STAR=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/starfusion/bin/STAR

## Index genome (run once)
## Both fasta file and index, and gtf file need to be from Gencode (with "chr" suffix), version 36.
## cd $STAR_GENOME_DIR
##$STAR \
##    --runThreadN 20 \
##    --runMode genomeGenerate \
##    --genomeDir $STAR_GENOME_DIR \
##    --genomeFastaFiles $STAR_GENOME \
##    --sjdbGTFfile $STAR_GTF \
##    --sjdbOverhang 99

## Run STAR with ISOFOX speficic settings.
if [ ! -d ${STAR_ISOFOX}/${SAMPLE_ID} ]
then
mkdir -p ${STAR_ISOFOX}/${SAMPLE_ID} && cd ${STAR_ISOFOX}/${SAMPLE_ID}
else
cd ${STAR_ISOFOX}/${SAMPLE_ID}
fi

if [ ! -f ${SAMPLE_ID}Aligned.out.bam ]
then
echo "No STAR alignment bam for ISOFOX analysis, running STAR now..."    
$STAR \
    --runThreadN 20 \
    --genomeDir $STAR_GENOME_DIR \
    --sjdbGTFfile $STAR_GTF \
    --readFilesIn $R1 $R2 \
    --readFilesCommand zcat \
    --outFileNamePrefix $SAMPLE_ID \
    --outSAMtype BAM Unsorted --outSAMunmapped Within --outBAMcompression 0 --outSAMattributes All --outFilterMultimapNmax 10 \
    --outFilterMismatchNmax 3 limitOutSJcollapsed 3000000 -chimSegmentMin 10 --chimOutType WithinBAM SoftClip \
    --chimJunctionOverhangMin 10 --chimSegmentReadGapMax 3 --chimScoreMin 1 --chimScoreDropMax 30 --chimScoreJunctionNonGTAG 0 \
    --chimScoreSeparation 1 --outFilterScoreMinOverLread 0.33 --outFilterMatchNminOverLread 0.33 --outFilterMatchNmin 35 \
    --alignSplicedMateMapLminOverLmate 0.33 --alignSplicedMateMapLmin 35 --alignSJstitchMismatchNmax 5 -1 5 5
echo "STAR alignment for ISOFOX done."    
fi

## Tidy outputs for ISOFOX.
if [ ! -f ${SAMPLE_ID}.final.bam ]
then
echo "Sorting and indexing bam file"
samtools sort -@ 10 -n -o ${SAMPLE_ID}Aligned.sorted.bam ${SAMPLE_ID}Aligned.out.bam
samtools fixmate -m ${SAMPLE_ID}Aligned.sorted.bam ${SAMPLE_ID}Aligned.sorted.fixmate.bam
samtools sort -@ 10 -o ${SAMPLE_ID}Aligned.sorted.fixmate.position.sorted.bam ${SAMPLE_ID}Aligned.sorted.fixmate.bam
samtools markdup -@ 4 ${SAMPLE_ID}Aligned.sorted.fixmate.position.sorted.bam ${SAMPLE_ID}.final.bam
samtools index -@ 4 -b ${SAMPLE_ID}.final.bam ${SAMPLE_ID}.final.bam.bai
rm *sorted*
fi

## Run ISOFOX
if [[ ! -f ${SAMPLE_ID}.isf.gene_data.csv ]]
then
echo "Sorted and indexed bam file for ISOFOX generated..."
echo "Running ISOFOX..."
java $JVM_OPTS $JVM_TMP_DIR -jar $ISOFOX_JAR \
    -sample ${SAMPLE_ID} \
    -functions "TRANSCRIPT_COUNTS;NOVEL_LOCATIONS;FUSIONS" \
    -bam_file ${SAMPLE_ID}.final.bam \
    -ref_genome $STAR_GENOME \
    -ref_genome_version HG38 \
    -gene_transcripts_dir $HMF_ENSEMBLE \
    -output_dir $STAR_ISOFOX/${SAMPLE_ID} \
    -apply_calc_frag_lengths \
    -apply_exp_rates \
    -exp_counts_file $ISOFOX_EXPECTED_COUNTS \
    -apply_gc_bias_adjust \
    -exp_gc_ratios_file $ISOFOX_EXPECTED_GC \
    -long_frag_limit 550 \
    -apply_map_qual_adjust \
    -known_fusion_file $ISOFOX_KNOWN_FUSION \
    -threads 8 
echo "ISOFOX done."
else
echo "ISOFOX data already generated, shutting down."
fi    
    
###########     
## Generate read count and GC bias estimates. Run once but cater for read lengths.
###java $JVM_OPTS $JVM_TMP_DIR -jar $ISOFOX_JAR \
##    -functions "EXPECTED_TRANS_COUNTS" \
##    -output_dir $STAR_ISOFOX \
##    -gene_transcripts_dir $HMF_ENSEMBLE \
##    -read_length 135 \
##    -long_frag_limit 550 \
##    -exp_rate_frag_lengths "50-0;75-0;100-0;125-0;150-0;200-0;250-0;300-0;400-0;550-0" \
# #   -threads 20 

##$JVM_OPTS $JVM_TMP_DIR -jar $ISOFOX_JAR \
##    -functions "EXPECTED_GC_COUNTS" \
##    -output_dir $STAR_ISOFOX \
##    -ref_genome $STAR_GENOME \
##    -gene_transcripts_dir $HMF_ENSEMBLE \
##    -read_length 135 \
##    -threads 20 
    
    
