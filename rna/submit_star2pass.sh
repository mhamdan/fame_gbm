#!/bin/bash

# To run this script, do 
# qsub -t 1-n submit_star2pass.sh CONFIG IDS
#
#$ -N star2pass
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=8G
#$ -pe sharedmem 8
#$ -l h_rt=72:00:00

unset MODULEPATH
. /etc/profile.d/modules.sh

CONFIG=$1
IDS=$2

BATCH=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 1`
FASTQ_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 2`
SAMPLE_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 3`
SAMPLE_TYPE=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 4`

source $CONFIG
source activate starfusion

## Build genome index (run once)
## Based on https://physiology.med.cornell.edu/faculty/skrabanek/lab/angsd/lecture_notes/STARmanual.pdf
## and https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4631051/#S8title

##cd $STAR_GENOME_DIR

##STAR \
##    --runThreadN 20 \
##    --runMode genomeGenerate \
##    --genomeDir $STAR_GENOME_DIR \
##    --genomeFastaFiles $STAR_GENOME \
##    --sjdbGTFfile $STAR_GTF \
##    --sjdbOverhang 99

## Use two-pass method to quantify expression, and generate bam files for downstream analysis.
## Run in transcriptomic output for RSEM quantification.
## Based on https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4631051/#S8title and 
## https://github.com/deweylab/RSEM#example-main

## Define fastq files
INPUT_DIR=/exports/igmm/eddie/Glioblastoma-WGS/RNA-seq/raw/source/${BATCH}/fastqs
R1=$INPUT_DIR/${FASTQ_ID}_R1.fastq.gz
R2=$INPUT_DIR/${FASTQ_ID}_R2.fastq.gz

cd $STAR_RESULTS 

STAR \
    --runThreadN 20 \
    --genomeDir $STAR_GENOME_DIR \
    --sjdbGTFfile $STAR_GTF \
    --sjdbOverhang 99 \
    --readFilesIn $R1 $R2 \
    --readFilesCommand zcat \
    --outFileNamePrefix ${BATCH}_${SAMPLE_ID}_${SAMPLE_TYPE} \
    --twopassMode Basic \
    --quantMode TranscriptomeSAM \
    --outSAMtype BAM SortedByCoordinate \
    --outSAMunmapped Within \
    --outSAMattributes Standard \
    --chimSegmentMin 12 \
    --chimJunctionOverhangMin 8 \
    --chimOutJunctionFormat 1 \
    --alignSJDBoverhangMin 10 \
    --alignMatesGapMax 100000 \
    --alignIntronMax 100000 \ 
    --alignSJstitchMismatchNmax 5 -1 5 5 \
    --outSAMattrRGline ID:GRPundef \
    --chimMultimapScoreRange 3 \
    --chimScoreJunctionNonGTAG -4 \
    --chimMultimapNmax 20 \
    --chimNonchimScoreDropMin 10 \
     --peOverlapNbasesMin 12 \
     --peOverlapMMp 0.1 \
     --alignInsertionFlush Right \
     --alignSplicedMateMapLminOverLmate 0 \
     --alignSplicedMateMapLmin 30

## Generate signal files:
##if [ ! -f ${PATIENT_ID}.bw ]
##then
##echo "Generating coverage file..."
##STAR \
##    --runMode inputAlignmentsFromBAM \
##    --inputBAMfile ${BATCH}_${SAMPLE_ID}_${SAMPLE_TYPE}Aligned.sortedByCoord.out.bam \
##    --outWigType bw \
##    --outWigStrand Stranded
##fi








