#!/bin/bash

#This script runs kallisto algorithm to call gene expression from bulk RNA-seq data.
#It takes a pre-indexed transcriptome and paired-end fastq files (even numbers)
#To run: qsub -t <1-N> submit_kallisto.sh CONFIG IDS

#$ -cwd
#$ -N kallisto
#$ -j y
#$ -S /bin/bash
#$ -l h_vmem=8G
#$ -l h_rt=32:00:00
#$ -pe sharedmem 8

CONFIG=$1
IDS=$2

source $CONFIG

BATCH=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 1`
FASTQ_FILE=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 2`
SAMPLE_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 3`
SAMPLE_TYPE=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 4`

OUTPUT_DIR=/exports/igmm/eddie/Glioblastoma-WGS/RNA-seq/results/kallisto/h5/${BATCH}_${SAMPLE_ID}_${SAMPLE_TYPE}

## Kallisto version 0.46.0

## Index file v94 (within $CONFIG), downloaded from https://github.com/pachterlab/kallisto-transcriptome-indices/releases
## KALLISTO_INDEX=/exports/igmm/eddie/Glioblastoma-WGS/resources/RNA-seq_resources/kallisto/Homo_sapiens.GRCh38.cdna.all.release-94_k31.idx

mkdir $OUTPUT_DIR

#Kallisto quant
#Create separate directories for each sample kallisto output.
kallisto quant \
    -i $KALLISTO_INDEX \
    -t 20 \
    --bias \
    --fusion \
    -o $OUTPUT_DIR \
    ${FASTQ_FILE}_R1.fastq.gz ${FASTQ_FILE}_R2.fastq.gz

cd $OUTPUT_DIR
rm run_info.json
mv fusion.txt ../../fusion/${BATCH}_${SAMPLE_ID}_${SAMPLE_TYPE}.fusion.txt


###### for downstream analysis use tips from https://rnabio.org/module-04-kallisto/0004/02/01/Alignment_Free_Kallisto/ to combine abundance data from multiple samples.
##paste */abundance.tsv | cut -f 1,2,5,10,15,20,25,30 > transcript_tpms_all_samples.tsv
##ls -1 */abundance.tsv | perl -ne 'chomp $_; if ($_ =~ /(\S+)\/abundance\.tsv/){print "\t$1"}' | perl -ne 'print "target_id\tlength$_\n"' > header.tsv
##cat header.tsv transcript_tpms_all_samples.tsv | grep -v "tpm" > transcript_tpms_all_samples.tsv2
##mv transcript_tpms_all_samples.tsv2 transcript_tpms_all_samples.tsv
##rm -f header.tsv
## Compile txg as from https://support.bioconductor.org/p/123134/


