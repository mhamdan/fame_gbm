#!/bin/bash

#$ -cwd
#$ -N salmon_index
#$ -j y
#$ -S /bin/bash
#$ -l h_vmem=150G
#$ -l h_rt=25:00:00

## Based on https://combine-lab.github.io/alevin-tutorial/2019/selective-alignment/
## Step 0: Get binary version of Salmon v1.10.0
##wget https://github.com/COMBINE-lab/salmon/releases/download/v1.10.0/salmon-1.10.0_linux_x86_64.tar.gz
##tar xzvf salmon-1.10.0_linux_x86_64.tar.gz
export PATH=/exports/igmm/eddie/Glioblastoma-WGS/scripts/salmon-latest_linux_x86_64/bin:$PATH

## For all genes:
## Step 1: Download the reference transcriptome and concatenate decoy sequences
RNA_INDICES_DIR=/exports/igmm/eddie/Glioblastoma-WGS/RNA-seq/results/salmon/indices
cd $RNA_INDICES_DIR
##wget ftp://ftp.ebi.ac.uk/pub/databases/gencode/Gencode_human/release_44/gencode.v44.transcripts.fa.gz
##wget ftp://ftp.ebi.ac.uk/pub/databases/gencode/Gencode_human/release_44/GRCh38.primary_assembly.genome.fa.gz
##grep "^>" <(gunzip -c GRCh38.primary_assembly.genome.fa.gz) | cut -d " " -f 1 > decoys.txt
##sed -i.bak -e 's/>//g' decoys.txt
##cat gencode.v44.transcripts.fa.gz GRCh38.primary_assembly.genome.fa.gz > gentrome.gencode.v44.fa.gz
##rm decoys.txt.bak

## Step 2: Build the salmon index
salmon index -t gentrome.gencode.v44.fa.gz -d decoys.txt -p 32 -i gentrome.gencode.v44.salmon.index --gencode -k 31

## Repeat for noncoding RNA genes:
## Step 1: Download the reference transcriptome and concatenate decoy sequences
##wget ftp://ftp.ebi.ac.uk/pub/databases/gencode/Gencode_human/release_44/gencode.v44.lncRNA_transcripts.fa.gz
##cat gencode.v44.lncRNA_transcripts.fa.gz GRCh38.primary_assembly.genome.fa.gz > lncrna.gencode.v44.fa.gz

## Step 2: Build the salmon index
##salmon index -t lncrna.gencode.v44.fa.gz -d decoys.txt -p 32 -i lncrna.gencode.v44.salmon.index --gencode -k 31

## Generate transcript-to-gene mapping files:
## For all genes:
##zcat gencode.v44.transcripts.fa.gz | grep "^>" | cut -f 1,4 | sed -e 's/^>//' -e 's/gene://' -e 's/\.[0-9]*$//' | tr ' ' '\t' > gencode.v44.transcripts_to_genes.txt

## For lncRNA genes:
##zcat gencode.v44.lncRNA_transcripts.fa.gz | grep "^>" | cut -f 1,4 | sed -e 's/^>//' -e 's/gene://' -e 's/\.[0-9]*$//' | tr ' ' '\t' > gencode.v44.lncRNA_transcripts_to_genes.txt

### For mouse (just for all genes):
##cd $RNA_INDICES_DIR/mouse
##wget ftp://ftp.ebi.ac.uk/pub/databases/gencode/Gencode_mouse/release_M33/gencode.vM33.transcripts.fa.gz
##wget ftp://ftp.ebi.ac.uk/pub/databases/gencode/Gencode_mouse/release_M33/GRCm39.primary_assembly.genome.fa.gz
##grep "^>" <(gunzip -c GRCm39.primary_assembly.genome.fa.gz) | cut -d " " -f 1 > decoys.txt
##sed -i.bak -e 's/>//g' decoys.txt
##cat gencode.vM33.transcripts.fa.gz GRCm39.primary_assembly.genome.fa.gz > gentrome.gencode.vM33.fa.gz

## Step 2: Build the salmon index
##salmon index -t gentrome.gencode.vM33.fa.gz -d decoys.txt -p 32 -i gentrome.gencode.vM33.salmon.index --gencode -k 31






