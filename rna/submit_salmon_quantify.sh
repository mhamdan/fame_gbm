#!/bin/bash

#$ -cwd
#$ -N salmon_quantify
#$ -j y
#$ -S /bin/bash
#$ -l h_vmem=12G
#$ -l h_rt=3:00:00
#$ -pe sharedmem 12

## Salmon version v1.10.0 using pre-build binary downloaded from https://github.com/COMBINE-lab/salmon/releases
## https://github.com/COMBINE-lab/salmon/releases/download/v1.10.0/salmon-1.10.0_linux_x86_64.tar.gz

IDS=$1

BATCH=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 1`
FASTQ_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 2`
SAMPLE_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 3`
TYPE=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 4`
TMP_DIR=/exports/igmm/eddie/Glioblastoma-WGS/RNA-seq/results/salmon/preprocessed_AH/fastp

## Trim using fastp:
echo "Automatically detecting adaptor sequences and removing them..."
if [ ! -f ${TMP_DIR}/fastqs/${SAMPLE_ID}_R2.fastq.gz]; then 
    /exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/snakemake/bin/fastp -i ${FASTQ_ID}_R1.fastq.gz -I ${FASTQ_ID}_R2.fastq.gz \
        -o ${TMP_DIR}/fastqs/${SAMPLE_ID}_R1.fastq.gz -O ${TMP_DIR}/fastqs/${SAMPLE_ID}_R2.fastq.gz \
        -h ${TMP_DIR}/qc/${SAMPLE_ID}.fastp.html -w 16
fi


## Quantify using salmon, with bootstrapping:
export PATH=/exports/igmm/eddie/Glioblastoma-WGS/scripts/salmon-latest_linux_x86_64/bin:$PATH

### For all genes:
OUTPUT_DIR_ALL_GENES=/exports/igmm/eddie/Glioblastoma-WGS/RNA-seq/results/salmon/preprocessed_AH/salmon_b100/all_genes
salmon quant -i /exports/igmm/eddie/Glioblastoma-WGS/RNA-seq/results/salmon/indices/gentrome.gencode.v44.salmon.index \
             -l A \
             -1 ${TMP_DIR}/fastqs/${SAMPLE_ID}_R1.fastq.gz \
             -2 ${TMP_DIR}/fastqs/${SAMPLE_ID}_R2.fastq.gz \
             -p 12 \
             -o $OUTPUT_DIR_ALL_GENES/${SAMPLE_ID} \
             --gcBias \
             --validateMappings \
             --seqBias \
             --numBootstraps 100

### For just long noncoding RNA genes:
##LNCRNA_INDEX=/exports/igmm/eddie/Glioblastoma-WGS/RNA-seq/results/salmon/indices/lncrna.gencode.v44.salmon.index
##OUTPUT_DIR_LNCRNA_GENES=/exports/igmm/eddie/Glioblastoma-WGS/RNA-seq/results/salmon/preprocessed_AH/salmon_b100/lncRNA
##salmon quant -i $LNCRNA_INDEX \
             -l A \
             -1 ${TMP_DIR}/fastqs/${SAMPLE_ID}_R1.fastq.gz \
             -2 ${TMP_DIR}/fastqs/${SAMPLE_ID}_R2.fastq.gz \
             -p 12 \
             -o $OUTPUT_DIR_LNCRNA_GENES/${SAMPLE_ID} \
             --gcBias \
             --validateMappings \
             --seqBias \
             --numBootstraps 100



