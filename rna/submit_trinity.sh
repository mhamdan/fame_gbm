#!/bin/bash

# To run this script, do 
# qsub -t 1-n submit_trinity.sh CONFIG IDS
#
#$ -N trinity
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=8G
#$ -pe sharedmem 8
#$ -l h_rt=72:00:00

CONFIG=$1
IDS=$2

source $CONFIG

PATIENT_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1`
R1=$RNA_READS/${PATIENT_ID}_R1_001.fastq.gz
R2=$RNA_READS/${PATIENT_ID}_R2_001.fastq.gz

mkdir -p ${TRINITY_DIR}/${PATIENT_ID}_trinity
cd ${TRINITY_DIR}/${PATIENT_ID}_trinity

source activate trinity 

Trinity --seqType fq --left $R1 --right $R2 \
    --max_memory 50G \
    --output ${TRINITY_DIR}/${PATIENT_ID}_trinity


