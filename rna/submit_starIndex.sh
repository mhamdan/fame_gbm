#!/bin/bash

## Build a STAR index specific to the STAR version:

#$ -N indexSTAR
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=250G
#$ -l h_rt=10:00:00

STAR_BIN=$1
STAR_GENOME=$2
STAR_GTF=$3
STAR_INDEX_DIR=$4

## Index genome (run once)
## Both fasta file and index, and gtf file need to be from Gencode (with "chr" suffix), version 36.

$STAR_BIN \
    --runThreadN 20 \
    --runMode genomeGenerate \
    --genomeDir $STAR_INDEX_DIR \
    --genomeFastaFiles $STAR_GENOME \
    --sjdbGTFfile $STAR_GTF \
    --sjdbOverhang 99






