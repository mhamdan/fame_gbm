#!/bin/bash

# To run this script, do 
# qsub -t n submit_cicero.sh CONFIG IDS
#
#$ -N CICERO
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=8G
#$ -pe sharedmem 8
#$ -l h_rt=120:00:00

## Based on https://github.com/stjude/CICERO
## CICERO is a clinical grade RNA-seq fusion analyser.
## Need BLAT server installed.

CONFIG=$1
IDS=$2

PATIENT_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1`

source $CONFIG

export PATH=/exports/igmm/eddie/Glioblastoma-WGS/scripts/CICERO-0.3.0/src/scripts:$PATH
export PERL5LIB=/exports/igmm/eddie/Glioblastoma-WGS/scripts/CICERO-0.3.0/src/perllib
export PERL5LIB=/exports/igmm/eddie/Glioblastoma-WGS/scripts/CICERO-0.3.0/dependencies/lib/perl
export PATH=/exports/igmm/eddie/Glioblastoma-WGS/scripts/gfServer:$PATH

Cicero.sh \
    -n 16 \
    -b $STAR_ALIGNED_BAM \
    -g GRCh38_no_alt \
    -r $CICERO_REF \
    -o $CICERO_DIR




