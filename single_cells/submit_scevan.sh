#!/bin/bash

#$ -N scevan
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=32G
#$ -pe sharedmem 16
#$ -l h_rt=60:00:00

export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/seurat/bin:$PATH

Rscript /exports/igmm/eddie/Glioblastoma-WGS/scripts/fame_gbm/R/scevan.R




