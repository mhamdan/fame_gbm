#!/bin/bash

#$ -N arc_count
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=16G
#$ -pe sharedmem 16
#$ -l h_rt=48:00:00

## Full path to library files with sample id in column 1 and full path to library file for that sample in column 2
LIBRARY_LIST=$1

SAMPLE_ID=`head -n $SGE_TASK_ID $LIBRARY_LIST | cut -f 1| tail -n 1`
LIBRARY=`head -n $SGE_TASK_ID $LIBRARY_LIST | cut -f 2| tail -n 1`

BAM_DIR=/gpfs/igmmfs01/eddie/Glioblastoma-WGS/scmultiome/bams
ARC_REF=/gpfs/igmmfs01/eddie/Glioblastoma-WGS/scripts/10x/refdata-cellranger-arc-GRCh38-2020-A-2.0.0
export PATH=/exports/igmm/eddie/Glioblastoma-WGS/scripts/10x/cellranger-arc-2.0.0:$PATH

cd $BAM_DIR

cellranger-arc count --id=${SAMPLE_ID} \
                       --reference=${ARC_REF} \
                       --libraries=${LIBRARY} 










