#!/bin/bash

#$ -N cellranger_count
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=16G
#$ -pe sharedmem 16
#$ -l h_rt=48:00:00

## Full path to library files
LIBRARY_LIST=$1

SAMPLE_ID=`head -n $SGE_TASK_ID $LIBRARY_LIST | cut -f 1| tail -n 1`

BAM_DIR=/gpfs/igmmfs01/eddie/Glioblastoma-WGS/scmultiome/bams/troubleshoot/gex
FASTQ_DIR=/exports/igmm/eddie/Glioblastoma-WGS/scmultiome/fastqs/gex/LEB02_${SAMPLE_ID}
COUNT_REF=/gpfs/igmmfs01/eddie/Glioblastoma-WGS/scripts/10x/refdata-gex-GRCh38-2020-A
export PATH=/exports/igmm/eddie/Glioblastoma-WGS/scripts/10x/cellranger-6.0.2:$PATH

cd $BAM_DIR

cellranger count --id=$SAMPLE_ID \
                        --fastqs=$FASTQ_DIR \
                        --sample=$SAMPLE_ID \
                        --include-introns \
                        --chemistry=ARC-v1 \
                        --transcriptome=$COUNT_REF
                        
                        

