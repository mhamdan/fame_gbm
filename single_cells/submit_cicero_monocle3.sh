#!/bin/bash

#$ -N cicero_monocle3
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=350G
#$ -l h_rt=40:00:00

export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/monocle/bin:$PATH
export TMPDIR=/exports/igmm/eddie/Glioblastoma-WGS/scmultiome/tmp

SAMPLE=$1

Rscript --vanilla /exports/igmm/eddie/Glioblastoma-WGS/scripts/fame_gbm/R/cicero_monocle3.R $SAMPLE




