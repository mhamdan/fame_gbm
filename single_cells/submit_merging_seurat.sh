#!/bin/bash

#$ -N merging_seurat
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=128G
#########$ -pe sharedmem 16
#$ -l h_rt=72:00:00

export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/seurat/bin:$PATH

Rscript /exports/igmm/eddie/Glioblastoma-WGS/scripts/fame_gbm/R/Merge_seurat.R




