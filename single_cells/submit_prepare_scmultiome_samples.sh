#!/bin/bash

#$ -N TenXprep
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=2G
#$ -pe sharedmem 2
#$ -l h_rt=1:00:00

unset MODULEPATH
. /etc/profile.d/modules.sh

IDS=$1

SAMPLE_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1`
LIBRARY_DIR=/exports/igmm/eddie/Glioblastoma-WGS/scmultiome/params
SCRNA_FASTQ_DIR=/exports/igmm/eddie/Glioblastoma-WGS/scmultiome/fastqs/gex/LEB02_${SAMPLE_ID}
SCATAC_FASTQ_DIR=/exports/igmm/eddie/Glioblastoma-WGS/scmultiome/fastqs/atac/LEB01_${SAMPLE_ID}

cd $LIBRARY_DIR

if [ ! -f ${SAMPLE_ID}_library_file.csv ]
then
    echo "fastqs,sample,library_type" > ${SAMPLE_ID}_library_file.csv
    echo "${SCRNA_FASTQ_DIR},${SAMPLE_ID},Gene Expression" >> ${SAMPLE_ID}_library_file.csv
    echo "${SCATAC_FASTQ_DIR},${SAMPLE_ID},Chromatin Accessibility" >> ${SAMPLE_ID}_library_file.csv
else
    echo "Library file already exists"
fi









