#!/bin/bash

#$ -N vartrix
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=8G
#$ -pe sharedmem 8
#$ -l h_rt=22:00:00

SAMPLE_ID=$1

## To be used with Alleloscope as per https://github.com/seasoncloud/Alleloscope/tree/main/samples/SU008/scATAC
## Based on https://github.com/10XGenomics/vartrix
## wget https://github.com/10XGenomics/vartrix/releases/download/v1.1.22/vartrix_linux
## chmod 777 vartrix_linux
## VCF, BAM and FASTA files must have all sequences ie no alt contigs.

VARTRIX=/exports/igmm/eddie/Glioblastoma-WGS/scripts/vartrix-1.1.22/vartrix_linux
BARCODE=/exports/igmm/eddie/Glioblastoma-WGS/scmultiome/bams/${SAMPLE_ID}/outs/filtered_feature_bc_matrix/barcodes.tsv.gz
GERMLINE_VCF=/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/bcbio/${SAMPLE_ID}/*/${SAMPLE_ID}.germline.vcf.gz
SCATAC_BAM=/exports/igmm/eddie/Glioblastoma-WGS/scmultiome/bams/${SAMPLE_ID}/outs/atac_possorted_bam.bam
OUT_MTX=/exports/igmm/eddie/Glioblastoma-WGS/scmultiome/alleloscope/vartrix/${SAMPLE_ID}.alt_matrix.mtx
REF_MTX=/exports/igmm/eddie/Glioblastoma-WGS/scmultiome/alleloscope/vartrix/${SAMPLE_ID}.ref_matrix.mtx
ARC_REF=/gpfs/igmmfs01/eddie/Glioblastoma-WGS/scripts/10x/refdata-cellranger-arc-GRCh38-2020-A-2.0.0/fasta/genome.fa


$VARTRIX -v $GERMLINE_VCF -b $SCATAC_BAM -f $ARC_REF -c $BARCODE --out-matrix $OUT_MTX --ref-matrix $REF_MTX -s "coverage" --threads 12








