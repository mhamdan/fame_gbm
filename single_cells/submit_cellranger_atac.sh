#!/bin/bash

#$ -N cellranger_atac
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=16G
#$ -pe sharedmem 16
#$ -l h_rt=48:00:00

## Full path to library files
LIBRARY_LIST=$1

SAMPLE_ID=`head -n $SGE_TASK_ID $LIBRARY_LIST | cut -f 1| tail -n 1`

BAM_DIR=/gpfs/igmmfs01/eddie/Glioblastoma-WGS/scmultiome/bams/troubleshoot/atac
FASTQ_DIR=/exports/igmm/eddie/Glioblastoma-WGS/scmultiome/fastqs/atac/LEB01_${SAMPLE_ID}
ARC_REF=/gpfs/igmmfs01/eddie/Glioblastoma-WGS/scripts/10x/refdata-cellranger-arc-GRCh38-2020-A-2.0.0
export PATH=/exports/igmm/eddie/Glioblastoma-WGS/scripts/10x/cellranger-atac-2.0.0:$PATH

cd $BAM_DIR

cellranger-atac count --id=$SAMPLE_ID \
                        --fastqs=$FASTQ_DIR \
                        --sample=$SAMPLE_ID \
                        --chemistry=ARC-v1 \
                        --reference=$ARC_REF





