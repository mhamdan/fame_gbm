#!/bin/bash

#$ -N chromVAR
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=250G
###$ -pe sharedmem 16
#$ -l h_rt=48:00:00

export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/seurat/bin:$PATH
export TMPDIR=/exports/igmm/eddie/Glioblastoma-WGS/scmultiome/tmp

SAMPLE=$1

Rscript --vanilla /exports/igmm/eddie/Glioblastoma-WGS/scripts/fame_gbm/R/chromVAR.R $SAMPLE
##Rscript /exports/igmm/eddie/Glioblastoma-WGS/scripts/fame_gbm/R/footprinting.R
##Rscript /exports/igmm/eddie/Glioblastoma-WGS/scripts/fame_gbm/R/da_peaks.R






