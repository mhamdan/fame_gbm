#!/bin/bash 

#$ -N cellsnp-lite
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=16G
#$ -pe sharedmem 16
#$ -l h_rt=48:00:00

## This is a script to call sites with heterozygous SNPs in single cell data which can then be used to call allele-specific CN in schnapps.
## In this case we are using scRNA-seq data
## Install via conda as per instructions here: https://cellsnp-lite.readthedocs.io/en/latest/install.html
## We use the lite version as faster.
## Use Mode 1a (-R -b) for 10x data.
## Use --genotype as per https://github.com/single-cell-genetics/cellsnp-lite/issues/4

## Bash paramater, IDS contains sample names
IDS=$1

## Variables
SAMPLE_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1`
BAM=/exports/igmm/eddie/Glioblastoma-WGS/scmultiome/bams/${SAMPLE_ID}/outs/gex_possorted_bam.bam
BARCODE=/exports/igmm/eddie/Glioblastoma-WGS/scmultiome/bams/${SAMPLE_ID}/outs/raw_feature_bc_matrix/barcodes.tsv.gz
OUT_DIR=/exports/igmm/eddie/Glioblastoma-WGS/scmultiome/cellsnp/${SAMPLE_ID}
REGION_VCF=/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/bcbio/${SAMPLE_ID}/*/${SAMPLE_ID}N-germline-gatk-haplotype-annotated.vcf.gz

## Environment
export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/cellSNP/bin:$PATH

## Commands:
mkdir $OUT_DIR 
cellsnp-lite -s $BAM -b $BARCODE -O $OUT_DIR -R $REGION_VCF -p 20 --minMAF 0.1 --minCOUNT 20 --gzip --genotype


## End


