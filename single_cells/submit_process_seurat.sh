#!/bin/bash

#$ -N process_seurat
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=250G
#$ -l h_rt=48:00:00

export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/seurat/bin:$PATH
export TMPDIR=/exports/igmm/eddie/Glioblastoma-WGS/scmultiome/tmp

#Rscript /exports/igmm/eddie/Glioblastoma-WGS/scripts/fame_gbm/R/combine_peaks.R
#Rscript /exports/igmm/eddie/Glioblastoma-WGS/scripts/fame_gbm/R/scmultiome_individual.R
#Rscript /exports/igmm/eddie/Glioblastoma-WGS/scripts/fame_gbm/R/process_NSC_and_NPE.R
#Rscript /exports/igmm/eddie/Glioblastoma-WGS/scripts/fame_gbm/R/scmultiome_NSC_NPE_integration.R
#Rscript /exports/igmm/eddie/Glioblastoma-WGS/scripts/fame_gbm/R/process_GSCs_NSCs.R
#Rscript /exports/igmm/eddie/Glioblastoma-WGS/scripts/fame_gbm/R/one_GSC_vs_NSCs.R
#Rscript /exports/igmm/eddie/Glioblastoma-WGS/scripts/fame_gbm/R/chromVAR.R
#Rscript /exports/igmm/eddie/Glioblastoma-WGS/scripts/fame_gbm/R/single_cells/process_abdelfattah.R
Rscript /exports/igmm/eddie/Glioblastoma-WGS/scripts/fame_gbm/R/single_cells/process_ruiz_moreno.R


