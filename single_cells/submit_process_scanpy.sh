#!/bin/bash

#$ -N process_scanpy
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=250G
#$ -l h_rt=98:00:00

export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/scanpy/bin:$PATH

python /exports/igmm/eddie/Glioblastoma-WGS/scripts/fame_gbm/python/process_h5ad_file.py


