#!/bin/bash

#$ -N inferCNV
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=16G
#$ -pe sharedmem 16
#$ -l h_rt=150:00:00

export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/infercnv/bin:$PATH

SAMPLE=$1

Rscript --vanilla /exports/igmm/eddie/Glioblastoma-WGS/scripts/fame_gbm/R/inferCNV.R $SAMPLE






