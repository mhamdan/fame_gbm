#!/bin/bash

#$ -N copykat
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=350G
#$ -l h_rt=48:00:00

export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/copykat/bin:$PATH

Rscript /exports/igmm/eddie/Glioblastoma-WGS/scripts/fame_gbm/R/Copykat.R







