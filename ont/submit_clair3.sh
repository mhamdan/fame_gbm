#!/bin/bash

# Based on https://github.com/HKU-BAL/Clair3#option-3--bioconda
# It is a germline variant calling for long-reads with phasing capability

#$ -N clair3
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=16G
#$ -pe sharedmem 16
#$ -l h_rt=180:00:00

unset MODULEPATH
. /etc/profile.d/modules.sh

export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/snakemake/bin:$PATH
export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/clair3/bin:$PATH

SAMPLE=$1

ONT_BAM_DIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/ONT/bams/ngmlr
NGMLR_BAM=${ONT_BAM_DIR}/${SAMPLE}_ngmlr.bam
REF=/exports/igmm/eddie/Glioblastoma-WGS/resources/refgenome38/hg38.fa
CONDA_PREFIX=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/clair3
MODEL_NAME=r941_prom_hac_g360+g422
OUTPUT_DIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/ONT/ssv/clair3/${SAMPLE}

## Updated June 2022 => this command below is wrong i think- as not using germline bam
run_clair3.sh --bam_fn=$NGMLR_BAM --ref_fn=$REF --threads=16 --platform="ont" \
  --enable_phasing \
  --model_path="${CONDA_PREFIX}/bin/models/${MODEL_NAME}" \
  --output=${OUTPUT_DIR}               










