#!/bin/bash

## https://github.com/lh3/minimap2

## SGE options
#$ -N minimap2
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=16G
#$ -pe sharedmem 16
#$ -l h_rt=300:00:00

unset MODULEPATH
. /etc/profile.d/modules.sh

export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/snakemake/bin:$PATH
export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/ONT/bin:$PATH

FASTQ=$1
SAMPLE=$2

ONT_BAM_DIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/ONT/bams/guppy4/minimap2
MINIMAP_SAM=${ONT_BAM_DIR}/${SAMPLE}_minimap.sam
MINIMAP_BAM=${ONT_BAM_DIR}/${SAMPLE}_minimap.bam
REF=/exports/igmm/eddie/Glioblastoma-WGS/resources/refgenome38/hg38.fa.gz  
REF_INDEX=/exports/igmm/eddie/Glioblastoma-WGS/resources/refgenome38/hg38.mmi
SEQTK=/exports/igmm/eddie/Glioblastoma-WGS/scripts/seqtk/seqtk
ONT_QC_DIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/ONT/qc/
MINIMAP2=/exports/igmm/eddie/Glioblastoma-WGS/scripts/minimap2-2.24_x64-linux/minimap2

## Run once
##echo "Indexing minimap2 reference..."
##$MINIMAP2 -d $REF_INDEX $REF   

if [[ ! -f $MINIMAP_BAM ]]
then
    echo "Aligning ONT fastq with minimap2..."
    $MINIMAP2 -ax map-ont $REF $FASTQ > $MINIMAP_SAM

    ## Need samtools 1.9 as per https://github.com/philres/ngmlr/issues/82
    echo "Sorting and converting to bam file..."
    module load roslin/samtools/1.9
    samtools sort -@ 16 -o $MINIMAP_BAM $MINIMAP_SAM
    samtools index -@ 16 $MINIMAP_BAM
fi

if [[ -f $MINIMAP_BAM ]]
then 
    rm $MINIMAP_SAM
    echo "minimap.bam file exists..."
    echo "Producing QC stats..."
    seqkit bam $MINIMAP_BAM 2> $ONT_QC_DIR/${SAMPLE}_minimap_alignments.seqkit.tsv
    NanoPlot -t 16 --bam $MINIMAP_BAM --loglength -o $ONT_QC_DIR/bams/${SAMPLE}_minimap_bam_nanoplot --plots kde --tsv_stats --raw --N50 -f json

fi


