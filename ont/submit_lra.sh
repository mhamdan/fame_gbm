#!/bin/bash

### Align ONT fasqs to the reference genome
### Using lra as this has the best F1 score as of Novemeber 2021. 

## SGE options
#$ -N lra
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=16G
#$ -pe sharedmem 16
#$ -l h_rt=200:00:00

unset MODULEPATH
. /etc/profile.d/modules.sh

export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/ONT/bin:$PATH
export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/snakemake/bin:$PATH

FASTQ=$1
SAMPLE=$2

ONT_BAM_DIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/ONT/bams/lra
LRA_SAM=${ONT_BAM_DIR}/${SAMPLE}_lra.sam
LRA_BAM=${ONT_BAM_DIR}/${SAMPLE}_lra.bam
REF=/exports/igmm/eddie/Glioblastoma-WGS/resources/refgenome38/hg38.fa.gz  
## This has been dual indexed using "lra index -ONT $REF"
SEQTK=/exports/igmm/eddie/Glioblastoma-WGS/scripts/seqtk/seqtk
ONT_QC_DIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/ONT/qc/

## Based on https://github.com/ChaissonLab/LRA
## Tutorial from epi2me https://labs.epi2me.io/notebooks/Structural_Variation_Tutorial.html
## Several pointers: 
## https://github.com/ChaissonLab/LRA/issues/11

echo "Aligning ONT fastq with lra..."
$SEQTK seq -A $FASTQ | lra align -ONT -p s -t 16 $REF - > $LRA_SAM 

echo "Sorting and converting to bam file..."
samtools calmd -@ 16 -u $LRA_SAM $REF | samtools sort -@ 16 -o $LRA_BAM $LRA_SAM && samtools index -@ 16 $LRA_BAM

echo "Counting alignment statistics..."
seqkit bam $LRA_BAM 2> $ONT_BAM_DIR/${SAMPLE}_lra_alignments.seqkit.tsv
NanoPlot -t 16 --bam $LRA_BAM --loglength -o $ONT_QC_DIR/bams/${SAMPLE}_lra_bam_nanoplot --plots kde --tsv_stats

























