#!/bin/bash

#$ -N qdnaseq
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=150G
#$ -l h_rt=24:00:00

export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/qdnaseq/bin:$PATH
QDNASEQ=/exports/igmm/eddie/Glioblastoma-WGS/scripts/fame_gbm/R/QDNAseq.R

TUMOUR_BAM=$1
NORMAL_BAM=$2
SAMPLE_ID=$3
OUTPUT_DIR=$4

Rscript $QDNASEQ $TUMOUR_BAM $NORMAL_BAM $SAMPLE_ID $OUTPUT_DIR




