#!/bin/bash

### Based on https://github.com/adamewing/methylartist

#$ -N methylartist
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=8G
#$ -pe sharedmem 16
#$ -l h_rt=20:00:00

## Define SGE inputs
SAMPLE=$1
REGION=$2
HIGHLIGHT=$3
GENE=$4

## Need python3.8 or above
export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/py38/bin:$PATH

## Define files
ONT_BAM_DIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/ONT/bams/ngmlr
NGMLR_BAM=${ONT_BAM_DIR}/${SAMPLE}_ngmlr.bam
METHYLATION_CALLS=/exports/igmm/eddie/Glioblastoma-WGS/WGS/ONT/methylation/${SAMPLE}_ngmlr_methylation_calls.tsv
METHYLARIST_OUTPUT=/exports/igmm/eddie/Glioblastoma-WGS/WGS/ONT/methylation/methylartist
GTF=/exports/igmm/eddie/Glioblastoma-WGS/resources/gencode.v39.annotation.sorted.gtf.gz
REF=/exports/igmm/eddie/Glioblastoma-WGS/resources/refgenome38/hg38.fa

## Run commands 
cd $METHYLARIST_OUTPUT
###methylartist db-nanopolish -m $METHYLATION_CALLS -d $METHYLARIST_OUTPUT/${SAMPLE}.nanopolish.db -t 2.0 -s ## Run once
###echo $NGMLR_BAM $METHYLARIST_OUTPUT/${SAMPLE}.nanopolish.db > $METHYLARIST_OUTPUT/${SAMPLE}_data.txt

## Highlight a few areas (using --highlight_bed)
###methylartist locus -d ${METHYLARIST_OUTPUT}/${SAMPLE}_data.txt -i $REGION -g $GTF --highlight_bed $HIGHLIGHT --genes ${GENE} --samplepalette magma

## Highlight one area
methylartist locus -d ${METHYLARIST_OUTPUT}/${SAMPLE}_data.txt -i $REGION -g $GTF -l $HIGHLIGHT --genes ${GENE} --samplepalette magma












