#!/bin/bash

## This script splits N number of files into folders containing N number of files

#$ -N split_files_into_subfolders
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=64G
#$ -l h_rt=3:00:00

ORIGINAL_DIR=$1
N=$2 ## number of files per subfolder

#### To split files into subfolders to enable running in parallel:
## N refers to number of files per subfolder

cd $ORIGINAL_DIR
for f in *; 
do 
    d=split_$(printf %03d $((i/$N+1)))
    mkdir -p $d
    mv "$f" $d
    let i++
done




