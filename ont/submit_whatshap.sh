#!/bin/bash

## Phasing of germline SNPs and indels on long-read bam file
## Install via conda install whatshap
## This version as of June 2022 is v1.4

#$ -N whatshap
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=64G
#$ -l h_rt=12:00:00

unset MODULEPATH
. /etc/profile.d/modules.sh

## Define SGE parameters
SAMPLE_ID=$1

export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/ONT/bin:$PATH

## Internal inputs:
REF=/exports/igmm/eddie/Glioblastoma-WGS/resources/refgenome38/hg38.fa
ONT_BAM=/exports/igmm/eddie/Glioblastoma-WGS/WGS/ONT/bams/ngmlr/${SAMPLE_ID}_ngmlr.bam
GERMLINE_VCF=/exports/igmm/eddie/Glioblastoma-WGS/WGS/ONT/germline_small_muts/${SAMPLE_ID}.germline.snpeff.vcf.gz
GERMLINE_VCF_MULTIALLELIC=/exports/igmm/eddie/Glioblastoma-WGS/WGS/ONT/germline_small_muts/${SAMPLE_ID}.germline.snpeff_multiallelic.vcf.gz
WHATSHAP_INPUT_VCF=/exports/igmm/eddie/Glioblastoma-WGS/WGS/ONT/germline_small_muts/${SAMPLE_ID}.whatshap_input.vcf.gz
WHATSHAP_OUTPUT_VCF=/exports/igmm/eddie/Glioblastoma-WGS/WGS/ONT/germline_small_muts/${SAMPLE_ID}.whatshap_output.vcf

## Inspired by Rausch et al, phasing study on ONT reads in medulloblastoma
## Also according to recommended workflow: https://whatshap.readthedocs.io/en/latest/guide.html#recommended-workflow
## Use --ignore-read-groups as running with single samples

if [[ ! -f $WHATSHAP_INPUT_VCF ]]
then
    ## Prepare vcf file
    bcftools norm -f $REF -m -both $GERMLINE_VCF | bgzip > $GERMLINE_VCF_MULTIALLELIC
    bcftools annotate -x INFO,^FORMAT/GT $GERMLINE_VCF_MULTIALLELIC | bgzip > $WHATSHAP_INPUT_VCF
    tabix -p vcf $WHATSHAP_INPUT_VCF

    ## Run whatshap:
    whatshap phase \
        --indels --ignore-read-groups \
        --reference $REF \
        $WHATSHAP_INPUT_VCF \
        $ONT_BAM \
        -o $WHATSHAP_OUTPUT_VCF

    bgzip $WHATSHAP_OUTPUT_VCF
    tabix -p vcf ${WHATSHAP_OUTPUT_VCF}.gz
fi

## Troubleshoot subsetting to just chr22 as there is a ploidy error despite producing a phased VCF:
SUBSET_INPUT=/exports/igmm/eddie/Glioblastoma-WGS/WGS/ONT/germline_small_muts/${SAMPLE_ID}.whatshap_input_chr22.vcf
SUBSET_OUTPUT=/exports/igmm/eddie/Glioblastoma-WGS/WGS/ONT/germline_small_muts/${SAMPLE_ID}.whatshap_output_chr22.vcf

bcftools filter -r chr22 $GERMLINE_VCF_MULTIALLELIC > bgzip > ${SUBSET_INPUT}.gz
tabix -p vcf ${SUBSET_INPUT}.gz

## Run whatshap:
whatshap phase \
    --indels --ignore-read-groups \
    --reference $REF \
    ${SUBSET_INPUT}.gz \
    $ONT_BAM \
    -o $SUBSET_OUTPUT

bgzip $SUBSET_OUTPUT
tabix -p vcf ${SUBSET_OUTPUT}.gz


###./addAFs.sh whatshap.vcf.gz
###mv merged.bcf whatshap.af.bcf
###mv merged.bcf.csi whatshap.af.bcf.csi








