#!/bin/bash

## SGE options
#$ -N subset_ONT_bams
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=16G
#$ -pe sharedmem 16
#$ -l h_rt=200:00:00

SAMPLE=$1

export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/snakemake/bin:$PATH
export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/ONT/bin:$PATH

ONT_BAM_DIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/ONT/bams/ngmlr
NGMLR_BAM=${ONT_BAM_DIR}/${SAMPLE}_ngmlr.bam

for chrom in `seq 1 22`
do
    samtools view -@ 16 -b $NGMLR_BAM chr${chrom} > ${ONT_BAM_DIR}/${SAMPLE}_ngmlr.chr${chrom}.bam
    samtools index -@ 16 ${ONT_BAM_DIR}/${SAMPLE}_ngmlr.chr${chrom}.bam
done







