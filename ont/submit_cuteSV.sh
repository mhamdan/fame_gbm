#!/bin/bash

## https://github.com/tjiangHIT/cuteSV

## SGE options
#$ -N cuteSV
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=16G
#$ -pe sharedmem 16
#$ -l h_rt=3:00:00

unset MODULEPATH
. /etc/profile.d/modules.sh

export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/snakemake/bin:$PATH
export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/ONT/bin:$PATH

## Updated in June 2022 to version 1.0.13 using conda as per https://github.com/tjiangHIT/cuteSV

BAM_FILE=$1
CUTESV_VCF_DIR=$2

## Define input paths:
REF=/exports/igmm/eddie/Glioblastoma-WGS/resources/refgenome38/hg38.fa ## need to be unzipped
BASENAME=`basename $BAM_FILE`
SAMPLE_ID=${BASENAME%.bam}
CUTESV_VCF=$CUTESV_VCF_DIR/${SAMPLE_ID}_cutesv1.0.13_raw.vcf
CUTESV_VCF_NO_ALT=$CUTESV_VCF_DIR/${SAMPLE_ID}_cutesv1.0.13_raw_noAlt.vcf
CUTESV_VCF_NO_ALT_SORTED=$CUTESV_VCF_DIR/${SAMPLE_ID}_cutesv1.0.13_raw_noAlt_sorted.vcf.gz
WORK_DIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/ONT/workdir/${SAMPLE_ID}_cutesv

## use --retain_work_dir to make sure work dir containing all tmp files are not deleted run is finished.
## This tmp work dir is needed for bcftools sort to function.
## Can delete the directory after.

rm -rf $WORK_DIR

if [[ ! -f ${CUTESV_VCF}.gz ]]
then
     mkdir -p $WORK_DIR

     cuteSV -t 16 --genotype -S ${SAMPLE_ID} \
          --max_cluster_bias_INS 100 --diff_ratio_merging_INS 0.3 \
          --max_cluster_bias_DEL 100 --diff_ratio_merging_DEL 0.3 \
          --retain_work_dir \
          -md 500 -mi 500 \
          $BAM_FILE $REF $CUTESV_VCF $WORK_DIR

     bgzip $CUTESV_VCF

fi

## Exclude non-canonical chromosomes, sort and index VCF:
zcat $CUTESV_VCF | grep -v 'random\|Un\|alt' > $CUTESV_VCF_NO_ALT
bcftools sort -T $WORK_DIR -Oz -o $CUTESV_VCF_NO_ALT_SORTED $CUTESV_VCF_NO_ALT ## output in gzipped format
tabix -p vcf $CUTESV_VCF_NO_ALT_SORTED
rm $CUTESV_VCF_NO_ALT ## remove this redundant file



