#!/bin/bash

#
#$ -N mosdepth_SV
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=16G
#$ -pe sharedmem 16
#$ -l h_rt=64:00:00

unset MODULEPATH
. /etc/profile.d/modules.sh


SAMPLE=$1

export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/snakemake/bin:$PATH
export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/ONT/bin:$PATH

## Use mosdepth, based on https://github.com/brentp/mosdepth
## Need version 0.2.9 which in turn needs py2.
## Install within py2 conda

MOSDEPTH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/py2/bin/mosdepth
ONT_BAM_DIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/ONT/bams/ngmlr
NGMLR_BAM=${ONT_BAM_DIR}/${SAMPLE}_ngmlr.bam
MOSDEPTH_OUTPUT=/exports/igmm/eddie/Glioblastoma-WGS/WGS/ONT/coverage
REF=/exports/igmm/eddie/Glioblastoma-WGS/resources/refgenome38/hg38.fa.gz  

## Need a bed file if want to call coverage across reginos within SVs

cd $MOSDEPTH_OUTPUT/ngmlr

$MOSDEPTH --fast-mode -t 4 -Q 20 $SAMPLE $NGMLR_BAM 










