#!/bin/bash

## https://github.com/cytham/nanovar

## SGE options
#$ -N nanoVAR
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=16G
#$ -pe sharedmem 16
#$ -l h_rt=5:00:00

unset MODULEPATH
. /etc/profile.d/modules.sh

export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/snakemake/bin:$PATH
export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/ONT/bin:$PATH

## v1.4.1 installed using conda as per https://github.com/cytham/nanovar

SAMPLE_ID=$1

## Define input paths:
ONT_BAM_DIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/ONT/bams/ngmlr
NGMLR_BAM=${ONT_BAM_DIR}/${SAMPLE_ID}_ngmlr.bam
REF=/exports/igmm/eddie/Glioblastoma-WGS/resources/refgenome38/hg38.fa ## need to be unzipped
NANOVAR_VCF=/exports/igmm/eddie/Glioblastoma-WGS/WGS/ONT/sv/LRS/raw/${SAMPLE_ID}_ngmlr_nanovar_1.4.1_raw.vcf
NANOVAR_VCF_NO_ALT=/exports/igmm/eddie/Glioblastoma-WGS/WGS/ONT/sv/LRS/raw/${SAMPLE_ID}_ngmlr_nanovar_1.4.1_raw_noAlt.vcf
NANOVAR_VCF_NO_ALT_SORTED=/exports/igmm/eddie/Glioblastoma-WGS/WGS/ONT/sv/LRS/raw/${SAMPLE_ID}_nanovar_1.4.1_raw_noAlt_sorted.vcf.gz
WORK_DIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/ONT/workdir/${SAMPLE_ID}_nanoVar

if [[ ! -f ${NANOVAR_VCF}.gz ]]
then
     mkdir -p $WORK_DIR && cd $WORK_DIR

     nanovar -t 16 -f hg38 --cnv hg38 -x ont -c 2 -l 50 $NGMLR_BAM $REF $WORK_DIR
     
     mv $WORK_DIR/${SAMPLE_ID}_ngmlr.vcf $NANOVAR_VCF
     bgzip $NANOVAR_VCF

fi

## Exclude non-canonical chromosomes, sort and index VCF:
zcat ${NANOVAR_VCF}.gz | grep -v 'random\|Un\|alt' > $NANOVAR_VCF_NO_ALT
bcftools sort -T $WORK_DIR -Oz -o $NANOVAR_VCF_NO_ALT_SORTED $NANOVAR_VCF_NO_ALT
tabix -p vcf $NANOVAR_VCF_NO_ALT_SORTED
bgzip $NANOVAR_VCF_NO_ALT





