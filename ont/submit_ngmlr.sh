#!/bin/bash

### Align ONT fasqs to the reference genome, based on https://github.com/philres/ngmlr

## SGE options
#$ -N ngmlr
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=90G
####$ -pe sharedmem 16
#$ -l h_rt=300:00:00

unset MODULEPATH
. /etc/profile.d/modules.sh

export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/snakemake/bin:$PATH
export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/ONT/bin:$PATH

FASTQ=$1
SAMPLE=$2

ONT_BAM_DIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/ONT/bams/guppy4/ngmlr
NGMLR_SAM=${ONT_BAM_DIR}/${SAMPLE}_ngmlr.sam
NGMLR_BAM=${ONT_BAM_DIR}/${SAMPLE}_ngmlr.bam
REF=/exports/igmm/eddie/Glioblastoma-WGS/resources/refgenome38/hg38.fa.gz  
SEQTK=/exports/igmm/eddie/Glioblastoma-WGS/scripts/seqtk/seqtk
ONT_QC_DIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/ONT/qc/

echo "Aligning ONT fastq with ngmlr..."
ngmlr -t 12 -r $REF -q $FASTQ -x ont -o $NGMLR_SAM

## Need samtools 1.9 as per https://github.com/philres/ngmlr/issues/82
echo "Sorting and converting to bam file..."
module load roslin/samtools/1.9
samtools sort -@ 16 -o $NGMLR_BAM $NGMLR_SAM
samtools index -@ 16 $NGMLR_BAM

echo "Producing QC stats..."
seqkit bam $NGMLR_BAM 2> $ONT_QC_DIR/${SAMPLE}_ngmlr_alignments.seqkit.tsv
NanoPlot -t 16 --bam $NGMLR_BAM --loglength -o $ONT_QC_DIR/bams/${SAMPLE}_ngmlr_bam_nanoplot --plots kde --tsv_stats --raw --N50 -f json











