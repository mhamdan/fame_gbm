#!/bin/bash

## Based on https://github.com/eldariont/svim/wiki
## SGE options
#$ -N svim
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=16G
#$ -pe sharedmem 16
#$ -l h_rt=200:00:00

unset MODULEPATH
. /etc/profile.d/modules.sh

export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/snakemake/bin:$PATH
export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/ONT/bin:$PATH
REF=/exports/igmm/eddie/Glioblastoma-WGS/resources/refgenome38/hg38.fa.gz

BAM_FILE=$1
SVIM_VCF_DIR=$2

BASENAME=`basename $BAM_FILE`
SAMPLE_ID=${BASENAME%.bam}
SVIM_VCF=$SVIM_VCF_DIR/${SAMPLE_ID}_svim_raw.vcf
SVIM_VCF_NO_ALT=$SVIM_VCF_DIR/${SAMPLE_ID}_svim_raw_noAlt.vcf
SVIM_VCF_NO_ALT_SORTED=$SVIM_VCF_DIR/${SAMPLE_ID}_svim_raw_noAlt_sorted.vcf.gz
WORK_DIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/ONT/workdir/${SAMPLE_ID}_svim

rm -rf $WORK_DIR

if [[ ! -f ${SVIM_VCF}.gz ]]
then
    mkdir -p $WORK_DIR
    svim alignment \
        --sample $SAMPLE_ID \
        --insertion_sequences \
        $WORK_DIR $BAM_FILE $REF
    mv $WORK_DIR/variants.vcf $SVIM_VCF
    bgzip $SVIM_VCF
fi

## Exclude non-canonical chromosomes, sort and index VCF:
zcat $SVIM_VCF | grep -v 'random\|Un\|alt' > $SVIM_VCF_NO_ALT
bcftools sort -T $WORK_DIR -Oz -o $SVIM_VCF_NO_ALT_SORTED $SVIM_VCF_NO_ALT
tabix -p vcf $SVIM_VCF_NO_ALT_SORTED
rm $SVIM_VCF_NO_ALT



