#!/bin/bash

## This script performs base calling using Guppy version 5.
## Guppy version 5 is already installed as a module at Roslin, so use this.
## Use Fast5 directory as input.
## Use TITAN-X GPU as compute 
## Config files are listed at /exports/applications/apps/community/roslin/guppy/5.0.11-gpu/data; this depends on the flow cell + sequence combination used to generate fast5 files.
## For my samples (E28 and E34, run on promethion in Nov 2021, use dna_r9.4.1_450bps_hac_prom)

#$ -N guppy5
#$ -cwd
#$ -pe gpu-titanx 1
#$ -l h_vmem=32G
#$ -l h_rt=15:00:00

# Initialise the environment modules and load guppy version 5.0.11
. /etc/profile.d/modules.sh
module load roslin/guppy/5.0.11-gpu

## IDS file contains txt file that has split_id as column 1, and paths to subfolders containing split files as column 2
## OUTPUT_PATH is path to split fastqs

IDS=$1
OUTPUT_PATH=$2

SPLIT_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 1`
SPLIT_FAST5_DIR=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 2`
SPLIT_FASTQ_DIR=${OUTPUT_PATH}/$SPLIT_ID

## Set to this but chance depending on pore standard and accuracy mode (hac = high accuracy)
MODE=dna_r9.4.1_450bps_hac_prom.cfg
CONFIG_FILE=/exports/applications/apps/community/roslin/guppy/5.0.11-gpu/data/$MODE

# Base calling:
## If no previous calls (judged by presence of directory):
if [[ ! -d $SPLIT_FASTQ_DIR ]]
then

    mkdir -p $SPLIT_FASTQ_DIR

    guppy_basecaller \
        -i $SPLIT_FAST5_DIR \
        -s $SPLIT_FASTQ_DIR \
        -c $CONFIG_FILE \
        --device auto \
        --compress_fastq \
        --verbose_logs \
        --num_callers 2
else

    ## If previously started but not finished:

    guppy_basecaller \
            -i $SPLIT_FAST5_DIR \
            -s $SPLIT_FASTQ_DIR \
            -c $CONFIG_FILE \
            --device auto \
            --compress_fastq \
            --verbose_logs \
            --num_callers 2 \
            --resume
            
fi


















