#!/bin/bash

## https://github.com/fritzsedlazeck/Sniffles

## SGE options
#$ -N sniffles
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=250G
####$ -pe sharedmem 16  ## Don't use threads, does not work with sniffles ## Update June 2022- default run on 4 threads
#$ -l h_rt=200:00:00

## Update June 2022: Switch to Sniffles2: conda install sniffles=2.0 

unset MODULEPATH
. /etc/profile.d/modules.sh

export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/snakemake/bin:$PATH
export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/ONT/bin:$PATH
###export PATH=/exports/igmm/eddie/Glioblastoma-WGS/scripts/Sniffles-master/bin/sniffles-core-1.0.12:$PATH ## no longer using Sniffles v1 as of June 2022.
REF=/exports/igmm/eddie/Glioblastoma-WGS/resources/refgenome38/hg38.fa

## SGE inputs:
BAM_FILE=$1
SNIFFLES_VCF_DIR=$2

## Paths to sniffles:
BASENAME=`basename $BAM_FILE`
SAMPLE_ID=${BASENAME%.bam}
SNIFFLES_VCF=$SNIFFLES_VCF_DIR/${SAMPLE_ID}_sniffles2_raw.vcf
SNIFFLES_VCF_NO_ALT=$SNIFFLES_VCF_DIR/${SAMPLE_ID}_sniffles2_raw_noAlt.vcf
SNIFFLES_VCF_NO_ALT_SORTED=$SNIFFLES_VCF_DIR/${SAMPLE_ID}_sniffles2_raw_noAlt_sorted.vcf.gz
WORK_DIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/ONT/workdir/${SAMPLE_ID}_sniffles

rm -rf $WORK_DIR

## Run Sniffles 2: Using --non-germline is needed for somatic SVs.
if [[ ! -f ${SNIFFLES_VCF}.gz ]]
then
    mkdir -p $WORK_DIR
    sniffles --input $BAM_FILE \
        --vcf $SNIFFLES_VCF \
        --non-germline \
        --reference $REF \
        -t 4
    bgzip $SNIFFLES_VCF

fi

zcat $SNIFFLES_VCF | grep -v 'random\|Un\|alt' > $SNIFFLES_VCF_NO_ALT
bcftools sort -T $WORK_DIR -Oz -o $SNIFFLES_VCF_NO_ALT_SORTED $SNIFFLES_VCF_NO_ALT
tabix -p vcf $SNIFFLES_VCF_NO_ALT_SORTED
rm $SNIFFLES_VCF_NO_ALT



## Archive (v1):
##sniffles --mapped_reads $NGMLR_BAM --vcf $SNIFFLES_VCF --tmp_file $WORK_DIR 
## https://github.com/fritzsedlazeck/Sniffles/issues/210
##sniffles --mapped_reads $NGMLR_BAM --vcf $SNIFFLES_VCF





