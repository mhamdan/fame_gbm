#!/bin/bash

## This script automates subsetting a SORTED bam file and indexing the subset bam file
## It will use the base name of the original bam file as the prefix of the subset bam


## SGE options
#$ -N subset_bam
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=100G
#$ -l h_rt=5:00:00

unset MODULEPATH
. /etc/profile.d/modules.sh

export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/ONT/bin:$PATH

ORIGINAL_BAM=$1
CHR_OF_INTEREST=$2

BASENAME=`basename $ORIGINAL_BAM`
DIRNAME=`dirname $ORIGINAL_BAM`
ID=${BASENAME%.bam}
SUBSET_BAM=${DIRNAME}/${ID}_$CHR_OF_INTEREST.bam

samtools view $ORIGINAL_BAM $CHR_OF_INTEREST -b > $SUBSET_BAM
samtools index -@ 16 $SUBSET_BAM





