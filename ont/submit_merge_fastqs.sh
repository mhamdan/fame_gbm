#!/bin/bash

### This script takes all outputs from Guppy5 array runs and merge fastqs into single pass/fail fastq files
### Then it takes the pass fastq files and filter lambda reads (if present, usually is) from them
### Then it runs various QC metrics

#$ -N merge_fastqs
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=120G
#$ -l h_rt=12:00:00

## IDS file contains txt file that has split_id as column 1, and paths to subfolders containing split files as column 2
## GUPPY_OUTPUT_DIR is path to parent dir of split fastq files

SAMPLE_ID=$1
GUPPY_OUTPUT_DIR=$2

unset MODULEPATH
. /etc/profile.d/modules.sh

export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/snakemake/bin:$PATH
export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/ONT/bin:$PATH

cd $GUPPY_OUTPUT_DIR

## Concatenate split fastqs into one file and get summary:
if [[ ! -f ${SAMPLE_ID}.sequencing_summary.txt ]]
then
    zcat */*pass/*fastq.gz > ${SAMPLE_ID}.pass.fastq && bgzip ${SAMPLE_ID}.pass.fastq
    zcat */*fail/*fastq.gz > ${SAMPLE_ID}.fail.fastq && bgzip ${SAMPLE_ID}.fail.fastq
    cat */sequencing_summary.txt > ${SAMPLE_ID}.sequencing_summary.txt ## this combines both passed and failed reads
fi

## Remove lambda reads:
if [[ -f ${SAMPLE_ID}.pass.fastq.gz ]]
then
    if [[ ! -f ${SAMPLE_ID}.pass.nolambda.fastq.gz ]]
    then
        echo "Merged fastq file generated for passed reads."
        echo "Removing lambda sequence..."

        LAMBDA_FASTA=/exports/igmm/eddie/Glioblastoma-WGS/scripts/nanolyse/reference/lambda.fasta.gz
        zcat ${SAMPLE_ID}.pass.fastq.gz | NanoLyse --reference $LAMBDA_FASTA | bgzip > ${SAMPLE_ID}.pass.nolambda.fastq.gz

    fi
    
    ## QC on both lambda and no lambda file:
    ONT_QC_DIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/ONT/qc/guppy5/fastqs
    ONT_QC_DIR_ORIGINAL=$ONT_QC_DIR/${SAMPLE_ID}
    ONT_QC_DIR_NO_LAMBDA=$ONT_QC_DIR/${SAMPLE_ID}_nolambda

    ## Original fastq:
    if [[ ! -f $ONT_QC_DIR_ORIGINAL/${SAMPLE_ID}_reads.seqkit.tsv ]]
    then
        echo "QC on original fastq..."
        mkdir -p $ONT_QC_DIR_ORIGINAL

        echo "Running nanoplot QC..."
        NanoPlot -t 16 --fastq ${SAMPLE_ID}.pass.fastq.gz --loglength -o $ONT_QC_DIR_ORIGINAL/${SAMPLE_ID}_fastq_nanoplot --plots kde --tsv_stats --raw --N50 -f json

        echo "Running seqkit QC..."
        seqkit stats -T -a -b -j 16 ${SAMPLE_ID}.pass.fastq.gz > $ONT_QC_DIR_ORIGINAL/${SAMPLE_ID}_reads.seqkit.tsv
        
        ## fastcat is slow
        ##echo "Running fastcat QC..."
        ##fastcat -s $SAMPLE_ID --read $ONT_QC_DIR_ORIGINAL/${SAMPLE_ID}_per_read_stats.txt ${SAMPLE_ID}.pass.fastq.gz
    fi

    ## No lambda:
    if [[ ! -f $ONT_QC_DIR_NO_LAMBDA/${SAMPLE_ID}_reads.seqkit.tsv ]]
    then

        echo "QC on lambda filtered fastq..."
        mkdir -p $ONT_QC_DIR_NO_LAMBDA

        echo "Running nanoplot QC..."
        NanoPlot -t 16 --fastq ${SAMPLE_ID}.pass.nolambda.fastq.gz --loglength -o $ONT_QC_DIR_NO_LAMBDA/${SAMPLE_ID}_fastq_nanoplot --plots kde --tsv_stats --raw --N50 -f json

        echo "Running seqkit QC..."
        seqkit stats -T -a -b -j 16 ${SAMPLE_ID}.pass.nolambda.fastq.gz > $ONT_QC_DIR_NO_LAMBDA/${SAMPLE_ID}_reads.seqkit.tsv

        ## fastcat is slow
        ##echo "Running fastcat QC..."
        ##fastcat -s $SAMPLE_ID --read $ONT_QC_DIR_NO_LAMBDA/${SAMPLE_ID}_per_read_stats.txt ${SAMPLE_ID}.pass.nolambda.fastq.gz
    fi
fi









