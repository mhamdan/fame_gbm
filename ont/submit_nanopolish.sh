#!/bin/bash

#$ -N nanopolish
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=16G
#$ -pe sharedmem 16
#$ -l h_rt=300:00:00

## Define SGE inputs
FAST5_DIR=$1
FILTERED_FASTQ=$2
SUMMARY_FILE=$3
SAMPLE=$4

## Need python3 
export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/snakemake/bin:$PATH

## Define files
NANOPOLISH=/exports/igmm/eddie/Glioblastoma-WGS/scripts/nanopolish/nanopolish
CALC_METH_FREQ=/exports/igmm/eddie/Glioblastoma-WGS/scripts/nanopolish/scripts/calculate_methylation_frequency.py
REF=/exports/igmm/eddie/Glioblastoma-WGS/resources/refgenome38/hg38.fa.gz
ONT_BAM_DIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/ONT/bams/ngmlr
NGMLR_BAM=${ONT_BAM_DIR}/${SAMPLE}_ngmlr.bam
METHYLATION_CALLS=/exports/igmm/eddie/Glioblastoma-WGS/WGS/ONT/methylation/${SAMPLE}_ngmlr_methylation_calls.tsv
METHYLATION_FREQ=/exports/igmm/eddie/Glioblastoma-WGS/WGS/ONT/methylation/${SAMPLE}_ngmlr_methylation_pileup.tsv

## Run commands 
##$NANOPOLISH index -d $FAST5_DIR -s $SUMMARY_FILE $FILTERED_FASTQ ## Index, run once -> Error: Could not find fast5 filename column in the header; fast5 files need unzipping. ## Also does not work with sequencing summary produced by Guppy
###$NANOPOLISH index -d $FAST5_DIR $FILTERED_FASTQ ## Index, run once.
$NANOPOLISH call-methylation -v -t 16 -r $FILTERED_FASTQ -b $NGMLR_BAM -g $REF > $METHYLATION_CALLS ## Call methylation
$CALC_METH_FREQ $METHYLATION_CALLS > $METHYLATION_FREQ ## pileup












