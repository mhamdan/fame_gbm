#!/bin/bash

### Run QC on ONT fastqs
#
#$ -N ONT_fq_qc
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=16G
#$ -pe sharedmem 16
#$ -l h_rt=20:00:00

unset MODULEPATH
. /etc/profile.d/modules.sh

export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/ONT/bin:$PATH

FILE=$1 ##Full path to fastq file
SAMPLE=$2

ONT_QC_DIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/ONT/qc/

echo "Running nanoplot QC..."
NanoPlot -t 16 --fastq $FILE --loglength -o $ONT_QC_DIR/${SAMPLE}_fastq_nanoplot --plots kde --tsv_stats --raw --N50 -f json

echo "Running seqkit QC..."
seqkit stats -T -a -b -j 16 $FILE > $ONT_QC_DIR/${SAMPLE}_reads.seqkit.tsv

echo "Running fastcat QC..."
fastcat -s $SAMPLE --read $ONT_QC_DIR/${SAMPLE}_per_read_stats.txt $FILE










