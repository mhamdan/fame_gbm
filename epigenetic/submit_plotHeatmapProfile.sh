#!/bin/bash

#$ -N plotHeatmapProfile
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=12G
#$ -l h_rt=12:00:00
#$ -pe sharedmem 12
    
CONFIG=$1
VARIANT=$2
REGION_1=$3
REGION_2=$4
REGION_3=$5
REGION_NAME=$6

### GENOMIC_ELEMENT points to a bed path of genomic elements

source $CONFIG

## Deeptools need python 3.65
export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/py365/bin:$PATH

BW_DIR=/exports/igmm/eddie/Glioblastoma-WGS/ChIP-seq/mitotic_bookmarking/coverage/merged/normalised
MATRIX_DIR=/exports/igmm/eddie/Glioblastoma-WGS/ChIP-seq/mitotic_bookmarking/downstream/deeptools/heatmap/computeMatrix
HEATMAP_DIR=/exports/igmm/eddie/Glioblastoma-WGS/ChIP-seq/mitotic_bookmarking/downstream/deeptools/heatmap/figures

computeMatrix reference-point \
    --referencePoint center \
    -R $REGION_1 $REGION_2 $REGION_3 \
    -S ${BW_DIR}/${VARIANT}*normalised.merged.bw \
    -b 5000 -a 5000 \
    --binSize 10 \
    --skipZeros --missingDataAsZero \
    --blackListFileName $BLACKLISTED_PEAKS_MM10 \
    -p 32 \
    --scale 10 \
    --smartLabels \
    -o ${MATRIX_DIR}/${VARIANT}_${REGION_NAME}.matrix.gz \
    --outFileNameMatrix ${MATRIX_DIR}/${VARIANT}_${REGION_NAME}.matrix.tab \
    --outFileSortedRegions ${MATRIX_DIR}/${VARIANT}_${REGION_NAME}.matrix.bed 
    
plotHeatmap \
    -m ${MATRIX_DIR}/${VARIANT}_${REGION_NAME}.matrix.gz \
    -out ${HEATMAP_DIR}/${VARIANT}_${REGION_NAME}.heatmap.png \
    --heatmapHeight 18 \
    --legendLocation best \
    --verbose 



#plotProfile -m ${CHIP_SEQ_ID}.matrix.gz \
#    -out ${CHIP_SEQ_ID}.pergroup.Sox2_centre_250.profile.png \
#    --perGroup 
    
    
    
    
    
    
    
