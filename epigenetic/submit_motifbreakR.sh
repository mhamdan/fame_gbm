#!/bin/bash

#$ -N motifbreakR
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=150G
#$ -l h_rt=300:00:00

export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/R4.0/bin:$PATH

Rscript --vanilla /exports/igmm/eddie/Glioblastoma-WGS/scripts/fame_gbm/R/motifbreakR.R 






