#!/bin/bash

# To run this script, do 
# qsub -t 1-n submit_filter_bowtie2.sh CONFIG IDS
#
# CONFIG is the path to the file scripts/config.sh which contains environment variables set to
# commonly used paths and files in the script
# IDS is a list of sample ids, one per line, where tumor and normal samples are designated by
# the addition of a T or an N to the sample id.
# TYPE is either sample name or input
#
#$ -N filter_btw2
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=8G
#$ -l h_rt=24:00:00
#$ -pe sharedmem 8

CONFIG=$1
IDS=$2

source $CONFIG

unset MODULEPATH
. /etc/profile.d/modules.sh

module load igmm/apps/sambamba/0.5.9

PATIENT_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1`

BAM_DIR=/exports/igmm/eddie/Glioblastoma-WGS/ChIP-seq/H3K27ac-seq_GBM/bams/bwt2

cd $BAM_DIR

################################################################################################
### Based on https://hbctraining.github.io/Intro-to-ChIPseq/lessons/03_align_and_filtering.html

PATIENT_ID=Mack_GSC15_H3K27AC
REP=${PATIENT_ID}.sorted.bam
UNIQUE_REP=${PATIENT_ID}.unique.sorted.bam
SORTED_UNIQUE_REP=${PATIENT_ID}.sorted.unique.sorted.bam
MARKDUP_REP=${PATIENT_ID}.markdup.position.fixmate.sorted.unique.sorted.bam
FILTERED_REP=${PATIENT_ID}.filtered.markdup.position.fixmate.sorted.unique.sorted.bam
FINAL_REP=${PATIENT_ID}.final.bam

## Remove multi-mapped and supplementary reads and retain only uniquely mapped reads.
## Based on https://bioinformatics.stackexchange.com/questions/508/obtaining-uniquely-mapped-reads-from-bwa-mem-alignment
sambamba view -h -t 20 -f bam -F "[XS] == null and not unmapped and not duplicate" $REP > $UNIQUE_REP
samtools sort -@ 10 -n -o $SORTED_UNIQUE_REP $UNIQUE_REP

## Filter blacklisted regions
## Peaks downloaded from https://github.com/Boyle-Lab/Blacklist/tree/master/lists
bedtools intersect -v -a $SORTED_UNIQUE_REP -b $BLACKLISTED_PEAKS > $FILTERED_REP
samtools sort -@ 10 -o $FINAL_REP $FILTERED_REP
samtools index $FINAL_REP









