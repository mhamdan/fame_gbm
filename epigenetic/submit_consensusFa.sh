#!/bin/bash

# To run this script, do 
# qsub -t 1-n submit_consensusFa.sh CONFIG IDS
#
#$ -N consensusFa
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=8G
#$ -pe sharedmem 8
#$ -l h_rt=96:00:00

unset MODULEPATH
. /etc/profile.d/modules.sh

CONFIG=$1
IDS=$2
TYPE=$3

source $CONFIG

PATIENT_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1`
BAM_DIR=$ALIGNMENTS/${PATIENT_ID}${TYPE}/${PATIENT_ID}${TYPE}
INPUT_BAM=$BAM_DIR/${PATIENT_ID}${TYPE}-ready.bam
OUTPUT_FQ=/exports/igmm/eddie/Glioblastoma-WGS/analysis/Consensus_fasta/${PATIENT_ID}${TYPE}.consensus.fq
OUTPUT_FA=/exports/igmm/eddie/Glioblastoma-WGS/analysis/Consensus_fasta/${PATIENT_ID}${TYPE}.consensus.fa

# Based on http://seqanswers.com/forums/showthread.php?t=15914 and
# https://www.biostars.org/p/367626/

# samtools mpileup -uf $REFERENCE $INPUT_BAM | bcftools call -mv -Oz -o $BAM_DIR/${PATIENT_ID}${TYPE}.calls.vcf.gz 
# tabix $BAM_DIR/${PATIENT_ID}${TYPE}.calls.vcf.gz 
# cat $REFERENCE | bcftools consensus $BAM_DIR/${PATIENT_ID}${TYPE}.calls.vcf.gz > $OUTPUT_FA


samtools mpileup -uf $REFERENCE $INPUT_BAM | bcftools call -c | vcfutils.pl vcf2fq > $OUTPUT_FQ
seqtk seq -aQ64 -q20 -n N $OUTPUT_FQ > $OUTPUT_FA
rm $OUTPUT_FQ



