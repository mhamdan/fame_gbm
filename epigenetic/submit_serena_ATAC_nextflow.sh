!#/bin/bash

### This script is used to submit the nextflow pipeline for ATAC-seq analysis

### To run this script, do
### qsub -t 1-n submit_serena_ATAC_nextflow.sh INPUT_SAMPLE_SHEET OUTPUT_DIR CUSTOM_RESOURCES READ_LENGTH EMAIL   
### where INPUT_SAMPLE_SHEET is the path to the sample sheet
### OUTPUT_DIR is the path to the output directory
### CUSTOM_RESOURCES is the path to the custom resources file
### READ_LENGTH is the read length
### EMAIL is the email address to send the report to

#$ -N atac_nextflow
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=12G
#$ -pe sharedmem 12
#$ -l h_rt=120:00:00


unset MODULEPATH
. /etc/profile.d/modules.sh

export NXF_CONDA_CACHEDIR="/exports/cmvm/eddie/scs/groups/spollar2-PollardLab/Serena/anaconda/"
export PATH=/exports/cmvm/eddie/scs/groups/spollar2-PollardLab/Serena/anaconda/envs/nf-core/bin:$PATH
export PATH=/exports/cmvm/eddie/scs/groups/spollar2-PollardLab/Serena/anaconda/envs/nf-core:$PATH

INPUT_SAMPLE_SHEET=$1
OUTPUT_DIR=$2
CUSTOM_RESOURCES=$3
READ_LENGTH=$4
EMAIL=$5

echo "Running nextflow pipeline for ATAC-seq analysis..."
echo "Input sample sheet: $INPUT_SAMPLE_SHEET"
echo "Output directory: $OUTPUT_DIR"
echo "Custom resources: $CUSTOM_RESOURCES"
echo "Read length: $READ_LENGTH"
echo "Email: $EMAIL"

nextflow run nf-core/atacseq \
    --input $INPUT_SAMPLE_SHEET \
    --outdir $OUTPUT_DIR \
    --genome hg38 -profile eddie,singularity --aligner bwa \
    --email $EMAIL \
    -c $CUSTOM_RESOURCES \
    --read_length $READ_LENGTH \
    --macs_gsize 2700000000 -resume


### Turns out you can't submit a qsub job for nextflow



