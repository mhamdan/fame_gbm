#!/bin/bash

# To run this script, do 

#$ -N differential_motif_enrichment
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=64G
#$ -l h_rt=10:00:00

## BED_LIST contains full paths to bed files 

CONFIG=$1
INPUT_BED_FILE=$2
CONTROL_BED_FILE=$3
ANALYSIS_NAME=$4
HOMER_OUTDIR=$5

RESULT_DIR=${HOMER_OUTDIR}/${ANALYSIS_NAME}/

source $CONFIG

#### GBM ATAC-seq sites within GBM H3K27ac sites 
#### CDS regions excluded

## Get enriched motifs
findMotifsGenome.pl \
     $INPUT_BED_FILE \
     hg38 \
     $RESULT_DIR \
     -bg $CONTROL_BED_FILE \
     -size given \
     -p 8

     
     
     

