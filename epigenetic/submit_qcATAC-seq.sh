#!/bin/bash

# To run this script, do 
# qsub -t 1-n submit_qcATAC-seq.sh CONFIG IDS
#
# CONFIG is the path to the file scripts/config.sh which contains environment variables set to
# commonly used paths and files in the script
# IDS is a list of sample ids, one per line, where tumor and normal samples are designated by
# the addition of a T or an N to the sample id.
# TYPE is either sample name or input
#
#$ -N filterATAC-seq
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=16G
#$ -l h_rt=72:00:00
#$ -pe sharedmem 8

CONFIG=$1
IDS=$2

source $CONFIG

SRR=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 1`
BAM=/exports/igmm/eddie/Glioblastoma-WGS/ATAC-seq/brain_regions/bams

cd $BAM

################################################################################################
### BWA actually follows the SAM spec and reports Phred scores as MAPQ values.  
### The calculation is based on the number of optimal (best) alignments found, 
### as well as the number of sub-optimal alignments combined with the 
### Phred scores of the bases which differ between the optimal and sub-optimal alignments.
### https://sequencing.qcfail.com/articles/mapq-values-are-really-useful-but-their-implementation-is-a-mess/

REP=${SRR}.sorted.bam
UNIQUE_REP=${SRR}.sorted.unique.bam
SORTED_UNIQUE_REP=${SRR}.sorted.unique.sorted.bam
FIXMATE_REP=${SRR}.sorted.unique.sorted.fixmate.bam
POSITION_FIXMATE_REP=${SRR}.sorted.unique.sorted.fixmate.position.bam
MARKDUP_REP=${SRR}.sorted.unique.sorted.fixmate.position.dup.bam
NO_MITO=${SRR}.sorted.unique.sorted.fixmate.position.dup.noChrM.bam
NO_BLACKLIST=${SRR}.sorted.unique.sorted.fixmate.position.dup.noChrM.noBlacklist.bam
FINAL_REP=${SRR}.final.bam

## Remove multi-mapped reads and retaining only uniquely mapped reads.
## Based on https://bioinformatics.stackexchange.com/questions/508/obtaining-uniquely-mapped-reads-from-bwa-mem-alignment

samtools view -h -F 4 $REP | grep -v -e 'XA:Z:' -e 'SA:Z:' > $UNIQUE_REP
samtools sort -@ 10 -n -o $SORTED_UNIQUE_REP $UNIQUE_REP
samtools fixmate -m $SORTED_UNIQUE_REP $FIXMATE_REP
samtools sort -@ 10 -o $POSITION_FIXMATE_REP $FIXMATE_REP
samtools markdup $POSITION_FIXMATE_REP $MARKDUP_REP

##Remove mitochondrial reads
samtools view $MARKDUP_REP | egrep	-v	chrM | samtools	view -bT $REFERENCE - -o $NO_MITO

## Filter blacklisted regions; Peaks downloaded from https://github.com/Boyle-Lab/Blacklist/tree/master/lists
bedtools intersect -v -a $NO_MITO -b $BLACKLISTED_PEAKS > $NO_BLACKLIST

## Sort
samtools sort -@ 10 -o $FINAL_REP $NO_BLACKLIST

## Index the sorted filtered bam files
samtools index $FINAL_REP

## Remove redundant intermediate files
#rm *unique* 








