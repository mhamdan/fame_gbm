#!/bin/bash

#
#$ -N bdgcmp_bigwig
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=12G
#$ -l h_rt=2:00:00
#$ -pe sharedmem 8

CONFIG=$1
IDS=$2
BDG_DIR=$3

source $CONFIG

unset MODULEPATH
. /etc/profile.d/modules.sh

module load igmm/apps/MACS2/2.1.1  

FACTOR_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 1`

cd $BDG_DIR

macs2 bdgcmp \
    -t ${FACTOR_ID}_normalised_treat_pileup.bdg \
    -c ${FACTOR_ID}_normalised_control_lambda.bdg \
    -m ppois \
    --outdir $BDG_DIR \
    -o ${FACTOR_ID}_normalised





