#!/bin/bash

### To merge peaks
## Merge chip-seq replicates based on IDR-like processing (but can scale up to >3 peaks)
## Based on https://github.com/rhysnewell/ChIP-R
## Install within a conda environment

#$ -N consensus_rep
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=12G
#$ -pe sharedmem 12
#$ -l h_rt=10:00:00


CONFIG=$1
IDS=$2
CHIPR_DIR=$3

source $CONFIG

SAMPLE_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 1`
PEAK_REP_1=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 2`
PEAK_REP_2=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 3`
PEAK_REP_3=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 4`
CONSENSUS_PREFIX=${CHIPR_DIR}/${SAMPLE_ID}.consensus.peaks

### unzip files:
bgzip -d $PEAK_REP_1
bgzip -d $PEAK_REP_2
bgzip -d $PEAK_REP_3

### Consensus peaks among 3 replicates
echo "Running chipr, creating consensus 2 of 3 peaksets..."
chipr -i ${PEAK_REP_1%.gz} ${PEAK_REP_2%.gz} ${PEAK_REP_3%.gz} -m 2 -o $CONSENSUS_PREFIX  
echo "Consensus peaks for ${SAMPLE_ID} generated"






















