#!/bin/bash

# This script combines multiple bigwig tracks into one unified track.
# Signal values are averaged.

#$ -N wiggletools
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=250G
#$ -l h_rt=72:00:00

export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/wiggletools/bin:$PATH
CHROM_SIZES=/exports/igmm/eddie/Glioblastoma-WGS/resources/hg38.chrom.sizes.hla.txt

## This can either be atac or h3k27ac or other epigenomic datasets:
DIRECTORY=$1
EPIGENOME_MARK=$2

cd $DIRECTORY

echo "Running for $EPIGENOME_MARK ..."
wiggletools write ${EPIGENOME_MARK}_averaged.wig mean *bw
wigToBigWig ${EPIGENOME_MARK}_averaged.wig $CHROM_SIZES ${EPIGENOME_MARK}_averaged.bw




