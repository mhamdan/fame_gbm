#!/bin/bash

# To run this script, do 
# qsub -t 1-n submit_macs2_pooled.sh CONFIG IDS
#
#$ -N macs2_pooled
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=12G
#$ -l h_rt=6:00:00
#$ -pe sharedmem 8

CONFIG=$1
IDS=$2
BAM_DIR=$3
OUTPUT_DIR=$4

source $CONFIG

unset MODULEPATH
. /etc/profile.d/modules.sh

module load igmm/apps/MACS2/2.1.1  

VARIANT_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 1`

cd $BAM_DIR

## Change -g according to species under study
## For interphase
###macs2 callpeak \
    -t ${VARIANT_ID}-Interphase-ChIP.merged.final.bam \
    -c ${VARIANT_ID}-Interphase-Input.merged.final.bam Bl6-Interphase-ChIP.merged.final.bam Bl6-Interphase-Input.merged.final.bam \
    -n ${VARIANT_ID}-Interphase_normalised \
    --outdir $OUTPUT_DIR \
    --bdg --SPMR \
    -p 1e-3 \
    -g mm

## For mitosis
macs2 callpeak \
    -t ${VARIANT_ID}-Mitotic-ChIP.merged.final.bam \
    -c ${VARIANT_ID}-Mitotic-Input.merged.final.bam Bl6-Mitotic-ChIP.merged.final.bam Bl6-Mitotic-Input.merged.final.bam \
    -n ${VARIANT_ID}-Mitotic_normalised \
    --outdir $OUTPUT_DIR \
    --bdg --SPMR \
    -p 1e-3 \
    -g mm

    
    
    
    
    
