#!/bin/bash

#$ -N plotCorr
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=120G
#$ -l h_rt=12:00:00

## QC on original sorted bam files using deeptools
## Based on https://deeptools.readthedocs.io/en/develop/index.html
## Generate multiBamSummary for all bam files:

CONFIG=$1
BATCH=$2
##NPZ_FILE=$2
BAMS_DIR=$3
OUTPUT_DIR=$4

source $CONFIG

## Deeptools need python 3.65
export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/py365/bin:$PATH

## Gather all sorted bam files and generate summary of chr1 in a numpy array
cd $BAMS_DIR
BAM_FILES=`ls *.bam | grep -v / | tr '\n' " "`

echo "Preparing multibam summary bins..."
multiBamSummary bins \
    --bamfiles $BAM_FILES \
    -o $OUTPUT_DIR/${BATCH}_scores_per_bin.npz \
    --outRawCounts $OUTPUT_DIR/${BATCH}_scores_per_bin.tab \
    --blackListFileName $BLACKLISTED_PEAKS_MM10 \
    --binSize 10000 \
    --numberOfProcessors 32 
    
## Plot correlations 
echo "Plotting correlation heatmap..."
plotCorrelation -in $OUTPUT_DIR/${BATCH}_scores_per_bin.npz \
    --removeOutliers \
    --whatToPlot heatmap \
    --skipZeros \
    --corMethod spearman \
    -o $OUTPUT_DIR/${BATCH}.corr.spearman.png \
    --outFileCorMatrix $OUTPUT_DIR/${BATCH}.corr.spearman.readCounts.tab

## Plot PCA
echo "Plotting PCA..."
plotPCA -in $OUTPUT_DIR/${BATCH}_scores_per_bin.npz \
    --transpose \
    --plotHeight 20 \
    --plotWidth 30 \
     --outFileNameData $OUTPUT_DIR/${BATCH}.PCA.transposed.tab \
    -o $OUTPUT_DIR/${BATCH}.PCA.png 



echo "Done!"











