#!/bin/bash

# To run this script, do 
# qsub submit_getFasta.sh CONFIG BED DONOR
#
#$ -N getfasta
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=8G
####$ -pe sharedmem 8
#$ -l h_rt=90:00:00

unset MODULEPATH
. /etc/profile.d/modules.sh

CONFIG=$1
BED=$2
OUTPUT_DIR=$3
OUTPUT_FASTA=$4

##DONOR_ID=$3

source $CONFIG

#PATIENT_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1`
#SAMPLE_ID=${PATIENT_ID}${TYPE}

##INPUT_DIR=/exports/igmm/eddie/Glioblastoma-WGS/resources/refgenome38/chromosomes
#BED_FILE=/exports/igmm/eddie/Glioblastoma-WGS/analysis/Consensus_merged_shared.bed
#INPUT_DIR=/exports/igmm/eddie/Glioblastoma-WGS/analysis/Consensus_fasta/sample_level
#OUTPUT_DIR=/exports/igmm/eddie/Glioblastoma-WGS/ChIP-seq/SOX_TF

#### This will intersect region of interest against fasta file.

#for chrom in `seq 1 22`
#do
#    bedtools getfasta -fi ${INPUT_DIR}/chr${chrom}.fa -bed $BED_FILE -fo ${OUTPUT_DIR}/Reference.chr${chrom}.SOX_merged.fa
#done

##bedtools getfasta -fi ${INPUT_DIR}/${SAMPLE_ID}.consensus.fa -bed $BED_FILE -fo ${OUTPUT_DIR}/${SAMPLE_ID}.consensus.SOXshared.fa

bedtools getfasta -fi $REFERENCE -bed $BED > $OUTPUT_DIR/$OUTPUT_FASTA





