#!/bin/bash

#$ -N b2f2h
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=4G
#$ -pe sharedmem 4
#$ -l h_rt=12:00:00

CONFIG=$1
IDS=$2

unset MODULEPATH
. /etc/profile.d/modules.sh

source $CONFIG

SAMPLE_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1`

SOX2=${SAMPLE_ID}_Sox2.macs2idr.narrowPeak
SOX9=${SAMPLE_ID}_Sox9.macs2idr.narrowPeak
NARROWPEAK_DIR=/exports/igmm/eddie/Glioblastoma-WGS/ChIP-seq/SOX_TF/qc/IDR
FASTA_DIR=/exports/igmm/eddie/Glioblastoma-WGS/ChIP-seq/SOX_TF/downstream/fasta

cd $NARROWPEAK_DIR


if [[ ! -f $FASTA_DIR/${SAMPLE_ID}_Sox9_only.fasta ]]
then
echo "Intersecting bed files and getting DNA sequences..."
### Bedtools + getfasta:
## Find SOX2 and SOX9 co-binding regions:
bedtools intersect -a $SOX2 -b $SOX9 | grep -vsi random | grep -vsi Un | grep -vsi chrM | grep -vsi alt | cut -f 1,2,3 > ${SAMPLE_ID}_Sox2_Sox9_overlap.bed
bedtools getfasta -fi $REFERENCE -bed ${SAMPLE_ID}_Sox2_Sox9_overlap.bed > $FASTA_DIR/${SAMPLE_ID}_Sox2_Sox9.overlap.fasta

## Find SOX2 region only
bedtools intersect -a $SOX2 -b $SOX9 -v | grep -vsi random | grep -vsi Un | grep -vsi chrM | grep -vsi alt | cut -f 1,2,3 > ${SAMPLE_ID}_Sox2_only.bed
bedtools getfasta -fi $REFERENCE -bed ${SAMPLE_ID}_Sox2_only.bed > $FASTA_DIR/${SAMPLE_ID}_Sox2_only.fasta

## Find SOX9 region only
bedtools intersect -a $SOX9 -b $SOX2 -v | grep -vsi random | grep -vsi Un | grep -vsi chrM | grep -vsi alt | cut -f 1,2,3 > ${SAMPLE_ID}_Sox9_only.bed
bedtools getfasta -fi $REFERENCE -bed ${SAMPLE_ID}_Sox9_only.bed > $FASTA_DIR/${SAMPLE_ID}_Sox9_only.fasta

else
echo "Intersected bed files and their DNA sequences already generated..."
fi

### FIMO:
## Require creation of parent directory

export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/pyclone/bin:$PATH
OUTPUT_DIR=/exports/igmm/eddie/Glioblastoma-WGS/ChIP-seq/SOX_TF/downstream/fimo/results/${SAMPLE_ID}
MOTIF=/exports/igmm/eddie/Glioblastoma-WGS/ChIP-seq/SOX_TF/downstream/fimo/motifs/SOX2_dimer_motif_meme.txt
SEQUENCE_FASTA=/exports/igmm/eddie/Glioblastoma-WGS/ChIP-seq/SOX_TF/downstream/fasta

if [[ ! -d $OUTPUT_DIR ]]
then
echo "Finding motif of interest..."

mkdir -p $OUTPUT_DIR

fimo \
    --max-stored-scores 500000 \
    --thresh 1e-4 \
    --o $OUTPUT_DIR/overlap \
    --verbosity 4 \
    --bfile --motif-- \
    $MOTIF \
    $FASTA_DIR/${SAMPLE_ID}_Sox2_Sox9.overlap.fasta

fimo \
    --max-stored-scores 500000 \
    --thresh 1e-4 \
    --o $OUTPUT_DIR/Sox2_only \
    --verbosity 4 \
    --bfile --motif-- \
    $MOTIF \
    $FASTA_DIR/${SAMPLE_ID}_Sox2_only.fasta

fimo \
    --max-stored-scores 500000 \
    --thresh 1e-4 \
    --o $OUTPUT_DIR/Sox9_only \
    --verbosity 4 \
    --bfile --motif-- \
    $MOTIF \
    $FASTA_DIR/${SAMPLE_ID}_Sox9_only.fasta  

echo "Done."

else
echo "FIMO files already generated..."
fi

### HOMER annotation:
HOMER_DIR=/exports/igmm/eddie/Glioblastoma-WGS/ChIP-seq/SOX_TF/downstream/homer 

if [[ ! -f $HOMER_DIR/${SAMPLE_ID}_Sox9_only.genes.txt ]]
then
echo "Using HOMER and its hg38 annotations to assign peaks to genes..."

annotatePeaks.pl ${SAMPLE_ID}_Sox2_Sox9_overlap.bed hg38 > $HOMER_DIR/${SAMPLE_ID}_Sox2_Sox9_overlap.genes.txt
annotatePeaks.pl ${SAMPLE_ID}_Sox2_only.bed hg38 > $HOMER_DIR/${SAMPLE_ID}_Sox2_only.genes.txt
annotatePeaks.pl ${SAMPLE_ID}_Sox9_only.bed hg38 > $HOMER_DIR/${SAMPLE_ID}_Sox9_only.genes.txt
echo "Done."
else
echo "Homer annotations already generated..."
fi

### HOMER dimer:
HOMER_DIR=/exports/igmm/eddie/Glioblastoma-WGS/ChIP-seq/SOX_TF/downstream/homer 
FIMO_RESULTS=/exports/igmm/eddie/Glioblastoma-WGS/ChIP-seq/SOX_TF/downstream/fimo/results
cd $FIMO_RESULTS
annotatePeaks.pl ${SAMPLE_ID}_Sox2_Sox9_overlap.dimer.bed hg38 > $HOMER_DIR/${SAMPLE_ID}_Sox2_Sox9_overlap.dimer.genes.txt



