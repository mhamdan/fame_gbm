#!/bin/bash

#$ -N consensus_homer_annotation
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=8G
#$ -l h_rt=2:00:00

CONFIG=$1

unset MODULEPATH
. /etc/profile.d/modules.sh

source $CONFIG

### HOMER de novo:
BED_DIR=/exports/igmm/eddie/Glioblastoma-WGS/ChIP-seq/SOX_TF/downstream/beds
HOMER_DIR=/exports/igmm/eddie/Glioblastoma-WGS/ChIP-seq/SOX_TF/downstream/homer 

###annotatePeaks.pl ${BED_DIR}/Merged_rep_Consensus5_overlap.bed hg38 -CpG -annStats > $HOMER_DIR/Merged_rep_Consensus5_overlap.annotated.stats.txt
###annotatePeaks.pl ${BED_DIR}/Merged_rep_consensus5_Sox2_exclusive.bed hg38 -CpG -annStats > $HOMER_DIR/Merged_rep_consensus5_Sox2_exclusive.annotated.stats.txt
###annotatePeaks.pl ${BED_DIR}/Merged_rep_consensus5_Sox9_exclusive.bed hg38 -CpG -annStats > $HOMER_DIR/Merged_rep_consensus5_Sox9_exclusive.annotated.stats.txt
annotatePeaks.pl ${BED_DIR}/random_sites_5000.bed hg38 -CpG -annStats > $HOMER_DIR/Random_sites_5000.annotated.stats.txt
