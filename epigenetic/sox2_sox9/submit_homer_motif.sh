#!/bin/bash

# To run this script, do 

#$ -N homer_motif
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=8G
#$ -l h_rt=10:00:00

## BED_LIST contains full paths to bed files 

CONFIG=$1
BED_FILE=$2
BED_NAME=$3
HOMER_OUTDIR=$4

RESULT_DIR=${HOMER_OUTDIR}/${BED_NAME}/

source $CONFIG

#### Merged_rep_Consensus5_overlap.bed (for co-bound SOX2/SOX9 binding sites)
#### co_bound_SOX2_SOX9_peaks_within_SEs_regions_to_genes.bed (for co-bound SOX2/SOX9 binding sites in SEs)
#### *summits.bed for individual sample peak summits

## Get enriched motifs
findMotifsGenome.pl \
     $BED_FILE \
     hg38 \
     $RESULT_DIR \
     -size given

     
     
     

