#!/bin/bash

#$ -N scanFimo
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_rt=72:00:00
#$ -pe sharedmem 8
#$ -l h_vmem=8G

CONFIG=$1
FASTA=$2
MOTIF=$3
NAME=$4

source $CONFIG

### FIMO:
echo "Finding motif of interest..."
export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/pyclone/bin:$PATH
OUTPUT_DIR=/exports/igmm/eddie/Glioblastoma-WGS/ChIP-seq/SOX_TF/downstream/fimo/results

fimo \
    --max-stored-scores 100000 \
    --thresh 1e-3 \
    --o $OUTPUT_DIR/$NAME \
    --verbosity 2 \
    --bfile --motif-- \
    --parse-genomic-coord \
    $MOTIF \
    $FASTA






##PATIENT_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1`
##SAMPLE_ID=${PATIENT_ID}${TYPE}

#INPUT_DIR=/exports/igmm/eddie/Glioblastoma-WGS/analysis/genomes/Consensus_fasta/region_level
#OUTPUT_DIR=/exports/igmm/eddie/Glioblastoma-WGS/analysis/fimo/region_level
## Scanning large size genomes, based on here: https://groups.google.com/forum/#!topic/meme-suite/yCdcLPTb3jw

#if [[ ! -d $OUTPUT_DIR/${SAMPLE_ID} ]]; then
 #   mkdir $OUTPUT_DIR/${SAMPLE_ID}
#fi

#cd $INPUT_DIR

#for file in ${SAMPLE_ID}*;
#do
#    fimo --max-stored-scores 500000 \
#    --thresh 1e-6 \
#    --o ${OUTPUT_DIR}/${SAMPLE_ID}/${file}.fimo \
#    $MOTIF \
#    $file
#done

######


##fimo --max-stored-scores 500000 --thresh 1e-6 --o hg38_intervals.fimo $MOTIF $INPUT_DIR/hg38_intervals.fa

##fimo --max-stored-scores 500000 --thresh 1e-6 $MOTIF $INPUT_DIR/$SAMPLE_ID.fa
#for chrom in `seq 1 22`
#do
#fimo --max-stored-scores 500000 --thresh 1e-6 --o Reference.${chrom}.fimo $MOTIF $INPUT_DIR/Reference.chr${chrom}.SOX_merged.fa

