#!/bin/bash

#$ -N epigenetic/submit_callChipPeaks.sh
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=8G
#$ -l h_rt=3:00:00
#$ -pe sharedmem 8

IDS=$1
CHIP_BAMS_DIR=$2
OUTPUT_DIR=$3

unset MODULEPATH
. /etc/profile.d/modules.sh

module load igmm/apps/MACS2/2.1.1  

SAMPLE_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1`

cd $OUTPUT_DIR

## Some samples only have one input rep: Use input Rep1 for all

## For SOX2:
### Rep1:
CHIP_REP1=$CHIP_BAMS_DIR/${SAMPLE_ID}_Sox2_Rep1.final.bam
INPUT=$CHIP_BAMS_DIR/${SAMPLE_ID}_Input_Rep1.final.bam

macs2 callpeak \
    -t $CHIP_REP1 \
    -c $INPUT \
    -n ${SAMPLE_ID}_Sox2_Rep1 \
    -p 1e-5 \
    -g hs

### Rep2:
CHIP_REP2=$CHIP_BAMS_DIR/${SAMPLE_ID}_Sox2_Rep2.final.bam
INPUT=$CHIP_BAMS_DIR/${SAMPLE_ID}_Input_Rep1.final.bam

macs2 callpeak \
    -t $CHIP_REP2 \
    -c $INPUT \
    -n ${SAMPLE_ID}_Sox2_Rep2 \
    -p 1e-5 \
    -g hs

## For SOX9:
### Rep1:
CHIP_REP1=$CHIP_BAMS_DIR/${SAMPLE_ID}_Sox9_Rep1.final.bam
INPUT=$CHIP_BAMS_DIR/${SAMPLE_ID}_Input_Rep1.final.bam

macs2 callpeak \
    -t $CHIP_REP1 \
    -c $INPUT \
    -n ${SAMPLE_ID}_Sox9_Rep1 \
    -p 1e-5 \
    -g hs

### Rep2:
CHIP_REP2=$CHIP_BAMS_DIR/${SAMPLE_ID}_Sox9_Rep2.final.bam
INPUT=$CHIP_BAMS_DIR/${SAMPLE_ID}_Input_Rep1.final.bam

macs2 callpeak \
    -t $CHIP_REP2 \
    -c $INPUT \
    -n ${SAMPLE_ID}_Sox9_Rep2 \
    -p 1e-5 \
    -g hs

