#!/bin/bash

# To run this script, do 
# qsub submit_plotCoverage.sh CONFIG BATCH
#
#$ -N plotCov
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=32G
#$ -l h_rt=72:00:00

##

CONFIG=$1
BATCH=$2

source $CONFIG

## Deeptools need python 3.65
export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/py365/bin:$PATH

## Gather all sorted bam files and generate summary in a numpy array
cd $CHIP_BAMS_DIR
BAM_FILES=`ls *Rep*final*.bam | grep -v / | tr '\n' " "`

plotCoverage \
    --bamfiles $BAM_FILES \
    --plotFile $CHIP_DEEPTOOLS/${BATCH}.coverage.png \
    --plotTitle "SOX TF Chip-seq coverage" \
    --smartLabels \
     --plotWidth 20 \
     --plotHeight 30 \
    --ignoreDuplicates \
    --minMappingQuality 10 \
    --region chr1 \
    --numberOfProcessors max \
    --outRawCounts $CHIP_DEEPTOOLS/${BATCH}.coverage.tab
    
    
    
    
    
    
