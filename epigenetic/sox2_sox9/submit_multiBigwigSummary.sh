#!/bin/bash

#$ -N multiBigWig
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=12G
#$ -l h_rt=40:00:00

CONFIG=$1
BIGWIG_DIR=$2
OUTPUT_DIR=$3
GENOMIC_REGION_BED=$4
GENOMIC_REGION=$5

source $CONFIG

## Deeptools need python 3.65
export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/py365/bin:$PATH
BW_DIR=/exports/igmm/eddie/Glioblastoma-WGS/ChIP-seq/SOX_TF/qc/deeptools/coverage/bigwig
INPUT_DIR=/exports/igmm/eddie/Glioblastoma-WGS/ChIP-seq/SOX_TF/downstream/homer
OUTPUT_DIR=/exports/igmm/eddie/Glioblastoma-WGS/ChIP-seq/SOX_TF/qc/deeptools/multibigwig
BED=${SAMPLE_ID}_Sox2_Sox9_merged_overlap_base_intergenic.bed

#### Genome wide
#multiBigwigSummary bins \
##    -b $BW_DIR/${SAMPLE_ID}_Sox2.normalised.bw $BW_DIR/${SAMPLE_ID}_Sox9.normalised.bw \
##   -o $OUTPUT_DIR/${SAMPLE_ID}_scores_per_bin.npz \
##    --outRawCounts $OUTPUT_DIR/${SAMPLE_ID}_scores_per_bin.tab \
##    --verbose
   
#### Specific region
## Overlapping regions: What is the signal intensity of overlap binding regions for each Sox2 and Sox9 binding sites?

cd $INPUT_DIR
multiBigwigSummary BED-file \
    -b $BW_DIR/${SAMPLE_ID}_Sox2.normalised.bw $BW_DIR/${SAMPLE_ID}_Sox9.normalised.bw \
   -o $OUTPUT_DIR/${SAMPLE_ID}_${BED%.bed}_scores_per_bin.npz \
    --outRawCounts $OUTPUT_DIR/${SAMPLE_ID}_${BED%.bed}_scores_per_bin.tab \
    --verbose \
    --BED $BED
    
## Generate coverage counts genome-wide to be used with PCA tools:
##multiBigwigSummary bins \
##    -b $BIGWIG_DIR/*.bw \
##    -o $OUTPUT_DIR/scores_per_bin.npz \
##    --outRawCounts $OUTPUT_DIR/scores_per_bin.tab \
##    --blackListFileName $BLACKLISTED_PEAKS_MM10 \
##    --binSize 10000 \
##    --numberOfProcessors 32 

## Generate coverage counts of all at various genomic elements:
## Provide bed files of the genomic element of interest:
## Use HOMER elements:

##multiBigwigSummary BED-file \
##    -b $BIGWIG_DIR/*.bw \
##    -o $OUTPUT_DIR/${GENOMIC_REGION}_scores_per_bin.npz \
##    --outRawCounts $OUTPUT_DIR/${GENOMIC_REGION}_scores_per_bin.tab \
##    --BED $GENOMIC_REGION_BED \
##    --blackListFileName $BLACKLISTED_PEAKS_MM10













    
