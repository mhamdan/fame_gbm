#!/bin/bash

# To run this script, do 
# qsub -t 1-n submit_callATACPeaks.sh CONFIG IDS
#
# CONFIG is the path to the file scripts/config.sh which contains environment variables set to
# commonly used paths and files in the script
# IDS is a list of sample ids, one per line, where tumor and normal samples are designated by
# the addition of a T or an N to the sample id.
# TYPE is either sample name or input
#
#$ -N ATACpeaks
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=16G
#$ -l h_rt=72:00:00
#$ -pe sharedmem 10

CONFIG=$1
IDS=$2

source $CONFIG

SRR=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 1`

## Define sample
REP=${SRR}.final.bam

cd /exports/igmm/eddie/Glioblastoma-WGS/ATAC-seq/brain_regions/bams

## Call peaks using settings of Corces et al. Science 2018 here- 
## https://science.sciencemag.org/content/sci/suppl/2018/10/24/362.6413.eaav1898.DC1/aav1898_Corces_SM.pdf

macs2 callpeak \
    -t $REP \
    -n ${SRR} \
    -f BAMPE \
    -g hs \
    --nomodel \
    --shift -100 \
    --extsize 200 \
    --call-summits \
    --nolambda 
    




