#!/bin/bash

## To submit qsub -t 1-N submit_GDCFetch.sh token manifest
#$ -N GDC
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -r yes
#$ -l h_vmem=8G
#$ -pe sharedmem 8
#$ -l h_rt=12:00:00

TOKEN=$1
MANIFEST=$2

FILE_ID=`head -n $SGE_TASK_ID $MANIFEST | tail -n 1 | cut -f 1`
GDC_CLIENT=/exports/igmm/eddie/Glioblastoma-WGS/scripts/gdc-client

$GDC_CLIENT download -t $TOKEN -d /exports/igmm/eddie/Glioblastoma-WGS/WGS/raw/source/DBGAP $FILE_ID



