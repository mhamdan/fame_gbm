#!/bin/bash

#### PULL DATA FROM A SPECIFIC GOOGLE CLOUD BUCKET ####
#### THIS HAS EGRESS COST IF TRANSFERING ACROSS REGIONS (IF DATA ARE NOT FROM THE UK) #### 

# To run this script, do 
# qsub -t n submit_gcloud_pull.sh URL
#
#$ -N gcloud_pull
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=8G
#$ -pe sharedmem 8
#$ -l h_rt=120:00:00

URL=$1

## Define files/directories
FILE=`head -n $SGE_TASK_ID $URL | tail -n 1`

GSUTIL=/gpfs/igmmfs01/eddie/Glioblastoma-WGS/scripts/google-cloud-sdk/bin/gsutil

cd /exports/igmm/eddie/Glioblastoma-WGS/WGS/raw/source/HMF/RNA/fastqs
$GSUTIL -u ivory-infinity-291911 cp $FILE ./











