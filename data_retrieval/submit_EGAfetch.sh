#!/bin/bash

# To run this script, do 
# qsub -t 1-n submit_EGAfetch.sh CREDENTIAL PARAMS BATCH
#
#$ -N EGA
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=8G
#$ -pe sharedmem 8
#$ -l h_rt=150:00:00

PARAMS=$1
CREDENTIAL=$2
BATCH=$3

## Credential file is .json format containing username and password.
## Param file contains all EGADXXXX, must be complete path.

DATASET=`head -n $SGE_TASK_ID $PARAMS | tail -n 1`
DIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/raw/source/${BATCH}
PYEGA3=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/pyega3/bin/pyega3

cd $DIR
echo "$DATASET"
$PYEGA3 -c 10 -cf $CREDENTIAL fetch --max-retries -1 $DATASET > ${DATASET}.download.log




