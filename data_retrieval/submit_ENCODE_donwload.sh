#!/bin/bash


#$ -N ENCODE_download
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=4G
##$ -pe sharedmem 2
#$ -l h_rt=20:00:00

unset MODULEPATH
. /etc/profile.d/modules.sh


FILE=$1
URL=`head -n $SGE_TASK_ID $FILE | tail -n 1`
BAM_DIR=/exports/igmm/eddie/Glioblastoma-WGS/ChIP-seq/H3K27ac-seq_GBM/bams/ENCODE

cd $BAM_DIR
curl -O -J -L $URL







