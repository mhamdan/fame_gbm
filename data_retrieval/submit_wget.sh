#!/bin/bash
#To run this script, do qsub -t 1-n <script> <URL>

#$ -cwd
#$ -N wget_files
#$ -j y
#$ -S /bin/bash
#$ -l h_vmem=4G
#$ -l h_rt=80:00:00
####$ -pe sharedmem 4

# Initialise the modules framework
##. /etc/profile.d/modules.sh

#Define parameters
FILE_LIST=$1

#URL=`head -n $SGE_TASK_ID $FILE_LIST | tail -n 1 | awk '{ print $1 }'`
URL=`head -n $SGE_TASK_ID $FILE_LIST | tail -n 1|cut -f 5`
#echo "head -n $SGE_TASK_ID $PARAMS | tail -n 1 | awk '{print "ftp://"$0}'"
##URL=`head -n $SGE_TASK_ID $PARAMS | tail -n 1 | awk '{print "ftp://"$0}'`

#FILE=`head -n $SGE_TASK_ID $FILE_LIST | tail -n 1`




cd /exports/igmm/eddie/Glioblastoma-WGS/ChIP-seq/H3K27ac-seq_GBM/reads/Single_end_reads
wget -c $URL


## wget -c http://s3.amazonaws.com/nanopore-human-wgs/rel7/rel_7.fastq.gz


##cd /exports/igmm/eddie/Glioblastoma-WGS/ATAC-seq/Stein_ATAC

###cd /exports/igmm/eddie/Glioblastoma-WGS/WGS/raw/source/NSCs

##wget -c http://ftp1.igc.ed.ac.uk/secret/04164099/Chandra_NSCs_WGS_raw.tar.gz
###tar -xvf Chandra_NSCs_WGS_raw.tar.gz
#wget command
#echo "wget -c -r -nH --cut-dirs=4 --no-parent --reject="index.html*" $URL"
##wget -c -r -nH --cut-dirs=4 --no-parent --reject="index.html*" $URL


#If password is needed
#wget -c -r -nH --cut-dirs=4 --no-parent --reject="index.html*" --user $USER --password $PASSWORD $PARAMS


#To download files given a URL. --no-parent and --reject is to prevent downloading index.html files.
#wget -cr --no-parent --reject "index.html*" $1
#wget -cr --no-parent $1 #if no robot files

#To download ftps given a list of ftps in txt file.
#cat $1 | xargs wget -cr --no-parent --reject "index.html*"
#cat $URL | xargs wget -c -r -nH --cut-dirs=4 --no-parent --reject="index.html*"

