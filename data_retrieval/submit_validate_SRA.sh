#!/bin/bash

#This script is to validate the downloaded SRA files from NCBI. 
#To run this script, do qsub <script> $1

#$ -cwd
#$ -N Validate_SRA_files
#$ -j y
#$ -S /bin/bash
#$ -l h_vmem=32G
#$ -l h_rt=90:00:00

# Initialise the modules framework
. /etc/profile.d/modules.sh

#Load SRA toolkit
module load igmm/apps/sratoolkit/2.8.2-1

#Define file containing path to fastq/SRA files.
file=$1

#Validate SRA files and put stout in separate logs.
#for file in SRR*; do
#echo ${file} > ${file}.validate.log; 
#vdb-validate ${file} > ${file}.validate.log; 
#done

#for file in */* ; do
#vdb-validate ${file}; 
#done

cat $file | xargs vdb-validate