#!/bin/bash

#$ -N snakemake_run
#$ -S /bin/bash
#$ -j y
#$ -cwd
#$ -l h_rt=200:00:00
#$ -l h_vmem=2G
#$ -V

SNAKEMAKE_DIR=$1

cd $SNAKEMAKE_DIR

export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/atac_seq/bin:$PATH
export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/condabin/conda:$PATH

snakemake -j 100 \
    --cluster "qsub -V -l h_rt=100:00:00 -l h_vmem=320G" \
    --latency-wait 60 --rerun-incomplete \
    --jobname "{rulename}.{jobid}.snakejob.sh" \
    -kpr \
    --use-conda --conda-prefix env --conda-frontend conda

