#!/bin/bash

#$ -N snakemake_run
#$ -S /bin/bash
#$ -j y
#$ -cwd
#$ -l h_rt=200:00:00
#$ -l h_vmem=32G
#$ -V

export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/atac_seq/bin:$PATH
export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/condabin/conda:$PATH

# snakemake -j 50 \
#     --cluster "qsub -V -l h_rt=100:00:00 -l h_vmem=200G" \
#     --latency-wait 60 --rerun-incomplete \
#     --use-conda --conda-prefix env --conda-frontend conda

# snakemake --profile eddie -s workflows/$SNAKEFILE --cluster-config conf/cluster.yaml

# mkdir -p "${WORKDIR}/logs/drmaa"
# snakemake ${OPTS} --jobs ${NUM_JOBS} -k --use-conda --latency-wait 120 \
#     --max-jobs-per-second 2 \
#     cluster_json="${CLUSTRCONF}" \
#     --restart-times 0 \
#     --cluster-config "${CLUSTRCONF}" \
#     --configfile "${CONFIGFILE}" \
#     --jobname "{jobid}.{cluster.name}" \
#     --drmaa " -S /bin/bash -j {cluster.j} -M {cluster.M} -m {cluster.m} -q {cluster.queu} -l nodes={cluster.nodes}:ppn={cluster.ppn},walltime={cluster.walltime} -l mem={cluster.mem}gb -e ${WORKDIR}/{cluster.stderr} -o ${WORKDIR}/{cluster.stdout}" \
#     $EXTRA_OPTS $TARGET

snakemake --jobs 10 -kpr --latency-wait 120 --max-jobs-per-second 2 \
    --use-conda --conda-prefix env --conda-frontend conda \
    --restart-times 0 \
    --rerun-incomplete \
    --cluster-config conf/cluster.json \
    --cluster "qsub -V -l h_rt={cluster.time} -l h_vmem={cluster.mem}"