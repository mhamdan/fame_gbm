#!/bin/bash

## NeoSV calls neoantigens from structural variants as per https://genomebiology.biomedcentral.com/articles/10.1186/s13059-023-03005-9

#$ -N neoSV
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=12G
#$ -pe sharedmem 12
#$ -l h_rt=92:00:00

## Based on https://github.com/ysbioinfo/NeoSV
## Using python 3.6 within a conda environment
## Here I used py37 conda environment previously set up.
## conda activate py37
## pip install neosv
## Need netmhcpan installed and configured, here we use the module already installed on Eddie => there's none!
## Tried to install netmhcpan 4.0, but can't find link to the MHC binding data
## Installed and tested netmhcpan 4.1b instead - test examples worked.

## Need reference file, pre-download using the following command (run once):
## export PYENSEMBL_CACHE_DIR=/exports/igmm/eddie/Glioblastoma-WGS/scripts/NeoSV/pysensembl_cache_dir # specify the location for storing reference
## pyensembl install --release 96 --species human # download, for hg19 please use release 75, for hg38 please used release 96

## For testing, use the following command: Test data are in hg19
## export PYENSEMBL_CACHE_DIR=/exports/igmm/eddie/Glioblastoma-WGS/scripts/NeoSV/pysensembl_cache_dir # specify the location for storing reference
## pyensembl install --release 75 --species human # download, for hg19 please use release 75, for hg38 please used release 96
## To test, run:
## git clone https://github.com/ysbioinfo/NeoSV.git
## NEOSV_DIR=/exports/igmm/eddie/Glioblastoma-WGS/scripts/NeoSV
## cd $NEOSV_DIR
## export PYENSEMBL_CACHE_DIR=/exports/igmm/eddie/Glioblastoma-WGS/scripts/NeoSV/pysensembl_cache_dir # specify the location for storing reference
## NETMHCPAN=/exports/igmm/eddie/Glioblastoma-WGS/scripts/netMHCpan-4.1/netMHCpan ## note this is not version 4.0 as per NeoSV original instructions
## neosv -vf test.sv.vcf -hf test.hla.txt -np $NETMHCPAN -o test -p test -r 75    

## Didn't work because of the following error:
##Traceback (most recent call last):
##  File "/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/py37/bin/neosv", line 8, in <module>
##    sys.exit(main())
##  File "/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/py37/lib/python3.7/site-packages/neosv/main.py", line 62, in main
##    write_fusion(file_fusion, sv_fusions, dict_epitope)
##  File "/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/py37/lib/python3.7/site-packages/neosv/output.py", line 31, in write_fusion
##    for allele in dict_neo[neoepitope]:
##KeyError: 'HCEKDIDECAL'

## Although some outputs were present
## Try using 4.1 link but on 4.0 instead => didn't exist

## Changed original scripts in the following ways:
## Referred to issue on github: https://github.com/ysbioinfo/NeoSV/issues/1
## Changed line 45 of https://github.com/ysbioinfo/NeoSV/blob/main/neosv/netMHC.py as per suggestion
## Didn't work still! Still generated 3 output files apart from *.anno.txt

## Also had issues here :
## def write_fusion(filepath, svfusions, dict_neo):
##    """
##    :param filepath: outfile for fusion derived neoantigen
##    :param svfusions: a list of svfusion classes
##    :param dict_neo: a dictionary for neoepitopes
##    :return: None
##    """
##    with open(filepath, 'w') as f:
##        header = '\t'.join(['chrom1', 'pos1', 'gene1', 'transcript_id1',
##                            'chrom2', 'pos2', 'gene2', 'transcript_id2',
##                            'svpattern', 'svtype', 'frameshift',
##                            'neoantigen', 'allele', 'affinity', 'rank'])
##        f.write(header + '\n')
##        for svfusion in svfusions:
##           for neoepitope in svfusion.neoepitopes:
##                print("neoepitope", neoepitope) ## AH: seems to work if add this:
##                for allele in dict_neo[neoepitope]:
##                    if dict_neo[neoepitope][allele][2] == 'PASS':
##                        affinity = str(dict_neo[neoepitope][allele][0])
##                        rank = str(dict_neo[neoepitope][allele][1])
##                        f.write('\t'.join(['\t'.join(svfusion.output()), neoepitope, allele, affinity, rank]) + '\n')
## This worked and produced an output file but for some it stilled failed
## Turned out that some did not have any HLA-allele (failed LILAC run)
## /exports/igmm/eddie/Glioblastoma-WGS/scripts/NeoSV/test/test.net.out.txt

## Two errors:
## 1) KeyError: 'HCEKDIDECAL'
## 2) HLA haplotype ending with 3 characters instead of 2 (e.g. HLA-A*01:111 instead of HLA-A*01:11) not recognised

## Need to prepare input files for NeoSV:
## 1) VCF file with SV calls
## 2) HLA haplotype file
## 3) NetMHCpan path

## SGE input:
PARAM_FILE=$1

## Define NeoSV inputs:
OUTPUT_PREFIX=`head -n $SGE_TASK_ID $PARAM_FILE | tail -n 1 | cut -f 1` 
SV_VCF_ORIGINAL=`head -n $SGE_TASK_ID $PARAM_FILE | tail -n 1 | cut -f 2` 
HLA_ALLELE=`head -n $SGE_TASK_ID $PARAM_FILE | tail -n 1 | cut -f 3` 
OUTPUT_DIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/immune/neosv/output/${OUTPUT_PREFIX} ## make sure to store in separate folder due to filtering step result_filter.py later.
mkdir $OUTPUT_DIR 

## Load module and paths:
export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/py37/bin:$PATH
export PYENSEMBL_CACHE_DIR=/exports/igmm/eddie/Glioblastoma-WGS/scripts/NeoSV/pysensembl_cache_dir # specify the location for storing reference
NETMHCPAN=/exports/igmm/eddie/Glioblastoma-WGS/scripts/netMHCpan-4.1/netMHCpan ## note this is not version 4.0 as per NeoSV original instructions

## Run NeoSV:
gunzip ${SV_VCF_ORIGINAL}.gz
cat $SV_VCF_ORIGINAL | grep -si pass | grep -si mateid > ${SV_VCF_ORIGINAL}_passed.vcf ## passed calls only and exclude single breakends, if not input.py will give out warnings and fail to run
SV_VCF_PASS=${SV_VCF_ORIGINAL}_passed.vcf
cat $SV_VCF_PASS | wc -l
neosv -vf $SV_VCF_PASS -hf $HLA_ALLELE \
    -np $NETMHCPAN -o $OUTPUT_DIR \
    -p $OUTPUT_PREFIX -r 96 \
    -ic 500 \
    -rc 2  

## Run https://github.com/ysbioinfo/NeoSV/blob/9dc698510ba1e39211cf3b5083ec114911b35374/neosv/result_filter.py when all files done
## When it says "duplicate SVs" removed, it's referred to to the formatting of bedpe that they use
## Turns out the failure tied to this error 
