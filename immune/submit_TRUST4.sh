#!/bin/bash

## Create new conda environment
## Install TRUST4 using conda as per https://github.com/liulab-dfci/TRUST4

# To run this script, do 
# qsub -t 1-n submit_TRUST4.sh IDS

#$ -N trust4
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=8G
#$ -pe sharedmem 8
#$ -l h_rt=22:00:00

## SGE INPUT:
IDS=$1

## SCRIPT PARAMS:
BATCH=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 1`
FASTQ_FILE=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 2`
SAMPLE_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 3`
SAMPLE_TYPE=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 4`
RUN_ID=${SAMPLE_ID}_${SAMPLE_TYPE}

OUTPUT_DIR=/exports/igmm/eddie/Glioblastoma-WGS/RNA-seq/results/TRUST4
GENOME_FASTA=/exports/igmm/eddie/Glioblastoma-WGS/scripts/TRUST4/hg38_bcrtcr.fa
IMGT_REF=/exports/igmm/eddie/Glioblastoma-WGS/scripts/TRUST4/human_IMGT+C.fa

unset MODULEPATH
. /etc/profile.d/modules.sh

export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/trust4/bin:$PATH

run-trust4 \
    -f $GENOME_FASTA \
    --ref $IMGT_REF \
    -1 ${FASTQ_FILE}_R1.fastq.gz \
    -2 ${FASTQ_FILE}_R2.fastq.gz \
    -o ${OUTPUT_DIR}/${RUN_ID}















