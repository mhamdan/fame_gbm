#!/bin/bash

## This script runs VEP annotation on an ensembl VCF and run pVACtools to annotate neoantigen properties:
## Need VEP, pvactools and annotation of HLA genotyping using LILAC

# To run this script, do 
# qsub -t 1-n submit_pvactools.sh PARAM_FILE HLA_HAPLOTYPE

#$ -N pvactools
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=12G
#$ -pe sharedmem 12
#$ -l h_rt=22:00:00

PARAM_FILE=$1
HLA_HAPLOTYPE=$2

##### Annotate VCF with VEP info:

## 1) Install VEP and VEP cache locally:
## Install VEP through conda
## https://github.com/Ensembl/ensembl-vep
## conda install -c bioconda ensembl-vep
## Follow instructions to install VEP database locally
## Use --cache option and point cache directory using --dir-cache

## 2) Install VEP plugins 
## https://pvactools.readthedocs.io/en/latest/pvacseq/additional_commands.html#install-vep-plugin-label
## Use --dir_plugins to point to VEP_plugin folder

## Inputs:
### ORIGINAL_VCF points to ensemble calls (full paths)

PATIENT_ID=`head -n $SGE_TASK_ID $PARAM_FILE | tail -n 1 | cut -f 1` 
NEW_TYPE=`head -n $SGE_TASK_ID $PARAM_FILE | tail -n 1 | cut -f 2`
STAGE=`head -n $SGE_TASK_ID $PARAM_FILE | tail -n 1 | cut -f 3`
ORIGINAL_VCF=`head -n $SGE_TASK_ID $PARAM_FILE | tail -n 1 | cut -f 4`

## Check if vep annotation already exists.
VEP_VCF=/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/ssv/ensemble/${STAGE}/${PATIENT_ID}${NEW_TYPE}.ssv.vep.vcf

if [[ ! -f $VEP_VCF ]]; then

    echo "No VEP annotation for ${PATIENT_ID}${TYPE}, doing it now..."
    export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/vep/bin:$PATH

    vep --cache --offline --dir_cache /exports/igmm/eddie/Glioblastoma-WGS/scripts/vep/vep_library \
        --force_overwrite --symbol --format vcf --vcf --symbol --terms SO --tsl --hgvs \
        --plugin Frameshift --plugin Wildtype --dir_plugins /exports/igmm/eddie/Glioblastoma-WGS/scripts/VEP_plugins \
        -i $ORIGINAL_VCF -o $VEP_VCF 
        
else
    echo "VEP annotation for ${PATIENT_ID}${TYPE} present- proceeding..."
fi

## 3) Install pvactools using conda
## VCF needs GT annotation (comes as default form our ensemble approach).
## Need coverage data but can be parsed directly when calling pVacSeq (using --normal-sample-name)

##### PVACTOOLS:
unset MODULEPATH
. /etc/profile.d/modules.sh

module load igmm/apps/python/2.7.10
export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/pvactools/bin:$PATH

OUTPUT_DIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/immune/pvactools/v3/${STAGE}/${PATIENT_ID}${NEW_TYPE}
rm -rf $OUTPUT_DIR
mkdir -p $OUTPUT_DIR

### pvacseq needs chromosomal length info in VCF header:
VEP_VCF_REHEADER=$OUTPUT_DIR/${PATIENT_ID}${NEW_TYPE}.ssv.vep.reheader.vcf
REF_FAI=/exports/igmm/eddie/Glioblastoma-WGS/resources/refgenome38/hg38.fa.fai
bcftools reheader --fai $REF_FAI -o $VEP_VCF_REHEADER $VEP_VCF

### SAMPLE_NAME needs to be what's in the VCF headers
TUMOR_SAMPLE_NAME=`cat $VEP_VCF_REHEADER | grep "#CHROM" | cut -f 10`
NORMAL_SAMPLE_NAME=`cat $VEP_VCF_REHEADER | grep "#CHROM" | cut -f 11`
HLA_ALLELE=`cat $HLA_HAPLOTYPE | grep ${PATIENT_ID}${NEW_TYPE} | cut -f 2`

##BLAST=/exports/igmm/eddie/Glioblastoma-WGS/scripts/ncbi-blast-2.15.0+
##MHC_I_IEDB=/exports/igmm/eddie/Glioblastoma-WGS/scripts

## Run pvacseq:
if [[ ! -f $OUTPUT_DIR/${TUMOR_SAMPLE_NAME}.filtered.tsv ]]; then

    echo "Running pvacseq for ${PATIENT_ID}${NEW_TYPE}..."
    echo $VEP_VCF_REHEADER
    echo $TUMOR_SAMPLE_NAME
    echo $HLA_ALLELE
    echo $OUTPUT_DIR
    
    pvacseq run \
        -e1 8,9,10,11 \
        -c 1 \
        -b 500 \
        --top-score-metric lowest \
        -d full \
        --pass-only \
        -t 10 \
        --normal-sample-name $NORMAL_SAMPLE_NAME \
        $VEP_VCF_REHEADER \
        $TUMOR_SAMPLE_NAME \
        $HLA_ALLELE \
        NetMHCpan \
        $OUTPUT_DIR

else

    echo "pvacseq for ${PATIENT_ID}${NEW_TYPE} already run, skipping..."

fi

























