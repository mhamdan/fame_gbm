#!/bin/bash

## Neo-antigen prediction from RNA-derived gene fusion
## https://icbi.i-med.ac.at/software/NeoFuse/NeoFuse.shtml
## Run using singularity

## Download NeoFuse script (from link above) and append to $PATH 

## Update August 2022: Move to latest scripts from github https://github.com/icbi-lab/NeoFuse
## This is due to YARA's tmp directory problems while running previous singularity build from the icbi website.
## We now use v1.2.1
## git clone the .git file from github page (instead of using the website)

## Prepare singularity: 
## Move .singularity directory to somewhere with more storage (Guidance is https://docs.ccs.uky.edu/pages/viewpage.action?pageId=655402)
## cd /exports/eddie/scratch/mhamdan && mkdir .singularity && cd && ln -s /exports/eddie/scratch/mhamdan/.singularity .

## Build NeoFuse singularity sif file:
## NeoFuse -B --singularity

## Use the same reference genome and indexes for ISOFOX analysis (STAR indexes)

# To run this script, do 
# qsub -t 1-n submit_neofuse.sh IDS

#$ -N neofuse
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=12G
#$ -pe sharedmem 12
#$ -l h_rt=92:00:00

## SGE INPUT:
IDS=$1

## SCRIPT PARAMS:
BATCH=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 1`
FASTQ_FILE=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 2`
SAMPLE_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 3`
SAMPLE_TYPE=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 4`
RUN_ID=${SAMPLE_ID}_${SAMPLE_TYPE}

OUTPUT_DIR=/exports/igmm/eddie/Glioblastoma-WGS/RNA-seq/results/NeoFuse
NEOFUSE_DIR=/exports/igmm/eddie/Glioblastoma-WGS/scripts/NeoFuse
NEOFUSE_SCRIPT=$NEOFUSE_DIR/NeoFuse

## STAR references (must be v2.7.1a)
STAR_INDEX_DIR=/exports/igmm/eddie/Glioblastoma-WGS/resources/RNA-seq_resources/star/index_2.7.1a
STAR_GENOME=/exports/igmm/eddie/Glioblastoma-WGS/resources/RNA-seq_resources/star/genome/GRCh38.p13.genome.fa
STAR_GTF=/exports/igmm/eddie/Glioblastoma-WGS/resources/RNA-seq_resources/star/genome/gencode.v36.annotation.gtf

cd $NEOFUSE_DIR

unset MODULEPATH
. /etc/profile.d/modules.sh

## As per https://github.com/icbi-lab/NeoFuse/issues/3 and
## https://github.com/icbi-lab/NeoFuse/issues/16
####TMPDIR=/exports/eddie/scratch/mhamdan ## didn't work
####export SINGULARITY_TMPDIR=$TMPDIR ## didn't work
####export TEMPDIR=$OUTPUT_DIR ## didn't work
####export TEMPDIR=/mnt/out/$(echo ${OUTDIR##*/}) ## didn't work
####export TMPDIR=/mnt/out/$(echo ${OUTDIR##*/}) ## didn't work
####export TMPDIR=/mnt/out/$(echo ${OUTPUT_DIR##*/}) ## didn't work

## Need to be exactly this command not "igmm/app/etc/singularity"....
##module load singularity

##$NEOFUSE_SCRIPT \
##    -1 ${FASTQ_FILE}_R1.fastq.gz \
##   -2 ${FASTQ_FILE}_R2.fastq.gz \
##    -s $STAR_INDEX_DIR \
##    -g $STAR_GENOME \
##    -a $STAR_GTF \
##    -o $OUTPUT_DIR \
##    -d $RUN_ID \
##    -V GRCh38 \
##    -n 20 \
##    -m 8 \
##    -M 11 \
##    -c L \
##    --singularity


### Corrected commands using path v1.2.2a as per https://github.com/icbi-lab/NeoFuse/issues/16
### Update 9/8/22: Now use 1.2.2a
### Delete the cache within .singularity before creating a new sif object

module load singularity

$NEOFUSE_SCRIPT \
    -1 ${FASTQ_FILE}_R1.fastq.gz \
    -2 ${FASTQ_FILE}_R2.fastq.gz \
    -s $STAR_INDEX_DIR \
    -g $STAR_GENOME \
    -a $STAR_GTF \
    -o $OUTPUT_DIR \
    -d $RUN_ID \
    -n 20 \
    -m 8 \
    -M 11 \
    -c L \
    --singularity















