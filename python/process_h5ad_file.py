import scipy as scipy
import pandas as pd
import numpy as np
import pandas as pd
import scanpy as sc

## Import h5ad file
print("Loading h5ad file...")
adata = sc.read_h5ad("/exports/igmm/eddie/Glioblastoma-WGS/scRNA-seq/ruiz_moreno_et_al/extended_GBMap.h5ad")

## Convert sparse matrix of gene expression to dataframe:
print("Converting sparse matrix of gene expression to dataframe...")
df = pd.DataFrame.sparse.from_spmatrix(adata.X)

## Attach cell barcodes to dataframe:
print("Attaching cell barcodes to dataframe...")
df['cell_barcodes'] = adata.obs.index

## Sample down to 5% of cells:
print("Sampling down to 5% of cells...")
df_subset = df.sample(frac=0.05, random_state=1)

## Save dataframe as csv file:
print("Saving dataframe as csv file...")
df_subset.to_csv("/exports/igmm/eddie/Glioblastoma-WGS/scRNA-seq/ruiz_moreno_et_al/extended_GBMap_subset_0.05_gene_expression_data.csv", index=True)



