#!/usr/bin/bash

#Configuration file for common directory and file locations for scripts

## Paths:
export PATH=/exports/igmm/eddie/NextGenResources/bcbio-1.1.5/anaconda/bin:$PATH
export PATH=/exports/igmm/eddie/NextGenResources/bcbio-1.1.5/tools/bin:$PATH 
export PATH=/exports/igmm/eddie/NextGenResources/bcbio-1.1.5:$PATH
export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/bin:$PATH
export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/snakemake/bin:$PATH

## Base:
BASE=/exports/igmm/eddie/Glioblastoma-WGS
WGS=/exports/igmm/eddie/Glioblastoma-WGS/WGS
##BAM_DIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/raw/source/${BATCH}
##BAM_DIR=/exports/igmm/eddie/Glioblastoma-WGS/ChIP-seq/${BATCH}
ALIGNMENTS=$WGS/alignments
PARAMS=$WGS/params
SOURCE=$WGS/raw/source
READS=$WGS/raw/reads
LANES=$WGS/raw/lanes

BCBIO_CONFIG=$BASE/bcbio/config
BCBIO_WORK=$BASE/bcbio/work

SCRIPTS=$BASE/scripts
FAME_GBM=$SCRIPTS/fame_gbm
TITANCNA=$FAME_GBM/R/titanCNA.R
SEQUENZA=$FAME_GBM/R/sequenza.R
RESOURCES=$BASE/resources

METRICS=$WGS/metrics
PON=$METRICS/pon

QC=$WGS/qc
READ_BIAS=$QC/read_orientation_bias
CONT_DIR=$QC/calculate_contamination
VERIFYBAMID_DIR=$QC/verifybamid2

GERMLINE_DIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/germline
GDB_DIR=$GERMLINE_DIR/gdb

BCBIO_VARIANTS=$WGS/variants/bcbio
SSV=$WGS/variants/ssv

VARIANTS=$WGS/variants
M2_VARIANTS_UNFILTERED=$SSV/samples/Mutect2/unfiltered
M2_VARIANTS_FILTERED=$SSV/samples/Mutect2/filtered
M2_VARIANTS_PASSED=$SSV/samples/Mutect2/passed
M2_VARIANTS_FORMATTED=$SSV/samples/Mutect2/formatted_vcf

STRELKA2_DIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/bcbio/${STAGE}/${PATIENT_ID}/${PATIENT_ID}${TYPE}
S2_VARIANTS_PASSED=$SSV/samples/Strelka2/passed
S2_VARIANTS_OXOG=$SSV/samples/Strelka2/oxog_filtered
S2_VARIANTS_FORMATTED=$SSV/samples/Strelka2/formatted_vcf

VARDICT_DIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/bcbio/${STAGE}/${PATIENT_ID}/${PATIENT_ID}${TYPE}
VARDICT_VARIANTS_PASSED=$SSV/samples/Vardict/passed
VARDICT_VARIANTS_OXOG=$SSV/samples/Vardict/oxog_filtered
VARDICT_VARIANTS_FORMATTED=$SSV/samples/Vardict/formatted_vcf

INTERSECT_DIR=$SSV/intersect
M2_S2=$INTERSECT_DIR/M2_S2
M2_Var=$INTERSECT_DIR/M2_Var
S2_Var=$INTERSECT_DIR/S2_Var

CONSENSUS_DIR=$SSV/consensus
ENSEMBLE_DIR=$SSV/ensemble
ENSEMBLE_VCF=$ENSEMBLE_DIR/${PATIENT_ID}${TYPE}.ssv.vcf.gz

CNV=$WGS/variants/cnv
PREPROCESSED_INTERVALS=$CNV/interval_list
READ_COUNTS=$CNV/read_counts
COPY_RATIOS=$CNV/copy_ratios
ALLELIC_COUNTS=$CNV/allelic_counts
MODEL_SEGMENTS=$CNV/model_segments
GISTIC_SEGMENTS=$CNV/gistic2/seg_files
PLOT_MODEL_SEGMENTS=$CNV/plot_model_segments
CALLED_COPY_RATIOS=$CNV/called_copy_ratios

TITAN_DIR=$CNV/titan

MANTA_UNFILTERED_VCF=/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/bcbio/${PATIENT_ID}/${PATIENT_ID}T/${PATIENT_ID}-manta.vcf.gz 
MANTA_HIGH_CONF_VCF=/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/sv/manta/${PATIENT_ID}-manta.high.conf.vcf.gz
MANTA_LOW_CONF_VCF=/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/sv/manta/${PATIENT_ID}-manta.low.conf.vcf.gz

BLAST=/exports/igmm/eddie/Glioblastoma-WGS/SOX/blast

WORK_DIR=$BCBIO_WORK/$PATIENT_ID
JVM_OPTS="-Dsamjdk.use_async_io_read_samtools=false -Dsamjdk.use_async_io_write_samtools=true -Dsamjdk.use_async_io_write_tribble=false -Dsamjdk.compression_level=1 -Xms12g -Xmx12g"
JVM_TMP_DIR="-Djava.io.tmpdir=$WORK_DIR/bcbiotx"
JAVA=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/snakemake/bin/java

GERMLINE_DIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/germline/
 
## Files:
BCBIO_ALIGNMENT_TEMPLATE=$BCBIO_CONFIG/templates/align.yaml
BCBIO_ALIGNMENT_TEMPLATE_TUMOUR_ONLY=$BCBIO_CONFIG/templates/align_tumour_only.yaml
BCBIO_ALIGNMENT_TEMPLATE_CHIP_SEQ=$BCBIO_CONFIG/templates/chipseq.yaml
BCBIO_VARIANT_TEMPLATE=$BCBIO_CONFIG/templates/variant.yaml
BCBIO_SVARIANT_TEMPLATE=$BCBIO_CONFIG/templates/svariant.yaml
BCBIO_ALIGNMENT_TEMPLATE_RNA_SEQ=$BCBIO_CONFIG/templates/rnaseq.yaml
TUMOR_DIR=$ALIGNMENTS/${PATIENT_ID}${TYPE}/${PATIENT_ID}${TYPE} 
NORMAL_DIR=$ALIGNMENTS/${PATIENT_ID}N/${PATIENT_ID}N
ALIGNED_BAM_FILE_TUMOR=$TUMOR_DIR/${PATIENT_ID}${TYPE}-ready.bam
ALIGNED_BAM_FILE_NORMAL=$NORMAL_DIR/${PATIENT_ID}N-ready.bam
ALIGNED_CRAM_FILE_TUMOR=$TUMOR_DIR/${PATIENT_ID}${TYPE}-ready.cram 
ALIGNED_CRAM_FILE_NORMAL=$NORMAL_DIR/${PATIENT_ID}N-ready.cram
####ALIGNED_BAM_FILE=$ALIGNMENTS/${PATIENT_ID}${TYPE}/${PATIENT_ID}${TYPE}/${PATIENT_ID}${TYPE}-ready.bam

## CIDER:
CIDER_JAR=/exports/igmm/eddie/Glioblastoma-WGS/scripts/hmftools-cider-v1.0.2/cider-v1.0.2.jar
OUTPUT_CIDER=/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/immune/cider/${STAGE}

## GRIDSS-PURPLE-LINX:
AMBER_JAR=/exports/igmm/eddie/Glioblastoma-WGS/scripts/hmftools-amber-v3.3/amber-3.3.jar
COBALT_JAR=/exports/igmm/eddie/Glioblastoma-WGS/scripts/hmftools-cobalt-v1.8/cobalt-1.8.jar
### PURPLE_JAR=/exports/igmm/eddie/Glioblastoma-WGS/scripts/hmftools-purple-v2.47/purple-2.47.jar
PURPLE_JAR=/exports/igmm/eddie/Glioblastoma-WGS/scripts/hmftools-purple-v3.5/purple_v3.5.jar

PURPLE_ANNOTATE_STRELKA_AD=/exports/igmm/eddie/Glioblastoma-WGS/scripts/hmftools-purple-v2.47/purity-ploidy-estimator/src/main/java/com/hartwig/hmftools/purple/tools/AnnotateStrelkaWithAllelicDepth.java
PURPLE_SNV_INPUT=/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/ssv/ensemble/${PATIENT_ID}${TYPE}.ssv.snpeff.vcf.gz
##PURPLE_SNV_OUTPUT=/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/purple/${STAGE}/${PATIENT_ID}${TYPE}/${PATIENT_ID}${TYPE}.purple.somatic.vcf.gz
PURPLE_SNV_OUTPUT=/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/purple/v2/${STAGE}/${PATIENT_ID}${NEW_TYPE}/${PATIENT_ID}${TYPE}.purple.somatic.vcf.gz

PURPLE_SV_OUTPUT=/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/purple/v2/${STAGE}/${PATIENT_ID}${NEW_TYPE}/${PATIENT_ID}${TYPE}.purple.sv.vcf.gz
##PURPLE_CNV_GENE_OUTPUT=/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/purple/${STAGE}/${PATIENT_ID}${TYPE}/${PATIENT_ID}${TYPE}.purple.cnv.gene.tsv
PURPLE_CNV_GENE_OUTPUT=/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/purple/v2/${STAGE}/${PATIENT_ID}${NEW_TYPE}/${PATIENT_ID}${TYPE}.purple.cnv.gene.tsv

OUTPUT_AMBER=/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/amber/${STAGE}
OUTPUT_PURPLE=/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/purple/${STAGE}
OUTPUT_COBALT=/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/cobalt/${STAGE}
GC_PROFILE=/exports/igmm/eddie/Glioblastoma-WGS/resources/HMFTools-Resources/Cobalt/GC_profile.hg38.1000bp.cnp
HET=/exports/igmm/eddie/Glioblastoma-WGS/resources/HMFTools-Resources/Amber3/GermlineHetPon.hg38.vcf.gz
CIRCOS=/exports/igmm/eddie/Glioblastoma-WGS/scripts/circos-0.69-9/bin/circos

GERMLINE_HOTSPOTS=/exports/igmm/eddie/Glioblastoma-WGS/resources/hmf_pipeline_resources_38_v5.28/variants/KnownHotspots.germline.38.vcf.gz
SOMATIC_HOTSPOTS=/exports/igmm/eddie/Glioblastoma-WGS/resources/hmf_pipeline_resources_38_v5.28/variants/KnownHotspots.somatic.38.vcf.gz
GERMLINE_FREQ_DEL=/exports/igmm/eddie/Glioblastoma-WGS/resources/hmf_pipeline_resources_38_v5.28/copy_number/cohort_germline_del_freq.38.csv
HMF_ENSEMBLE=/exports/igmm/eddie/Glioblastoma-WGS/resources/hmf_pipeline_resources_38_v5.28/common/ensembl_data
DRIVER_GENE_PANEL=/exports/igmm/eddie/Glioblastoma-WGS/resources/hmf_pipeline_resources_38_v5.28/common/DriverGenePanel.38.tsv
HMF_FUSION=/exports/igmm/eddie/Glioblastoma-WGS/resources/hmf_pipeline_resources_38_v5.28/sv/known_fusion_data.38.csv

### GENE_PANEL=/exports/igmm/eddie/Glioblastoma-WGS/resources/HMFTools-Resources/DriverGenePanel.hg38.tsv
### HOTSPOTS=/exports/igmm/eddie/Glioblastoma-WGS/resources/HMFTools-Resources/KnownHotspots.hg38.vcf.gz

GRIPSS_JAR=/exports/igmm/eddie/Glioblastoma-WGS/scripts/hmftools-gripss-v1.7/gripss-1.7.jar
GRIPSS_FUSION=/exports/igmm/eddie/Glioblastoma-WGS/resources/HMFTools-Resources/GRIPSS/KnownFusionPairs.hg38.bedpe
GRIDSS=/exports/igmm/eddie/Glioblastoma-WGS/scripts/gridss-2.10.0/scripts/gridss.sh
GRIDSS_JAR=/exports/igmm/eddie/Glioblastoma-WGS/scripts/gridss-2.10.0/gridss-2.10.0-gridss-jar-with-dependencies.jar
GRIDSS_OUTPUT=/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/sv/gridss/results
GRIDSS_ASSEMBLY=/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/sv/gridss/results/${PATIENT_ID}${TYPE}.assembly.bam
GRIDSS_WORKING_DIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/sv/gridss/working_dir/${PATIENT_ID}${TYPE}
GRIDSS_RAW_VCF=/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/sv/gridss/results/${STAGE}/${PATIENT_ID}${TYPE}.gridss.raw.vcf
GRIDSS_RM=/exports/igmm/eddie/Glioblastoma-WGS/scripts/gridss-2.10.0/scripts/gridss_annotate_vcf_repeatmasker.sh
GRIDSS_FINAL_FILTERED=/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/sv/gridss/results/${STAGE}/${PATIENT_ID}${TYPE}.gridss.final.filtered.vcf
GRIDSS_FINAL_FILTERED_RM=/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/sv/gridss/results/${STAGE}/${PATIENT_ID}${TYPE}.gridss.final.filtered.rm.vcf
GRIDSS_PON_FILTERED=/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/sv/gridss/results/${STAGE}/${PATIENT_ID}${TYPE}.gridss.pon.filtered.vcf
GRIDSS_PON_FILTERED_RM=/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/sv/gridss/results/${STAGE}/${PATIENT_ID}${TYPE}.gridss.pon.filtered.rm.vcf
GRIDSS_PON=/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/sv/gridss/pondir/${BATCH}
LIBGRIDSS=/exports/igmm/eddie/Glioblastoma-WGS/scripts/gridss
GRIDSS_PLOT_DIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/sv/gridss/plotdir
GRIDSS_SOMATIC_FILTER=/exports/igmm/eddie/Glioblastoma-WGS/scripts/gridss/gridss_somatic_filter.R

### LINX_JAR=/exports/igmm/eddie/Glioblastoma-WGS/scripts/hmftools-sv-linx-v1.11/sv-linx_v1.11.jar
LINX_JAR=/exports/igmm/eddie/Glioblastoma-WGS/scripts/hmftools-linx-v1.20/linx_v1.20.jar

OUTPUT_LINX=/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/sv/linx/${STAGE}
FRAGILE_SITES=/exports/igmm/eddie/Glioblastoma-WGS/resources/HMFTools-Resources/Linx/fragile_sites_hmf.hg38.csv
LINE_ELEMENTS=/exports/igmm/eddie/Glioblastoma-WGS/resources/HMFTools-Resources/Linx/line_elements.hg38.csv
HELI_REP_ORIGIN=/exports/igmm/eddie/Glioblastoma-WGS/resources/HMFTools-Resources/Linx/heli_rep_origins.bed
VIRAL_HOST_REF=/exports/igmm/eddie/Glioblastoma-WGS/resources/HMFTools-Resources/Linx/viral_host_ref.csv
###HMF_FUSION=/exports/igmm/eddie/Glioblastoma-WGS/resources/HMFTools-Resources/Linx/known_fusion_data.csv  

## LILAC:
REF_NO_ALT=$RESOURCES/refgenome38/hg38_no_alt/GCA_000001405.15_GRCh38_no_alt_plus_hs38d1_analysis_set.fna.gz
####LILAC_JAR=$BASE/scripts/hmftools-lilac-v1.2/lilac_v1.2.jar
LILAC_JAR=$BASE/scripts/hmftools-lilac-v1.3_rc1/lilac_v1.3_rc1.jar
LILAC_RESOURCES=$RESOURCES/lilac
LILAC_OUTPUT=$VARIANTS/lilac/output
LILAC_INPUT=$VARIANTS/lilac/input

SLICED_BAM_DIR=$LILAC_INPUT
SLICED_BAM_NORMAL=$LILAC_INPUT/${PATIENT_ID}${NEW_TYPE}.germline.hla_sliced.bam
SLICED_BAM_NORMAL_REALIGNED=$LILAC_INPUT/${PATIENT_ID}${NEW_TYPE}.germline.hla_sliced_realigned_to_no_alt.bam
###SLICED_BAM_NORMAL_REALIGNED_HLA_SUBSET=$LILAC_INPUT/${PATIENT_ID}${NEW_TYPE}.germline.hla_sliced_realigned_to_no_alt_HLA_subset.bam
SLICED_BAM_TUMOR=$LILAC_INPUT/${PATIENT_ID}${NEW_TYPE}.tumour.hla_sliced.bam
SLICED_BAM_TUMOR_REALIGNED=$LILAC_INPUT/${PATIENT_ID}${NEW_TYPE}.tumour.hla_sliced_realigned_to_no_alt.bam
###SLICED_BAM_TUMOR_REALIGNED_HLA_SUBSET=$LILAC_INPUT/${PATIENT_ID}${NEW_TYPE}.tumour.hla_sliced_realigned_to_no_alt_HLA_subset.bam

## LOHHLA:
HLA_FASTA=/exports/igmm/eddie/Glioblastoma-WGS/resources/lohhla/hla_nuc.fasta ## this does not include introns
HLA_DAT=/exports/igmm/eddie/Glioblastoma-WGS/resources/lohhla/hla.dat
NOVOALIGN_BIN=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/lohhla/bin

### might delete:
##SLICED_BAM_NORMAL_SORTED=$LILAC_INPUT/${PATIENT_ID}${TYPE}.germline.hla_sliced_sorted.bam
##SLICED_NORMAL_FQ1=$LILAC_INPUT/${PATIENT_ID}${TYPE}.germline.hla_sliced_sorted_R1.fastq
##SLICED_NORMAL_FQ2=$LILAC_INPUT/${PATIENT_ID}${TYPE}.germline.hla_sliced_sorted_R2.fastq
##HLA_REALIGNED_NO_ALT_NORMAL=$LILAC_INPUT/${PATIENT_ID}${TYPE}.germline.hla_to_no_alt.bam
##SLICED_BAM_TUMOR=$LILAC_INPUT/${PATIENT_ID}${TYPE}.tumour.hla_sliced.bam
##SLICED_BAM_TUMOR_SORTED=$LILAC_INPUT/${PATIENT_ID}${TYPE}.tumour.hla_sliced_sorted.bam
##SLICED_TUMOR_FQ1=$LILAC_INPUT/${PATIENT_ID}${TYPE}.tumour.hla_sliced_sorted_R1.fastq
##SLICED_TUMOR_FQ2=$LILAC_INPUT/${PATIENT_ID}${TYPE}.tumour.hla_sliced_sorted_R2.fastq
##HLA_REALIGNED_NO_ALT_TUMOR=$LILAC_INPUT/${PATIENT_ID}${TYPE}.tumour.hla_to_no_alt.bam

## Jabba:
GCMAP=/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/sv/jabba/gcmap
FRAG_RESULT=/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/sv/jabba/fragCounter

### SVCLONE
SVCLONE_BASE=$VARIANTS/sv/svclone
SVCLONE_INPUT=$SVCLONE_BASE/input
SVCLONE_CONFIG=$SVCLONE_INPUT/configs/${BATCH}_svclone_config.ini
SVCLONE_PLOIDY_PURITY=$SVCLONE_INPUT/purity_ploidy/${SAMPLE_ID}_purity_ploidy.txt
SVCLONE_CNV=$SVCLONE_INPUT/cnvs/${SAMPLE_ID}_cnvs.txt
SVCLONE_OUTPUT=$SVCLONE_BASE/output
SVCLONE_ANN=$SVCLONE_OUTPUT/${SAMPLE_ID}/${SAMPLE_ID}_svin.txt
SVCLONE_COUNT=$SVCLONE_OUTPUT/${SAMPLE_ID}/${SAMPLE_ID}_svinfo.txt

## Misc:
MSISENSOR=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/bin/msisensor
MSI_DIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/MSI
GATK4=/exports/igmm/software/pkg/el7/apps/bcbio/1.1.5/share/anaconda/share/gatk4-4.1.4.1-1/gatk
GATK4_BIN=$GATK4
#GATK4_JAR=/exports/igmm/software/pkg/el7/apps/bcbio/1.2.0/anaconda/share/gatk4-4.1.4.1-1/gatk-package-4.1.4.1-local.jar
GATK4_JAR=/gpfs/igmmfs01/software/pkg/el7/apps/bcbio/1.1.5/share/anaconda/share/gatk4-4.1.4.1-1/gatk-package-4.1.4.1-local.jar
PICARD_JAR=/exports/igmm/software/pkg/el7/apps/bcbio/1.2.0/anaconda/share/picard-2.23.8-0/picard.jar
REPEAT_MASKER_EXE=/exports/igmm/eddie/Glioblastoma-WGS/resources/RepeatMasker/RepeatMasker
ENCODE_BLACKLIST=$RESOURCES/encode4_GRCh38_blacklist.bed.gz
REPEAT_MASKER=$RESOURCES/hg38.fa.out.bed
BLACKLISTED_PEAKS=/exports/igmm/eddie/Glioblastoma-WGS/resources/hg38-blacklist.v2.bed
BLACKLISTED_PEAKS_MM10=/exports/igmm/eddie/Glioblastoma-WGS/resources/mm10-blacklist.v2.bed.gz
BOWTIE_REF=$RESOURCES/refgenome38/hg38
BWA_REF=$RESOURCES/refgenome38/hg38.fa
REFERENCE=$RESOURCES/refgenome38/hg38.fa
REFERENCE_DICT=$RESOURCES/refgenome38/hg38.dict
REFERENCE_HG37=$RESOURCES/refgenome37/Homo_sapiens.GRCh37.dna.primary_assembly.fa
REFERENCE_DICT_HG37=$RESOURCES/refgenome37/Homo_sapiens.GRCh37.dna.primary_assembly.dict
KNOWN_SITES=/exports/igmm/eddie/NextGenResources/bcbio-1.1.5/genomes/Hsapiens/hg38/variation/dbsnp-151.vcf.gz
KNOWN_GATK_SITES=$RESOURCES/Mutect2_resources/small_exac_common_3.hg38.vcf.gz
GATK_AF_GNOMAD=$RESOURCES/Mutect2_resources/af-only-gnomad.hg38.vcf.gz
INTERVAL_LIST=$RESOURCES/Mutect2_resources/wgs_calling_regions.hg38.interval_list
INTERVAL_LIST_CNV_COMMON_SITES=$CNV/interval_list/cnv_somatic_somatic-hg38_af-only-gnomad.hg38.AFgt0.02.interval_list
FUNCOTATOR_DIR=$RESOURCES/Mutect2_resources/funcotator_dataSources.v1.6.20190124s
CHAIN38TO37=/exports/igmm/eddie/Glioblastoma-WGS/resources/liftover/hg38ToHg19.over.chain
CHAIN19TO38=/exports/igmm/eddie/Glioblastoma-WGS/resources/liftover/hg19ToHg38.over.chain
VIRAL_GENOME=/exports/igmm/eddie/Glioblastoma-WGS/resources/viralgenome/human_virus.fa
SNPEFF_JAR=/exports/igmm/eddie/Glioblastoma-WGS/resources/snpEff/snpEff.jar
HG38_CHROM_SIZE=/exports/igmm/eddie/Glioblastoma-WGS/resources/hg38.chrom.sizes
GISTIC2_DIR=/exports/igmm/eddie/Glioblastoma-WGS/scripts/GISTIC2
GISTIC2=$GISTIC2_DIR/gistic2
GISTICREF=$GISTIC2_DIR/refgenefiles/hg38.UCSC.add_miR.160920.refgene.mat
CHAINFINDER_DIR=/exports/igmm/eddie/Glioblastoma-WGS/scripts/ChainFinder_1.0.1
CHAINFINDER=$CHAINFINDER_DIR/run_ChainFinder.sh
MCR_ENV=$SCRIPTS/MCR/v81

## RNA-seq:
RNA_LANES=/exports/igmm/eddie/Glioblastoma-WGS/RNA-seq/raw/lane
KALLISTO_INDEX=$RESOURCES/RNA-seq_resources/kallisto/Homo_sapiens.GRCh38.cdna.all.release-94_k31.idx
CHR_LENGTH=$RESOURCES/RNA-seq_resources/chrom.txt
T2G=$RESOURCES/RNA-seq_resources/transcripts_to_genes.txt
STAR_GENOME_DIR=$RESOURCES/RNA-seq_resources/star/genome
STAR_GENOME=$RESOURCES/RNA-seq_resources/star/genome/GRCh38.p13.genome.fa
STAR_GTF=$RESOURCES/RNA-seq_resources/star/genome/gencode.v36.annotation.gtf
STAR_RESULTS=/exports/igmm/eddie/Glioblastoma-WGS/RNA-seq/results/STAR
STAR_ISOFOX=/exports/igmm/eddie/Glioblastoma-WGS/RNA-seq/results/STAR_ISOFOX
STAR_FUSION=/exports/igmm/eddie/Glioblastoma-WGS/scripts/STAR-Fusion-v1.9.1/STAR-Fusion
STAR_FUSION_DIR=/exports/igmm/eddie/Glioblastoma-WGS/RNA-seq/results/STAR_FUSION
STAR_FUSION_LIB=/exports/igmm/eddie/Glioblastoma-WGS/resources/RNA-seq_resources/star-fusion/GRCh38_gencode_v29_CTAT_lib_Mar272019.plug-n-play/ctat_genome_lib_build_dir
STAR_ALIGNED_BAM=/exports/igmm/eddie/Glioblastoma-WGS/RNA-seq/results/STAR/${PATIENT_ID}Aligned.sortedByCoord.out.bam
TRINITY_DIR=/exports/igmm/eddie/Glioblastoma-WGS/RNA-seq/results/TRINITY
CICERO_DIR=/exports/igmm/eddie/Glioblastoma-WGS/RNA-seq/results/CICERO
CICERO_REF=/exports/igmm/eddie/Glioblastoma-WGS/scripts/CICERO-0.3.0/ref/reference/Homo_sapiens/GRCh38_no_alt
##ISOFOX_JAR=/exports/igmm/eddie/Glioblastoma-WGS/scripts/hmftools-isofox-v1.1/isofox_v1.1.1.jar
ISOFOX_JAR=/exports/igmm/eddie/Glioblastoma-WGS/scripts/hmftools-isofox-v1.3/isofox_v1.3.jar
ISOFOX_EXPECTED_COUNTS=/exports/igmm/eddie/Glioblastoma-WGS/resources/RNA-seq_resources/ISOFOX/read_151_exp_counts.csv
ISOFOX_EXPECTED_GC=/exports/igmm/eddie/Glioblastoma-WGS/resources/RNA-seq_resources/ISOFOX/read_100_exp_gc_ratios.csv
ISOFOX_KNOWN_FUSION=/exports/igmm/eddie/Glioblastoma-WGS/resources/RNA-seq_resources/ISOFOX/known_fusion_data.38.csv

RNA_READS=/exports/igmm/eddie/Glioblastoma-WGS/RNA-seq/raw/reads
RSEM_REF=/exports/igmm/eddie/Glioblastoma-WGS/resources/RNA-seq_resources/rsem/genome
RSEM_QUANT=/exports/igmm/eddie/Glioblastoma-WGS/RNA-seq/results/alt_rsem/Quant
RSEM_BAM=/exports/igmm/eddie/Glioblastoma-WGS/RNA-seq/results/alt_rsem/transcriptome_bam
RSEM_LOG=/exports/igmm/eddie/Glioblastoma-WGS/RNA-seq/results/alt_rsem/log

## ChIP-seq:
PHANTOMPEAKQUAL=/exports/igmm/eddie/Glioblastoma-WGS/scripts/phantompeakqualtools/
XCOR=/exports/igmm/eddie/Glioblastoma-WGS/ChIP-seq/qc/phantompeakqualtools/xcor
SINGLE_END_READS=/exports/igmm/eddie/Glioblastoma-WGS/ChIP-seq/H3K27ac-seq_GBM/reads/Single_end_reads
PAIRED_END_READS=/exports/igmm/eddie/Glioblastoma-WGS/ChIP-seq/H3K27ac-seq_GBM/reads/Paired_end_reads
CHIP_BAMS_DIR=/exports/igmm/eddie/Glioblastoma-WGS/ChIP-seq/${BATCH}/bams
CHIP_QC_DIR=/exports/igmm/eddie/Glioblastoma-WGS/ChIP-seq/${BATCH}/qc
CHIP_DEEPTOOLS=$CHIP_QC_DIR/deeptools
CHIP_IDR=$CHIP_QC_DIR/IDR

## Mitotic bookmarking chip-seq
MM10_REF=/exports/igmm/eddie/Glioblastoma-WGS/resources/refgenome_mmusculus/mm10.fa.gz
MM10_REF_FASTA=/exports/igmm/eddie/Glioblastoma-WGS/resources/refgenome_mmusculus/mm10.fa
GENCODE_MM10=/exports/igmm/eddie/Glioblastoma-WGS/resources/mm10_gencode.vM25.basic.annotation.gtf
MM10_CHROM_SIZE_NO_ALT=/exports/igmm/eddie/Glioblastoma-WGS/resources/refgenome_mmusculus/mm10.chrom_sizes.no_alt



