#!/bin/bash

## This script runs MutEnricher analysis for larger genomic elements ie 2kb tiles.
### Install all dependencies as per https://github.com/asoltis/MutEnricher

## To install:
### create new conda environment python >= 3.6
### git clone https://github.com/asoltis/MutEnricher.git && cd
### pip install numpy
### pip install scipy
### pip install Cython 
### pip install cyvcf2==0.2.0 ## This only works if your htslib is <1.1; if >1.1 need to compile as per https://github.com/brentp/cyvcf2#installation; In my case, i simply used conda
### conda install -c bioconda cyvcf2 
### pip install pysam
### Then Cythonize math functions code as per https://github.com/asoltis/MutEnricher/wiki/Installation-Guide

#$ -N MutEnricher_noncoding
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=300G
#####$ -pe sharedmem 12
#$ -l h_rt=300:00:00

export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/mutenricher/bin:$PATH

ELEMENT=$1
ELEMENT_NAME=$2
VARIANT_TYPE=$3
COV_INTERVAL_FILE=$4
VCF_LIST=$5

MUTENRICHER_SCRIPT_DIR=/exports/igmm/eddie/Glioblastoma-WGS/scripts/MutEnricher
MUTENRICHER=$MUTENRICHER_SCRIPT_DIR/mutEnricher.py
MUTENRICHER_ELEMENT_DIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/recurrent_small_mutations/MutEnricher/elements
BLACKLIST_SSMs=$MUTENRICHER_ELEMENT_DIR/Blacklisted_SSMs.tsv
REF=/exports/igmm/eddie/Glioblastoma-WGS/resources/refgenome38/hg38.fa
MUTENRICHER_RESULT_NON_CODING=/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/recurrent_small_mutations/MutEnricher/outputs/non_coding

## Run mutenricher
python $MUTENRICHER noncoding $ELEMENT $VCF_LIST --use-local --blacklist $BLACKLIST_SSMs -p 32 -c $COV_INTERVAL_FILE -o $MUTENRICHER_RESULT_NON_CODING/$ELEMENT_NAME/$VARIANT_TYPE --ap-convits 25












