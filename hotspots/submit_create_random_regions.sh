#!/bin/bash

## This script runs create_random_region.R script
## It takes a region of interest and create N number of randomised regions using attribuets relevant to the region of interest

#$ -N randomise
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=250G
#$ -l h_rt=24:00:00

## SGE parameters:
REGION_FILE=$1
OUTPUT_DIR=$2
NPERMS=$3

## Run adwgs
export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/R4.0/bin:$PATH
CREATE_RANDOM_REGION=/exports/igmm/eddie/Glioblastoma-WGS/scripts/fame_gbm/R/hotspots/create_random_regions.R

Rscript $CREATE_RANDOM_REGION $REGION_FILE $OUTPUT_DIR $NPERMS

## Test
