#!/bin/bash

# To run this script, do 
# qsub -t n submit_oncodriveclustl.sh
#
#$ -N ONCODRIVECLUSTL
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=16G
#$ -pe sharedmem 16
#$ -l h_rt=150:00:00

OUTDIR=/exports/igmm/eddie/Glioblastoma-WGS/scripts/oncodriveCLUSTL/results

export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/py365/bin:$PATH
export BGDATA_LOCAL=/exports/igmm/eddie/Glioblastoma-WGS/scripts/oncodriveCLUSTL/.bgdata

cd /exports/igmm/eddie/Glioblastoma-WGS/scripts/oncodriveCLUSTL

oncodriveclustl -i Final_filtered_hg38_SSMs.tsv -r 2kbtiles_noYM.tsv -g hg38 --simulation-mode region_restricted -o $OUTDIR -emut 3 --seed 999

## From http://bbglab.irbbarcelona.org/oncodriveclustl/faq














