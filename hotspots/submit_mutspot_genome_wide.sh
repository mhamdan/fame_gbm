#!/bin/bash

## Documentation is at https://github.com/skandlab/MutSpot/blob/master/Documentation.md
#$ -N mutspot_genome_wide
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=250G
#$ -l h_rt=33:00:00

export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/mutspot/bin:$PATH

SNV_MAF=$1
INDEL_MAF=$2

MUTSPOT_GENOME_WIDE=/exports/igmm/eddie/Glioblastoma-WGS/scripts/fame_gbm/R/hotspots/MutSpot_genome_wide.R

Rscript $MUTSPOT_GENOME_WIDE $SNV_MAF $INDEL_MAF



