#!/bin/bash

## Documentation is at https://github.com/skandlab/MutSpot/blob/master/Documentation.md
#$ -N mutspot_region
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=300G
#$ -l h_rt=50:00:00

export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/mutspot/bin:$PATH

SNV_MAF=$1
INDEL_MAF=$2
REGION_BED=$3
REGION_NAME=$4

MUTSPOT_REGION=/exports/igmm/eddie/Glioblastoma-WGS/scripts/fame_gbm/R/hotspots/MutSpot_region.R

Rscript $MUTSPOT_REGION $SNV_MAF $INDEL_MAF $REGION_BED $REGION_NAME



