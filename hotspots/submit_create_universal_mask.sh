## As per https://gist.github.com/lh3/9d6dcfc3436a735ef197
### Not done => instead use GATK WGS calling intervals

# low-complexity by mDUST
mdust hs37d5.fa -c -w7 -v28 \
  | hs37d5.mdust-w7-v28.txt \
  | cut -f1,3,4 \
  | gawk -vOFS="\t" '{--$2;print}' \
  | bgzip > 01compositional/hs37d5.mdust-w7-v28.bed.gz

# long homopolymer runs
seqtk hrun hs37d5.fa \
  | bgzip > 01compositional/hs37d5.hrun.bed.gz

# satellite sequences reported by RepeatMasker. rmsk.txt.gz is downloaded from UCSC.
zcat rmsk.txt.gz \
  | grep Satellite \
  | cut -f6,7,8 \
  | sed s,^chr,, \
  | perl -pe 's/^[^\s_]+_([^\s_]+)_random/$1.1/' \
  | tr "gl" "GL" \
  | sort-alt -k1,1N -k2,2n \
  | bgzip > 01compositional/hs37d5.satellite.bed.gz

# Low-complexity regions identified by RepeatMasker
zcat rmsk.txt.gz \
  | grep Low_complexity \
  | cut -f6,7,8 \
  | sed s,^chr,, \
  | perl -pe 's/^[^\s_]+_([^\s_]+)_random/$1.1/' \
  | tr "gl" "GL" \
  | sort-alt -k1,1N -k2,2n \
  | bgzip > 01compositional/hs37d5.rmsk-lc.bed.gz

# merge the four filters above with 10bp flanking sequences. bedmerge can be replaced by bedtools
zcat hs37d5.hrun.bed.gz hs37d5.rmsk-lc.bed.gz hs37d5.satellite.bed.gz hs37d5.mdust-w7-v28.bed.gz \
  | sort-alt -k1,1N -k2,2n \
  | gawk -v OFS="\t" '{$2=$2<10?0:$2-10;$3+=10;print}' \
  | bioutils.lua bedmerge \
  | bgzip > 01compositional.bed.gz

#
# generate 02structural.bed.gz
#

bcftools view -D hs37d5.fasta.fai -GeS unflt.vcf.gz \
  | ./filter.pl \
  | awk '$6<0&&($7>=100||($7>=50&&((/^[0-9]/&&$4>=22000)||(/^X/&&$4>=19000))))' \
  | k8 clustreg.js \
  | gawk -v OFS="\t" '{$2=$2<150?0:$2-150;$3+=150;print}' \
  | bioutils.lua bedmerge \
  | bgzip > 02structural.bed.gz

#
# Mappability filter is generated with instructions here:
#   http://lh3lh3.users.sourceforge.net/snpable.shtml
#

#
# Merge compositional, structural and mappability filters
#
zcat 01compositional/01compositional.bed.gz 02structural/02structural.bed.gz 03mappable/hs37d5.mask75_50.bed.gz \
  | sort-alt -k1,1N -k2,2n \
  | bioutils.lua bedmerge \
  | k8 merge-close.js \
  | bgzip > um75-hs37d5.bed.gz
