#!/bin/bash

#$ -N ONCODRIVEFML
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=32G
#$ -l h_rt=150:00:00

ELEMENT_FILE=$1
ELEMENT_NAME=$2

DIR=/exports/igmm/eddie/Glioblastoma-WGS/Non-coding/oncodriveFML
ELEMENT=/exports/igmm/eddie/Glioblastoma-WGS/Non-coding/oncodriveFML/elements/$ELEMENT_FILE
OUTDIR=/exports/igmm/eddie/Glioblastoma-WGS/Non-coding/oncodriveFML/results/$ELEMENT_NAME

export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/oncodriveFML/bin:$PATH
export BGDATA_LOCAL=/exports/igmm/eddie/Glioblastoma-WGS/Non-coding/oncodriveCLUSTL/.bgdata

cd $DIR

oncodrivefml -i Final_filtered_hg38_SSMs.tsv -e $ELEMENT -s wgs -o $OUTDIR --signature-correction wg --seed 123 --force





