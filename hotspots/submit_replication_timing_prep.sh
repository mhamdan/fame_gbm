#!/bin/bash

## This script runs the R script to calculate average replication timing profiles in H1 and H9 NPC from ENCODE

#$ -N RT_average
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=280G
#####$ -pe sharedmem 12
#$ -l h_rt=300:00:00

## SGE parameters:
## None

## Run adwgs
export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/R4.0/bin:$PATH
REPLICATION_TIMING_PREP=/exports/igmm/eddie/Glioblastoma-WGS/scripts/fame_gbm/R/hotspots/replication_timing.R

Rscript $REPLICATION_TIMING_PREP





