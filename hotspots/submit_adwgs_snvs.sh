#!/bin/bash

## This script runs ActiveDriverWGS analysis to detect recurrent mutations for SNVs

#$ -N adwgs_snvs
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=250G
#$ -l h_rt=300:00:00

## SGE parameters:
ELEMENT_BED=$1
ELEMENT_NAME=$2
WINDOW_SIZE=$3

## Run adwgs
export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/adwgs/bin:$PATH
ADWGS_SCRIPT=/exports/igmm/eddie/Glioblastoma-WGS/scripts/fame_gbm/R/hotspots/adwgs_snvs.R

Rscript $ADWGS_SCRIPT $ELEMENT_BED $ELEMENT_NAME $WINDOW_SIZE



