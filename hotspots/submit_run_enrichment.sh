#!/bin/bash

## This script runs run_enrichment.R script
## It takes a region of interest and a set of randomised regions and run enrichment against 127 epigenomes of a specific chromHMM state

#$ -N chromHMM_enrichment
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=12G
#$ -pe sharedmem 12
#$ -l h_rt=120:00:00

## SGE parameters:
REGION_FILE=$1
OUTPUT_DIR=$2
NPERMS=$3
STATE_FILE=$4 ## this points to a txt file with one state per row

STATE=`head -n $SGE_TASK_ID $STATE_FILE | tail -n 1`

## Run adwgs
export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/R4.0/bin:$PATH
RUN_ENRICHMENT=/exports/igmm/eddie/Glioblastoma-WGS/scripts/fame_gbm/R/hotspots/run_enrichment.R

Rscript $RUN_ENRICHMENT $REGION_FILE $OUTPUT_DIR $NPERMS $STATE

