#!/bin/bash

#$ -N NUMT
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=16G
#$ -l h_rt=20:00:00
#$ -pe sharedmem 16

## This script takes a BAM file, subset it to mitochondrial reads and filter out NUMT reads.

CONFIG=$1
IDS=$2
STAGE=$3
TYPE=$4

source $CONFIG

PATIENT_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1`
SAMPLE_ID=${PATIENT_ID}${TYPE}

BAM_DIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/alignments/${SAMPLE_ID}/${SAMPLE_ID}
BAM=${BAM_DIR}/${SAMPLE_ID}-ready.bam
MT_BAM_DIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/mitochondrial_DNA/bams/${STAGE}
MT_BAM=$MT_BAM_DIR/${SAMPLE_ID}.mt.bam
RTN=/exports/igmm/eddie/Glioblastoma-WGS/scripts/mtRtN/RtN/Nix_binary/./rtn
HUMANS_FA=/exports/igmm/eddie/Glioblastoma-WGS/scripts/mtRtN/RtN/humans.fa
CALABRESE_NUMTS=/exports/igmm/eddie/Glioblastoma-WGS/scripts/mtRtN/RtN/Calabrese_Dayama_Smart_Numts.fa
FILTERED_MT_BAM=$MT_BAM_DIR/${SAMPLE_ID}.mt.rtn.bam
PILEUP_DIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/mitochondrial_DNA/pileup/${STAGE}
PILEUP=${PILEUP_DIR}/${SAMPLE_ID}.mt.rtn.pileup

## Extract Mitochondrial reads
echo "Extracting mitochondrial reads..."
samtools view -@ 16 -b $BAM chrM > $MT_BAM
samtools index $MT_BAM

## Run RNT to exclude NUMT reads
echo "Excluding NUMT reads..."
$RTN -p -h $HUMANS_FA -n $CALABRESE_NUMTS -b $MT_BAM

## Sort NUMT-filtered mt bams and get pileup 
echo "Generating pileups..."
samtools sort $FILTERED_MT_BAM | samtools mpileup -f $REFERENCE - > $PILEUP 
echo "Done!"




