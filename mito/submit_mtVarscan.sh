#!/bin/bash

#$ -N mtVarscan
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=8G
#$ -pe sharedmem 8
#$ -l h_rt=40:00:00

CONFIG=$1
IDS=$2

source $CONFIG

PATIENT_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 1`
NPURITY=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 2`
NPILEUP=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 3`
TPURITY=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 4`
TPILEUP=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 5`

VARSCAN=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/bin/varscan
OUTPUT_DIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/mitochondrial_DNA/varscan

## Call variants using Varscan
cd $OUTPUT_DIR
$VARSCAN somatic $NPILEUP $TPILEUP $PATIENT_ID \
    --strand-filter 1 --min-avg-qual 20 --min-coverage 3 --min-reads 2 --min-var-freq 0.01 \
    --normal-purity $NPURITY --tumor-purity $TPURITY --output-vcf 1

## Output in VCF so that can annotate using https://mitimpact.css-mendel.it

##$VARSCAN mpileup2snp $PILEUP --min-coverage 3 --min-reads2 2 --min-avg-qual 20 --min-var-freq 0.01 --strand-filter 1 --output-vcf 1 > $SNP_VCF
###$VARSCAN mpileup2indel $PILEUP --min-coverage 3 --min-reads2 2 --min-avg-qual 20 --min-var-freq 0.01 --strand-filter 1 --output-vcf 1 > $INDEL_VCF

## Not using GATK
## https://gatk.broadinstitute.org/hc/en-us/community/posts/360067821871-Mutect2-mitochondria-mode-weird
## https://github.com/broadinstitute/gatk/issues?q=is%3Aissue+mitochondria+is%3Aopen

## JVM_OPTS="-Dsamjdk.use_async_io_read_samtools=false -Dsamjdk.use_async_io_write_samtools=true -Dsamjdk.use_async_io_write_tribble=false -Dsamjdk.compression_level=1 -Xms16g -Xmx16g"
## JVM_TMP_DIR="-Djava.io.tmpdir=$WORK_DIR/bcbiotx"
## GATK4_JAR= new 4.2.0.0
##java $JVM_OPTS $JVM_TMP_DIR -jar $GATK4_JAR Mutect2 \
## java -jar gatk Mutect2 -R genome.fa --mitochondria-mode -I mybam.bam -O myoutvcf.vcf

## Another way to call mt variants (bcftools)
## https://www.biostars.org/p/431777/
##samtools mpileup -uf human_g1k_v37.fasta sample.bam --region MT | bcftools call -m --ploidy 1  > sample_mt_only.vcf

