#!/bin/bash

#$ -N mtpileup
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=8G
#$ -pe sharedmem 8
#$ -l h_rt=40:00:00

unset MODULEPATH
. /etc/profile.d/modules.sh

CONFIG=$1
IDS=$2
TYPE=$3

PATIENT_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1`
SAMPLE_ID=${PATIENT_ID}${TYPE}

source $CONFIG

MT_BAM_DIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/mtDNA/bams
PILEUP_DIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/mtDNA/pileup
FILTERED_MT_BAM=${MT_BAM_DIR}/${SAMPLE_ID}.mt.rtn.bam
PILEUP=${PILEUP_DIR}/${SAMPLE_ID}.mt.rtn.pileup

## Sort NUMT-filtered mt bams and get pileup 
samtools sort $FILTERED_MT_BAM | samtools mpileup -f $REFERENCE - > $PILEUP 




