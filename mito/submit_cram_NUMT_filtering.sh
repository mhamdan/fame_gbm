#!/bin/bash

#$ -N NUMT
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=16G
#$ -l h_rt=20:00:00
#$ -pe sharedmem 16

## This script takes a CRAM file, subset it to mitochondrial reads and filter out NUMT reads.

CONFIG=$1
IDS=$2
BATCH=$3
TYPE=$4

source $CONFIG

PATIENT_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1`
SAMPLE_ID=${PATIENT_ID}${TYPE}

CRAM_DIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/alignments/${BATCH}/${SAMPLE_ID}/${SAMPLE_ID}
CRAM=${CRAM_DIR}/${SAMPLE_ID}-ready.cram
MT_BAM_DIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/alignments/mtDNA
MT_BAM=$MT_BAM_DIR/${SAMPLE_ID}.mt.bam
RTN=/exports/igmm/eddie/Glioblastoma-WGS/scripts/mtRtN/RtN/Nix_binary/./rtn
HUMANS_FA=/exports/igmm/eddie/Glioblastoma-WGS/scripts/mtRtN/RtN/humans.fa
CALABRESE_NUMTS=/exports/igmm/eddie/Glioblastoma-WGS/scripts/mtRtN/RtN/Calabrese_Dayama_Smart_Numts.fa

## Extract Mitochondrial reads
if [[ ! -f $MT_BAM ]];then
samtools view -h -b -@ $NSLOTS $CRAM chrM -o $MT_BAM
samtools index $MT_BAM
fi

## Run RNT to exclude NUMT reads
$RTN -p -h $HUMANS_FA -n $CALABRESE_NUMTS -b $MT_BAM




#AUTO_BAM=${SAMPLE_ID}.realigned.sorted.bam
#CHRM_BED=
##READ1=${SAMPLE_ID}.mt.R1.fastq.gz
#READ2=${SAMPLE_ID}.mt.R2.fastq.gz
#HIGHERQUAL_PY= 
#MITO_SCORES=
#AUTO_SCORES=
## Get fastq files from previously aligned bam files (subsetted to chrM).
#bazam -bam ${MT_BAM} -L $CHRM_BED -pad 0 -r1 ${SAMPLE_ID}.mt.R1.fastq -r2 ${SAMPLE_ID}.mt.R2.fastq 
#gzip *.fastq

## Realign mitochodnrial fastqs to autosomes (now called autosomal bam)
#bwa mem index ${READ1} ${READ2} | samtools view -hb - > ${SAMPLE_ID}.realigned.bam 
#samtools sort ${SAMPLE_ID}.realigned.bam  > ${SAMPLE_ID}.realigned.sorted.bam 
#samtools index ${SAMPLE_ID}.realigned.sorted.bam 

## Get MAPQ scores from each of original mitochondrial bam and autosomal bam, compare their quality scores and filter out.
#python $HIGHERQUAL_PY $MT_BAM $MITO_SCORES
#python $HIGHERQUAL_PY $AUTO_BAM $AUTO_SCORES
#Rscript compareQuality.R $AUTO_SCORES $MITO_SCORES ${SAMPLE_ID}.compareQual.txt
#picard FilterSamReads I=$MT_BAM O=${SAMPLE_ID}.filtered.mt.bam \
#        READ_LIST_FILE=${SAMPLE_ID}.compareQual.txt FILTER=excludeReadList












