#!/bin/bash

# To run this script, do 
# qsub -t 1-n submit_snpEff_mtDNA_variants.sh CONFIG IDS
#
#$ -N snpEff_mt
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=8G
#$ -pe sharedmem 8
#$ -l h_rt=170:00:00

unset MODULEPATH
. /etc/profile.d/modules.sh

CONFIG=$1
IDS=$2

PATIENT_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1`
VCF_DIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/alignments/mtDNA/vcfs
SNV_VCF=${PATIENT_ID}.snp.vcf.gz
INDEL_VCF=${PATIENT_ID}.indel.vcf.gz

source $CONFIG

## Use NC_012920 for mitochondrial genome

java -Xmx4G -jar $SNPEFF_JAR -i vcf -o vcf NC_012920 \
    $VCF_DIR/$SNV_VCF > $VCF_DIR/${PATIENT_ID}.snpeff.snp.vcf

bgzip $VCF_DIR/${PATIENT_ID}.snpeff.snp.vcf
bcftools index $VCF_DIR/${PATIENT_ID}.snpeff.snp.vcf.gz

java -Xmx4G -jar $SNPEFF_JAR -i vcf -o vcf NC_012920 \
    $VCF_DIR/$INDEL_VCF > $VCF_DIR/${PATIENT_ID}.snpeff.indel.vcf
    
bgzip $VCF_DIR/${PATIENT_ID}.snpeff.indel.vcf
bcftools index $VCF_DIR/${PATIENT_ID}.snpeff.indel.vcf.gz







