#!/bin/bash

# To run this script, do 
# qsub -t 1-n submit_cider.sh CONFIG IDS
#
#$ -N cider
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=16G
#$ -pe sharedmem 8
#$ -l h_rt=72:00:00

##Define SGE parameters

CONFIG=$1
IDS=$2
STAGE=$3
TYPE=$4

##Define files/paths
PATIENT_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1`

source $CONFIG
ALIGNED_BAM_FILE_TUMOR=$ALIGNMENTS/${PATIENT_ID}${TYPE}/${PATIENT_ID}${TYPE}/${PATIENT_ID}${TYPE}-ready.bam

## Define blastdb:
BLAST=/exports/igmm/eddie/Glioblastoma-WGS/scripts/ncbi-blast-2.15.0+
BLASTDB=/exports/igmm/eddie/Glioblastoma-WGS/scripts/ncbi-blast-2.15.0+/blastdb

## Running Cider version 1.0.2
java -Xmx16G -cp $CIDER_JAR com.hartwig.hmftools.cider.CiderApplication \
   -sample ${PATIENT_ID}${TYPE} \
   -bam $ALIGNED_BAM_FILE_TUMOR \
   -output_dir ${OUTPUT_CIDER}/${PATIENT_ID}${TYPE} \
   -ref_genome_version V38 \
   -write_cider_bam \
   -blast $BLAST \
   -blast_db $BLASTDB \
   -threads 16
















