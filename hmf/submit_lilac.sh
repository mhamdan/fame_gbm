#!/bin/bash

## As per https://github.com/hartwigmedical/hmftools/tree/master/lilac
## Resources downloaded from https://nextcloud.hartwigmedicalfoundation.nl/s/LTiKTd8XxBqwaiC?path=%2FHMFTools-Resources%2FLilac
## Using those from hg38 reference.
## IDS points to a .txt whose columns are patient id, stage (primary or recurrent) and type suffix (T/T1/T2).

#$ -N LILAC
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=12G
#$ -pe sharedmem 12
#$ -l h_rt=10:00:00


unset MODULEPATH
. /etc/profile.d/modules.sh

CONFIG=$1
IDS=$2

## Define files/directories
PATIENT_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 1`
TYPE=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 2`
NEW_TYPE=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 3`
STAGE=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 4`
LILAC_SNV=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 5`
LILAC_CNV=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 6`

SAMPLE_ID=${PATIENT_ID}${NEW_TYPE}

source $CONFIG

echo "Running for $SAMPLE_ID using sliced realigned germline/tumour bams..."

## Subset to HLA locus (ie chr6)
##java -jar $LILAC_JAR \
##   -sample $SAMPLE_ID \
##   -reference_bam $SLICED_BAM_NORMAL_REALIGNED_HLA_SUBSET \
##   -tumor_bam $SLICED_BAM_TUMOR_REALIGNED_HLA_SUBSET \
##   -ref_genome $REF_NO_ALT \
##   -ref_genome_version 38 \
##   -resource_dir $LILAC_RESOURCES \
##   -somatic_vcf $PURPLE_SNV_OUTPUT \
##   -gene_copy_number $PURPLE_CNV_GENE_OUTPUT \
##   -output_dir $LILAC_OUTPUT \
##   -write_all_files \
##   -log_debug

## Not subset but just take the extracted bams that have been realigned to no alt genome:
## Change sample format for TCGA recurrent (to NEW_TYPE)
java -jar $LILAC_JAR \
   -sample $SAMPLE_ID \
   -reference_bam $SLICED_BAM_NORMAL_REALIGNED \
   -tumor_bam $SLICED_BAM_TUMOR_REALIGNED \
   -ref_genome $REF_NO_ALT \
   -ref_genome_version 38 \
   -resource_dir $LILAC_RESOURCES \
   -somatic_vcf $LILAC_SNV \
   -gene_copy_number $LILAC_CNV \
   -output_dir $LILAC_OUTPUT \
   -write_all_files \
   -log_debug












