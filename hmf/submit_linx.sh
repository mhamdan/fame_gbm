#!/bin/bash
# To run this script, do 
# qsub -t 1-n submit_linx.sh CONFIG IDS
#
#$ -N LINX
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=8G
#$ -pe sharedmem 8
#$ -l h_rt=72:00:00


##Define SGE parameters

CONFIG=$1
IDS=$2

##Define files/paths
PATIENT_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1`

source $CONFIG

##mkdir -p $OUTPUT_LINX/${PATIENT_ID}T

##echo "Running LINX for ${PATIENT_ID}..."
##java $JVM_OPTS $JVM_TMP_DIR -jar $LINX_JAR \
    -sample ${PATIENT_ID}T \
    -sv_vcf $PURPLE_SV_OUTPUT \
    -purple_dir $OUTPUT_PURPLE/${PATIENT_ID} \
    -output_dir $OUTPUT_LINX/${PATIENT_ID}T \
    -ref_genome_version HG38 \
    -fragile_site_file $FRAGILE_SITES \
    -line_element_file $LINE_ELEMENTS \
    -replication_origins_file $HELI_REP_ORIGIN \
    -viral_hosts_file $VIRAL_HOST_REF \
    -gene_transcripts_dir $HMF_ENSEMBLE \
    -check_fusions \
    -known_fusion_file $HMF_FUSION \
    -check_drivers \
    -log_debug \
    -write_vis_data


## Run visualisation within a conda environment.
## Need to make sure all perl modules are installed- try manually first and check which modules not installed.
## Make sure to install perl modules first, and then the R modules otherwise install.packages("magick") would not work as could not find library.
## Will need several R dependencies installed as per https://github.com/hartwigmedical/hmftools/blob/master/sv-linx/README_VIS.md#dependencies

##mkdir -p $OUTPUT_LINX/${PATIENT_ID}T/plot
##mkdir -p $OUTPUT_LINX/${PATIENT_ID}T/data

export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/linx/bin:$PATH

echo "Visualising LINX for ${PATIENT_ID}..."
java $JVM_OPTS $JVM_TMP_DIR -cp $LINX_JAR com.hartwig.hmftools.linx.visualiser.SvVisualiser \
    -sample ${PATIENT_ID}T \
    -gene_transcripts_dir $HMF_ENSEMBLE \
    -plot_out $OUTPUT_LINX/${PATIENT_ID}T/plot \
    -data_out $OUTPUT_LINX/${PATIENT_ID}T/data \
    -segment $OUTPUT_LINX/${PATIENT_ID}T/${PATIENT_ID}T.linx.vis_segments.tsv \
    -link $OUTPUT_LINX/${PATIENT_ID}T/${PATIENT_ID}T.linx.vis_sv_data.tsv \
    -exon $OUTPUT_LINX/${PATIENT_ID}T/${PATIENT_ID}T.linx.vis_gene_exon.tsv \
    -cna $OUTPUT_LINX/${PATIENT_ID}T/${PATIENT_ID}T.linx.vis_copy_number.tsv \
    -protein_domain $OUTPUT_LINX/${PATIENT_ID}T/${PATIENT_ID}T.linx.vis_protein_domain.tsv \
    -fusion $OUTPUT_LINX/${PATIENT_ID}T/${PATIENT_ID}T.linx.vis_fusion.tsv \
    -circos $CIRCOS \
    -threads 16









