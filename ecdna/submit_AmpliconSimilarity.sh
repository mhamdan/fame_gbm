#!/bin/bash

# To run this script, do 
# qsub submit_AmpliconSimilarity.sh CYCLE_GRAPH_FILE OUTPUT_FILE
### This is an updated version to 0.4.10 (July 2022)

#$ -N AC2AS
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=12G
#$ -pe sharedmem 12
#$ -l h_rt=10:00:00

unset MODULEPATH
. /etc/profile.d/modules.sh

CYCLE_GRAPH_FILE=$1
OUTPUT_FILE_PREFIX=$2

### Based on https://github.com/jluebeck/AmpliconClassifier
### Used in biorxiv.org/content/early/2022/07/25/2022.07.25.501144?ct

export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/AA/bin:$PATH
export AA_DATA_REPO=/exports/igmm/eddie/Glioblastoma-WGS/scripts/AmpliconArchitect/data_repo
AS=/exports/igmm/eddie/Glioblastoma-WGS/scripts/AmpliconClassifier/amplicon_similarity.py
AA_SIMILARITY_DIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/ecdna/AA_similarity

### Input path file is formatted as sample path_to_amplicon1_cycle.txt path_to_amplicon1_graph.txt
cd $AA_SIMILARITY_DIR
echo "Calculating amplicon similarity among list of amplicons, run using default options..."
python $AS \
    --ref GRCh38 \
    --input $CYCLE_GRAPH_FILE \
    --include_path_in_amplicon_name \
    -o $OUTPUT_FILE_PREFIX

    
    
    
    
    
    

