#!/bin/bash

# To run this script, do 
# qsub -t 1-N submit_CycleViz.sh IDS
#
#$ -N CycleViz
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=8G
#$ -pe sharedmem 8
#$ -l h_rt=10:00:00

unset MODULEPATH
. /etc/profile.d/modules.sh

## IDS contains paths to amplicon breakpoint_graph_converted (BPG) cycle txt file (column 1) and amplicon graph txt file (column 2)
IDS=$1

BPG=`head -n $SGE_TASK_ID $IDS | cut -f 1| tail -n 1`
GRAPH=`head -n $SGE_TASK_ID $IDS | cut -f 2| tail -n 1`

export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/AA/bin:$PATH
export AA_DATA_REPO=/exports/igmm/eddie/Glioblastoma-WGS/scripts/AmpliconArchitect/data_repo

CYCLEVIZ=/exports/igmm/eddie/Glioblastoma-WGS/scripts/AmpliconArchitect/src/CycleViz/CycleViz.py
CYCLEVIZ_DIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/ecdna/CycleViz_output

cd $CYCLEVIZ_DIR

python $CYCLEVIZ \
    --cycles_file $BPG \
    --cycle 1 \
    -g $GRAPH \
    --ref hg38 --gene_fontsize 10 --label_segs --tick_fontsize 8 

python $CYCLEVIZ \
    --cycles_file $BPG \
    --cycle 2 \
    -g $GRAPH \
    --ref hg38 --gene_fontsize 10 --label_segs --tick_fontsize 8 

python $CYCLEVIZ \
    --cycles_file $BPG \
    --cycle 3 \
    -g $GRAPH \
    --ref hg38 --gene_fontsize 10 --label_segs --tick_fontsize 8 

python $CYCLEVIZ \
    --cycles_file $BPG \
    --cycle 4 \
    -g $GRAPH \
    --ref hg38 --gene_fontsize 10 --label_segs --tick_fontsize 8 

python $CYCLEVIZ \
    --cycles_file $BPG \
    --cycle 5 \
    -g $GRAPH \
    --ref hg38 --gene_fontsize 10 --label_segs --tick_fontsize 8 







