#!/bin/bash

# To run this script, do 
# qsub -t 1-N submit_ConvertCycle.sh IDS
#
#$ -N ConvertCycle
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=8G
#$ -pe sharedmem 8
#$ -l h_rt=10:00:00

unset MODULEPATH
. /etc/profile.d/modules.sh

## IDS contains paths to amplicon cycle txt file (column 1) and amplicon graph txt file (column 2)
IDS=$1

CYCLE=`head -n $SGE_TASK_ID $IDS | cut -f 1| tail -n 1`
GRAPH=`head -n $SGE_TASK_ID $IDS | cut -f 2| tail -n 1`

export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/AA/bin:$PATH
export AA_DATA_REPO=/exports/igmm/eddie/Glioblastoma-WGS/scripts/AmpliconArchitect/data_repo

CONVERT_CYCLE=/exports/igmm/eddie/Glioblastoma-WGS/scripts/AmpliconArchitect/src/CycleViz/convert_cycles_file.py
CYCLEVIZ_DIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/ecdna/CycleViz_output

cd $CYCLEVIZ_DIR

python $CONVERT_CYCLE \
    -c $CYCLE \
    -g $GRAPH 
    
    
    
    
    
    
    

