#!/bin/bash

#$ -N gistic
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=250G
#$ -l h_rt=12:00:00

CONFIG=$1
SEG_FILE=$2
OUTPUT_DIR=$3

### Both SEG_FILE and OUTPUT_DIR need to be full paths (not relative paths).

### Seg file consists of GATK seg individual sample files with no header and no "chr", concatenated to include all samples.
### Parameters adjusted based on https://gatkforums.broadinstitute.org/firecloud/discussion/8254/gistic2-0
### and https://github.com/TheJacksonLaboratory/GLASS/blob/1c554908f00172e9437cac6e75aaf81429852d17/bin/gistic_run.pbs

## 18/3/2022: Use PURPLE's somatic CNV segment file, processed using R/purple_to_gistic.R script
## Do not remove chrX before analysis

## 20/7/2022: Updating PURPLE version to 3.4 hence re-running GISTIC with new CNV segments.

source $CONFIG

unset DISPLAY

cd $GISTIC2_DIR

$GISTIC2 \
    -b $OUTPUT_DIR \
    -seg $SEG_FILE \
    -refgene $GISTICREF \
    -genegistic 0 -smallmem 1 -broad 1 -armpeel 1 \
    -maxseg 5000 \
    -brlen 0.75 \
    -cap 1.5 \
    -js 4 \
    -conf 0.99 \
    -qvt 0.25 \
    -savegene 1 \
    -gcm mean \
    -rx 1 \
    -ta 0.5 \
    -td 0.5 \
    -scent median

    

    
    
    
