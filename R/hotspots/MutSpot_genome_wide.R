#!/usr/bin/env Rscript

## Load libraries:
.p <- c("future", "MutSpot", "GenomicRanges", "gUtils", "tidyverse")
suppressPackageStartupMessages(lapply(.p, require, character.only=T))
Sys.setenv(DEFAULT_GENOME = "BSgenome.Hsapiens.UCSC.hg38::Hsapiens")

## "gUtils" and "tidyverse" are packages specific to my edits

## Use parallelisation and increase max memory usage per run:
plan("multicore")
options(future.globals.maxSize = 20000 * 1024^2)

## Load args:
args <- commandArgs(trailingOnly = TRUE)

snv_maf <- as.character(args[1]) 
indel_maf <- as.character(args[2]) 
genomic_features <- as.character(args[3]) 

## Assign default paths:
output_dir <- "/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/MutSpot/output"
work_dir <- "/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/MutSpot/workdir"
genomic_features <- "/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/MutSpot/features/genomic_features.txt" ## standard for all analyses irrespective of SNVs/indels

## Edit functions to correct for path errors and other bugs:
source("/exports/igmm/eddie/Glioblastoma-WGS/scripts/fame_gbm/R/MutSpot_helper.R")

### Identify hotspots genome-wide (without restricting to any genomic element)
#### ?100kb tiles
MutSpot_edited(snv.mutations = snv_maf, 
               indel.mutations = indel_maf, 
               cutoff.nucleotide.new = 0.98, 
               working.dir = work_dir,
               cores= 32, 
               top.nucleotide = 3,  
               min.count.indel = 4,
               min.count.snv = 4,
               genomic.features.fixed.snv = genomic_features,
               genomic.features.fixed.indel = genomic_features,
               genomic.fetaures.fixed = genomic_features,
               debug = TRUE,
               genome.build = "Ch38")

## Save all data: this is added from me
save(list = ls(all.names = TRUE), file = paste0(output_dir, "/genomw_wide_mutspot_results.RData"))


##### END OF SCRIPT #####




