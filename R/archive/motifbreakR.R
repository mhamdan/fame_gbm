.p <- c("motifbreakR", "BiocParallel", "BSgenome.Hsapiens.UCSC.hg38")
lapply(.p, require, character.only=T)
register(MulticoreParam(32))

print("Loading formatted mutation file....")
primary_muts <- readRDS("/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/motifbreakR/MotifbreakR_small_mutations_primary_filtered_no_hypermutated_gr.rds")

print("Running motifbreakR...")
results <- motifbreakR(snpList = primary_muts, filterp = TRUE, pwmList = motifbreakR_motif,
                       threshold = 0.1, method = "ic", BPPARAM = BiocParallel::bpparam(),
                       bkg = c(A=0.25, C=0.25, G=0.25, T=0.25))

saveRDS(primary_muts, "/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/motifbreakR/MotifbreakR_small_mutations_primary_filtered_no_hypermutated_results.rds")








