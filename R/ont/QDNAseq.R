#!/usr/bin/env Rscript

## This script runs copy number calling from tumour-normal bams of WGS data.
## Can be used with nanopore WGS data.
## Uses added loess residuals from 1000G project to improve normalisation to germline/control copy numbers.

## Installation for PSCBS is from https://github.com/HenrikBengtsson/PSCBS
## Need aroma.ligth and DNAcopy from bioconductor

## Load libraries:
.p <- c("future", "QDNAseq", "QDNAseq.hg38", "PSCBS")
suppressPackageStartupMessages(lapply(.p, require, character.only=T))

## Use parallelisation and increase max memory usage per run:
plan("multicore")
options(future.globals.maxSize = 20000 * 1024^2)

## Load args:
args <- commandArgs(trailingOnly = TRUE)
tumour_bam <- as.character(args[1]) ## path to tumour bam
normal_bam <- as.character(args[2]) ## path to normal bam
sample_id <-  as.character(args[3])
output_dir <-  as.character(args[4])

## Functions:
## Hg38 bins have been pre-generated as per https://github.com/ccagc/QDNAseq/issues/59
## Use the package QDNAseq.hg38 to retrieve bins of N size for hg38.
## This will have mappability, blacklist and residual annotations from 1000 genomes project 

createCNprofile <- function(tumour_bam, normal_bam, sample_id, output_dir, bin.size = 1, alpha = 0.001) {
  
  message(paste0("Generating ", bin.size, "kb bins..."))
  bins <- getBinAnnotations(genome = "hg38", as.numeric(bin.size)) ## this binsize is in N kb so choose 1 if want 1000bp bins
  
  ## Bin read counts:
  message("Counting reads in tumour and normal bams within each bin...")
  r <- binReadCounts(bins, bamfiles = c(tumour_bam, normal_bam), minMapq=20, isSecondaryAlignment=FALSE)
  
  ## Filter out blacklisted and low mappability sites:
  message("Annotating blacklisted and low mappability sites...")
  r <- applyFilters(r, residual = T, blacklist = T, mappability = 10, bases = 100)
  
  ## Estimate and correct for gc count and mappability metrics:
  message("Applying GC and mappability corrections...")
  r <- estimateCorrection(r, variables=c("gc", "mappability"))
  r <- correctBins(r, method = "median")
  
  ## Normalise corrected counts
  message("Normalising counts in bins...")
  r <- normalizeBins(r, method = "mean")
  
  ## Normalise to internal control bam (ideally matched germline)
  message("Normalising to germline/control counts...")
  r <- compareToReference(r, c(2, FALSE), force=T) ## To normalise to the second counts in the object ie normal sample
  r <- normalizeBins(r, method = "mean", force=T)
  
  ## Save in dataframe and only take ones to be used ie not filtered out.
  # x <- data.frame(r@featureData@data$chromosome, r@featureData@data$start, r@assayData$copynumber, r@featureData@data$use)
  # xx <- subset(x, r.featureData.data.use==T)
  # xx$r.featureData.data.use <- NULL
  # colnames(xx) <- c("chromosome", "start", "copynumber")
  # xx$chromosome <- as.numeric(as.character(xx$chromosome))
  # 
  ## transform counts (Anscombe transformation as in QDNAseq) ## NOT DONE:
  ## xx$y <- sqrt(xx$y * 3/8)
  
  # ## Screen for large areas of genome without coverage ie larger centromeres:
  # gaps <- findLargeGaps(chromosome = xx$chromosome, x = xx$start, minLength = 5e+06)
  # 
  # ## Get the segments outside of the gaps
  # knownSegments <- gapsToSegments(gaps)
  # 
  # ## Use Circular Binary Segmentation to segment CN regions (this takes longer than the rest)
  # fit <- segmentByCBS(y= xx$copynumber, knownSegments = knownSegments, alpha = as.numeric(alpha))
  # 
  ## reverse transform ## NOT DONE
  ## fit$output$mean <- (fit$output$mean^2) * 8/3
  
  message("Segmenting copy number data...")
  r <- segmentBins(r, transformFun="log2", alpha = as.numeric(alpha), min.width=2, undo.splits="none")
  r <- callBins(r, method="cutoff")
  
  message("Saving final data...")
  saveRDS(r, paste0(output_dir, "/", sample_id, "_cn_segments.rds"))
  
  # saveRDS(fit, paste0(sample_id, "_cbs_fit.rds"))
  
  # save(r, file = rplot.file)
  
  # exportBins(r, file = snakemake@output[["bed1"]], type="segments", format="bed")
  
  
  #pdf(pdf.file)
  
  #plot(r)
  
  #plotTracks(fit, Clim = c(0,100))
  
  #plotTracks(fit, Clim = c(0,4))
  
  #dev.off()
  # 
  # source("/lnec/SANSON/nanopore/pipeline/scripts/CNplot.R")
  # 
  # CNplot(r)
  # 
  # ggsave(pdf.file, width=225, height=45, units = "mm")
  # 
  # 
  # ### simplify segments
  # 
  # segments=data.frame(chr = as.character(r@featureData@data$chromosome), start = r@featureData@data$start, end = r@featureData@data$end, seg.mean=r@assayData$segmented[,1], call=r@assayData$calls[,1])
  # 
  # 
  # library(dplyr)
  # 
  # 
  # shortSeg <- segments %>% arrange(chr,start) %>% 
  #   
  #   mutate(identical=as.integer(seg.mean != lag(seg.mean))) %>% 
  #   
  #   mutate(identical=ifelse(is.na(identical),1,identical)) %>% 
  #   
  #   mutate(segmentNo = cumsum(identical)) %>% 
  #   
  #   group_by(segmentNo) %>%
  #   
  #   summarise(chr=first(chr), start=min(start), end=max(end), seg.mean=log2(max(seg.mean)),call=first(call)) %>%
  #   
  #   select(-segmentNo)
  # 
  # 
  # # write .seg file for IGV
  # 
  # write.table(data.frame(sample=sub(".*(\\d{4}T).*","\\1",bam), shortSeg), file=bed, quote=F,row.names = F,sep="\t")
  
}

createCNprofile(tumour_bam, normal_bam, sample_id, output_dir)













