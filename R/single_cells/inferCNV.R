#!/usr/bin/env Rscript
## Create new conda environment with R4 and python 3.7
## conda install rjags
## Check options here https://rdrr.io/github/broadinstitute/inferCNV/man/run.html

## Load libraries:
message("Loading libraries...")
Sys.setenv(RETICULATE_PYTHON = "/gpfs/igmmfs01/eddie/Glioblastoma-WGS/anaconda/envs/infercnv/bin/python3")
.p <- c("infercnv","tictoc","leiden","reticulate","future","Seurat","Signac"); suppressPackageStartupMessages(lapply(.p, require, character.only=T))
options(future.globals.maxSize = 1000 * 1024^2)

## Organise paths:
args = commandArgs(trailingOnly=TRUE)
sample <- args[1]
seurat_dir <- "/exports/igmm/eddie/Glioblastoma-WGS/scmultiome/seurat/"
inferCNV_input_dir <- "/exports/igmm/eddie/Glioblastoma-WGS/scmultiome/infercnv/input/"
gene_order_file <- paste0(inferCNV_input_dir, "InferCNV_gene_ordering_file.txt")
inferCNV_output_dir <- paste0("/exports/igmm/eddie/Glioblastoma-WGS/scmultiome/infercnv/output/", sample)

## Load paired data:
message(paste0("Loading paired GSC and NSC data for ", sample))
seurat_obj <- readRDS(paste0(seurat_dir, sample, "_NSC.rds")); Idents(seurat_obj) <- "orig.ident"
exp_raw <- seurat_obj@assays$SCT@counts
#exp_raw <- exp_raw[,grep("NS12|NS17", colnames(exp_raw), value = T, invert = T)]
rm(seurat_obj)
gc()

## Prepare annotation file
message("Preparing annotation file...")
write.table(data.frame(V1=colnames(exp_raw), V2=stringr::str_split(colnames(exp_raw),"_", simplify = T)[,1]),
            paste0(inferCNV_input_dir, sample, "_annotations.txt"), sep = "\t", quote = F, col.names = T, row.names = F)
annotations <- paste0(inferCNV_input_dir, sample, "_annotations.txt")

## Create inferCNV object
message("Creating infercnv object...")
infercnv_obj = CreateInfercnvObject(raw_counts_matrix= exp_raw,
                                    annotations_file= annotations,
                                    delim="\t",
                                    gene_order_file= gene_order_file, ## This is created using gtf_to_position_file.py
                                    # ref_group_names= "NS9FB-B"
                                    ref_group_names= c("NS12ST-A", "NS17ST-A", "NS9FB-B"))


## Run inferCNV
message("Running inferCNV...")
tic()
infercnv_obj = infercnv::run(infercnv_obj,
                             cutoff = 0.1,
                             out_dir = inferCNV_output_dir,
                             window_length=101,
                             cluster_by_groups=FALSE, ## cluster by cells
                             HMM=T,
                             plot_steps=F,
                             scale_data=F, ## Off as using SCT scaling
                             denoise=T,
                             max_centered_threshold='auto',
                             tumor_subcluster_partition_method='leiden',
                             num_threads=6,
                             analysis_mode='subclusters',
                             HMM_type='i6')
toc()

message("Done!")

## Using external data as control:
## Merging GSC and external normal control:
# message(paste0("Loading control data..."))
# cerebellum <- readRDS(paste0(seurat_dir, "human_brain_3k_seurat_integrated.rds"))
# cerebellum_counts <- cerebellum@assays$RNA@counts
# colnames(cerebellum_counts) <- paste0("Normal_", colnames(cerebellum_counts))
# 
# message(paste0("Loading GSC data..."))
# class(sample)
# GSC <- readRDS(paste0(seurat_dir, "filtered/", sample, "_seurat_obj_filtered.rds"))
# GSC_counts <- GSC@assays$RNA@counts
# colnames(GSC_counts) <- paste0(sample, "_", colnames(GSC_counts))
# 
# message("Merging data...")
# exp_raw <- cbind(cerebellum_counts, GSC_counts)
# 
# ## Prepare annotation file
# message("Preparing annotation file...")
# write.table(data.frame(V1=colnames(exp_raw), V2=stringr::str_split(colnames(exp_raw),"_", simplify = T)[,1]), 
#             paste0(inferCNV_input_dir, sample, "_annotations.txt"), sep = "\t", quote = F, col.names = T, row.names = F)
# annotations <- paste0(inferCNV_input_dir, sample, "_annotations.txt")
# 
# ## Create inferCNV object
# message("Creating infercnv object...")
# infercnv_obj = CreateInfercnvObject(raw_counts_matrix= exp_raw,
#                                     annotations_file= annotations,
#                                     delim="\t",
#                                     gene_order_file= gene_order_file, ## This is created using gtf_to_position_file.py
#                                     ref_group_names= "Normal")
# 
# ## Run inferCNV
# message("Running inferCNV...")
# tic()
# infercnv_obj = infercnv::run(infercnv_obj, 
#                              cutoff = 0.1, 
#                              out_dir = paste0(inferCNV_output_dir,"_cerebellum"), 
#                              window_length=101,
#                              cluster_by_groups=FALSE, ## cluster by cells
#                              HMM=F,
#                              plot_steps=F, 
#                              scale_data=T, ## On as no normal cells within GSCs
#                              denoise=T, 
#                              max_centered_threshold='auto',
#                              tumor_subcluster_partition_method='leiden',
#                              num_threads=16,
#                              analysis_mode='subclusters')
# toc()
# 
# message("Done!")


