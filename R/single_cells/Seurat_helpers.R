## Define functions:
print("Loading custom functions...")
Regress <- function(.obj_filtered) {
  
  tic()
  obj_filtered <- .obj_filtered
  
  print("Calculating cell cycle scores...")
  ## https://github.com/satijalab/seurat/issues/4552
  ## https://github.com/satijalab/seurat/discussions/4259
  DefaultAssay(obj_filtered) <- "RNA"
  obj_filtered <- SCTransform(obj_filtered, method = "glmGamPoi", do.scale = FALSE, do.center = FALSE, verbose=F) 
  obj_filtered <- CellCycleScoring(obj_filtered, s.features = .s.genes, g2m.features = .g2m.genes, set.ident = TRUE, verbose=F)
  obj_filtered <- RunPCA(obj_filtered, features = c(.s.genes, .g2m.genes), verbose=F)
#  p1 <- DimPlot(obj_filtered, pt.size = 0.001, cols = viridis_pal(alpha =0.8, option="D")(length(unique(obj_filtered$Phase))))
  obj_filtered$CC.Difference <- obj_filtered$S.Score - obj_filtered$G2M.Score
  
  print("SCTranform, regressing to library size, percentage mt, and CC.difference...")
  DefaultAssay(obj_filtered) <- "RNA"
  obj_filtered <- SCTransform(obj_filtered, method = "glmGamPoi", do.scale = TRUE, do.center = TRUE, verbose=F, vars.to.regress = c("nCount_RNA","percent.mt","CC.Difference"), return.only.var.genes=F)
  
  print("Comparing before and after regression...")
  obj_filtered <- RunPCA(obj_filtered, features = c(.s.genes, .g2m.genes), verbose=F)
#  p2 <-DimPlot(obj_filtered, pt.size = 0.001, cols = viridis_pal(alpha =0.8, option="D")(length(unique(obj_filtered$Phase))))
  
#  print("Plotting...")
#  sample <- unique(obj_filtered$orig.ident)
#  pdf(paste0(sample, "_CCdif_regression.pdf"), height = 3, width=8)
#  p<-p1+p2
#  print(p)
#  dev.off()
  
  toc()
  obj_filtered
  
} ## Can be used with merged objects, just name the object in "sample"
Integrate <- function(.obj_filtered) {
  
  tic()
  obj_filtered <- .obj_filtered
  
 # print("Transferring anchors and integrating RNA data...")
  DefaultAssay(obj_filtered) <- "SCT"
 # sample.list <- SplitObject(obj_filtered, split.by = "orig.ident")
  #features <- SelectIntegrationFeatures(object.list = sample.list)
 # anchors <- FindIntegrationAnchors(object.list = sample.list, anchor.features = features)
 # obj_filtered <- IntegrateData(anchorset = anchors)
  
 # print("Running PCA, UMAP and tSNE for integrated RNA data...")
#  DefaultAssay(obj_filtered) <- "integrated"
 # obj_filtered <- ScaleData(obj_filtered, verbose = FALSE)
  obj_filtered <- RunPCA(obj_filtered, features = VariableFeatures(obj_filtered), verbose =FALSE)
  obj_filtered <- RunUMAP(obj_filtered, dims = 1:50, reduction.name = "rna.umap", reduction.key = "rnaUMAP_", verbose = FALSE)
  obj_filtered <- RunTSNE(obj_filtered, dims = 1:50, reduction.name = "rna.tsne", reduction.key = "rnaTSNE_", verbose = FALSE)
  
  ## ATAC
  print("Running LSI for ATAC...")
  DefaultAssay(obj_filtered) <- "ATAC"
  obj_filtered <- FindTopFeatures(obj_filtered, min.cutoff = 'q0')
  obj_filtered <- RunTFIDF(obj_filtered)
  obj_filtered <- RunSVD(obj_filtered)
  
  print("Running UMAP and tSNE for ATAC...")
  obj_filtered <- RunUMAP(obj_filtered, reduction = 'lsi', dims = 2:50, reduction.name = "atac.umap", reduction.key = "atacUMAP_", verbose=F)
  obj_filtered <- RunTSNE(obj_filtered, reduction = 'lsi', dims = 2:50, reduction.name = "atac.tsne", reduction.key = "atacTSNE_", verbose=F)
  
  ## Joint neighbor graph using both assays
  print("Finding multimodal neighbours...")
  obj_filtered <- FindMultiModalNeighbors(obj_filtered, reduction.list = list("pca", "lsi"), dims.list = list(1:50, 2:40), verbose = F)
  
  ## Joint UMAP visualization
  print("Performing joint UMAP and tSNE...")
  obj_filtered <- RunUMAP(object = obj_filtered, nn.name = "weighted.nn", reduction.name = "wnn.umap", reduction.key = "wnnUMAP_", verbose = F)
  obj_filtered <- RunTSNE(object = obj_filtered, nn.name = "weighted.nn", reduction.name = "wnn.tsne", reduction.key = "wnnTSNE_", verbose = F)
  
  toc()
  obj_filtered
  
}
Cluster <- function(.obj_filtered){
  
  tic()
  obj_filtered <- .obj_filtered
  ## Clustering
  print("Clustering based on SML algorithm...")
  obj_filtered <- FindClusters(object = obj_filtered, verbose = T, graph.name = "wsnn", resolution = c(0.01,0.05,0.1,0.5,1), algorithm = 3)
  print(paste0("The number of clusters is ", max(as.numeric(obj_filtered$seurat_clusters))))
  
  toc()
  obj_filtered  
}

## Loading files:
print("Loading annotation files...")
.cc.genes <- readLines(con = "regev_lab_cell_cycle_genes.txt")
.s.genes <- .cc.genes[1:43]
.g2m.genes <- .cc.genes[44:97]
.annotations <- readRDS("annotations.rds")
