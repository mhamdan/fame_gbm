## Process all GSCs and NSCs

source("/exports/igmm/eddie/Glioblastoma-WGS/scripts/fame_gbm/R/seurat_helpers_eddie.R")
options(future.globals.maxSize = 40000 * 1024^2) # for X Gb RAM
future.seed=TRUE

# tic()
# message("Loading all filtered objects...")
# E31 <- readRDS("E31_seurat_obj_filtered_tidied.rds")
# E34 <- readRDS("E34_seurat_obj_filtered_tidied.rds")
# E51 <- readRDS("E51_seurat_obj_filtered_tidied.rds")
# E53 <- readRDS("E53_seurat_obj_filtered_tidied.rds")
# E55 <- readRDS("E55_seurat_obj_filtered_tidied.rds")
# E57 <- readRDS("E57_seurat_obj_filtered_tidied.rds")
# NS9FB_B <- readRDS("NS9FB-B_seurat_obj_filtered_tidied.rds")
# NS12ST_A <- readRDS("NS12ST-A_seurat_obj_filtered_tidied.rds")
# NS17ST_A <- readRDS("NS17ST-A_seurat_obj_filtered_tidied.rds")
# 
# combined <- merge(E31, y=c(E34, E51, E53, E55, E57, NS9FB_B, NS12ST_A, NS17ST_A), 
#                   add.cell.ids = c("E31","E34","E51", "E53","E55","E57","NS9FB-B", "NS12ST-A","NS17ST-A"), project = "combined_9", merge.data = FALSE) ## Merging normalised data
# rm(E31, E34, E51, E53, E55, E57, NS9FB_B, NS12ST_A, NS17ST_A)
# saveRDS(combined, "combined_9_filtered_tidied.rds")
# gc()
# toc()

# tic()
# message("Loading merged GSC and NSC objects...")
# combined <- readRDS("combined_9_filtered_tidied.rds")
# toc()
# 
# tic()
# message("Running LSI, UMAP, and tSNE on ATAC data...")
# DefaultAssay(combined) <- "ATAC"
# combined <- combined %>%
#   RunTFIDF(verbose=F) %>%
#   FindTopFeatures(min.cutoff = "q0", verbose=F) %>%
#   RunSVD(verbose=F) %>%
#   FindNeighbors(reduction = "lsi", dims = 2:30, verbose=F) %>%
#   FindClusters(algorithm = 3, graph.name = "ATAC_snn", resolution = c(0.01,0.05,0.1,0.5,1), verbose=F) %>%
#   RunUMAP(reduction = 'lsi', dims = 2:30, reduction.name = "atac.umap", reduction.key = "atacUMAP_", verbose=F) %>%
#   RunTSNE(reduction = 'lsi', dims = 2:30, reduction.name = "atac.tsne", reduction.key = "atacTSNE_", verbose=F)
# toc()
# 
# tic()
# message("Computing gene activity matrix...")
# gene.activities <- GeneActivity(combined, verbose=F)
# combined[['GA']] <- CreateAssayObject(counts = gene.activities, verbose=F)
# combined <- NormalizeData(object = combined, assay = 'GA', normalization.method = 'LogNormalize', scale.factor = median(combined$nCount_GA), verbose=F)
# saveRDS(combined, "combined_9_WNN.rds")
# toc()

tic()
message("Running SCT, PCA, UMAP and tSNE on RNA data...")
combined <- readRDS("combined_9_WNN.rds")
DefaultAssay(combined) <- "RNA"
combined <- combined %>% Regress_nonSCT()
saveRDS(combined, "combined_9_WNN.rds")
combined <- combined %>% RunPCA(reduction.name = "pca", features = VariableFeatures(combined), verbose =F) %>%
  FindNeighbors(reduction = "pca", dims = 1:30, verbose=F) %>%
  FindClusters(algorithm = 3, resolution = c(0.01,0.05,0.1,0.5,1), verbose=F) %>%
  RunUMAP(reduction = "pca", dims = 1:30, reduction.name = "rna.umap", reduction.key = "rnaUMAP_", verbose = F) %>%
  RunTSNE(reduction = "pca", dims = 1:30, reduction.name = "rna.tsne", reduction.key = "rnaTSNE_", verbose = F)
toc()

tic()
message("Combining ATAC and RNA embeddings...")
combined <- combined %>% FindMultiModalNeighbors(reduction.list = list("pca", "lsi"), knn.graph.name = "wnn", snn.graph.name = "wsnn", weighted.nn.name = "weighted.nn", dims.list = list(1:30, 2:30), verbose = F) %>%
  FindClusters(graph.name = "wsnn", algorithm = 3, resolution = c(0.01,0.05,0.1,0.5,1), verbose=F) %>%
  RunUMAP(nn.name = "weighted.nn", reduction.name = "wnn.umap", reduction.key = "wnnUMAP_", verbose = F) %>%
  RunTSNE(reduction = "pca", nn.name = "weighted.nn", reduction.name = "wnn.tsne", reduction.key = "wnnTSNE_", verbose = F)
gc()
toc()

tic()
message("Saving WNN object...")
saveRDS(combined, "combined_9_WNN.rds")
message("Done!")
toc()



