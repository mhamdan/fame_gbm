#!/bin/bash

## This script runs veclonal.R script

#$ -N veclonal
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=10G
#$ -pe sharedmem 10
#$ -l h_rt=20:00:00

## SGE parameters:
SAMPLE_FILE=$1
SAMPLE=`head -n $SGE_TASK_ID $SAMPLE_FILE | tail -n 1`

echo $SAMPLE

## Run adwgs
export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/R4.0/bin:$PATH
VECLONAL=/exports/igmm/eddie/Glioblastoma-WGS/scripts/fame_gbm/R/timing/veclonal.R

Rscript $VECLONAL $SAMPLE

