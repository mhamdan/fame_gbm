#!/bin/bash

# To run this script, do 
# qsub -t 1-n submit_mosdepth.sh IDS TYPE REGION

#
#$ -N mosdepth
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=8G
#$ -pe sharedmem 8
#$ -l h_rt=64:00:00

unset MODULEPATH
. /etc/profile.d/modules.sh

CONFIG=$1
IDS=$2
STAGE=$3
TYPE=$4


source $CONFIG 

PATIENT_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1`
SAMPLE_ID=${PATIENT_ID}${TYPE}

## Use mosdepth, based on https://github.com/brentp/mosdepth
## Need version 0.2.9 which in turn needs py2.
## Install within py2 conda

MOSDEPTH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/py2/bin/mosdepth
INPUT=/exports/igmm/eddie/Glioblastoma-WGS/WGS/alignments/${SAMPLE_ID}/${SAMPLE_ID}/${SAMPLE_ID}-ready.bam
OUTPUT_DIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/qc/coverage/genome

##cd $OUTPUT_DIR

##$MOSDEPTH --fast-mode -t 4 -Q 20 $SAMPLE_ID $INPUT 

##Subset to chrM only
cd $OUTPUT_DIR/per-base/${STAGE}

if [[ -f ${SAMPLE_ID}.per-base.bed.gz ]]; then
zcat ${SAMPLE_ID}.per-base.bed.gz | grep -si chrM | bgzip > ${SAMPLE_ID}.mt.bed.gz
fi












