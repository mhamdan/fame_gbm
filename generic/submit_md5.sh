#!/bin/bash

# To run this script, do 
# qsub -t 1-n submit_md5.sh PARAMS
#
#$ -N md5
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_rt=6:00:00
#$ -l h_vmem=8G
#$ -pe sharedmem 8

unset MODULEPATH
. /etc/profile.d/modules.sh

PARAMS=$1

FILE=`head -n $SGE_TASK_ID $PARAMS | tail -n 1 | cut -f 1`

##DIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/raw/source/DBGAP
##DIR=/exports/igmm/eddie/Glioblastoma-WGS/BGI
DIR=/exports/igmm/eddie/Glioblastoma-WGS/ChIP-seq/SOX_TF/raw/fastqs

cd $DIR

gunzip $FILE
md5sum ${FILE%.gz} > ${FILE%.gz}.md5
gzip ${FILE%.gz}







