#!/bin/bash

#$ -N crossmap
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=16G
#$ -pe sharedmem 2
#$ -l h_rt=4:00:00

CONFIG=$1
ID=$2

OLD_BED=`head -n $SGE_TASK_ID $ID | tail -n 1 | cut -f 1`
NEW_BED=`head -n $SGE_TASK_ID $ID | tail -n 1 | cut -f 2`

DIR=/exports/igmm/eddie/Glioblastoma-WGS/ATAC-seq/Stein_ATAC

source $CONFIG

cd $DIR

echo "Crossmapping bed file"
/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/py365/bin/CrossMap.py bed \
    $CHAIN19TO38 \
    $OLD_BED $NEW_BED 

sort -k 1,1 -k2,2n $NEW_BED > ${NEW_BED%.bed}.sorted.bed

    
    
    
    
    