#!/bin/bash

# To run this script, do 
# qsub -t 1-n submit_vcf2vcf.sh CONFIG IDS#
#
#$ -N vcf2vcf
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=8G
#$ -pe sharedmem 8
#$ -l h_rt=12:00:00

unset MODULEPATH
. /etc/profile.d/modules.sh

CONFIG=$1
IDS=$2

PATIENT_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1`
TUMOR=${PATIENT_ID}T
NORMAL=${PATIENT_ID}N

source $CONFIG

VCF2VCF="/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/snakemake/bin/vcf2vcf.pl"

#########################################################################################################################
#### For Strelka2
#########################################################################################################################

STRELKA2_OLD_VCF=$S2_VARIANTS_OXOG/${PATIENT_ID}-strelka2.AD.annotated.oxog.normalised.passed.filtered.vcf
STRELKA2_NEW_VCF=$S2_VARIANTS_FORMATTED/${PATIENT_ID}.s2.formatted.vcf

bcftools index ${STRELKA2_OLD_VCF}.gz

bcftools view \
    -Ov \
    -o $STRELKA2_OLD_VCF \
    ${STRELKA2_OLD_VCF}.gz

$VCF2VCF \
    --input-vcf $STRELKA2_OLD_VCF \
    --output-vcf $STRELKA2_NEW_VCF \
    --vcf-tumor-id $TUMOR \
    --vcf-normal-id $NORMAL \
    --ref-fasta $REFERENCE 

bcftools view \
    -Oz \
    -o ${STRELKA2_NEW_VCF}.gz \
    $STRELKA2_NEW_VCF

bcftools index ${STRELKA2_NEW_VCF}.gz

rm $STRELKA2_OLD_VCF
rm $STRELKA2_NEW_VCF

#########################################################################################################################
#### For Mutect2
#########################################################################################################################

MUTECT2_OLD_VCF=$M2_VARIANTS_PASSED/${PATIENT_ID}-Mutect2.normalised.passed.filtered.vcf
MUTECT2_NEW_VCF=$M2_VARIANTS_FORMATTED/${PATIENT_ID}.m2.formatted.vcf

bcftools index ${MUTECT2_OLD_VCF}.gz

bcftools view \
    -Ov \
    -o $MUTECT2_OLD_VCF \
    ${MUTECT2_OLD_VCF}.gz

$VCF2VCF \
    --input-vcf $MUTECT2_OLD_VCF \
    --output-vcf $MUTECT2_NEW_VCF \
    --vcf-tumor-id $TUMOR \
    --vcf-normal-id $NORMAL \
    --ref-fasta $REFERENCE 

bcftools view \
    -Oz \
    -o ${MUTECT2_NEW_VCF}.gz \
    $MUTECT2_NEW_VCF

bcftools index ${MUTECT2_NEW_VCF}.gz

rm $MUTECT2_OLD_VCF
rm $MUTECT2_NEW_VCF


#########################################################################################################################
#### For Vardict
#########################################################################################################################

VARDICT_OLD_VCF=$VARDICT_VARIANTS_OXOG/${PATIENT_ID}-vardict.oxog.normalised.passed.filtered.vcf
VARDICT_NEW_VCF=$VARDICT_VARIANTS_FORMATTED/${PATIENT_ID}.var.formatted.vcf

bcftools index ${VARDICT_OLD_VCF}.gz

bcftools view \
    -Ov \
    -o $VARDICT_OLD_VCF \
    ${VARDICT_OLD_VCF}.gz

$VCF2VCF \
    --input-vcf $VARDICT_OLD_VCF \
    --output-vcf $VARDICT_NEW_VCF \
    --vcf-tumor-id $TUMOR \
    --vcf-normal-id $NORMAL \
    --ref-fasta $REFERENCE 

bcftools view \
    -Oz \
    -o ${VARDICT_NEW_VCF}.gz \
    $VARDICT_NEW_VCF

bcftools index ${VARDICT_NEW_VCF}.gz

rm $VARDICT_OLD_VCF
rm $VARDICT_NEW_VCF


#########################################################################################################################
#### Merge all vcfs to create an ensemble call set
#########################################################################################################################

bcftools concat -a -d none \
    -Oz \
    -o $ENSEMBLE_DIR/${PATIENT_ID}.final.vcf.gz \
    ${STRELKA2_NEW_VCF}.gz ${VARDICT_NEW_VCF}.gz ${MUTECT2_NEW_VCF}.gz
    
bcftools index $ENSEMBLE_DIR/${PATIENT_ID}.final.vcf.gz
  





