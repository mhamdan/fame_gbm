#!/bin/bash

# To run this script, do 
# qsub -t 1-n submit_subset_BAM2Fasta.sh CONFIG IDS TYPE
#
#$ -N subsetB2F
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=8G
#$ -pe sharedmem 8
#$ -l h_rt=90:00:00

unset MODULEPATH
. /etc/profile.d/modules.sh

CONFIG=$1
IDS=$2
TYPE=$3

source $CONFIG

PATIENT_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1`
SAMPLE_ID=${PATIENT_ID}${TYPE}

BAM_FILE=/exports/igmm/eddie/Glioblastoma-WGS/WGS/alignments/${SAMPLE_ID}/${SAMPLE_ID}/${SAMPLE_ID}-ready.bam
BED_FILE=/exports/igmm/eddie/Glioblastoma-WGS/analysis/Consensus_merged_shared.bed
FASTA_DIR=/exports/igmm/eddie/Glioblastoma-WGS/analysis/fasta
BAM_DIR=/exports/igmm/eddie/Glioblastoma-WGS/analysis/bam

#### This will split bam files into individual chromosomes and intersect region of interest and convert it to fasta file.

##for chrom in `seq 1 22`
##do
##    samtools view $BAM_FILE chr${chrom} -b |\
##    intersectBed -a - -b $BED_FILE |\
##    samtools fasta - > $FASTA_DIR/${SAMPLE_ID}.chr${chrom}.SOX_merged.fa
##done

for chrom in `seq 1 22`
do
    samtools view $BAM_FILE chr${chrom} -b |\
    intersectBed -a - -b $BED_FILE > ${BAM_DIR}/${SAMPLE_ID}.chr${chrom}.SOX_merged.bam
done








