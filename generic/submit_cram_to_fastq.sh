#!/bin/bash

### Adapted from Alison Meynert
#$ -N cram_to_fastq
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_rt=300:00:00
#$ -l h_vmem=12G
#$ -pe sharedmem 12

# Parameter file consists of old ID in 1st column and new ID in 2nd column

CONFIG=$1
IDS=$2
OLD_TYPE=$3
NEW_TYPE=$4

. /etc/profile.d/modules.sh
MODULEPATH=$MODULEPATH:/exports/igmm/software/etc/el7/modules

source $CONFIG

CRAM_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 1`
OLD_SAMPLE_ID=${CRAM_ID}${OLD_TYPE}
echo "Old sample is $OLD_SAMPLE_ID"

PATIENT_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 2`
NEW_SAMPLE_ID=${PATIENT_ID}${NEW_TYPE}
echo "New sample is $NEW_SAMPLE_ID"

CRAM_DIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/raw/source/HMF/crams
FASTQ_DIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/raw/source/HMF/fastqs

##### QC and convert to fastq files ##### 
cd $CRAM_DIR
echo "Processing for ${OLD_SAMPLE_ID}_dedup.realigned.cram"

# Flagstat original bam file
if [ ! -f ${OLD_SAMPLE_ID}.cram.flagstats.txt ]; then 
samtools flagstat ${OLD_SAMPLE_ID}_dedup.realigned.cram > ${OLD_SAMPLE_ID}.cram.flagstats.txt
fi

# samtools sort needs REF_PATH and REF_CACHE; http://www.htslib.org/workflow/cram.html
# Also very important when converting from cram file, is setting up the tmp directory to non-root folder to avoid overloading of storage.
export REF_PATH=/exports/igmm/eddie/Glioblastoma-WGS/resources/refgenome38/samtools_cache/%2s/%2s/%s:http://www.ebi.ac.uk/ena/cram/md5/%s
export REF_CACHE=/exports/igmm/eddie/Glioblastoma-WGS/resources/refgenome38/samtools_cache/%2s/%2s/%s
TMP=/exports/igmm/eddie/Glioblastoma-WGS/WGS/raw/source/HMF/crams/tmp

mkdir -p $TMP/${OLD_SAMPLE_ID}

echo "Sorting ${OLD_SAMPLE_ID}_dedup.realigned.cram"
samtools sort -n -m 2G -@ 12 -T $TMP/${OLD_SAMPLE_ID} -o $WORK_DIR/${OLD_SAMPLE_ID}.query_sorted.cram ${OLD_SAMPLE_ID}_dedup.realigned.cram

# 3) Recheck the flagstat of sorted CRAM files
samtools flagstat $WORK_DIR/${OLD_SAMPLE_ID}.query_sorted.cram > $WORK_DIR/${OLD_SAMPLE_ID}.query_sorted.cram.flagstats.txt

# 4) Check differences between the two txt files to see discrepancies
echo "flagstats diff: BEGIN"
diff ${OLD_SAMPLE_ID}.cram.flagstats.txt $WORK_DIR/${OLD_SAMPLE_ID}.query_sorted.cram.flagstats.txt
echo "flagstats diff: END"
echo "Sorted cram file validated"

# 5) Convert to fastq files
echo "Converting to fastq files with ${NEW_SAMPLE_ID} as prefix"
export CRAM_REFERENCE=$REFERENCE
bedtools bamtofastq \
    -i $WORK_DIR/${OLD_SAMPLE_ID}.query_sorted.cram \
    -fq $FASTQ_DIR/${NEW_SAMPLE_ID}_R1.fastq \
    -fq2 $FASTQ_DIR/${NEW_SAMPLE_ID}_R2.fastq
    
echo "Fastq files generated, gzipping.."
gzip -f $FASTQ_DIR/${NEW_SAMPLE_ID}_R1.fastq
gzip -f $FASTQ_DIR/${NEW_SAMPLE_ID}_R2.fastq

# 6) Fastqc
echo "Fastqc..."
module load igmm/apps/FastQC/0.11.4
fastqc -t 20 $FASTQ_DIR/${NEW_SAMPLE_ID}_R1.fastq.gz
fastqc -t 20 $FASTQ_DIR/${NEW_SAMPLE_ID}_R2.fastq.gz


########### Done ############







