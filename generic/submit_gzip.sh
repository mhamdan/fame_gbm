#!/bin/bash

### qsub submit_gzip.sh <FULL_PATH_TO_FILE>

#$ -N gzip
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_rt=30:00:00
#$ -l h_vmem=8G

FILE=$1
gzip -f $FILE

