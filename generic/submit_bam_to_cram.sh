#!/bin/bash

# From Alison Meynert
# To run this script, do 
# qsub -t 1-N submit_bam_to_cram.sh <list of bam files> <suffix>
#
# List of bam files is the absolute paths of file names of the BAM files.
# The CRAM file and flagstat files will be created in the same folder as
# the source BAM file.
# Suffix is any additional suffix between the sample id and the .bam extension. Can be empty.
# N is the number of lines in the file.
#
#$ -N bam_to_cram
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_rt=48:00:00
#$ -l h_vmem=4G
#$ -pe sharedmem 4

BAM_FILES=$1
SUFFIX=$2

BAM_FILE=`head -n $SGE_TASK_ID $BAM_FILES | tail -n 1`
BNAME=`basename $BAM_FILE`
ID=${BNAME%$SUFFIX.bam}
DNAME=`dirname $BAM_FILE`
CRAM_FILE=$DNAME/$ID.cram

. /etc/profile.d/modules.sh
MODULEPATH=$MODULEPATH:/exports/igmm/software/etc/el7/modules

module load igmm/libs/htslib/1.6
module load igmm/apps/samtools/1.6

REFERENCE_GENOME=/gpfs/igmmfs01/software/pkg/el7/apps/bcbio/share2/genomes/Hsapiens/hg38/seq/hg38.fa

# Basic info
echo `date` "This job is running on $HOSTNAME" >> $SGE_O_WORKDIR/$ID.conversion.$JOB_ID.$SGE_TASK_ID.log

if [ -e $BAM_FILE.md5 ]
then

  # Check md5sums for original bam and associated index
  echo `date` "Checking md5sums for original bam and associated index" >> $SGE_O_WORKDIR/$ID.conversion.$JOB_ID.$SGE_TASK_ID.log
  cd $DNAME
  if md5sum --status -c $BAM_FILE.md5
  then
    echo `date` "Original bam $BAM_FILE md5sum OK" >> $SGE_O_WORKDIR/$ID.conversion.$JOB_ID.$SGE_TASK_ID.log
  else
    echo `date` "Original bam $BAM_FILE md5sum FAIL" >> $SGE_O_WORKDIR/$ID.conversion.$JOB_ID.$SGE_TASK_ID.log
    exit 1
  fi
  if md5sum --status -c $BNAME*.bai.md5
  then
    echo `date` "Original bam.bai $BAM_FILE.bai md5sum OK" >> $SGE_O_WORKDIR/$ID.conversion.$JOB_ID.$SGE_TASK_ID.log
  else
    echo `date` "Original bam.bai $BAM_FILE.bai md5sum FAIL" >> $SGE_O_WORKDIR/$ID.conversion.$JOB_ID.$SGE_TASK_ID.log
    exit 1
  fi

  # Return to working directory
  cd $SGE_O_WORKDIR
fi

# Generate flagstat report for original BAM file
echo `date` "samtools flagstat for original bam" >> $SGE_O_WORKDIR/$ID.conversion.$JOB_ID.$SGE_TASK_ID.log
samtools flagstat $BAM_FILE > $BAM_FILE.flagstat

# Convert bam to cram and index
echo `date` "CRAM compression against $REFERENCE_GENOME" >> $SGE_O_WORKDIR/$ID.conversion.$JOB_ID.$SGE_TASK_ID.log
samtools view -@ $NSLOTS -h -C $BAM_FILE -o $CRAM_FILE -T $REFERENCE_GENOME

echo `date` "Indexing cram file $ID.cram" >> $SGE_O_WORKDIR/$ID.conversion.$JOB_ID.$SGE_TASK_ID.log
samtools index $CRAM_FILE
echo `date` "samtools flagstat for new cram" >> $SGE_O_WORKDIR/$ID.conversion.$JOB_ID.$SGE_TASK_ID.log
samtools flagstat $CRAM_FILE > $CRAM_FILE.flagstat

# Perform quick check on newly created cram
echo `date` "samtools quickcheck on new cram" >> $SGE_O_WORKDIR/$ID.conversion.$JOB_ID.$SGE_TASK_ID.log
if samtools quickcheck $CRAM_FILE
then
  echo `date` "New cram $CRAM_FILE passes samtools quickcheck" >> $SGE_O_WORKDIR/$ID.conversion.$JOB_ID.$SGE_TASK_ID.log
else
  echo `date` "New cram $CRAM_FILE fails samtools quickcheck" >> $SGE_O_WORKDIR/$ID.conversion.$JOB_ID.$SGE_TASK_ID.log
  exit 1
fi

# Compare flagstat info from original bam
echo `date` "Compare flagstat from original bam to new cram" >> $SGE_O_WORKDIR/$ID.conversion.$JOB_ID.$SGE_TASK_ID.log
if cmp $BAM_FILE.flagstat $CRAM_FILE.flagstat
then
  echo `date` "Original bam $BAM_FILE and new cram $CRAM_FILE samtools flagstat match" >> $SGE_O_WORKDIR/$ID.conversion.$JOB_ID.$SGE_TASK_ID.log
else
  echo `date` "Original bam $BAM_FILE and new cram $CRAM_FILE samtools flagstat do not match" >> $SGE_O_WORKDIR/$ID.conversion.$JOB_ID.$SGE_TASK_ID.log
  exit 1
fi

# Create md5sums for cram file and its index
echo `date` "Calculate md5sums for cram file $ID.cram and its index $ID.cram.crai" >> $SGE_O_WORKDIR/$ID.conversion.$JOB_ID.$SGE_TASK_ID.log
cd $DNAME
CNAME=`basename $CRAM_FILE`
md5sum $CNAME > $CNAME.md5
md5sum $CNAME.crai > $CNAME.crai.md5

# All done
echo `date` "Conversion complete" >> $SGE_O_WORKDIR/$ID.conversion.$JOB_ID.$SGE_TASK_ID.log
