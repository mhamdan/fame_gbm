#!/bin/bash

# To run this script, do
# qsub -t 1-n submit_renaming_files.sh IDS DIR
#
#$ -N renaming_files
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_rt=1:00:00
#$ -l h_vmem=4G
#$ -pe sharedmem 4

IDS=$1

SRR_NAME=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 7`
NEW_NAME=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 8`
DIR=/exports/igmm/eddie/Glioblastoma-WGS/ChIP-seq/H3K27ac-seq_GBM/reads/Paired_end_reads

cd $DIR
mv ${SRR_NAME}.fastq.gz ${NEW_NAME}.fastq.gz ## For paired end reads







