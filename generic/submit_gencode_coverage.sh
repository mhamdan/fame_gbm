#!/bin/bash

# To run this script, do 
# qsub -t 1-n submit_gencode_coverage.sh IDS TYPE

#
#$ -N gncd_cov
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=20G
#$ -pe sharedmem 16
#$ -l h_rt=64:00:00

unset MODULEPATH
. /etc/profile.d/modules.sh

module load igmm/apps/samtools/1.6
module load igmm/apps/BEDTools/2.27.1

IDS=$1
TYPE=$2

PATIENT_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1`
SAMPLE_ID=${PATIENT_ID}${TYPE}

## Based on https://github.com/TheJacksonLaboratory/GLASS/blob/1c554908f00172e9437cac6e75aaf81429852d17/snakemake/align.smk
## Exon annotations are based on "Basic" Gencode V34 downloaded here https://www.gencodegenes.org/human/

INPUT=/exports/igmm/eddie/Glioblastoma-WGS/WGS/alignments/${SAMPLE_ID}/${SAMPLE_ID}/${SAMPLE_ID}-ready.bam
GFF=/exports/igmm/eddie/Glioblastoma-WGS/resources/gencode.v34.basic.annotation.gff3
OUTPUT=/exports/igmm/eddie/Glioblastoma-WGS/WGS/qc/gencode_coverage/${SAMPLE_ID}_gencode_coverage.tsv

samtools view -q 10 -b $INPUT | bedtools coverage -a $GFF -b stdin -d > $OUTPUT




