#!/bin/bash

# This script subsets a fasta file given a list of region (bed) and outputs individual region fastas 
# To run this script, do qsub -t <1-n> submit_subsetFa_into_Regions.sh IDS TYPE

#$ -N subsetFas
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_rt=72:00:00
#$ -pe sharedmem 8
#$ -l h_vmem=8G

IDS=$1
TYPE=$2

PATIENT_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1`
SAMPLE_ID=${PATIENT_ID}${TYPE}

INPUT_DIR=/exports/igmm/eddie/Glioblastoma-WGS/analysis/genomes/Consensus_fasta/sample_level
OUTPUT_DIR=/exports/igmm/eddie/Glioblastoma-WGS/analysis/genomes/Consensus_fasta/region_level

######

while read line
do
    if [[ ${line:0:1} == '>' ]]
    then
        outfile=${OUTPUT_DIR}/${SAMPLE_ID}.${line#>}.fa
        echo $line > $outfile
    else
        echo $line >> $outfile
    fi
done < ${INPUT_DIR}/${SAMPLE_ID}.consensus.SOXshared.fa



