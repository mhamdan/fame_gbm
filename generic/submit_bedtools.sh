#!/bin/bash

#$ -N submit_bedtools
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=2G
#$ -pe sharedmem 2
#$ -l h_rt=1:00:00

unset MODULEPATH
. /etc/profile.d/modules.sh

##IDS=$1
CONFIG=$1
SAMPLE_ID=$2
##SAMPLE_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1`

source $CONFIG
cd /exports/igmm/eddie/Glioblastoma-WGS/ChIP-seq/SOX_TF/downstream/beds

##cat ${SAMPLE_ID}.genes.txt | grep -vsi peakid | grep -si promoter | cut -f 2-4 | sort -k1 > ${SAMPLE_ID}_promoters.bed
##cat ${SAMPLE_ID}.genes.txt | grep -vsi peakid | grep -si intergenic | cut -f 2-4 | sort -k1 > ${SAMPLE_ID}_intergenic.bed
##cat ${SAMPLE_ID}.genes.txt | grep -vsi peakid | grep -vsi intergenic | grep -vsi promoter | cut -f 2-4 | sort -k1 > ${SAMPLE_ID}_intragenic.bed


## Subset overlapping Sox2 and Sox9 regions to promoters/intragenic/intergenic regions of Sox2 TFBSs.
bedtools intersect -a ${SAMPLE_ID}_Sox2_Sox9_merged_overlap.bed -b homer/${SAMPLE_ID}_Sox2.genes.bed -wa -wb | grep -si promoter | cut -f 1-3 > ${SAMPLE_ID}_Sox2_Sox9_merged_overlap_Sox2_promoter.bed
bedtools intersect -a ${SAMPLE_ID}_Sox2_Sox9_merged_overlap.bed -b homer/${SAMPLE_ID}_Sox2.genes.bed -wa -wb | grep -si intergenic | cut -f 1-3 > ${SAMPLE_ID}_Sox2_Sox9_merged_overlap_Sox2_intergenic.bed
bedtools intersect -a ${SAMPLE_ID}_Sox2_Sox9_merged_overlap.bed -b homer/${SAMPLE_ID}_Sox2.genes.bed -wa -wb | grep -vsi intergenic | grep -vsi promoter | cut -f 1-3 > ${SAMPLE_ID}_Sox2_Sox9_merged_overlap_Sox2_intragenic.bed




