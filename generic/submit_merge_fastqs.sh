#!/bin/bash

### Merging fastqs from diferrent lanes

#$ -N Merge_fastqs
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=8G
#$ -pe sharedmem 4
#$ -l h_rt=10:00:00

## IDS contains sample_ids 

unset MODULEPATH
. /etc/profile.d/modules.sh

IDS=$1
BATCH=$2

SAMPLE_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 1`
PATIENT_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 2`
LANE_INPUT=/exports/igmm/eddie/Glioblastoma-WGS/WGS/raw/source/${BATCH}/RNA/fastqs
READ_OUTPUT=/exports/igmm/eddie/Glioblastoma-WGS/RNA-seq/raw/source/${BATCH}/fastqs

if [ ! -f ${READ_OUTPUT}/${PATIENT_ID}_R2.fastq.gz ]
then
    echo "Processing for $SAMPLE_ID..."
    zcat ${LANE_INPUT}/${SAMPLE_ID}*R1_001.fastq.gz > ${READ_OUTPUT}/${PATIENT_ID}_R1.fastq
    zcat ${LANE_INPUT}/${SAMPLE_ID}*R2_001.fastq.gz > ${READ_OUTPUT}/${PATIENT_ID}_R2.fastq
    
    echo "Fastq files generated, gzipping.."
    gzip -f ${READ_OUTPUT}/${PATIENT_ID}_R1.fastq
    gzip -f ${READ_OUTPUT}/${PATIENT_ID}_R2.fastq

    echo "Fastqc..."
    module load igmm/apps/FastQC/0.11.4
    fastqc -t 20 ${READ_OUTPUT}/${PATIENT_ID}_R1.fastq.gz
    fastqc -t 20 ${READ_OUTPUT}/${PATIENT_ID}_R2.fastq.gz
else
    echo "Paired fastq.gz files for $PATIENT_ID already generated, processing to Fastqc..."
    module load igmm/apps/FastQC/0.11.4
    fastqc -t 20 ${READ_OUTPUT}/${PATIENT_ID}_R1.fastq.gz
    fastqc -t 20 ${READ_OUTPUT}/${PATIENT_ID}_R2.fastq.gz
fi




