#!/bin/bash

### Adapted from Alison Meynert
#$ -N prep_bams
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_rt=20:00:00
#$ -l h_vmem=12G
#$ -pe sharedmem 12

# Parameter file consists of sample ID in first column and full bam file name in second column

CONFIG=$1
PARAMS=$2
BATCH=$3

##PARAM contains patient id, sample id, original bam id, path to original bam id

SAMPLE_ID=`head -n $SGE_TASK_ID $PARAMS | tail -n 1 | cut -f 1`
ORIGINAL_BAM_ID=`head -n $SGE_TASK_ID $PARAMS | tail -n 1 | cut -f 2`

source $CONFIG
WORK_DIR=$BCBIO_WORK/${ORIGINAL_BAM_ID}
BAM_DIR=/exports/igmm/eddie/Glioblastoma-WGS/ChIP-seq/${BATCH}/bams

##### QC and convert to fastq files ##### 

if [ ! -f $WORK_DIR/${ORIGINAL_BAM_ID}.query_sorted.bam ]
then
# 1) Validate
cd $BAM_DIR
echo "Creating tmp directory and validating ${ORIGINAL_BAM_ID}.bam"
mkdir -p tmp/${ORIGINAL_BAM_ID}
java -Djava.io.tmpdir=tmp/${ORIGINAL_BAM_ID} -Xmx16g -jar /exports/igmm/eddie/NextGenResources/software/picard-2.9.4/picard.jar \
    ValidateSamFile \
    I=${ORIGINAL_BAM_ID}.bam \
    MODE=SUMMARY \
    OUTPUT=${ORIGINAL_BAM_ID}.validate.summary.txt &> ${ORIGINAL_BAM_ID}.validate.log
rm -r tmp/${ORIGINAL_BAM_ID}

# 2) Flagstat original bam file
samtools flagstat ${ORIGINAL_BAM_ID}.bam > ${ORIGINAL_BAM_ID}.flagstats.txt

# 3) Sort the bam file
echo "Sorting ${ORIGINAL_BAM_ID}.bam"
sambamba sort \
    -t 12 -m 16G \
    -n -p \
    -o $WORK_DIR/${ORIGINAL_BAM_ID}.query_sorted.bam \
    --tmpdir=$WORK_DIR \
    ${ORIGINAL_BAM_ID}.bam

# 4) Recheck the flagstat of sorted BAM files
samtools flagstat $WORK_DIR/${ORIGINAL_BAM_ID}.query_sorted.bam > $WORK_DIR/${ORIGINAL_BAM_ID}.query_sorted.bam.flagstats.txt

# 5) Check differences between the two txt files to see discrepancies
echo "flagstats diff: BEGIN"
diff ${ORIGINAL_BAM_ID}.flagstats.txt $WORK_DIR/${ORIGINAL_BAM_ID}.query_sorted.bam.flagstats.txt
echo "flagstats diff: END"
fi

# 7) Convert to fastq files
if [ -f $WORK_DIR/${ORIGINAL_BAM_ID}.query_sorted.bam ]
then
echo "Sorted bam file validated"

echo "Converting to fastq files with ${SAMPLE_ID} as prefix"
bedtools bamtofastq \
    -i $WORK_DIR/${ORIGINAL_BAM_ID}.query_sorted.bam \
    -fq $LANES/${SAMPLE_ID}_R1.fastq \
    -fq2 $LANES/${SAMPLE_ID}_R2.fastq

echo "Fastq files generated, gzipping.."
gzip -f $LANES/${SAMPLE_ID}_R1.fastq
gzip -f $LANES/${SAMPLE_ID}_R2.fastq

echo "Fastqc..."
module load igmm/apps/FastQC/0.11.4
fastqc -t 20 $LANES/${SAMPLE_ID}_R1.fastq.gz
fastqc -t 20 $LANES/${SAMPLE_ID}_R2.fastq.gz
fi

if [ -f $LANES/${SAMPLE_ID}_R2.fastq.gz ]
then
rm -rf $WORK_DIR
echo "Preprocessing of original bam files done. Intermediate files deleted. Fastq files ready to map."
fi


########### Done ############
