#!/bin/bash

## To provide base level summary statistics at certain genomic regions given an alignment file
## As per https://github.com/genome/bam-readcount
## Using conda version as had difficulties compiling latest git release. (using v0.8).

## Example run command = qsub -t 1 ../../scripts/fame_gbm/submit_bam_readcounts.sh ../../scripts/fame_gbm/config.sh ../params/HMF_GBM_ids.txt T2 chr5:1295068-1295568

#$ -N bamreadcounts
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=8G
#$ -pe sharedmem 8
#$ -l h_rt=3:00:00

unset MODULEPATH
. /etc/profile.d/modules.sh

CONFIG=$1
IDS=$2
TYPE=$3
REGION=$4

## SiTES points to a bed file with list of regions of interest

source $CONFIG 

PATIENT_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1`
BAM_DIR=$ALIGNMENTS/${PATIENT_ID}${TYPE}/${PATIENT_ID}${TYPE}
ALIGNED_BAM_FILE=$BAM_DIR/${PATIENT_ID}${TYPE}-ready.bam
OUTPUT_DIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/bamreadcounts
PARSE_OUTPUT=/exports/igmm/eddie/Glioblastoma-WGS/scripts/bamreadcounts_parse_brc.py

bam-readcount -f $REFERENCE $ALIGNED_BAM_FILE $REGION > $OUTPUT_DIR/${PATIENT_ID}${TYPE}_${REGION}.txt
python $PARSE_OUTPUT $OUTPUT_DIR/${PATIENT_ID}${TYPE}_${REGION}.txt > $OUTPUT_DIR/${PATIENT_ID}${TYPE}_${REGION}_parsed.txt

















