#!/bin/bash

#$ -N mergeBams
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=16G
#$ -l h_rt=24:00:00

CONFIG=$1
source $CONFIG

CHIP_BAMS_DIR=/exports/igmm/eddie/Glioblastoma-WGS/ChIP-seq/SOX_TF/bams/sorted
cd $CHIP_BAMS_DIR

echo "Merging and indexing bam files..."
samtools merge E17_Sox2_sorted_merged.bam E17_Sox2_Rep1.sorted.bam E17_Sox2_Rep2.sorted.bam; samtools index E17_Sox2_sorted_merged.bam
samtools merge E17_Sox9_sorted_merged.bam E17_Sox9_Rep1.sorted.bam E17_Sox9_Rep2.sorted.bam; samtools index E17_Sox9_sorted_merged.bam
samtools merge E27_Sox2_sorted_merged.bam E27_Sox2_Rep1.sorted.bam E27_Sox2_Rep2.sorted.bam; samtools index E27_Sox2_sorted_merged.bam 
samtools merge E27_Sox9_sorted_merged.bam E27_Sox9_Rep1.sorted.bam E27_Sox9_Rep2.sorted.bam; samtools index E27_Sox9_sorted_merged.bam
samtools merge E28_Sox2_sorted_merged.bam E28_Sox2_Rep1.sorted.bam E28_Sox2_Rep2.sorted.bam; samtools index E28_Sox2_sorted_merged.bam
samtools merge E28_Sox9_sorted_merged.bam E28_Sox9_Rep1.sorted.bam E28_Sox9_Rep2.sorted.bam; samtools index E28_Sox9_sorted_merged.bam
samtools merge E21_Sox2_sorted_merged.bam E21_Sox2_Rep1.sorted.bam E21_Sox2_Rep2.sorted.bam; samtools index E21_Sox2_sorted_merged.bam
samtools merge E21_Sox9_sorted_merged.bam E21_Sox9_Rep1.sorted.bam E21_Sox9_Rep2.sorted.bam; samtools index E21_Sox9_sorted_merged.bam
samtools merge E31_Sox2_sorted_merged.bam E31_Sox2_Rep1.sorted.bam E31_Sox2_Rep2.sorted.bam; samtools index E31_Sox2_sorted_merged.bam
samtools merge E31_Sox9_sorted_merged.bam E31_Sox9_Rep1.sorted.bam E31_Sox9_Rep2.sorted.bam; samtools index E31_Sox9_sorted_merged.bam
samtools merge E34_Sox2_sorted_merged.bam E34_Sox2_Rep1.sorted.bam E34_Sox2_Rep2.sorted.bam; samtools index E34_Sox2_sorted_merged.bam
samtools merge E34_Sox9_sorted_merged.bam E34_Sox9_Rep1.sorted.bam E34_Sox9_Rep2.sorted.bam; samtools index E34_Sox9_sorted_merged.bam
samtools merge E37_Sox2_sorted_merged.bam E37_Sox2_Rep1.sorted.bam E37_Sox2_Rep2.sorted.bam; samtools index E37_Sox2_sorted_merged.bam
samtools merge E37_Sox9_sorted_merged.bam E37_Sox9_Rep1.sorted.bam E37_Sox9_Rep2.sorted.bam; samtools index E37_Sox9_sorted_merged.bam

samtools merge E17_Input_sorted_merged.bam E17_Input_Rep1.sorted.bam E17_Input_Rep2.sorted.bam; samtools index E17_Input_sorted_merged.bam
samtools merge E27_Input_sorted_merged.bam E27_Input_Rep1.sorted.bam E27_Input_Rep2.sorted.bam; samtools index E27_Input_sorted_merged.bam 
samtools merge E28_Input_sorted_merged.bam E28_Input_Rep1.sorted.bam E28_Input_Rep2.sorted.bam; samtools index E28_Input_sorted_merged.bam
cp E21_Input_Rep1.sorted.bam E21_Input_sorted.bam; samtools index E21_Input_sorted.bam
cp E31_Input_Rep1.sorted.bam E31_Input_sorted.bam; samtools index E31_Input_sorted.bam
cp E34_Input_Rep1.sorted.bam E34_Input_sorted.bam; samtools index E34_Input_sorted.bam
cp E37_Input_Rep1.sorted.bam E37_Input_sorted.bam; samtools index E37_Input_sorted.bam




