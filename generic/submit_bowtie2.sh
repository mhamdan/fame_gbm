#!/bin/bash

# To run this script, do 
# qsub -t 1-n submit_bowtie2.sh CONFIG IDS
#
# CONFIG is the path to the file scripts/config.sh which contains environment variables set to
# commonly used paths and files in the script
# IDS is a list of sample ids, one per line, where tumor and normal samples are designated by
# the addition of a T or an N to the sample id.
# TYPE is either sample name or input
#
#$ -N bwt2
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=8G
#$ -l h_rt=90:00:00
#$ -pe sharedmem 8

CONFIG=$1
IDS=$2

source $CONFIG

BOWTIE2=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/bin/bowtie2
READ_DIR=/exports/igmm/eddie/Glioblastoma-WGS/ChIP-seq/H3K27ac-seq_GBM/reads/Single_end_reads
BAM_DIR=/exports/igmm/eddie/Glioblastoma-WGS/ChIP-seq/H3K27ac-seq_GBM/bams/bwt2

## Align fastq reads 

cd $READ_DIR

##$BOWTIE2 -p 20 -q --local -x $BOWTIE_REF -U ${SAMPLE_ID}.fastq.gz | samtools sort -@ 10 - -T ${SAMPLE_ID} -o $BAM_DIR/${SAMPLE_ID}.sorted.bam 
##samtools index $BAM_DIR/${SAMPLE_ID}.sorted.bam
##samtools flagstat $BAM_DIR/${SAMPLE_ID}.sorted.bam > $BAM_DIR/${SAMPLE_ID}.sorted.bam.stats.out

$BOWTIE2 -p 20 -q --local -x $BOWTIE_REF -U Mack_GSC15_H3K27AC.fastq.gz | samtools sort -@ 10 - -T Mack_GSC15_H3K27AC -o $BAM_DIR/Mack_GSC15_H3K27AC.sorted.bam 
samtools index $BAM_DIR/Mack_GSC15_H3K27AC.sorted.bam 
samtools flagstat $BAM_DIR/Mack_GSC15_H3K27AC.sorted.bam  > $BAM_DIR/Mack_GSC15_H3K27AC.sorted.bam.stats.out











