#!/bin/bash

# To run this script, do 
# qsub -t 1-n submit_blastseq.sh CONFIG IDS TYPE INPUT LENGHT
#
#$ -N blastseq
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=16G
#$ -pe sharedmem 8
#$ -l h_rt=72:00:00

CONFIG=$1
IDS=$2
TYPE=$3
INPUT=$4
##LENGTH=$5

source $CONFIG

PATIENT_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1`
SAMPLE_ID=${PATIENT_ID}${TYPE}
DATABASE=$BLAST/${SAMPLE_ID}-subset.fa

cd $BLAST

##Align sequences to each tumour/normal fasta file
##blastn -db $DATABASE -query ${INPUT}.fa -out ${INPUT}_${PATIENT_ID}${TYPE}.outfmt6 -outfmt "6 qseqid sseqid pident nident mismatch length mismatch qcovs gapopen qstart qend qlen sstart send slen evalue bitscore"

blastn -db $DATABASE \
    -query ${INPUT}.fa \
    -out ${INPUT}_${SAMPLE_ID}.subset.outfmt6 \
    -outfmt "6 qseqid sseqid pident nident mismatch length mismatch qcovs gapopen qstart qend qlen sstart send slen evalue bitscore"

##blastn -db hg38 -query ${INPUT}.fa -out ${INPUT}_hg38.outfmt6 -outfmt "6 qseqid sseqid pident nident mismatch length mismatch qcovs gapopen qstart qend qlen sstart send slen evalue bitscore"




