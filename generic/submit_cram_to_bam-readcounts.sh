#!/bin/bash

### This script converts cram to bam and extract base coverage across a genomic region


# List of cram files is the absolute paths of file names of the cram files.
# The BAM file and flagstat files will be created in the same folder as
# the source CRAM file.
# N is the number of lines in the file.
#
#$ -N cram2bamreadcounts
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_rt=48:00:00
#$ -l h_vmem=12G
#$ -pe sharedmem 12

CONFIG=$1
CRAM_FILES=$2
REGION=$3
OUT_TYPE=$4

## SiTES points to a bed file with list of regions of interest

source $CONFIG 

CRAM_FILE=`head -n $SGE_TASK_ID $CRAM_FILES | tail -n 1`
CNAME=`basename $CRAM_FILE`
ID=${CNAME%.cram}
DNAME=`dirname $CRAM_FILE`
BAM_FILE=$DNAME/$ID.bam
SAMPLE_ID=${CNAME%-ready.cram}
DIR=$ALIGNMENTS/$SAMPLE_ID

## For bam-readcounts
OUTPUT_DIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/bamreadcounts
PARSE_OUTPUT=/exports/igmm/eddie/Glioblastoma-WGS/scripts/bamreadcounts_parse_brc.py


if [ ! -f $BAM_FILE ]
then

    . /etc/profile.d/modules.sh
    MODULEPATH=$MODULEPATH:/exports/igmm/software/etc/el7/modules

    module load igmm/libs/htslib/1.6
    module load igmm/apps/samtools/1.6

    ## Do not need reference genome if switching from cram to bam
    ##REFERENCE_GENOME=/gpfs/igmmfs01/software/pkg/el7/apps/bcbio/share2/genomes/Hsapiens/hg38/seq/hg38.fa

    # Basic info
    echo `date` "This job is running on $HOSTNAME" >> $SGE_O_WORKDIR/$ID.conversion.$JOB_ID.$SGE_TASK_ID.log

    if [ -e $CRAM_FILE.md5 ]
    then

    # Check md5sums for original cram and associated index
    echo `date` "Checking md5sums for cram file and associated index" >> $SGE_O_WORKDIR/$ID.conversion.$JOB_ID.$SGE_TASK_ID.log
    cd $DNAME
    if md5sum --status -c $CRAM_FILE.md5
    then
        echo `date` "Cram file $CRAM_FILE md5sum OK" >> $SGE_O_WORKDIR/$ID.conversion.$JOB_ID.$SGE_TASK_ID.log
    else
        echo `date` "Cram file $CRAM_FILE md5sum FAIL" >> $SGE_O_WORKDIR/$ID.conversion.$JOB_ID.$SGE_TASK_ID.log
        exit 1
    fi
    
    if md5sum --status -c $CNAME*.crai.md5
    then
        echo `date` "Cram cram.crai $CRAM_FILE.crai md5sum OK" >> $SGE_O_WORKDIR/$ID.conversion.$JOB_ID.$SGE_TASK_ID.log
    else
        echo `date` "Cram cram.crai $CRAM_FILE.crai md5sum FAIL" >> $SGE_O_WORKDIR/$ID.conversion.$JOB_ID.$SGE_TASK_ID.log
        exit 1
    fi

    # Return to working directory
    cd $SGE_O_WORKDIR
    fi

    # Generate flagstat report for CRAM file
    if [ ! -f $CRAM_FILE.flagstat ]; then
    echo `date` "samtools flagstat for cram file" >> $SGE_O_WORKDIR/$ID.conversion.$JOB_ID.$SGE_TASK_ID.log
    samtools flagstat $CRAM_FILE > $CRAM_FILE.flagstat
    fi

    # Convert cram to bam, index and flagstat
    echo `date` "BAM decompression from cram" >> $SGE_O_WORKDIR/$ID.conversion.$JOB_ID.$SGE_TASK_ID.log
    samtools view -@ $NSLOTS -h -b $CRAM_FILE -o $BAM_FILE
    echo `date` "Indexing bam file $ID.bam" >> $SGE_O_WORKDIR/$ID.conversion.$JOB_ID.$SGE_TASK_ID.log
    samtools index $BAM_FILE
    echo `date` "samtools flagstat for bam file" >> $SGE_O_WORKDIR/$ID.conversion.$JOB_ID.$SGE_TASK_ID.log
    samtools flagstat $BAM_FILE > $BAM_FILE.flagstat

    # Perform quick check on newly created bam
    echo `date` "samtools quickcheck on new bam" >> $SGE_O_WORKDIR/$ID.conversion.$JOB_ID.$SGE_TASK_ID.log
    if samtools quickcheck $BAM_FILE
    then
    echo `date` "New bam $BAM_FILE passes samtools quickcheck" >> $SGE_O_WORKDIR/$ID.conversion.$JOB_ID.$SGE_TASK_ID.log
    else
    echo `date` "New bam $BAM_FILE fails samtools quickcheck" >> $SGE_O_WORKDIR/$ID.conversion.$JOB_ID.$SGE_TASK_ID.log
    exit 1
    fi

    # Compare flagstat info from cram
    echo `date` "Compare flagstat from cram to bam" >> $SGE_O_WORKDIR/$ID.conversion.$JOB_ID.$SGE_TASK_ID.log
    if cmp $CRAM_FILE.flagstat $BAM_FILE.flagstat
    then
    echo `date` "$CRAM_FILE and $BAM_FILE samtools flagstat match" >> $SGE_O_WORKDIR/$ID.conversion.$JOB_ID.$SGE_TASK_ID.log
    else
    echo `date` "$CRAM_FILE and $BAM_FILE samtools flagstat do not match" >> $SGE_O_WORKDIR/$ID.conversion.$JOB_ID.$SGE_TASK_ID.log
    exit 1
    fi

    # Create md5sums for bam file and its index
    echo `date` "Calculate md5sums for bam file $ID.bam and its index $ID.bam.bai" >> $SGE_O_WORKDIR/$ID.conversion.$JOB_ID.$SGE_TASK_ID.log
    cd $DNAME
    BNAME=`basename $BAM_FILE`
    md5sum $BNAME > $BNAME.md5
    md5sum $BNAME.bai > $BNAME.bai.md5

    # All done
    echo `date` "Conversion complete" >> $SGE_O_WORKDIR/$ID.conversion.$JOB_ID.$SGE_TASK_ID.log

fi

if [ -f $BAM_FILE ]
then
echo "Cram to bam done, running bam-readcount to extract coverage at base pair resolution within ${REGION}..."
bam-readcount -f $REFERENCE $BAM_FILE $REGION > $OUTPUT_DIR/${SAMPLE_ID}_${REGION}.txt
python $PARSE_OUTPUT $OUTPUT_DIR/${SAMPLE_ID}_${REGION}.txt > $OUTPUT_DIR/${SAMPLE_ID}_${REGION}_parsed.txt
fi

### Do not run this if bams have not been staged to datastore!!!
###if [ -f $OUTPUT_DIR/${SAMPLE_ID}_${REGION}_parsed.txt ]
####then
###echo "${SAMPLE_ID}_${REGION}_parsed.txt generated, deleting alignment files..."
####rm -rf $DIR
###fi




















