#!/bin/bash

#$ -N seqtk
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=16G
#$ -pe sharedmem 16
#$ -l h_rt=120:00:00

## Based on https://www.biostars.org/p/374959/
## For scATACseq R2 fastqs
## Trim other reads based on reads with barcodes (R2) of length 24bp

CONFIG=$1

source $CONFIG

SEQTK=/exports/igmm/eddie/Glioblastoma-WGS/scripts/seqtk/seqtk

## GEX:

GEX_FASTQ_DIR=/exports/igmm/eddie/Glioblastoma-WGS/scmultiome/fastqs/gex/test/LEB02_E34
R1_FILE=E34_S1_R1_001.fastq.gz
R2_FILE=E34_S1_R2_001.fastq.gz
I1_FILE=E34_S1_I1_001.fastq.gz
I2_FILE=E34_S1_I2_001.fastq.gz

cd $GEX_FASTQ_DIR

$SEQTK comp $R1_FILE | awk '{ if (($2 == 28)) { print} }' | cut -f 1 > selected-sequences-names.list
$SEQTK subseq $R1_FILE selected-sequences-names.list | gzip > ${R1_FILE%.fastq.gz}_trimmed.fastq.gz
$SEQTK subseq $R2_FILE selected-sequences-names.list | gzip > ${R2_FILE%.fastq.gz}_trimmed.fastq.gz
$SEQTK subseq $I1_FILE selected-sequences-names.list | gzip > ${I1_FILE%.fastq.gz}_trimmed.fastq.gz
$SEQTK subseq $I2_FILE selected-sequences-names.list | gzip > ${I2_FILE%.fastq.gz}_trimmed.fastq.gz

mv ${R1_FILE%.fastq.gz}_trimmed.fastq.gz $R1_FILE
mv ${R2_FILE%.fastq.gz}_trimmed.fastq.gz $R2_FILE
mv ${I1_FILE%.fastq.gz}_trimmed.fastq.gz $I1_FILE
mv ${I2_FILE%.fastq.gz}_trimmed.fastq.gz $I2_FILE

rm selected-sequences-names.list

fastqc -t 20 *fastq.gz

## ATAC

ATAC_FASTQ_DIR=/exports/igmm/eddie/Glioblastoma-WGS/scmultiome/fastqs/atac/test/LEB01_E34
R1_FILE=E34_S1_R1_001.fastq.gz
R2_FILE=E34_S1_R2_001.fastq.gz
R3_FILE=E34_S1_R3_001.fastq.gz
I1_FILE=E34_S1_I1_001.fastq.gz

cd $ATAC_FASTQ_DIR

$SEQTK comp $R2_FILE | awk '{ if (($2 == 16)) { print} }' | cut -f 1 > selected-sequences-names.list
$SEQTK subseq $R1_FILE selected-sequences-names.list | gzip > ${R1_FILE%.fastq.gz}_trimmed.fastq.gz
$SEQTK subseq $R2_FILE selected-sequences-names.list | gzip > ${R2_FILE%.fastq.gz}_trimmed.fastq.gz
$SEQTK subseq $R3_FILE selected-sequences-names.list | gzip > ${R3_FILE%.fastq.gz}_trimmed.fastq.gz
$SEQTK subseq $I1_FILE selected-sequences-names.list | gzip > ${I1_FILE%.fastq.gz}_trimmed.fastq.gz

mv ${R1_FILE%.fastq.gz}_trimmed.fastq.gz $R1_FILE
mv ${R2_FILE%.fastq.gz}_trimmed.fastq.gz $R2_FILE
mv ${R3_FILE%.fastq.gz}_trimmed.fastq.gz $R3_FILE
mv ${I1_FILE%.fastq.gz}_trimmed.fastq.gz $I1_FILE

rm selected-sequences-names.list

fastqc -t 20 *fastq.gz

##
