#!/bin/bash

# To run this script, do 
# qsub -t 1-n submit_merge_samples_vcfs.sh CONFIG BATCHID BATCH
#
#$ -N merge_vcfs
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=16G
#$ -pe sharedmem 2
#$ -l h_rt=12:00:00

unset MODULEPATH
. /etc/profile.d/modules.sh

CONFIG=$1
BATCHID=$2
BATCH=$3

####################################################################################################################################
## 
## bcftools merge
## 
## Merge multiple VCF/BCF files from non-overlapping sample sets to create one multi-sample file.
## ***Only run when all sample vcf files normalised and sorted. ***
##
####################################################################################################################################

bcftools merge \
    --merge both \
    $CONSENSUS_DIR/${BATCHID}*.vcf.gz | \
    bcftools sort \
    -Oz \
    -o $CONSENSUS_DIR/${BATCH}.consensus.vcf.gz

bcftools index $CONSENSUS_DIR/${BATCH}.consensus.vcf.gz

