#!/bin/bash
#
# Data staging job script that copies a directory from DataStore to Eddie with rsync
# 
# Job will restart from where it left off if it runs out of time 
# (so setting an accurate hard runtime limit is less important)
# So set up acceptable minimum time limit but don't set up too long ie >24 hours as it won't get
# to run quickly on SGE.

#$ -N staging 
#$ -cwd
# Choose the staging environment
#$ -q staging
#$ -l h_rt=24:00:00 

# Make the job resubmit itself if it runs out of time: rsync will start where it left off
#$ -r yes
#$ -notify
###trap 'exit 99' sigusr1 sigusr2 sigterm

SOURCE=$1
DESTINATION=$2

# Source and destination directories
rsync -rt ${SOURCE} ${DESTINATION}

# Source path on DataStore in the staging environment
# Note: these paths are only available on the staging nodes
# It should start with one of /exports/csce/datastore, /exports/chss/datastore, /exports/cmvm/datastore or /exports/igmm/datastore
# Destination path on Eddie. It should be on the fast Eddie HPC filesystem, starting with one of:
# /exports/csce/eddie, /exports/chss/eddie, /exports/cmvm/eddie, /exports/igmm/eddie or /exports/eddie/scratch, 
# Note: do not use -p or -a (implies -p) as this can break file ACLs at the destination





