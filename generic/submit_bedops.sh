#!/bin/bash

# To run this script, do 
# qsub submit_bedops.sh
#
#$ -N bedops
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=32G
#$ -l h_rt=96:00:00

unset MODULEPATH
. /etc/profile.d/modules.sh

export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/snakemake/bin:$PATH
export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/:$PATH


cd /exports/igmm/eddie/Glioblastoma-WGS/analysis

vcf2bed --keep-header < GCGR.consensus.vcf > GCGR.consensus.bed


