#!/bin/bash

#$ -N BWA_index
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=55G
#$ -l h_rt=3:00:00

CONFIG=$1
REF=$2
source $CONFIG
bwa index $REF
