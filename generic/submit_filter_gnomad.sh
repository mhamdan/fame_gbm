#!/bin/bash

#$ -N filter_g
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=8G
#$ -l h_rt=1:00:00
#$ -pe sharedmem 8

CONFIG=$1
PARAM_FILE=$2

####SGE_TASK_ID=2
####CONFIG=/exports/igmm/eddie/Glioblastoma-WGS/scripts/fame_gbm/config.sh
####PARAM_FILE=/exports/igmm/eddie/Glioblastoma-WGS/WGS/params/purple_linx_v2_params.txt

source $CONFIG

PATIENT_ID=`head -n $SGE_TASK_ID $PARAM_FILE | tail -n 1 | cut -f 1`
STAGE=`head -n $SGE_TASK_ID $PARAM_FILE | tail -n 1 | cut -f 3`
TUMOR_SNPEFF_VCF_ID=`head -n $SGE_TASK_ID $PARAM_FILE | tail -n 1 | cut -f 4`
SNPEFF_VCF_FILE=`head -n $SGE_TASK_ID $PARAM_FILE | tail -n 1 | cut -f 5`
OUTPUT_DIR=$ENSEMBLE_DIR/${STAGE}/${PATIENT_ID}
INPUT_VCF=${PATIENT_ID}.ssv.snpeff.vcf
OUTPUT_VCF=${PATIENT_ID}.ssv.snpeff.gnomad.filtered.vcf

echo $OUTPUT_DIR
echo $INPUT_VCF
echo $OUTPUT_VCF

## Index variant VCF file
## Remove .gz extension from $SNPEFF_VCF_FILE
mkdir -p $OUTPUT_DIR

cp ${SNPEFF_VCF_FILE%.gz} $OUTPUT_DIR ## Copy the VCF file to the output directory
cd $OUTPUT_DIR && bgzip $INPUT_VCF
bcftools index ${INPUT_VCF}.gz

## Find overlap between VCF file and gnomAD
bcftools isec -i 'INFO/AF>=0.01' -p $OUTPUT_DIR -n=2 $GATK_AF_GNOMAD ${INPUT_VCF}.gz

## Filter out variants that are in gnomAD
cp $OUTPUT_DIR/0001.vcf $OUTPUT_DIR/$OUTPUT_VCF
bgzip $OUTPUT_DIR/$OUTPUT_VCF
