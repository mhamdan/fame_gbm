#!/bin/bash

# To run this script, do 
# qsub -t 1-n submit_batch_liftover.sh IDS
#
# CONFIG is the path to the file scripts/config.sh which contains environment variables set to
# commonly used paths and files in the script
# IDS is a list of sample ids, one per line, where tumor and normal samples are designated by
# the addition of a T or an N to the sample id.
# TYPE is either sample name or input
#
#$ -N liftover
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=16G
#$ -l h_rt=72:00:00
#$ -pe sharedmem 10

CONFIG=$1
IDS=$2

SAMPLE_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1`
source $CONFIG

## For VCF, use picard's 

##WORK_DIR=$BCBIO_WORK/$PATIENT_ID
##JVM_OPTS="-Dsamjdk.use_async_io_read_samtools=false -Dsamjdk.use_async_io_write_samtools=true -Dsamjdk.use_async_io_write_tribble=false -Dsamjdk.compression_level=1 -Xms16g -Xmx16g"
##JVM_TMP_DIR="-Djava.io.tmpdir=$WORK_DIR/bcbiotx"

#bcftools view -Ov -o $CONSENSUS_DIR/${PATIENT_ID}.consensus.vcf $CONSENSUS_DIR/${PATIENT_ID}.consensus.vcf.gz

#java $JVM_OPTS $JVM_TMP_DIR -jar $PICARD_JAR LiftoverVcf \
#     I=$CONSENSUS_DIR/${PATIENT_ID}.consensus.vcf \
#     O=$CONSENSUS_DIR/${PATIENT_ID}.consensus.hg37.vcf \
#     CHAIN=$CHAIN38TO37 \
#     REJECT=$CONSENSUS_DIR/${PATIENT_ID}.consensus.hg37.rejected.vcf \
#     R=$REFERENCE_HG37

## For bedfiles, use UCSC liftover tool: 
#FILE_DIR=/exports/igmm/eddie/Glioblastoma-WGS/ATAC-seq/results/PDirks/narrowPeak
#CHAIN_19to38=/exports/igmm/eddie/Glioblastoma-WGS/resources/liftover/hg19ToHg38.over.chain
#cd $FILE_DIR

export PATH=/exports/igmm/eddie/Glioblastoma-WGS/resources/liftover/hgdownload.cse.ucsc.edu/admin/exe/linux.x86_64:$PATH
INPUT=/exports/igmm/eddie/Glioblastoma-WGS/ChIP-seq/H3K27ac-seq_GBM/results/peaks/Roadmap_hg19/${SAMPLE_ID}-H3K27ac.narrowPeak.gz
OUTPUT=/exports/igmm/eddie/Glioblastoma-WGS/ChIP-seq/H3K27ac-seq_GBM/results/peaks/${SAMPLE_ID}_peaks.narrowPeak.gz
UNMAPPED=/exports/igmm/eddie/Glioblastoma-WGS/ChIP-seq/H3K27ac-seq_GBM/results/peaks/Roadmap_hg19/${SAMPLE_ID}-H3K27ac.narrowPeak.unmapped
CHAIN=/exports/igmm/eddie/Glioblastoma-WGS/resources/liftover/hg19ToHg38.over.chain

liftOver -bedPlus=6 \
    $INPUT \
    $CHAIN \
    $OUTPUT \
    $UNMAPPED
    
    
    
    
    
    
    
    
    
    
