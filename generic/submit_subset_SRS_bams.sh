#!/bin/bash

## SGE options
#$ -N subset_SRS_bams
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=16G
#$ -pe sharedmem 16
#$ -l h_rt=200:00:00

IDS=$1
BAM_DIR=$2
CHROM=$3

SAMPLE_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1`

export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/snakemake/bin:$PATH

##for chrom in `seq 1 22`
###do
##    samtools view -@ 16 -b $SRS_BAM chr${chrom} > ${SRS_BAM_DIR}/${SAMPLE}${TYPE}.chr${chrom}.bam
##    samtools index -@ 16 ${SRS_BAM_DIR}/${SAMPLE}${TYPE}.chr${chrom}.bam
##    samtools flagstat ${SRS_BAM_DIR}/${SAMPLE}${TYPE}.chr${chrom}.bam
##done

cd $BAM_DIR
samtools flagstat ${SAMPLE_ID}.final.bam > ${SAMPLE_ID}.final.stats.out
samtools view -@ 16 -b ${SAMPLE_ID}.final.bam ${CHROM} > ${SAMPLE_ID}.${CHROM}.final.bam
samtools index -@ 16 ${SAMPLE_ID}.${CHROM}.final.bam
samtools flagstat ${SAMPLE_ID}.${CHROM}.final.bam > ${SAMPLE_ID}.${CHROM}.final.stats.out




