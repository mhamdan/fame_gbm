#!/bin/bash

### Adapted from Alison Meynert
#$ -N prep_bams
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_rt=220:00:00
#$ -l h_vmem=16G
#$ -pe sharedmem 16

# Parameter file consists of sample ID in first column and full bam file name in second column

CONFIG=$1
PARAMS=$2

ORIGINAL_BAM_ID=`head -n $SGE_TASK_ID $PARAMS | tail -n 1 | cut -f 1`
SAMPLE_ID=`head -n $SGE_TASK_ID $PARAMS | tail -n 1 | cut -f 2`

source $CONFIG

BAM_DIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/raw/source/DKFZ/bams
FASTQ_DIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/raw/source/DKFZ/fastqs/recurrent

##### QC and convert to fastq files ##### 

# 1) Validate
cd $BAM_DIR
#echo "Current directory is $PWD"
#echo "Processing for ${ORIGINAL_BAM_ID}.bam"
#echo "Creating tmp directory and validating ${ORIGINAL_BAM_ID}.bam"
#mkdir -p tmp/${ORIGINAL_BAM_ID}
#java -Djava.io.tmpdir=tmp/${ORIGINAL_BAM_ID} -Xmx16g -jar /exports/igmm/eddie/NextGenResources/software/picard-2.9.4/picard.jar \
#    ValidateSamFile \
#    I=${ORIGINAL_BAM_ID}.bam \
#    MODE=SUMMARY \
#    OUTPUT=${ORIGINAL_BAM_ID}.validate.summary.txt &> ${ORIGINAL_BAM_ID}.validate.log
#rm -r tmp/${ORIGINAL_BAM_ID}

# 2) Flagstat original bam file
#samtools flagstat ${ORIGINAL_BAM_ID}.bam > ${ORIGINAL_BAM_ID}.flagstats.txt

# 3) Sort the bam file
echo "Sorting ${ORIGINAL_BAM_ID}.bam"
WORK_DIR=$BCBIO_WORK/${ORIGINAL_BAM_ID}
mkdir -p $WORK_DIR
##sambamba sort \
##    -t 12 -m 16G \
##    -n -p \
##    -o $WORK_DIR/${ORIGINAL_BAM_ID}.query_sorted.bam \
##    --tmpdir=$WORK_DIR \
##    ${ORIGINAL_BAM_ID}.bam
samtools sort -n -@ 10 -T ${SAMPLE_ID} -o $WORK_DIR/${ORIGINAL_BAM_ID}.query_sorted.bam ${ORIGINAL_BAM_ID}.bam

# 4) Recheck the flagstat of sorted BAM files
samtools flagstat $WORK_DIR/${ORIGINAL_BAM_ID}.query_sorted.bam > $WORK_DIR/${ORIGINAL_BAM_ID}.query_sorted.bam.flagstats.txt

# 5) Check differences between the two txt files to see discrepancies
echo "flagstats diff: BEGIN"
diff ${ORIGINAL_BAM_ID}.flagstats.txt $WORK_DIR/${ORIGINAL_BAM_ID}.query_sorted.bam.flagstats.txt
echo "flagstats diff: END"

# 6) Convert to fastq files
echo "Sorted bam file validated"

#echo "Converting to fastq files with ${SAMPLE_ID} as prefix"
rm $FASTQ_DIR/${SAMPLE_ID}_R1.fastq
rm $FASTQ_DIR/${SAMPLE_ID}_R2.fastq

bedtools bamtofastq \
    -i $WORK_DIR/${ORIGINAL_BAM_ID}.query_sorted.bam \
    -fq $FASTQ_DIR/${SAMPLE_ID}_R1.fastq \
    -fq2 $FASTQ_DIR/${SAMPLE_ID}_R2.fastq
    
echo "Fastq files generated, gzipping.."
if [ -f $FASTQ_DIR/${SAMPLE_ID}_R2.fastq ]; then
gzip -f $FASTQ_DIR/${SAMPLE_ID}*.fastq
fi

# 7) Fastqc
echo "Fastqc..."
module load igmm/apps/FastQC/0.11.4
fastqc -t 20 $FASTQ_DIR/${SAMPLE_ID}_R1.fastq.gz
fastqc -t 20 $FASTQ_DIR/${SAMPLE_ID}_R2.fastq.gz

#rm -rf $WORK_DIR
##echo "Preprocessing of original bam files done. Intermediate files deleted. Mapping using BWA.."

## Align, sort, index, and statistics.
##cd $SE_DIR
##bwa mem -M -t 20 $BWA_REF ${SAMPLE_ID}.fastq.gz | samtools sort -@ 10 - -T ${SAMPLE_ID} -o $BAM_DIR/${SAMPLE_ID}.sorted.bam
##echo "Indexing..."
##samtools index $BAM_DIR/${SAMPLE_ID}.sorted.bam
##samtools flagstat $BAM_DIR/${SAMPLE_ID}.sorted.bam > $BAM_DIR/${SAMPLE_ID}.stats.out


########### Done ############





