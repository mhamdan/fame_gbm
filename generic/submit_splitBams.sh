#!/bin/bash

#$ -N splitbams
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=8G
#$ -l h_rt=24:00:00
#$ -pe sharedmem 8

CONFIG=$1
IDS=$2
###CHIP=$3

source $CONFIG

SAMPLE_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1`

## For chip data:
##samtools view -b -h ${SAMPLE_ID}_${CHIP}_Rep1.final.bam chr7:1-159345973 -T $REFERENCE > ${SAMPLE_ID}_${CHIP}_Rep1.final.chr7.bam
##samtools view -b -h ${SAMPLE_ID}_${CHIP}_Rep2.final.bam chr7:1-159345973 -T $REFERENCE > ${SAMPLE_ID}_${CHIP}_Rep2.final.chr7.bam

## For input data:
##if [[ ! -f ${SAMPLE_ID}_Input_Rep2.final.chr7.bam ]]
##then
##samtools view -b -h ${SAMPLE_ID}_Input_Rep1.final.bam chr7:1-159345973 -T $REFERENCE > ${SAMPLE_ID}_Input_Rep1.final.chr7.bam
##samtools view -b -h ${SAMPLE_ID}_Input_Rep2.final.bam chr7:1-159345973 -T $REFERENCE > ${SAMPLE_ID}_Input_Rep2.final.chr7.bam
##fi

#bamtools split -in ${SAMPLE_ID}_${CHIP}_Rep1.final.bam -reference
#bamtools split -in ${SAMPLE_ID}_${CHIP}_Rep2.final.bam -reference
#bamtools split -in ${SAMPLE_ID}_Input_Rep1.final.bam -reference
#bamtools split -in ${SAMPLE_ID}_Input_Rep2.final.bam -reference

cd /exports/igmm/eddie/Glioblastoma-WGS/ChIP-seq/H3K27ac-seq_GBM/bams
samtools view -b ${SAMPLE_ID}.final.bam chr1 > ${SAMPLE_ID}.chr1.final.bam
samtools index ${SAMPLE_ID}.chr1.final.bam






