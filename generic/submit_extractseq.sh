#!/bin/bash

# To run this script, do 
# qsub -t 1-n submit_extractseq.sh CONFIG IDS
#
#$ -N extractseq
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=32G
#$ -pe sharedmem 8
#$ -l h_rt=96:00:00

unset MODULEPATH
. /etc/profile.d/modules.sh

CONFIG=$1
IDS=$2
TYPE=$3

source $CONFIG

PATIENT_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1`
INPUT=/exports/igmm/eddie/Glioblastoma-WGS/SOX/blast/${PATIENT_ID}${TYPE}.fa
OUTPUT=/exports/igmm/eddie/Glioblastoma-WGS/SOX/blast/${PATIENT_ID}${TYPE}.subset.fa

BAM_DIR=$ALIGNMENTS/${PATIENT_ID}${TYPE}/${PATIENT_ID}${TYPE}
ALIGNED_BAM_FILE=$BAM_DIR/${PATIENT_ID}${TYPE}-ready.bam
SUBSET_ALIGNED_BAM_FILE=$BAM_DIR/${PATIENT_ID}${TYPE}-subset.bam

#if [ ! -f $WORK_DIR/bcbiotx ]; then mkdir -p $WORK_DIR/bcbiotx; fi
#WORK_DIR=$BCBIO_WORK/$PATIENT_ID
#WORK_DIR=$BCBIO_WORK/generic
#JVM_OPTS="-Dsamjdk.use_async_io_read_samtools=false -Dsamjdk.use_async_io_write_samtools=true -Dsamjdk.use_async_io_write_tribble=false -Dsamjdk.compression_level=1 -Xms16g -Xmx16g"
#JVM_TMP_DIR="-Djava.io.tmpdir=$WORK_DIR/bcbiotx"

## Create an alternative reference sequence by combining a fasta with a vcf, within specified intervals.
## Based on -> https://gatk.broadinstitute.org/hc/en-us/articles/360037594571-FastaAlternateReferenceMaker

#java $JVM_OPTS $JVM_TMP_DIR -jar $GATK4_JAR FastaAlternateReferenceMaker \
#    -R $REFERENCE \
#    -O $OUTPUT \
#    -L $INTERVAL_LIST \
#    -V $VCF

## Extract sequences from reference genome (hg38), given interval list from GATK
## Based on -> https://gatk.broadinstitute.org/hc/en-us/articles/360037592571-ExtractSequences-Picard-

##seqkit faidx -f $INPUT 

#java $JVM_OPTS $JVM_TMP_DIR -jar $PICARD_JAR ExtractSequences \
#      INTERVAL_LIST=/exports/igmm/eddie/Glioblastoma-WGS/resources/Mutect2_resources/wgs_calling_regions.hg38.picard_interval_list \
#      R=$INPUT \
#      O=$OUTPUT


## Based on https://bedtools.readthedocs.io/en/latest/content/tools/getfasta.html
#samtools faidx $INPUT

#bedtools getfasta \
#    -fi $INPUT \
#    -bed /exports/igmm/eddie/Glioblastoma-WGS/resources/Mutect2_resources/wgs_calling_regions.hg38.interval_list.bed

#samtools view \
#    -L /exports/igmm/eddie/Glioblastoma-WGS/resources/Mutect2_resources/wgs_calling_regions.hg38.interval_list.bed \
#    -b $ALIGNED_BAM_FILE >$BAM_DIR/${PATIENT_ID}${TYPE}.subset.bam

#Try bedtools
# 1) subset bam file and index it
bedtools intersect -abam $ALIGNED_BAM_FILE \
    -b /exports/igmm/eddie/Glioblastoma-WGS/resources/Mutect2_resources/wgs_calling_regions.hg38.interval_list.bed > $SUBSET_ALIGNED_BAM_FILE

# 2) convert to fasta




# To prepare the picard style interval list.
#java $JVM_OPTS $JVM_TMP_DIR -jar $PICARD_JAR BedToIntervalList \
#      I=/exports/igmm/eddie/Glioblastoma-WGS/resources/Mutect2_resources/wgs_calling_regions.hg38.interval_list.bed \
#     SD=/exports/igmm/eddie/Glioblastoma-WGS/resources/refgenome38/hg38.dict
#      O=/exports/igmm/eddie/Glioblastoma-WGS/resources/Mutect2_resources/wgs_calling_regions.hg38.picard_interval_list \

