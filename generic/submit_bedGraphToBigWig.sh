#!/bin/bash

# To run this script, do 
# qsub -t 1-n submit_bedGraphToBigWig.sh IDS
#
# CONFIG is the path to the file scripts/config.sh which contains environment variables set to
# commonly used paths and files in the script
# IDS is a list of sample ids, one per line, where tumor and normal samples are designated by
# the addition of a T or an N to the sample id.
# 
#
#$ -N bedGraphToBigWig
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=8G
#$ -l h_rt=48:00:00
###$ -pe sharedmem 10

CONFIG=$1
##IDS=$2

##SAMPLE_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1`
FILE_DIR=/exports/igmm/eddie/Glioblastoma-WGS/ChIP-seq/SOX_TF/qc/deeptools/coverage

source $CONFIG

## Based on https://www.biostars.org/p/176875/
## Bedgraph files have been lifted to hg38, and sorted using sort -k1,1 -k2,2n

cd $FILE_DIR

## Make sure bedgraph has 4 columns only.

####awk '{print $1,$2,$3,$4}' ${SAMPLE_ID}_lifted_hg38_sorted.sortedFE.bedgraph > ${SAMPLE_ID}_lifted_hg38_sorted.sortedFE.4.bedgraph

for file in *; do bedGraphToBigWig $file $HG38_CHROM_SIZE ${file%.bedgraph}.bw; done 



