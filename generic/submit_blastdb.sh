#!/bin/bash

# To run this script, do 
# qsub -t 1-n submit_blastdb.sh CONFIG IDS TYPE
#
#$ -N blastdb
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=16G
#$ -pe sharedmem 16
#$ -l h_rt=24:00:00

unset MODULEPATH
. /etc/profile.d/modules.sh

CONFIG=$1
IDS=$2
TYPE=$3

source $CONFIG

PATIENT_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1`
SAMPLE_ID=${PATIENT_ID}${TYPE}
##INPUT=$BLAST/$PATIENT_ID.fa

cd $BLAST

##Build fasta reference indices for each tumour 

makeblastdb -in ${SAMPLE_ID}-subset.fa -title "subsetTCGA" -dbtype nucl
##makeblastdb -in hg38_intervals.fa -title "hg38_interval" -dbtype nucl




