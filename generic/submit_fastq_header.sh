#!/bin/bash

# To run this script, do 
# qsub -t 1-n submit_fastq_header.sh CONFIG IDS TYPE
#
#$ -N qcfastqheader
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=16G
#$ -pe sharedmem 16
#$ -l h_rt=120:00:00

unset MODULEPATH
. /etc/profile.d/modules.sh

CONFIG=$1
IDS=$2
TYPE=$3

PATIENT_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1`

source $CONFIG

cd $READS

zcat ${PATIENT_ID}${TYPE}_R1.fastq.gz | awk '{print (NR%4 == 1) ? "@"++i "/1" : $0}' > ${PATIENT_ID}${TYPE}_R1.fastq

if [[ -f ${PATIENT_ID}${TYPE}_R1.fastq ]]
then
gzip -f ${PATIENT_ID}${TYPE}_R1.fastq
fi

zcat ${PATIENT_ID}${TYPE}_R2.fastq.gz | awk '{print (NR%4 == 1) ? "@"++i "/2" : $0}' > ${PATIENT_ID}${TYPE}_R2.fastq

if [[ -f ${PATIENT_ID}${TYPE}_R2.fastq ]]
then
gzip -f ${PATIENT_ID}${TYPE}_R2.fastq
fi




##gunzip $INPUT.gz
##sed -i "s/${SEQUENCER_ID}//g" $INPUT
##gzip $INPUT
##zcat ${PATIENT_ID}${TYPE}_R2.fastq.gz | awk '{print (NR%4 == 1) ? "@1_" ++i : $0}' | gzip -c > ${PATIENT_ID}${TYPE}_R2.fastq.gz




