#!/bin/bash

#This script is to convert a bam file to a fasta file.  
#To run this script, do qsub -t <1-n> submit_bam_to_fasta.sh <CONFIG> <IDS> <TYPE>

#$ -N bam_to_fasta
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=16G
#$ -l h_rt=48:00:00
#$ -pe sharedmem 16

unset MODULEPATH
. /etc/profile.d/modules.sh

CONFIG=$1
IDS=$2
TYPE=$3

source $CONFIG

PATIENT_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1`
SAMPLE_ID=${PATIENT_ID}${TYPE}

INPUT_DIR=$ALIGNMENTS/$SAMPLE_ID/$SAMPLE_ID
BAM_DIR=$ALIGNMENTS/${PATIENT_ID}${TYPE}/${PATIENT_ID}${TYPE}
ALIGNED_BAM_FILE=$BAM_DIR/${PATIENT_ID}${TYPE}-ready.bam
SUBSET_ALIGNED_BAM_FILE=$BAM_DIR/${PATIENT_ID}${TYPE}-subset.bam
SUBSET_FASTQ=$BAM_DIR/${PATIENT_ID}${TYPE}-subset.fastq
SUBSET_FASTA=$BAM_DIR/${PATIENT_ID}${TYPE}-subset.fa

####

bamtofastq filename=$SUBSET_ALIGNED_BAM_FILE > $SUBSET_FASTQ
awk '{ if ('NR%4==1' || 'NR%4==2'){ gsub("@",">",$1); print }}' $SUBSET_FASTQ > $SUBSET_FASTA
rm $SUBSET_FASTQ
mv $SUBSET_FASTA /exports/igmm/eddie/Glioblastoma-WGS/SOX/blast/${PATIENT_ID}${TYPE}-subset.fa


#samtools fasta ${SAMPLE_ID}-ready.bam > ${SAMPLE_ID}.fa
#samtools fasta ${SAMPLE_ID}.subset.bam > ${SAMPLE_ID}.subset.fa
#mv *.fa ../../../../blast/










