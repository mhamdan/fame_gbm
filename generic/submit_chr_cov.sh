#!/bin/bash

# To run this script, do 
# qsub -t n submit_chr_cov.sh IDS TYPE
#
#$ -N chr_cov
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=8G
#$ -pe sharedmem 8
#$ -l h_rt=120:00:00

unset MODULEPATH
. /etc/profile.d/modules.sh

IDS=$1
TYPE=$2

## Define files/directories
PATIENT_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1`
SAMPLE_ID=${PATIENT_ID}${TYPE}

SEQKIT=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/snakemake/bin/seqkit

BAM_DIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/alignments/${SAMPLE_ID}/${SAMPLE_ID}
BAM=${SAMPLE_ID}-ready.bam

cd $BAM_DIR
$SEQKIT bam -C $BAM 2> ${SAMPLE_ID}.counts.tsv




















