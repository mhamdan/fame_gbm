#!/bin/bash

# To run this script, do 
# qsub -t 1-n submit_telomerehunter.sh IDS

#
#$ -N teloHunt
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=8G
#$ -pe sharedmem 8
#$ -l h_rt=64:00:00

export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/snakemake/bin:$PATH
export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/telomerehunter/bin:$PATH

IDS=$1

PATIENT_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1`

## Based on article https://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-019-2851-0#Abs1
## And code/explanation is based here https://www.dkfz.de/en/applied-bioinformatics/telomerehunter/telomerehunter.html#section3
## TelomereHunter takes alignment information into account and reports the abundance of variant repeats in telomeric sequences. 

TUMOR_BAM=/exports/igmm/eddie/Glioblastoma-WGS/WGS/alignments/${PATIENT_ID}T2/${PATIENT_ID}T2/${PATIENT_ID}T2-ready.bam
CONTROL_BAM=/exports/igmm/eddie/Glioblastoma-WGS/WGS/alignments/${PATIENT_ID}N/${PATIENT_ID}N/${PATIENT_ID}N-ready.bam
OUTPUT_DIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/telomerehunter
CYTOBAND=/exports/igmm/eddie/Glioblastoma-WGS/resources/hg38_cytoband.txt

telomerehunter \
    -ibt $TUMOR_BAM \
    -ibc $CONTROL_BAM \
    -o $OUTPUT_DIR \
    -p ${PATIENT_ID}T2 \
    -b $CYTOBAND
       
    
    
    
    
    
    
    
    






