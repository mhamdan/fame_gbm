#!/bin/bash

#$ -N telfusion
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=16G
#$ -pe sharedmem 16
#$ -l h_rt=150:00:00

## Based on https://github.com/cortes-ciriano-lab/telomere_fusions
## Find DNA sequence combinations specifically occuring within regions indicative of telomeric telomere_fusions
## As per https://t.co/QGyulFQ5nR

## SGE INPUT
IDS=$1
BATCH=$2
STAGE=$3
TYPE=$4

PATIENT_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 1`
SAMPLE_ID=${PATIENT_ID}${TYPE}

export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/snakemake/bin:$PATH
export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/telomerefusion/bin:$PATH

## FILE PATHS
ALIGNMENT_FILE=/exports/igmm/eddie/Glioblastoma-WGS/WGS/alignments/${SAMPLE_ID}/${SAMPLE_ID}/${SAMPLE_ID}-ready.bam
COVERAGE_SCRIPT=/exports/igmm/eddie/Glioblastoma-WGS/scripts/telomere_fusions/scripts/coverage_info.sh
FUSION_CALLER=/exports/igmm/eddie/Glioblastoma-WGS/scripts/telomere_fusions/scripts/fusion_caller.py
FUSION_QC=/exports/igmm/eddie/Glioblastoma-WGS/scripts/telomere_fusions/scripts/FusionReadsQC.R
OUTPUT_DIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/telomerefusion
COV_FILE=${OUTPUT_DIR}/coverage/${STAGE}/${SAMPLE_ID}_coverage.txt

## Extract sequencing information:
if [ ! -f $COV_FILE ]; then
    echo "Extracting sequencing information..."
    $COVERAGE_SCRIPT $ALIGNMENT_FILE $COV_FILE $SAMPLE_ID
    ## Edit the original coverage.sh script to include dynamic naming of tmpfile to avoid clashing
fi

cd $OUTPUT_DIR

## Extract telomere repeats (TTAGGG) and inverted telomere repeats (CCCTAA) allowing for one mismatch
if [ ! -f ${SAMPLE_ID}_readIDs ]; then
echo "Extracting telomere repeats..."
samtools view $ALIGNMENT_FILE | python $FUSION_CALLER --mode callfusions  --outprefix ${SAMPLE_ID}
fi 

## Extract mates:
echo "Extracting mates..."
samtools view $ALIGNMENT_FILE  | python $FUSION_CALLER --mode extractmates --outprefix ${SAMPLE_ID}

## Generate summary file:
echo "Generating summary file..."
python $FUSION_CALLER --mode summarise --outprefix ${SAMPLE_ID} --alignmentinfo $COV_FILE

## QC control:
echo "Performing QC..."
Rscript $FUSION_QC --summary_file ${SAMPLE_ID}_fusions_summary --ref_genome Hg38 --outprefix qc/${SAMPLE_ID} --read_length 150 



















