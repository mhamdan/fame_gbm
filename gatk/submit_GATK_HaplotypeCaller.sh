#!/bin/bash

# To run this script, do 
# qsub -t 1-n submit_GATK_HaplotypeCaller.sh CONFIG IDS
#
#$ -N haplotypeCall
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=16G
#$ -pe sharedmem 16
#$ -l h_rt=96:00:00

CONFIG=$1
IDS=$2

PATIENT_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1`
source $CONFIG

### Based on https://gatk.broadinstitute.org/hc/en-us/articles/360037225632-HaplotypeCaller

java $JVM_OPTS $JVM_TMP_DIR -jar $GATK4_JAR HaplotypeCaller  \
   -R $REFERENCE \
   -I $ALIGNED_BAM_FILE_NORMAL \
   -O $GERMLINE_DIR/vcfs/${PATIENT_ID}N.raw.g.vcf.gz \
   -ERC GVCF \
   -G AlleleSpecificAnnotation \
   -G AS_StandardAnnotation \
   -L $INTERVAL_LIST \
   -bamout $GERMLINE_DIR/bams/${PATIENT_ID}N.bamout.bam

   
   
   
