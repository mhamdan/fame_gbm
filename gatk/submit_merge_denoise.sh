#!/bin/bash

# To run this script, do 
# qsub -t 1-n submit_merge_denoise.sh CONFIG IDS TYPE
#
#$ -N merge_denoise
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=16G
#$ -pe sharedmem 2
#$ -l h_rt=2:00:00

unset MODULEPATH
. /etc/profile.d/modules.sh

CONFIG=$1
IDS=$2
TYPE=$3

PATIENT_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1`

source $CONFIG

cd $COPY_RATIOS

awk 'FNR==1 && NR!=1 {{ while (/^@|^CONTIG/) getline; }} 1 {{print}}' \
${PATIENT_ID}${TYPE}.autosomal.standardizedCR.tsv \
${PATIENT_ID}${TYPE}.allosomal.standardizedCR.tsv > ${PATIENT_ID}${TYPE}.merged.standardizedCR.tsv

awk 'FNR==1 && NR!=1 {{ while (/^@|^CONTIG/) getline; }} 1 {{print}}' \
${PATIENT_ID}${TYPE}.autosomal.denoisedCR.tsv \
${PATIENT_ID}${TYPE}.allosomal.denoisedCR.tsv > ${PATIENT_ID}${TYPE}.merged.denoisedCR.tsv


