#!/bin/bash

# To run this script, do 
# qsub -t 1-n submit_GATK_plot_model_segments.sh CONFIG IDS
#
#$ -N plotmodelSegments
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=16G
#$ -pe sharedmem 4
#$ -l h_rt=12:00:00

unset MODULEPATH
. /etc/profile.d/modules.sh

CONFIG=$1
IDS=$2

PATIENT_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1`

source $CONFIG

WORK_DIR=$BCBIO_WORK/$PATIENT_ID
JVM_OPTS="-Dsamjdk.use_async_io_read_samtools=false -Dsamjdk.use_async_io_write_samtools=true -Dsamjdk.use_async_io_write_tribble=false -Dsamjdk.compression_level=1 -Xms16g -Xmx16g"
JVM_TMP_DIR="-Djava.io.tmpdir=$WORK_DIR/bcbiotx"
GATK_JAVA="java $JVM_OPTS $JVM_TMP_DIR -jar $GATK4_JAR"



####################################################################################################################################################################################################
## Plotmodelsegments
####################################################################################################################################################################################################
##
## Use a guassian-kernel binary-segmentation algorithm to group contiguous copy ratios into segments
## Creates plots of denoised and segmented copy-ratio and minor-allele-fraction estimates
##
## See: https://gatk.broadinstitute.org/hc/en-us/articles/360037593891-PlotModeledSegments
##
## --minimum-contig-length set to 46,709,983, the length of the smallest of GRCh38's primary assembly contigs.
##
####################################################################################################################################################################################################

$GATK_JAVA PlotModeledSegments \
          --denoised-copy-ratios $COPY_RATIOS/${PATIENT_ID}T.merged.denoisedCR.tsv \
          --allelic-counts $MODEL_SEGMENTS/${PATIENT_ID}T.hets.tsv \
          --segments $MODEL_SEGMENTS/${PATIENT_ID}T.modelFinal.seg \
          --sequence-dictionary $REFERENCE_DICT \
          --output-prefix ${PATIENT_ID}T \
          --minimum-contig-length 46709983 \
          -O $PLOT_MODEL_SEGMENTS
 






