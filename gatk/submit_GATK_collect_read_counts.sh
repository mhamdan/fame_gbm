#!/bin/bash

# To run this script, do 
# qsub -t 1-n submit_GATK_collect_read_counts.sh CONFIG IDS
#
#$ -N Collect_read_counts
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=16G
#$ -pe sharedmem 4
#$ -l h_rt=12:00:00

unset MODULEPATH
. /etc/profile.d/modules.sh

CONFIG=$1
IDS=$2

PATIENT_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1`

source $CONFIG

WORK_DIR=$BCBIO_WORK/$PATIENT_ID
JVM_OPTS="-Dsamjdk.use_async_io_read_samtools=false -Dsamjdk.use_async_io_write_samtools=true -Dsamjdk.use_async_io_write_tribble=false -Dsamjdk.compression_level=1 -Xms16g -Xmx16g"
JVM_TMP_DIR="-Djava.io.tmpdir=$WORK_DIR/bcbiotx"
GATK_JAVA="java $JVM_OPTS $JVM_TMP_DIR -jar $GATK4_JAR"

if [ ! -f $WORK_DIR/bcbiotx ]; then mkdir -p $WORK_DIR/bcbiotx; fi

################################################################################################################################################################
## 
## Collect read counts at specified intervals for both tumour and normal samples. 
## Based on this: https://gatk.broadinstitute.org/hc/en-us/articles/360037592671-CollectReadCounts
## Doing this separately for auto and allosomal chromosomes
##
################################################################################################################################################################

$GATK_JAVA CollectReadCounts \
    -I $ALIGNED_BAM_FILE_TUMOR \
    -L $PREPROCESSED_INTERVALS/autosomal_preprocessed_intervals.interval_list \
    --interval-merging-rule OVERLAPPING_ONLY \
    -O $READ_COUNTS/${PATIENT_ID}T.autosomal.counts.hdf5
    
$GATK_JAVA CollectReadCounts \
    -I $ALIGNED_BAM_FILE_NORMAL \
    -L $PREPROCESSED_INTERVALS/autosomal_preprocessed_intervals.interval_list \
    --interval-merging-rule OVERLAPPING_ONLY \
    -O $READ_COUNTS/${PATIENT_ID}N.autosomal.counts.hdf5    
    
$GATK_JAVA CollectReadCounts \
    -I $ALIGNED_BAM_FILE_TUMOR \
    -L $PREPROCESSED_INTERVALS/allosomal_preprocessed_intervals.interval_list \
    --interval-merging-rule OVERLAPPING_ONLY \
    -O $READ_COUNTS/${PATIENT_ID}T.allosomal.counts.hdf5
    
$GATK_JAVA CollectReadCounts \
    -I $ALIGNED_BAM_FILE_NORMAL \
    -L $PREPROCESSED_INTERVALS/allosomal_preprocessed_intervals.interval_list \
    --interval-merging-rule OVERLAPPING_ONLY \
    -O $READ_COUNTS/${PATIENT_ID}N.allosomal.counts.hdf5     
    


