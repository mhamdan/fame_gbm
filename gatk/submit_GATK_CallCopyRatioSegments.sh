#!/bin/bash

# To run this script, do 
# qsub -t 1-n submit_GATK_CallCopyRatioSegments.sh CONFIG IDS
#
#$ -N Callcopyratio
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=16G
#$ -pe sharedmem 4
#$ -l h_rt=24:00:00

unset MODULEPATH
. /etc/profile.d/modules.sh

CONFIG=$1
IDS=$2

PATIENT_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1`

source $CONFIG

WORK_DIR=$BCBIO_WORK/$PATIENT_ID
JVM_OPTS="-Dsamjdk.use_async_io_read_samtools=false -Dsamjdk.use_async_io_write_samtools=true -Dsamjdk.use_async_io_write_tribble=false -Dsamjdk.compression_level=1 -Xms16g -Xmx16g"
JVM_TMP_DIR="-Djava.io.tmpdir=$WORK_DIR/bcbiotx"
GATK_JAVA="java $JVM_OPTS $JVM_TMP_DIR -jar $GATK4_JAR"

$GATK_JAVA CallCopyRatioSegments \
    --input $MODEL_SEGMENTS/${PATIENT_ID}T.cr.seg \
    --output $CALLED_COPY_RATIOS/${PATIENT_ID}T.called.seg
    
    
    
    
    