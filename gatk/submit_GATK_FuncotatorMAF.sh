#!/bin/bash

# To run this script, do 
# qsub -t 1-n submit_GATK_FuncotatorMAF.sh CONFIG IDS
#
#$ -N annoMAF
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=16G
#$ -pe sharedmem 8
#$ -l h_rt=48:00:00

CONFIG=$1
IDS=$2

source $CONFIG

PATIENT_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1`

source $CONFIG
WORK_DIR=$BCBIO_WORK/$PATIENT_ID/bcbiotx
mkdir -p $WORK_DIR
JVM_OPTS="-Dsamjdk.use_async_io_read_samtools=false -Dsamjdk.use_async_io_write_samtools=true -Dsamjdk.use_async_io_write_tribble=false -Dsamjdk.compression_level=1 -Xms16g -Xmx16g"
JVM_TMP_DIR="-Djava.io.tmpdir=$WORK_DIR"


####################################################################################################################################
##
## Annotate consensus variants using Funcotator:
##
## This will add GENCODE annotation as described here: https://gatk.broadinstitute.org/hc/en-us/articles/360035531732#1.1.1
## 
####################################################################################################################################

$GATK4 IndexFeatureFile \
     -I $ENSEMBLE_DIR/${PATIENT_ID}.ssv.snpeff.vcf.gz
     

java $JVM_OPTS $JVM_TMP_DIR -jar $GATK4_JAR Funcotator \
    --variant $ENSEMBLE_DIR/${PATIENT_ID}.ssv.snpeff.vcf.gz \
    -R $REFERENCE \
    --ref-version hg38 \
    --data-sources-path $FUNCOTATOR_DIR \
    --transcript-selection-mode CANONICAL \
    --remove-filtered-variants true \
    --annotation-default normal_barcode:${PATIENT_ID}N \
    --annotation-default tumor_barcode:${PATIENT_ID}T \
    --output $ENSEMBLE_DIR/${PATIENT_ID}.funcotated.maf \
    --output-file-format MAF










     
