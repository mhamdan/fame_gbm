#!/bin/bash

# To run this script, do 
# qsub submit_GATK_SelectVariants.sh CONFIG
#
#$ -N selectvariants
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=32G
#$ -l h_rt=12:00:00

unset MODULEPATH
. /etc/profile.d/modules.sh

CONFIG=$1

source $CONFIG

WORK_DIR_GENERIC=$BCBIO_WORK/generic
JVM_OPTS="-Dsamjdk.use_async_io_read_samtools=false -Dsamjdk.use_async_io_write_samtools=true -Dsamjdk.use_async_io_write_tribble=false -Dsamjdk.compression_level=1 -Xms16g -Xmx16g"
JVM_TMP_DIR_GENERIC="-Djava.io.tmpdir=$WORK_DIR_GENERIC"

TEST=/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/ssv/vcf/samples/unfiltered/test

### Unzip 
bcftools view -Ov -o $TEST/DO12034-unfiltered.vcf $TEST/DO12034-unfiltered.vcf.gz

### Normalised
bcftools norm \
   -f $REFERENCE \
   --check-ref we \
   -m-both \
   $TEST/DO12034-unfiltered.vcf | \
   bcftools sort \
   -Oz \
   -o $TEST/DO12034-unfiltered.normalised.vcf.gz

bcftools index -t $TEST/DO12034-unfiltered.normalised.vcf.gz

rm $TEST/DO12034-unfiltered.vcf

### Select SNV 
java $JVM_OPTS $JVM_TMP_DIR_GENERIC -jar $GATK4_JAR SelectVariants \
     -R $REFERENCE \
     -V $TEST/DO12034-unfiltered.normalised.vcf.gz  \
     --select-type-to-include SNP \
     -O $TEST/DO12034-unfiltered.normalised.snv.vcf.gz

### Select non-SNV  
java $JVM_OPTS $JVM_TMP_DIR_GENERIC -jar $GATK4_JAR SelectVariants \
     -R $REFERENCE \
     -V $TEST/DO12034-unfiltered.normalised.vcf.gz  \
     --select-type-to-exclude SNP \
     -O $TEST/DO12034-unfiltered.normalised.non-snv.vcf.gz    
     
     
     