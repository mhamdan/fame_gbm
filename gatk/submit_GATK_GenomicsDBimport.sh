#!/bin/bash

# To run this script, do 
# qsub -t 1-n submit_GATK_GenomicsDBimport.sh CONFIG
#
#$ -N genDBim
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=32G
#$ -l h_rt=200:00:00

CONFIG=$1

source $CONFIG

WORK_DIR_GENERIC=$BCBIO_WORK/generic
JVM_OPTS="-Dsamjdk.use_async_io_read_samtools=false -Dsamjdk.use_async_io_write_samtools=true -Dsamjdk.use_async_io_write_tribble=false -Dsamjdk.compression_level=1 -Xms16g -Xmx16g"
JVM_TMP_DIR_GENERIC="-Djava.io.tmpdir=$WORK_DIR_GENERIC"
GATK_JAVA_GENERIC="java $JVM_OPTS $JVM_TMP_DIR_GENERIC -jar $GATK4_JAR"

## sample_map contains sample ID in first column and path to g.vcf file in second column. it is a text file.
## need at least h_rt 200 hours for 41 samples

$GATK_JAVA_GENERIC GenomicsDBImport \
    -R $REFERENCE \
    -L $INTERVAL_LIST \
    --sample-name-map $GDB_DIR/gvcfs.sample_map \
    --genomicsdb-workspace-path $GDB_DIR/germline_database











