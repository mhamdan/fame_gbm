#!/bin/bash

# To run this script, do 
# qsub -t 1-n submit_GATK_collect_allelic_counts.sh CONFIG IDS TYPE
#
#$ -N collect_allelic_counts
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=16G
#$ -pe sharedmem 8
#$ -l h_rt=96:00:00

unset MODULEPATH
. /etc/profile.d/modules.sh

CONFIG=$1
IDS=$2
TYPE=$3

source $CONFIG

PATIENT_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1`
SAMPLE_ID=${PATIENT_ID}${TYPE}
ALIGNED_BAM_FILE=$ALIGNMENTS/$SAMPLE_ID/$SAMPLE_ID/${SAMPLE_ID}-ready.bam

WORK_DIR=$BCBIO_WORK/$PATIENT_ID
JVM_OPTS="-Dsamjdk.use_async_io_read_samtools=false -Dsamjdk.use_async_io_write_samtools=true -Dsamjdk.use_async_io_write_tribble=false -Dsamjdk.compression_level=1 -Xms16g -Xmx16g"
JVM_TMP_DIR="-Djava.io.tmpdir=$WORK_DIR/bcbiotx"
GATK_JAVA="java $JVM_OPTS $JVM_TMP_DIR -jar $GATK4_JAR"

####################################################################################################################
## CollectAllelicCounts
####################################################################################################################
## Collects reference and alternate allele counts at specified sites.
## This tabulates counts of the reference allele and counts of the dominant alternate common germline variant
## allele for each site in a given genomic intervals list.
## Based on https://gatkforums.broadinstitute.org/gatk/discussion/11683#5
## and https://gatk.broadinstitute.org/hc/en-us/articles/360037594071-CollectAllelicCounts
## and https://gatk.broadinstitute.org/hc/en-us/articles/360035890011?id=11683
##
####################################################################################################################

$GATK_JAVA CollectAllelicCounts \
            -I $ALIGNED_BAM_FILE \
            -L $INTERVAL_LIST_CNV_COMMON_SITES \
            -R $REFERENCE \
            -O $ALLELIC_COUNTS/${SAMPLE_ID}_allelicCounts.tsv \









