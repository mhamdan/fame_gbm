#!/bin/bash

# To run this script, do 
# qsub submit_GATK_cnv_PON.sh CONFIG BATCH CHROMOSOME 
# CHROMOSOME = either autosomal or allosomal
#
#$ -N cnvpon
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=32G
#$ -l h_rt=12:00:00

unset MODULEPATH
. /etc/profile.d/modules.sh

CONFIG=$1
BATCH=$2
CHROMOSOME=$3

source $CONFIG

WORK_DIR_GENERIC=$BCBIO_WORK/generic
JVM_OPTS="-Dsamjdk.use_async_io_read_samtools=false -Dsamjdk.use_async_io_write_samtools=true -Dsamjdk.use_async_io_write_tribble=false -Dsamjdk.compression_level=1 -Xms16g -Xmx16g"
JVM_TMP_DIR_GENERIC="-Djava.io.tmpdir=$WORK_DIR_GENERIC"
GATK_JAVA_GENERIC="java $JVM_OPTS $JVM_TMP_DIR_GENERIC -jar $GATK4_JAR"

####################################################################################################################################################################################################
## 
## CreateReadCountPanelofNormals.
##
## Generate a CNV panel of normals.In creating a PoN, CreateReadCountPanelOfNormals abstracts the counts data for the 
## samples and the intervals using Singular Value Decomposition (SVD, 1), a type of Principal Component Analysis (PCA, 1, 2, 3). 
## The normal samples in the PoN should match the sequencing approach of the case sample under scrutiny. 
## This applies especially to targeted exome data because the capture step introduces target-specific noise.
##
## Based on: https://gatk.broadinstitute.org/hc/en-us/articles/360037227572-CreateReadCountPanelOfNormals
##
####################################################################################################################################################################################################

####$GATK_JAVA_GENERIC GenomicsDBImport \
#    -R $REFERENCE \
#    -L $CNV/preprocessed_intervals.interval_list \
#    --sample-name-map $CNV/hdf5/${BATCH}.cnvpon.sample_map \
#    --genomicsdb-workspace-path $CNV/hdf5/${BATCH}_cnvpon_db


$GATK_JAVA_GENERIC CreateReadCountPanelOfNormals \
    --annotated-intervals $PREPROCESSED_INTERVALS/gc_${CHROMOSOME}_preprocessed_intervals.interval_list \
    -I $READ_COUNTS/E13N.${CHROMOSOME}.counts.hdf5 \
    -I $READ_COUNTS/E15N.${CHROMOSOME}.counts.hdf5 \
    -I $READ_COUNTS/E17N.${CHROMOSOME}.counts.hdf5 \
    -I $READ_COUNTS/E18N.${CHROMOSOME}.counts.hdf5 \
    -I $READ_COUNTS/E1N.${CHROMOSOME}.counts.hdf5 \
    -I $READ_COUNTS/E20N.${CHROMOSOME}.counts.hdf5 \
    -I $READ_COUNTS/E21N.${CHROMOSOME}.counts.hdf5 \
    -I $READ_COUNTS/E22N.${CHROMOSOME}.counts.hdf5 \
    -I $READ_COUNTS/E24N.${CHROMOSOME}.counts.hdf5 \
    -I $READ_COUNTS/E25N.${CHROMOSOME}.counts.hdf5 \
    -I $READ_COUNTS/E26N.${CHROMOSOME}.counts.hdf5 \
    -I $READ_COUNTS/E27N.${CHROMOSOME}.counts.hdf5 \
    -I $READ_COUNTS/E28N.${CHROMOSOME}.counts.hdf5 \
    -I $READ_COUNTS/E3N.${CHROMOSOME}.counts.hdf5 \
    -I $READ_COUNTS/E30N.${CHROMOSOME}.counts.hdf5 \
    -I $READ_COUNTS/E31N.${CHROMOSOME}.counts.hdf5 \
    -I $READ_COUNTS/E34N.${CHROMOSOME}.counts.hdf5 \
    -I $READ_COUNTS/E35N.${CHROMOSOME}.counts.hdf5 \
    -I $READ_COUNTS/E37N.${CHROMOSOME}.counts.hdf5 \
    -I $READ_COUNTS/E43N.${CHROMOSOME}.counts.hdf5 \
    -I $READ_COUNTS/E4N.${CHROMOSOME}.counts.hdf5 \
    -I $READ_COUNTS/E51N.${CHROMOSOME}.counts.hdf5 \
    -I $READ_COUNTS/E53N.${CHROMOSOME}.counts.hdf5 \
    -I $READ_COUNTS/E54N.${CHROMOSOME}.counts.hdf5 \
    -I $READ_COUNTS/E55N.${CHROMOSOME}.counts.hdf5 \
    -I $READ_COUNTS/E56N.${CHROMOSOME}.counts.hdf5 \
    -I $READ_COUNTS/E57N.${CHROMOSOME}.counts.hdf5 \
    -I $READ_COUNTS/E62N.${CHROMOSOME}.counts.hdf5 \
    -I $READ_COUNTS/E63N.${CHROMOSOME}.counts.hdf5 \
    -I $READ_COUNTS/E64N.${CHROMOSOME}.counts.hdf5 \
    -I $READ_COUNTS/E65N.${CHROMOSOME}.counts.hdf5 \
    --minimum-interval-median-percentile 10 \
    -O $READ_COUNTS/${BATCH}.cnv.${CHROMOSOME}.pon.hdf5
    
    


