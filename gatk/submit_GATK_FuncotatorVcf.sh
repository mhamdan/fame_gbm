#!/bin/bash

# To run this script, do 
# qsub -t 1-n submit_GATK_FuncotatorVcf.sh CONFIG IDS
# This script will only work if all ensemble tools used are of the same version.
#
#
#$ -N annoVCF
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=16G
#$ -pe sharedmem 4
#$ -l h_rt=12:00:00

unset MODULEPATH
. /etc/profile.d/modules.sh

CONFIG=$1
IDS=$2

source $CONFIG

PATIENT_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1`
TUMOR=${PATIENT_ID}T
NORMAL=${PATIENT_ID}N


####################################################################################################################################
##
## Annotate consensus variants using Funcotator:
##
## This will add GENCODE annotation as described here: https://gatk.broadinstitute.org/hc/en-us/articles/360035531732#1.1.1
## 
####################################################################################################################################

WORK_DIR=$BCBIO_WORK/$PATIENT_ID
JVM_OPTS="-Dsamjdk.use_async_io_read_samtools=false -Dsamjdk.use_async_io_write_samtools=true -Dsamjdk.use_async_io_write_tribble=false -Dsamjdk.compression_level=1 -Xms16g -Xmx16g"
JVM_TMP_DIR="-Djava.io.tmpdir=$WORK_DIR/bcbiotx"

if [ ! -f $WORK_DIR/bcbiotx ]; then mkdir -p $WORK_DIR/bcbiotx; fi

java $JVM_OPTS $JVM_TMP_DIR -jar $GATK4_JAR Funcotator \
    --variant $M2_VARIANTS_PASSED/${PATIENT_ID}-vtdecompblocksub.normalised.passed.filtered.vcf.gz \
    -R $REFERENCE \
    --ref-version hg38 \
    --data-sources-path $FUNCOTATOR_DIR \
    --transcript-selection-mode CANONICAL \
    --remove-filtered-variants true \
    --annotation-default normal_barcode:${PATIENT_ID}N \
    --annotation-default tumor_barcode:${PATIENT_ID}T \
    --output $FUNCOTATOR/${PATIENT_ID}.final.funcotated.vcf \
    --output-file-format VCF

bcftools sort \
    $FUNCOTATOR/${PATIENT_ID}.final.funcotated.vcf \
    -Oz \
    -o $FUNCOTATOR/${PATIENT_ID}.final.funcotated.vcf.gz
    
bcftools index $FUNCOTATOR/${PATIENT_ID}.final.funcotated.vcf.gz

rm $M2_VARIANTS_PASSED/${PATIENT_ID}-passed.filtered.vcf
rm $M2_VARIANTS_PASSED/${PATIENT_ID}-normalised.passed.filtered.vcf.*
rm $FUNCOTATOR/${PATIENT_ID}.final.funcotated.vcf
rm $FUNCOTATOR/${PATIENT_ID}.final.funcotated.vcf.tbi

