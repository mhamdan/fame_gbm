#!/bin/bash

# To run this script, do 
# qsub -t 1-n submit_GATK_ModelSegments.sh CONFIG IDS
#
#$ -N ModelSegments
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=16G
#$ -pe sharedmem 4
#$ -l h_rt=12:00:00

unset MODULEPATH
. /etc/profile.d/modules.sh

CONFIG=$1
IDS=$2

PATIENT_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1`

source $CONFIG

WORK_DIR=$BCBIO_WORK/$PATIENT_ID
JVM_OPTS="-Dsamjdk.use_async_io_read_samtools=false -Dsamjdk.use_async_io_write_samtools=true -Dsamjdk.use_async_io_write_tribble=false -Dsamjdk.compression_level=1 -Xms16g -Xmx16g"
JVM_TMP_DIR="-Djava.io.tmpdir=$WORK_DIR/bcbiotx"
GATK_JAVA="java $JVM_OPTS $JVM_TMP_DIR -jar $GATK4_JAR"


########################################################################################################################################################################################################################################################################################
##
## ModelSegments
##
########################################################################################################################################################################################################################################################################################
##
## Use a guassian-kernel binary-segmentation algorithm to group contiguouis copy ratios into segments
## See: https://gatkforums.broadinstitute.org/dsde/discussion/11683/ and https://gatk.broadinstitute.org/hc/en-us/articles/360037225892
##
## Possible inputs are: 
## 1) denoised copy ratios for the case sample, 
## 2) allelic counts for the case sample, and 
## 3) allelic counts for a matched-normal sample. All available inputs will be used to to perform segmentation and model inference.
##
########################################################################################################################################################################################################################################################################################


$GATK_JAVA ModelSegments \
          --denoised-copy-ratios $COPY_RATIOS/${PATIENT_ID}T.merged.denoisedCR.tsv \
          --allelic-counts $ALLELIC_COUNTS/${PATIENT_ID}T_allelicCounts.tsv \
          --normal-allelic-counts $ALLELIC_COUNTS/${PATIENT_ID}N_allelicCounts.tsv \
          --output-prefix ${PATIENT_ID}T \
          -O $MODEL_SEGMENTS/
          
          
          