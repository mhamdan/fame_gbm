#!/bin/bash

# To run this script, do 
# qsub -t 1-n submit_GATK_vcftofasta.sh CONFIG IDS
#
#$ -N vcftofasta.sh
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=16G
#$ -pe sharedmem 8
#$ -l h_rt=96:00:00

unset MODULEPATH
. /etc/profile.d/modules.sh

CONFIG=$1
IDS=$2

source $CONFIG

PATIENT_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1`
WORK_DIR=$BCBIO_WORK/$PATIENT_ID
VCF=/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/ssv/vcf/samples/passed/${PATIENT_ID}-normalised.passed.filtered.vcf.gz
OUTPUT=/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/ssv/fasta/${PATIENT_ID}.fa

if [ ! -f $WORK_DIR/bcbiotx ]; then mkdir -p $WORK_DIR/bcbiotx; fi

JVM_OPTS="-Dsamjdk.use_async_io_read_samtools=false -Dsamjdk.use_async_io_write_samtools=true -Dsamjdk.use_async_io_write_tribble=false -Dsamjdk.compression_level=1 -Xms16g -Xmx16g"
JVM_TMP_DIR="-Djava.io.tmpdir=$WORK_DIR/bcbiotx"

## Create an alternative reference sequence by combining a fasta with a vcf, within specified intervals.
## Based on -> https://gatk.broadinstitute.org/hc/en-us/articles/360037594571-FastaAlternateReferenceMaker

java $JVM_OPTS $JVM_TMP_DIR -jar $GATK4_JAR FastaAlternateReferenceMaker \
    -R $REFERENCE \
    -O $OUTPUT \
    -L $INTERVAL_LIST \
    -V $VCF



