#!/bin/bash

# To run this script, do 
# qsub -t 1-n submit_GATK_plot_denoised_copy_ratios.sh CONFIG IDS TYPE
#
#$ -N plot_denoised_copy_ratios
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=32G
#$ -pe sharedmem 4
#$ -l h_rt=72:00:00

##Define SGE parameters

unset MODULEPATH
. /etc/profile.d/modules.sh

CONFIG=$1
IDS=$2
TYPE=$3

##Define files/paths

source $CONFIG

PATIENT_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1`

WORK_DIR=$BCBIO_WORK/$PATIENT_ID

if [ ! -f $WORK_DIR/bcbiotx ]; then mkdir $WORK_DIR/bcbiotx; fi

JVM_OPTS="-Dsamjdk.use_async_io_read_samtools=false -Dsamjdk.use_async_io_write_samtools=true -Dsamjdk.use_async_io_write_tribble=false -Dsamjdk.compression_level=1 -Xms16g -Xmx16g"
JVM_TMP_DIR="-Djava.io.tmpdir=$WORK_DIR/bcbiotx"

########################################################################################################################################################################################################################################################################################
##
## Plot denoised copy ratios 
## Based on https://gatk.broadinstitute.org/hc/en-us/articles/360035531092?id=11682
## and https://gatk.broadinstitute.org/hc/en-us/articles/360040509591-PlotDenoisedCopyRatios
## 
## To omit alternate and decoy contigs from the plots, the --minimum-contig-length is set to 46,709,983
## the length of the smallest of GRCh38's primary assembly contigs.
## Denoised-plot files. Two versions of a plot showing both the standardized and denoised 
## copy ratios are output; the first covers the entire range of the copy ratios, while 
## the second is limited to copy ratios within [0, 4].
## Median-absolute-deviation files. These files contain the median absolute deviation (MAD) 
## for both the standardized (.standardizedMAD.txt) and denoised (.denoisedMAD.txt) copy ratios, 
## the change between the two (.deltaMAD.txt), and the change between the two scaled by the standardized MAD (.deltaScaledMAD.txt).
##
########################################################################################################################################################################################################################################################################################

if [ ! -f $CNV/plotcr/${PATIENT_ID}${TYPE} ]
then 
mkdir $CNV/plotcr/${PATIENT_ID}${TYPE}
java $JVM_OPTS $JVM_TMP_DIR -jar $GATK4_JAR PlotDenoisedCopyRatios \
    --standardized-copy-ratios $COPY_RATIOS/${PATIENT_ID}${TYPE}.merged.standardizedCR.tsv \
    --denoised-copy-ratios $COPY_RATIOS/${PATIENT_ID}${TYPE}.merged.denoisedCR.tsv \
    --sequence-dictionary $REFERENCE_DICT \
    --minimum-contig-length 46709983 \
    --output-prefix ${PATIENT_ID}${TYPE} \
    --output $CNV/plotcr/${PATIENT_ID}${TYPE}
fi

    
    