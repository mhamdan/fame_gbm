#!/bin/bash

#$ -N qcfastqheader
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=16G
#$ -pe sharedmem 16
#$ -l h_rt=20:00:00

unset MODULEPATH
. /etc/profile.d/modules.sh

READ_DIR=$1
SRR_ID=$2

cd $READ_DIR

zcat ${SRR_ID}.R1.fastq.gz | awk '{print (NR%4 == 1) ? "@"++i "/1" : $0}' > ${SRR_ID}_R1.fastq
gzip -f ${SRR_ID}_R1.fastq

zcat ${SRR_ID}.R2.fastq.gz | awk '{print (NR%4 == 1) ? "@"++i "/1" : $0}' > ${SRR_ID}_R2.fastq
gzip -f ${SRR_ID}_R2.fastq


