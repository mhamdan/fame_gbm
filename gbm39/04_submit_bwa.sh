#!/bin/bash

#$ -N BWA
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=16G
#$ -l h_rt=48:00:00
#$ -pe sharedmem 16

SRR_ID=$1 ## SRR basename
CELL_LINE_ID=$2
READ_DIR=$3 ## fastq dir full path
BAM_DIR=$4 ## bam dir full path

cd $READ_DIR

echo "Aligning $SRR_ID fastq files, renaming bam file using $CELL_LINE_ID name..."

REFERENCE=/exports/igmm/eddie/Glioblastoma-WGS/resources/refgenome38/hg38.fa
BWA=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/bin/bwa
SAMTOOLS=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/bin/samtools

$BWA mem -M -t 20 $REFERENCE ${SRR_ID}_R1.fastq.gz ${SRR_ID}_R2.fastq.gz | $SAMTOOLS sort -@ 10 - -T ${SRR_ID} -o $BAM_DIR/${CELL_LINE_ID}.sorted.bam
$SAMTOOLS index $BAM_DIR/${CELL_LINE_ID}.sorted.bam
$SAMTOOLS flagstat $BAM_DIR/${CELL_LINE_ID}.sorted.bam > $BAM_DIR/${CELL_LINE_ID}.stats.out

echo "Done aligning $SRR_ID and renaming the final sorted bam as $CELL_LINE_ID"

