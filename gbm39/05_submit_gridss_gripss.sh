#!/bin/bash

# Run tumour-only SV calling for cell lines

# To run this script, do 
# qsub -t 1-n submit_gridss.sh CONFIG IDS
#
#$ -N GRIDSS
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=16G
#$ -pe sharedmem 16
#$ -l h_rt=180:00:00

### Based on https://github.com/PapenfussLab/gridss
### Install latest releases from here https://github.com/PapenfussLab/GRIDSS/releases
### This release is 2.10
### If stops due to lack of time, can be resumed by just resubmitting the script.

CONFIG=$1

OUTDIR=/exports/igmm/eddie/Glioblastoma-WGS/GBM39/wgs/variants/gridss

source $CONFIG

## Step 1: GRIDSS
ALIGNED_BAM_FILE_TUMOR=/exports/igmm/eddie/Glioblastoma-WGS/GBM39/wgs/bams/
GRIDSS_RAW=${OUTDIR}/raw/GBM39.gridss.raw.vcf.gz
GRIDSS_ASSEMBLY=$OUTDIR/assembly/GBM39.assembly.bam
GRIDSS_WORKING_DIR=${OUTDIR}/working_dir/GBM39

$GRIDSS \
    --reference $REFERENCE \
    --output $GRIDSS_RAW \
    --assembly $GRIDSS_ASSEMBLY \
    --jvmheap 32g \
    --workingdir $GRIDSS_WORKING_DIR \
    --blacklist $ENCODE_BLACKLIST \
    --jar $GRIDSS_JAR \
    --steps All \
    --maxcoverage 50000 \
    --labels GBM39 \
    $ALIGNED_BAM_FILE_TUMOR

## Step 2: GRIPSS
### Filter out HMF PON:




  
