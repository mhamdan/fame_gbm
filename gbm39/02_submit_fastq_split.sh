#!/bin/bash

## Split paired end fastq merged in one file into R1 and R2 
#$ -N seqtk
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=16G
#$ -pe sharedmem 16
#$ -l h_rt=20:00:00

FASTQ_FILE=$1

SEQTK=/exports/igmm/eddie/Glioblastoma-WGS/scripts/seqtk/seqtk

$SEQTK seq -1 $FASTQ_FILE | gzip > SRR8236745.R1.fastq.gz
$SEQTK seq -2 $FASTQ_FILE | gzip > SRR8236745.R2.fastq.gz


