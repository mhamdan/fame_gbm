#!/bin/bash

#This script is to download/convert SRR to fastq files, given a txt file containing accession numbers. Make sure you know if samples are paired or singles
#To run this script, do qsub <script> txt.file

#$ -cwd
#$ -N SRR_to_fastq
#$ -j y
#$ -S /bin/bash
#$ -l h_vmem=8G
#$ -l h_rt=120:00:00

# Initialise the modules framework
. /etc/profile.d/modules.sh

#Load SRA toolkit
module load igmm/apps/sratoolkit/3.1.1

FILES=$1
SRR=`head -n $SGE_TASK_ID $FILES | tail -n 1`
OUTDIR=/exports/igmm/eddie/Glioblastoma-WGS/GBM39/wgs/fastq

#Based on tips here https://edwards.sdsu.edu/research/fastq-dump/
#And changes if paired/single end data.
cd $OUTDIR
fastq-dump --origfmt --gzip --skip-technical --outdir $OUTDIR $SRR
