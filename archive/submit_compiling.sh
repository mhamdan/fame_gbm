#!/bin/bash

# To run this script, do 
#
#$ -N compile
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=6G
#$ -pe sharedmem 6
#$ -l h_rt=2:00:00

IDS=$1

PATIENT_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1`

DIR=/exports/igmm/eddie/Glioblastoma-WGS/analysis/fimo/region_level/raw/${PATIENT_ID}T

cd $DIR

for file in */fimo.gff; do echo $file >> ../${PATIENT_ID}T.names.txt;done
for file in */fimo.gff; do cat $file| grep -vsi "#"|wc -l >> ../${PATIENT_ID}T.counts.txt;done

cd ../
paste ${PATIENT_ID}T.names.txt ${PATIENT_ID}T.counts.txt > ${PATIENT_ID}T.summary.txt


