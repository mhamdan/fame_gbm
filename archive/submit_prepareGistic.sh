#!/bin/bash

# To run this script, do 
# qsub -t 1-n submit_prepareGistic.sh CONFIG IDS
#
#$ -N prepGistic
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=4G
#$ -pe sharedmem 2
#$ -l h_rt=2:00:00

CONFIG=$1
IDS=$2

PATIENT_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1`
SAMPLE_ID=${PATIENT_ID}T

source $CONFIG

awk 'BEGIN{FS=OFS="\t"} {print (NR>1?"'$SAMPLE_ID'":"ID"), $0}' $MODEL_SEGMENTS/${SAMPLE_ID}.cr.seg | grep -v "@" | \
grep -vsi contig > $GISTIC_SEGMENTS/${SAMPLE_ID}.seg.txt






