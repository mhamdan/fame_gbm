#!/bin/bash

# To run this script, do 
# qsub -t 1-n submit_verifybamid.sh CONFIG IDS TYPE
#
# CONFIG = Path to the file scripts/config.sh which contains environment variables set to commonly used paths and files in the script
# IDS = List of sample ids, one per line, where tumor and normal samples are designated by the addition of a T or an N to the sample id.
# TYPE = T for tumour, N for normal
#
#$ -N verifybamid
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=16G
#$ -l h_rt=120:00:00

unset MODULEPATH
. /etc/profile.d/modules.sh

CONFIG=$1
IDS=$2
TYPE=$3

source $CONFIG

PATIENT_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1`

SAMPLE_ID=${PATIENT_ID}${TYPE}
BAM_FILE=$ALIGNMENTS/$SAMPLE_ID/$SAMPLE_ID/$SAMPLE_ID-ready.bam

echo "$BAM_FILE"

verifybamid2 1000g.phase3 100k b38 \
    --Reference $REFERENCE \
    --Output $VERIFYBAMID_DIR/$SAMPLE_ID \
    --BamFile $BAM_FILE




