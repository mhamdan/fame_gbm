#!/bin/bash

# To run this script, do 
# qsub -t 1-n submit_oxog_filter_v2.sh CONFIG IDS
#
#$ -N pass_VardictV2
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=16G
#$ -pe sharedmem 2
#$ -l h_rt=12:00:00

unset MODULEPATH
. /etc/profile.d/modules.sh

CONFIG=$1
IDS=$2

PATIENT_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1`

source $CONFIG
TUMOR=${PATIENT_ID}T
NORMAL=${PATIENT_ID}N

## 1) Standardise VCF format

VCF2VCF="/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/snakemake/bin/vcf2vcf.pl"

VARDICT_OLD_VCF=$VARDICT_DIR/${PATIENT_ID}T-vardict.vcf.gz
VARDICT_NEW_VCF=$VARDICT_VARIANTS_FORMATTED/${PATIENT_ID}.var.formatted.test.vcf

bcftools index $VARDICT_DIR/${PATIENT_ID}T-vardict.vcf.gz

bcftools view \
    -Ov \
    -o $VARDICT_DIR/${PATIENT_ID}T-vardict.vcf \
    $VARDICT_DIR/${PATIENT_ID}T-vardict.vcf.gz

$VCF2VCF \
    --input-vcf $VARDICT_DIR/${PATIENT_ID}T-vardict.vcf \
    --output-vcf $VARDICT_NEW_VCF \
    --vcf-tumor-id $TUMOR \
    --vcf-normal-id $NORMAL \
    --ref-fasta $REFERENCE 

bcftools view \
    -Oz \
    -o ${VARDICT_NEW_VCF}.gz \
    $VARDICT_NEW_VCF

bcftools index ${VARDICT_NEW_VCF}.gz



## This script will select passed variants, normalise small variants called by Vardict

INPUT_VAR=${VARDICT_NEW_VCF}.gz
echo $INPUT_VAR

OUTPUT_OXOG_ANNO_VAR=$VARDICT_VARIANTS_PASSED/${PATIENT_ID}-vardict.annoOxog.filtered.vcf.gz
OUTPUT_VAR=$VARDICT_VARIANTS_OXOG/${PATIENT_ID}-vardict.oxog.normalised.passed.filtered.vcf.gz
METRICS_FILE=$METRICS/${PATIENT_ID}T_artifact.pre_adapter_detail_metrics

## 1) Annotate for oxog artifact

if [ -f $METRICS_FILE ]
then
    echo "Annotating oxog artifacts from Vardict call for $PATIENT_ID"
    $GATK4 FilterByOrientationBias \
      --variant $INPUT_VAR \
      --artifact-modes 'G/T' \
      --pre-adapter-detail-file $METRICS_FILE \
      --output $OUTPUT_OXOG_ANNO_VAR
else
    echo "Vardict call for $PATIENT_ID already annotated"
fi



