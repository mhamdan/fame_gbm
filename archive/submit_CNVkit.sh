#!/bin/bash

# To run this script, do 
# qsub -t 1-n submit_CNVkit.sh CONFIG IDS BATCH
#
#$ -N CNVkit
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=8G
#$ -pe sharedmem 4
#$ -l h_rt=10:00:00

unset MODULEPATH
. /etc/profile.d/modules.sh

CONFIG=$1
IDS=$2

source $CONFIG

PATIENT_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1`
TUMOR_DIR=$ALIGNMENTS/${PATIENT_ID}T/${PATIENT_ID}T
NORMAL_DIR=$ALIGNMENTS/${PATIENT_ID}N/${PATIENT_ID}N
ALIGNED_BAM_FILE_TUMOR=$TUMOR_DIR/${PATIENT_ID}T-ready.bam
ALIGNED_BAM_FILE_NORMAL=$NORMAL_DIR/${PATIENT_ID}N-ready.bam
BED_FILE=/exports/igmm/eddie/Glioblastoma-WGS/analysis/bed_regions/SOX_motif_final.bed
OUTPUT_DIR=/exports/igmm/eddie/Glioblastoma-WGS/analysis/copynumber

export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/cnvkit/bin:$PATH

mkdir -p $OUTPUT_DIR/${PATIENT_ID}

cd $OUTPUT_DIR/${PATIENT_ID}

cnvkit.py batch \
    $ALIGNED_BAM_FILE_TUMOR \
    -n $ALIGNED_BAM_FILE_NORMAL \
        -m wgs \
        --targets $BED_FILE \
        -f $REFERENCE
        
cnvkit.py segment ${PATIENT_ID}T-ready.cnr -o ${PATIENT_ID}T-ready.cns
cnvkit.py call ${PATIENT_ID}T-ready.cns -o ${PATIENT_ID}T-ready.call.cns

        
