#!/usr/bin/bash

# To run this script, do 
# qsub -t 1-n submit_facets.sh IDS
#
#$ -N facets
#$ -j y
#$ -cwd
#$ -l h_vmem=16G
#$ -pe sharedmem 8
#$ -l h_rt=72:00:00


##Define SGE parameters
unset MODULEPATH
. /etc/profile.d/modules.sh

IDS=$1
PATIENT_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1`

export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/r_env/bin:$PATH
FACETS=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/r_env/bin/cnv_facets.R
DBSNP=/exports/igmm/eddie/NextGenResources/bcbio-1.1.5/genomes/Hsapiens/hg38/variation/dbsnp-151.vcf.gz
OUTDIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/cnv/facets

WGS=/exports/igmm/eddie/Glioblastoma-WGS/WGS
ALIGNMENTS=$WGS/alignments
TUMOR_DIR=$ALIGNMENTS/${PATIENT_ID}T/${PATIENT_ID}T
NORMAL_DIR=$ALIGNMENTS/${PATIENT_ID}N/${PATIENT_ID}N
ALIGNED_BAM_FILE_TUMOR=$TUMOR_DIR/${PATIENT_ID}T-ready.bam
ALIGNED_BAM_FILE_NORMAL=$NORMAL_DIR/${PATIENT_ID}N-ready.bam

mkdir -p $OUTDIR/$PATIENT_ID

######Based on https://github.com/dariober/cnv_facets

Rscript $FACETS \
    -n $ALIGNED_BAM_FILE_NORMAL \
    -t $ALIGNED_BAM_FILE_TUMOR \
    -vcf $DBSNP \
    -o $OUTDIR/$PATIENT_ID \
    --snp-nprocs 8 \
    -cv 25 400
    
