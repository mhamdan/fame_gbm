#!/bin/bash

# To run this script, do 
# qsub -t 1-n submit_vcf2maf.sh CONFIG IDS
# This script will only work if all ensemble tools used are of the same version.
#
#
#$ -N vcf2maf
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=8G
#$ -pe sharedmem 8
#$ -l h_rt=12:00:00

unset MODULEPATH
. /etc/profile.d/modules.sh

CONFIG=$1
IDS=$2

source $CONFIG

SAMPLE_ID=`head -n $SGE_TASK_ID $IDS | cut -f 1 | tail -n 1`
NORMAL=`head -n $SGE_TASK_ID $IDS | cut -f 6 | tail -n 1`
TUMOR=$SAMPLE_ID

MTDNA_DIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/mitochondrial_DNA/varscan

### Use version 1.6 so that you can skip troublesome VEP annotations, provided that VCFs are already annotated using SNPEFF.
### 03/08: Does not work for mitochondrial genome.
VCF2MAF="/exports/igmm/eddie/Glioblastoma-WGS/scripts/vcf2maf-1.6.20/vcf2maf.pl"

##bcftools view \
##    -Ov \
##    -o $MTDNA_DIR/${PATIENT_ID}.indel.vcf \
##    $MTDNA_DIR/${PATIENT_ID}.indel.vcf.gz 

### For SNV
$VCF2MAF \
    --input-vcf $MTDNA_DIR/${SAMPLE_ID}.snp.vcf \
    --output-maf $MTDNA_DIR/${SAMPLE_ID}.snp.maf \
    --tumor-id $TUMOR \
    --normal-id $NORMAL \
    --ref-fasta $REFERENCE \
    --filter-vcf $GATK_AF_GNOMAD \
    --ncbi-build GRCh38 \
    --species homo_sapiens \
    --vep-forks 1 \
    --vcf-tumor-id TUMOR --vcf-normal-id NORMAL \
    --vep-path /gpfs/igmmfs01/eddie/NextGenResources/bcbio-1.1.5/anaconda/pkgs/ensembl-vep-98.0-pl526hecc5488_0/share/ensembl-vep-98.0-0 \
    --vep-data /exports/igmm/eddie/Glioblastoma-WGS/resources/vep 

### For indels
$VCF2MAF \
    --input-vcf $MTDNA_DIR/${SAMPLE_ID}.indel.vcf \
    --output-maf $MTDNA_DIR/${SAMPLE_ID}.indel.maf \
    --tumor-id $TUMOR \
    --normal-id $NORMAL \
    --ref-fasta $REFERENCE \
    --filter-vcf $GATK_AF_GNOMAD \
    --ncbi-build GRCh38 \
    --species homo_sapiens \
    --vep-forks 1 \
    --vcf-tumor-id TUMOR --vcf-normal-id NORMAL \
    --vep-path /gpfs/igmmfs01/eddie/NextGenResources/bcbio-1.1.5/anaconda/pkgs/ensembl-vep-98.0-pl526hecc5488_0/share/ensembl-vep-98.0-0 \
    --vep-data /exports/igmm/eddie/Glioblastoma-WGS/resources/vep 







