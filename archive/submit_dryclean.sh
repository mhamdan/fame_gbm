#!/bin/bash

# To run this script, do 
# qsub -t 1-n submit_dryclean.sh.sh CONFIG IDS
#
#$ -N dryclean
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=8G
#$ -pe sharedmem 8
#$ -l h_rt=10:00:00

CONFIG=$1
IDS=$2

PATIENT_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1`

source $CONFIG

## Run dryclean
## Based on https://github.com/mskilab/dryclean
## Need a file path containing column 1 (sample ID) and column 2 (full paths to fragcounter file)
## Input to this is GC and mappability corrected read depth data from fragCounter
## Will need to create a PON first before running dryclean -> refer to https://github.com/mskilab/dryclean

source activate Jabba

JABBA_PATH=$(Rscript -e 'cat(paste0(installed.packages()["JaBbA", "LibPath"], "/JaBbA/extdata/"))')
export PATH=${PATH}:${JABBA_PATH}
export PATH=${PATH}:$(Rscript -e 'cat(paste0(installed.packages()["dryclean", "LibPath"], "/dryclean/extdata/"))')

echo "Performing dryclean for ${PATIENT_ID}..."

drcln -i $TUMOUR_FRAGCOUNT -p $DETERGENT -m ${PATIENT_ID} -c 32 -o $DRYCLEAN_RESULT




