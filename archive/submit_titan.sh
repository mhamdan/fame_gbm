#!/bin/bash

# To run this script, do 
# qsub -t 1-n submit_titan.sh CONFIG IDS
#
#$ -N titan
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=32G
#$ -pe sharedmem 8
#$ -l h_rt=72:00:00

##Define SGE parameters

unset MODULEPATH
. /etc/profile.d/modules.sh

CONFIG=$1
IDS=$2
MAXCLUSTER=$3
MAXPLOIDY=$4

##Define files/paths

source $CONFIG
module load roslin/gcc/7.3.0 
module load igmm/apps/R/3.6.3

PATIENT_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1`

INPUT_HETS=$TITAN_DIR/${PATIENT_ID}T/${PATIENT_ID}T.hets.tsv
INPUT_SEG=$TITAN_DIR/${PATIENT_ID}T/${PATIENT_ID}T.seg

## Based on https://github.com/gavinha/TitanCNA/tree/master/scripts/R_scripts
## https://github.com/TheJacksonLaboratory/GLASS/blob/1c554908f00172e9437cac6e75aaf81429852d17/snakemake/titan.smk
## TitanCNA.R script forked and modified from https://github.com/gavinha/TitanCNA/tree/master/scripts/R_scripts

for ploidy in $(seq 2 $MAXPLOIDY); do
    for cluster in $(seq 1 $MAXCLUSTER); do
        OUTDIR=$TITAN_DIR/${PATIENT_ID}T/${PATIENT_ID}T.ploidy.${ploidy}
        mkdir -p $OUTDIR
        Rscript $TITANCNA \
            --genomeBuild hg38 \
            --id ${PATIENT_ID}T \
            --hetFile $INPUT_HETS \
            --cnFile $INPUT_SEG \
            --normal_0 0.5 \
            --numClusters $cluster \
            --ploidy_0 $ploidy \
            --numCores 8 \
            --alphaK 10000 \
            --alphaKHigh 10000 \
            --minDepth 5 \
            --maxDepth 50000 \
            --chrs "c(1:22, \"X\")" \
            --estimatePloidy TRUE \
            --outDir $OUTDIR
    done;
done


#Rscript $TITANCNA \
#            --genomeBuild hg38 \
#            --id ${PATIENT_ID}T \
#            --hetFile $INPUT_HETS \
#            --cnFile $INPUT_SEG \
#            --normal_0 0.5 \
#            --numClusters $MAXCLUSTER \
#            --ploidy_0 $MAXPLOIDY \
#            --numCores 8 \
#            --alphaK 10000 \
#            --alphaKHigh 10000 \
#            --minDepth 5 \
#            --maxDepth 50000 \
#            --chrs "c(1:22, \"X\")" \
#            --estimatePloidy TRUE \
#            --outDir $TITAN_DIR/${PATIENT_ID}T/${PATIENT_ID}T.ploidy.${MAXPLOIDY}






