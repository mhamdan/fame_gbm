#!/bin/bash

# To run this script, do 
# qsub -t 1-n submit_strelka2.sh SAMPLES
#
# SAMPLES is a list of the sample names to do Strelka2 variant calling on
#
#$ -N strelka2
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_rt=24:00:00
#$ -l h_vmem=8G
#$ -pe sharedmem 8

unset MODULEPATH
. /etc/profile.d/modules.sh

SAMPLES=$1

SAMPLE=`head -n $SGE_TASK_ID $SAMPLES | tail -n 1 | awk '{ print $1 }'`

#STRELKA_INSTALL_PATH=/exports/igmm/eddie/bioinfsvice/ameynert/software/strelka-2.8.3.centos5_x86_64

STRELKA_INSTALL_PATH=/exports/igmm/eddie/NextGenResources/bcbio-1.1.5/anaconda/pkgs/strelka-2.9.10-0/bin

BASE_PATH=/exports/igmm/eddie/HGS-OvarianCancerA-SGP-WGS
TUMOR_BAM=$BASE_PATH/upload/${SAMPLE}a/${SAMPLE}a-ready.bam
NORMAL_BAM=$BASE_PATH/upload/${SAMPLE}b/${SAMPLE}b-ready.bam
WORK_DIR=$BASE_PATH/work/$SAMPLE/strelka2
MANTA_VCF=$BASE_PATH/work/$SAMPLE/structural/${SAMPLE}a/manta/results/variants/candidateSmallIndels.vcf.gz
REF=/exports/igmm/eddie/bioinfsvice/ameynert/software/bcbio/genomes/Hsapiens/hg38/seq/hg38.fa

mkdir -p $WORK_DIR
cd $WORK_DIR

${STRELKA_INSTALL_PATH}/bin/configureStrelkaSomaticWorkflow.py \
    --normalBam $NORMAL_BAM \
    --tumorBam $TUMOR_BAM \
    --referenceFasta $REF \
    --indelCandidates $MANTA_VCF \
    --runDir $WORK_DIR \
    --outputCallableRegions

./runWorkflow.py -m local -j $NSLOTS
