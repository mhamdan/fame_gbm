#!/bin/bash

# To run this script, do 
# qsub submit_fastadict.sh CONFIG
#
#$ -N submit_fastadict
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=32G
#$ -l h_rt=12:00:00

CONFIG=$1

source $CONFIG

WORK_DIR=/exports/igmm/eddie/Glioblastoma-WGS/bcbio/work/generic
JVM_OPTS="-Dsamjdk.use_async_io_read_samtools=false -Dsamjdk.use_async_io_write_samtools=true -Dsamjdk.use_async_io_write_tribble=false -Dsamjdk.compression_level=1 -Xms16g -Xmx16g"
JVM_TMP_DIR="-Djava.io.tmpdir=$WORK_DIR/bcbiotx"

java $JVM_OPTS $JVM_TMP_DIR -jar $PICARD_JAR CreateSequenceDictionary R=$REFERENCE_HG37
    
    
    
    
    