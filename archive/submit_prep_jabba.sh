#!/bin/bash

## To run this script, do qsub -t 1-N submit_prep_jabba.sh PARAM 
## PARAM is a .txt parameter file with no headers
## Column 1 contains absolute paths to PURPLE VCFs with extension ".purple.sv.vcf.gz"
## Column 2 contains absolute path to COBALT coverage files with extension  ".cobalt.tsv.gz"

#$ -N prep_jabba
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=4G
#$ -pe sharedmem 4
#$ -l h_rt=2:00:00

unset MODULEPATH
. /etc/profile.d/modules.sh

module load igmm/apps/bcbio/1.2.0

PARAM=$1

VCF_FILE=`head -n $SGE_TASK_ID $PARAM | cut -f 1 | tail -n 1`
COBALT_FILE=`head -n $SGE_TASK_ID $PARAM | cut -f 2 | tail -n 1`

## Repair partner breakpoint ID for PURPLE:
VCF_NAME=`basename $VCF_FILE`
VCF_DIR=`dirname $VCF_FILE`
echo "Repairing partner breakpoint IDs..."
zcat $VCF_FILE | sed 's/PARID/MATEID/' > $VCF_DIR/${VCF_NAME%.vcf.gz}_EDITED.vcf 
bgzip $VCF_DIR/${VCF_NAME%.vcf.gz}_EDITED.vcf 

## Tidy COBALT coverage data to match JaBbA requirements:
## This uses R script, with these libraries needed: "BSgenome.Hsapiens.UCSC.hg38", "tidyverse","gUtils"
## Install gUtils package as per https://github.com/mskilab/gUtils

## Load R or point to your own R path with all the libraries installed
module load igmm/apps/R/3.6.1

COBALT_TIDY_SCRIPT=/exports/igmm/eddie/public/ForJabba/Al/test/scripts/cobalt_tidy.R
Rscript $COBALT_TIDY_SCRIPT $COBALT_FILE














