#!/bin/bash

# To run this script, do 
# qsub -t 1-N submit_plausiblePaths.sh IDS
#
#$ -N PlausPath
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=8G
#$ -pe sharedmem 8
#$ -l h_rt=10:00:00

unset MODULEPATH
. /etc/profile.d/modules.sh

## This script is from Jens Leubeck of UCSD, not for public use.
## IDS contains paths to amplicon breakpoint_graph_converted (BPG) cycle txt file (column 1) and amplicon graph txt file (column 2)
IDS=$1

GRAPH=`head -n $SGE_TASK_ID $IDS | cut -f 2| tail -n 1`

PLAUSIBLEPATH=/exports/igmm/eddie/Glioblastoma-WGS/scripts/AmpliconArchitect/src/plausible_paths.py
CYCLEVIZ_DIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/ecdna/CycleViz_output

cd $CYCLEVIZ_DIR

python $PLAUSIBLEPATH \
    -g $GRAPH 
    
    
    
    
    
    
