#!/bin/bash

# To run this script, do 
# qsub -t 1-n submit_Titan_selectSolution.sh CONFIG IDS THRESHOLD
#
#$ -N ssol
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=16G
#$ -pe sharedmem 2
#$ -l h_rt=72:00:00

##Define SGE parameters

unset MODULEPATH
. /etc/profile.d/modules.sh

IDS=$1
THRESHOLD=$2

##Define files/paths
export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/r_env/bin:$PATH

PATIENT_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1`

## Select optimal cluster and ploidy from the total output calls from TitanCNA.
## Based on https://github.com/gavinha/TitanCNA/tree/master/scripts/R_scripts

SELECTSOLUTION_R=/exports/igmm/eddie/Glioblastoma-WGS/scripts/fame_gbm/R/selectSolution.R
TITAN_DIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/cnv/titan
run_ploidy2=$TITAN_DIR/${PATIENT_ID}T/${PATIENT_ID}T.ploidy.2
run_ploidy3=$TITAN_DIR/${PATIENT_ID}T/${PATIENT_ID}T.ploidy.3
run_ploidy4=$TITAN_DIR/${PATIENT_ID}T/${PATIENT_ID}T.ploidy.4
SOLUTIONS=/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/cnv/titan/solutions

Rscript $SELECTSOLUTION_R \
    --ploidyRun2=$run_ploidy2 \
    --ploidyRun3=$run_ploidy3 \
    --ploidyRun4=$run_ploidy4 \
    --threshold=$THRESHOLD \
    --outFile $SOLUTIONS/${PATIENT_ID}.optimalClusters.txt













