#!/bin/bash

#$ -N driverpower
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=32G
#$ -pe sharedmem 4
#$ -l h_rt=4:00:00


##Based on https://driverpower.readthedocs.io/en/latest/install.html

export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/dp/bin:$PATH

cd /exports/igmm/eddie/Glioblastoma-WGS/analysis/DriverPower

driverpower model \
    --feature train_feature.hdf5 \
    --response train_y.tsv \
    --method GBM \
    --name tutorial \
    --modelDir ./output
    
    
    
    
    
    