#!/bin/bash

#This script scans a given motif through a given genome.
#To run this script, do qsub -t <1-n> submit_markov.sh config ids type

#$ -N submit_markov
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=16G
#$ -l h_rt=72:00:00
#$ -pe sharedmem 4

unset MODULEPATH
. /etc/profile.d/modules.sh

module load igmm/apps/meme/4.11.1

CONFIG=$1
IDS=$2
TYPE=$3

source $CONFIG

PATIENT_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1`
SAMPLE_ID=${PATIENT_ID}${TYPE}
INPUT_DIR=$ALIGNMENTS/$SAMPLE_ID/$SAMPLE_ID

fasta-get-markov $INPUT_DIR/$SAMPLE_ID.fa



