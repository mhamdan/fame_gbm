#!/bin/bash

### To merge peaks
## Merge chip-seq replicates based on IDR-like processing (but can scale up to >3 peaks)
## Based on https://github.com/rhysnewell/ChIP-R
## Install within a conda environment

#$ -N consensus_rep
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=12G
#$ -pe sharedmem 12
#$ -l h_rt=10:00:00


CONFIG=$1
IDS=$2
MASC2_DIR=$3
CHIPR_DIR=$4
BAM_DIR=$5

source $CONFIG

SAMPLE_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1`

PEAK_REP_1=${MASC2_DIR}/${SAMPLE_ID}-ChIP-1_peaks.narrowPeak
PEAK_REP_2=${MASC2_DIR}/${SAMPLE_ID}-ChIP-2_peaks.narrowPeak
PEAK_REP_3=${MASC2_DIR}/${SAMPLE_ID}-ChIP-3_peaks.narrowPeak
MERGED_PEAKS_PREFIX=${CHIPR_DIR}/${SAMPLE_ID}.merged_peaks
MERGED_CHIP_BAM=${BAM_DIR}/merged/sorted/${SAMPLE_ID}-ChIP.merged.sorted.bam
MERGED_INPUT_BAM=${BAM_DIR}/merged/sorted/${SAMPLE_ID}-Input.merged.sorted.bam

### Merging replicates
echo "Running chipr, creating consensus 2 of 3 peaksets..."
chipr -i $PEAK_REP_1 $PEAK_REP_2 $PEAK_REP_3 -m 2 -o $MERGED_PEAKS_PREFIX  
echo "Merged peaks for ${SAMPLE_ID} generated"

### Merging bams
##echo "Merging chip bams..."
##samtools merge -@ 12 - ${BAM_DIR}/replicates/sorted/${SAMPLE_ID}-ChIP*.sorted.bam | samtools sort -@ 12 - -T ${SAMPLE_ID}-ChIP -o $MERGED_CHIP_BAM
##samtools index $MERGED_CHIP_BAM
##samtools flagstat $MERGED_CHIP_BAM > ${MERGED_CHIP_BAM}.stats.out
##echo "Done merging chip bams."

##echo "Merging input bams..."
##samtools merge -@ 12 - ${BAM_DIR}/replicates/sorted/${SAMPLE_ID}-Input*.sorted.bam | samtools sort -@ 12 - -T ${SAMPLE_ID}-Input -o $MERGED_INPUT_BAM
##samtools index $MERGED_INPUT_BAM
##samtools flagstat $MERGED_INPUT_BAM > ${MERGED_INPUT_BAM}.stats.out
##echo "Done merging input bams."























