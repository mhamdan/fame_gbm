#!/bin/bash

#$ -N geneWalk
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=32G
#$ -l h_rt=12:00:00

PROJECT=$1
GENE_LIST=$2

export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/genewalk/bin:$PATH
GENE_WALK_DIR=/exports/igmm/eddie/Glioblastoma-WGS/ChIP-seq/SOX_TF/downstream/genewalk

genewalk \
    --project $PROJECT \
    --genes $GENE_LIST \
    --id_type hgnc_symbol \
    --base_folder $GENE_WALK_DIR \
    --nproc 8 \
    --random_seed 9999
    
    
    
    