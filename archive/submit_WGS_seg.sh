#!/bin/bash

#$ -N WGS_seg
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=16G
#$ -pe sharedmem 16
#$ -l h_rt=48:00:00

SAMPLE_ID=$1

hg38_100=/exports/igmm/eddie/Glioblastoma-WGS/scripts/Alleloscope/data-raw/hg38.100Kb.windows.sorted.bed
TUMOR_BAM=/exports/igmm/eddie/Glioblastoma-WGS/WGS/alignments/GCGR/${SAMPLE_ID}T/${SAMPLE_ID}T/${SAMPLE_ID}T-ready.bam
NORMAL_BAM=/exports/igmm/eddie/Glioblastoma-WGS/WGS/alignments/GCGR/${SAMPLE_ID}N/${SAMPLE_ID}N/${SAMPLE_ID}N-ready.bam
BEDG_DIR=/exports/igmm/eddie/Glioblastoma-WGS/scmultiome/alleloscope/seg
TUMOUR_SEG=$BEDG_DIR/${SAMPLE_ID}.hg38.100Kb.windows.counts.tumor.bedg
NORMAL_SEG=$BEDG_DIR/${SAMPLE_ID}.hg38.100Kb.windows.counts.germline.bedg

export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/snakemake/bin:$PATH

# for tumor sample
bedtools intersect -a $hg38_100 -b $TUMOR_BAM -c -sorted > $TUMOUR_SEG

# for germline sample
bedtools intersect -a $hg38_100 -b $NORMAL_BAM -c -sorted > $NORMAL_SEG








