
#!/bin/bash

## This script finds telomere repeat regions based on https://github.com/linasieverling/TelomereRepeatLoci

#$ -N telrepeats
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=8G
#$ -pe sharedmem 8
#$ -l h_rt=90:00:00

unset MODULEPATH
. /etc/profile.d/modules.sh

IDS=$1

export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/snakemake/bin:$PATH
export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/telomerehunter/bin:$PATH

## First run telomerehunter to completion
## Then use the bam outputs for inputs:
WGS_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 1`
TUMOUR_INTRATELOMERIC_BAM=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 2`
CONTROL_INTRATELOMERIC_BAM=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 3`
TUMOUR_INTRATELOMERIC_BAM_SORTED=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 4`
CONTROL_INTRATELOMERIC_BAM_SORTED=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 5`
TUMOUR_ORIGINAL_BAM=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 6`
CONTROL_ORIGINAL_BAM=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 7`

TEL_REPEAT_OUTPUTS=/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/telomererepeats
TUMOUR_INTRATELOMERIC_DISCORDANT_READ_OUTPUT=${WGS_ID}_tumour_telomere_insertions.tsv
CONTROL_INTRATELOMERIC_DISCORDANT_READ_OUTPUT=${WGS_ID}_control_telomere_insertions.tsv
TUMOUR_INTRATELOMERIC_DISCORDANT_READ_OUTPUT_FILTERED=${WGS_ID}_tumour_discordant_reads_filtered_with_mapq.tsv
CONTROL_INTRATELOMERIC_DISCORDANT_READ_OUTPUT_FILTERED=${WGS_ID}_control_discordant_reads_filtered_with_mapq.tsv

## Define scripts:
FIND_DISCORDANT_READS=/exports/igmm/eddie/Glioblastoma-WGS/scripts/TelomereRepeatLoci/src/find_discordant_reads.py
ADD_MATE_MAPQ=/exports/igmm/eddie/Glioblastoma-WGS/scripts/TelomereRepeatLoci/src/add_mate_mapq.py
COUNT_DISCORDANT_READS=/exports/igmm/eddie/Glioblastoma-WGS/scripts/TelomereRepeatLoci/src/count_discordant_reads.R
FUNCTION_FILE=/exports/igmm/eddie/Glioblastoma-WGS/scripts/TelomereRepeatLoci/src/functions.R
GET_CANDIDATE_REGIONS=/exports/igmm/eddie/Glioblastoma-WGS/scripts/TelomereRepeatLoci/src/get_candidate_regions.R
FIND_FUSION_READS=/exports/igmm/eddie/Glioblastoma-WGS/scripts/TelomereRepeatLoci/src/find_fusion_reads.R
PREDICT_INSERTION_SITES=/exports/igmm/eddie/Glioblastoma-WGS/scripts/TelomereRepeatLoci/src/predict_insertion_sites.R
GET_CONSENSUS=/exports/igmm/eddie/Glioblastoma-WGS/scripts/TelomereRepeatLoci/src/get_consensus.R
MAKE_BED_FOR_VISUALISATION=/exports/igmm/eddie/Glioblastoma-WGS/scripts/TelomereRepeatLoci/src/make_bed_for_visualization.R

## STEP 1: Find candidate regions with discordant reads (requires intratelomeric BAM file):
### Discordant reads are: 1) mate is mapped and its reference ID is known, 2) mate is not an intratelomeric read
### Tabulate strand and mapq of each chromosomal mate, and remove reads mapping to decoy sequences. 
### Summarizes the number of discordant reads in the tumor and control sample. For this, the genome is split into strand-specific, 1 kb windows. For each window, the number of discordant reads with a mapping quality of over 30 is counted. 
### Candidate regions must contain a minimum number of discordant reads in the tumor sample (set to 3 and 4 for the PCAWG and neuroblastoma analysis, respectively) and a maximum number of discordant reads in the control sample (usually 0). 
### If specified by the user, windows contained in the blacklist are removed. This step is especially important to rule out false positives if no control sample is available.

## Tidy bam files:
samtools sort -@ 10 $TUMOUR_INTRATELOMERIC_BAM -o $TUMOUR_INTRATELOMERIC_BAM_SORTED
samtools index $TUMOUR_INTRATELOMERIC_BAM_SORTED
samtools sort -@ 10 $CONTROL_INTRATELOMERIC_BAM -o $CONTROL_INTRATELOMERIC_BAM_SORTED
samtools index $CONTROL_INTRATELOMERIC_BAM_SORTED

## For tumour bam:
cd $TEL_REPEAT_OUTPUTS
$FIND_DISCORDANT_READS -i $TUMOUR_INTRATELOMERIC_BAM_SORTED -o $TUMOUR_INTRATELOMERIC_DISCORDANT_READ_OUTPUT
$ADD_MATE_MAPQ -i $TUMOUR_INTRATELOMERIC_DISCORDANT_READ_OUTPUT -b $TUMOUR_ORIGINAL_BAM -o $TUMOUR_INTRATELOMERIC_DISCORDANT_READ_OUTPUT_FILTERED

## For normal bam:
$FIND_DISCORDANT_READS -i $CONTROL_INTRATELOMERIC_BAM_SORTED -o $CONTROL_INTRATELOMERIC_DISCORDANT_READ_OUTPUT 
$ADD_MATE_MAPQ -i $CONTROL_INTRATELOMERIC_DISCORDANT_READ_OUTPUT -b $CONTROL_ORIGINAL_BAM -o $CONTROL_INTRATELOMERIC_DISCORDANT_READ_OUTPUT_FILTERED

## Combined: Count discordan reads per 1 kb window
##$COUNT_DISCORDANT_READS -t $TUMOUR_INTRATELOMERIC_DISCORDANT_READ_OUTPUT_FILTERED -c ##$CONTROL_INTRATELOMERIC_DISCORDANT_READ_OUTPUT_FILTERED -o -f $FUNCTION_FILE
##$GET_CANDIDATE_REGIONS

## STEP 2: Find precise locus with clipped reads
### Search within clipped reads, those that span telomere repeat locus junctions. 
### Both soft-clipped and hard-clipped junctions are traversed.
### For each candidate region, the total number of clipped reads supporting the telomere repeat locus, the orientation of the telomere sequence (TTAGGG or CCCTAA on the forward strand) and the total number of TTAGGG and CCCTAA counts in the fusion reads is reported.
### Get the exact position of the telomere repeat locus from the position of the clipped reads 

##$FIND_FUSION_READS
##$PREDICT_INSERTION_SITES

## STEP 3: Construct telomeric sequences at the telomere repeat loci
### For each position in the clipped sequences, the frequency of each base is calculated. If a base has a frequency of at least 0.65, this base is used for the consensus sequence. Otherwise, it is set to "N". 
### Assuming that the telomere sequence at the repeat locus consists exclusively of t-type repeats, microhomology between the reference genome and the telomere sequence can be determined. 
### For this, the reference genome sequence 20 bp upstream of the telomere repeat locus is extracted. The t-type telomere repeat of the inserted telomere sequence that is closest to the locus is extended and each base pair is compared to that of the reference genome. Every match is counted as a base pair of sequence homology between the reference genome and the telomere sequence. 
### As soon as a base pair does not match, the microhomology is disrupted and further homology is not considered. If the bases upstream of the first t-type repeat in the inserted telomere sequence do not match an incomplete t-type repeat, the microhomology cannot be determined and is set to "?". The information on the telomere consensus sequence and the base pairs of microhomology is added to the telomere repeat locus table.
##$GET_CONSENSUS

## STEP 4: Make IGV-like plot:
### To rule out remaining false positives, each telomere repeat locus should be checked manually. T
##$MAKE_BED_FOR_VISUALISATION
















