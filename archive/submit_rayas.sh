#!/bin/bash


## This script calls templated insertion threads as detailed by Rausch et al 2022 biorxiv paper.
## To install git clone https://github.com/tobiasrausch/rayas.git
## cd src && git clone https://github.com/samtools/htslib.git
## make all

# To run this script, do 
# qsub -t n submit_rayas.sh IDS
#
#$ -N rayas
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=12G
#$ -pe sharedmem 12
#$ -l h_rt=40:00:00

IDS=$1
TYPE=$2
OUTPUT_TYPE=$3

PATIENT_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1`

RAYAS=/exports/igmm/eddie/Glioblastoma-WGS/scripts/rayas/src/rayas
REF=/exports/igmm/eddie/Glioblastoma-WGS/resources/refgenome38/hg38.fa
ALIGNED_BAM_FILE_TUMOR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/alignments/${PATIENT_ID}${TYPE}/${PATIENT_ID}${TYPE}/${PATIENT_ID}${TYPE}-ready.bam
ALIGNED_BAM_FILE_NORMAL=/exports/igmm/eddie/Glioblastoma-WGS/WGS/alignments/${PATIENT_ID}N/${PATIENT_ID}N/${PATIENT_ID}N-ready.bam
OUTPUT_DIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/sv/rayas

cd $OUTPUT_DIR
if [ ! -f ${PATIENT_ID}${OUTPUT_TYPE}.out.bed ]; then
$RAYAS call -o ${PATIENT_ID}${OUTPUT_TYPE}.out.bed -g $REF -m $ALIGNED_BAM_FILE_NORMAL $ALIGNED_BAM_FILE_TUMOR 
fi




