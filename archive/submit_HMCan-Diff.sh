#!/bin/bash

# To run this script, do 
# qsub -t n submit_HMCan-Diff.sh CONFIG C1 C2 CHIP
#
#$ -N HMCan_diff
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=100G
#$ -l h_rt=20:00:00

CONFIG=$1
C1=$2
C2=$3
CHIP=$4

source $CONFIG

ulimit -n 8192

HMCan_DIFF=/exports/igmm/eddie/Glioblastoma-WGS/scripts/hmcan-diff/src/HMCan-diff
HMCan_DIR=/exports/igmm/eddie/Glioblastoma-WGS/ChIP-seq/SOX_TF/qc/HMCan

cd $HMCan_DIR

$HMCan_DIFF \
    --name ${C1}_vs_${C2}_${CHIP}.chr7 \
    --C1_label ${C1}_${CHIP}.chr7 \
    --C2_label ${C2}_${CHIP}.chr7 \
    --C1_ChIP ${C1}_${CHIP}.chr7.txt \
    --C2_ChIP ${C2}_${CHIP}.chr7.txt \
    --C1_Control ${C1}_Input.chr7.txt \
    --C2_Control ${C2}_Input.chr7.txt \
    --format BAM --pairedEnd TRUE \
    --genomePath /exports/igmm/eddie/Glioblastoma-WGS/resources/refgenome38/chromosomes \
    --GCProfile $GC_PROFILE \
    --blackListFile $ENCODE_BLACKLIST \
    --PrintWig \
    --largeBin 1000 \
    --mergeDistance 200


#### Test example ####

##HMCan_DIR=/exports/igmm/eddie/Glioblastoma-WGS/scripts/hmcan-diff
##cd $HMCan_DIR

##$HMCan_DIFF \
##    --name hmcan-diff_example \
##    --C1_ChIP hmcan-diff_example/C1_files.txt \
##    --C2_ChIP hmcan-diff_example/C2_files.txt \
##    --C1_Control hmcan-diff_example/C1_control.txt \
##    --C2_Control hmcan-diff_example/C2_control.txt \
##    --format SAM \
##    --genomePath hmcan-diff_example/ \
##    --GCProfile hmcan-diff_example/GC_profile_100KbWindow_Mapp76_hg19.cnp \
##    --C1_minLength 145 --C1_medLength 150 --C1_maxLength 155 --C2_minLength 145 --C2_medLength 150 --C2_maxLength 155 \
##    --blackListFile hmcan-diff_example/hg19-blacklist.bed \
##    --PrintWig 
    


