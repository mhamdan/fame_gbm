# To run this script, do 
# qsub -t 1-n submit_facets_v2.sh IDS
#
#$ -N facetsv2
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=32G
###$ -pe sharedmem 8
#$ -l h_rt=48:00:00

##Define SGE parameters

unset MODULEPATH
. /etc/profile.d/modules.sh

ulimit -n 8192

IDS=$1
CVAL=$2

export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/r_env/bin:$PATH

PATIENT_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1 | awk '{ print $1 }'`

FACETS=/exports/igmm/eddie/Glioblastoma-WGS/scripts/fame_gbm/R/facets.R
INPUT=/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/cnv/facets/pileups/${PATIENT_ID}.csv.gz
OUTPUT=/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/cnv/facets/segments

##touch $OUTPUT/${PATIENT_ID}.facets.log

Rscript --vanilla $FACETS \
    -i $PATIENT_ID \
    -f $INPUT \
    -o $OUTPUT \
    -c $CVAL
    
    
    
    
    
    

