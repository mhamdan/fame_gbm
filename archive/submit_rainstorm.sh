#!/bin/bash

#$ -N rainstorm
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=100G
#$ -l h_rt=120:00:00

MUTATION_FILE=$1
BACKGROUND_MUTATION_FILE=$2
STAGE=$3

export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/R4.0/bin:$PATH

Rscript --vanilla /exports/igmm/eddie/Glioblastoma-WGS/scripts/fame_gbm/R/rainstorm.R $MUTATION_FILE $BACKGROUND_MUTATION_FILE $STAGE







