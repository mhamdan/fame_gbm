#!/bin/bash

# To run this script, do 
# qsub -t 1-n submit_prepareTitan.sh CONFIG IDS TYPE
#
#$ -N prepareTitan
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=8G
#$ -pe sharedmem 2
#$ -l h_rt=48:00:00

unset MODULEPATH
. /etc/profile.d/modules.sh

CONFIG=$1
IDS=$2
TYPE=$3

source $CONFIG

##FOR TCGA
PATIENT_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1`

##For tumour
SAMPLE_ID=${PATIENT_ID}${TYPE}
INPUT_HETS=$MODEL_SEGMENTS/${SAMPLE_ID}.hets.tsv 
INPUT_SEG=$COPY_RATIOS/${SAMPLE_ID}.merged.denoisedCR.tsv

if [ ! -f $TITAN_DIR/${SAMPLE_ID} ]; then mkdir $TITAN_DIR/${SAMPLE_ID}; fi

cat $INPUT_HETS | awk '/^[^@]/ {{ print $1,$2,$5,$3,$6,$4 }}' | tr ' ' '\\t' > $TITAN_DIR/${SAMPLE_ID}/${SAMPLE_ID}.hets.tsv
cat $INPUT_SEG | grep -v "@" > $TITAN_DIR/${SAMPLE_ID}/${SAMPLE_ID}.seg


