#!/bin/bash

# To run this script, do 
# qsub -t 1-n submit_amber-cobalt-purple.sh CONFIG IDS
#
#$ -N acp
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=16G
#$ -pe sharedmem 8
#$ -l h_rt=72:00:00

#### This script will automate amber and cobalt analyses.
#### Based on https://github.com/hartwigmedical/hmftools

##Define SGE parameters

CONFIG=$1
IDS=$2

##Define files/paths
PATIENT_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1`

source $CONFIG

## Need copynumber package installed in R (done through this conda env)
export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/r_env/bin:$PATH

#### AMBER

if [ ! -f $OUTPUT_AMBER/${PATIENT_ID}/${PATIENT_ID}T1.amber.baf.vcf.gz ]
then
    echo "Running Amber for ${PATIENT_ID}T1"
    java $JVM_OPTS $JVM_TMP_DIR -cp $AMBER_JAR com.hartwig.hmftools.amber.AmberApplication \
       -reference ${PATIENT_ID}N \
       -reference_bam $ALIGNED_BAM_FILE_NORMAL \
       -tumor ${PATIENT_ID}T1 \
       -tumor_bam $ALIGNED_BAM_FILE_TUMOR \
       -output_dir $OUTPUT_AMBER/${PATIENT_ID}T1 \
       -threads 16 \
       -loci $HET
else
    echo "Amber files already present for ${PATIENT_ID}T1"
fi

#### COBALT

if [ ! -f $OUTPUT_COBALT/${PATIENT_ID}/${PATIENT_ID}T1.cobalt.ratio.pcf ]
then
    echo "Running Cobalt for ${PATIENT_ID}T1"
    java $JVM_OPTS $JVM_TMP_DIR -cp $COBALT_JAR com.hartwig.hmftools.cobalt.CountBamLinesApplication \
        -reference ${PATIENT_ID}N \
        -reference_bam $ALIGNED_BAM_FILE_NORMAL \
        -tumor ${PATIENT_ID}T1 \
        -tumor_bam $ALIGNED_BAM_FILE_TUMOR \
        -output_dir $OUTPUT_COBALT/${PATIENT_ID}T1 \
        -threads 16 \
        -gc_profile $GC_PROFILE
else
    echo "Cobalt files already present for ${PATIENT_ID}T1"
fi

#### Annotated Strelka output with AD field

##if [ ! -f $S2_VARIANTS_OXOG/${PATIENT_ID}T1-strelka2.AD.annotated.oxog.normalised.passed.filtered.vcf.gz ]
##then
##    echo "Annotating Strelka with allelic depth field for ${PATIENT_ID}T1"
 ##   java $JVM_OPTS $JVM_TMP_DIR -cp $PURPLE_JAR com.hartwig.hmftools.purple.tools.AnnotateStrelkaWithAllelicDepth \
##        -in $S2_VARIANTS_OXOG/${PATIENT_ID}T1-strelka2.oxog.normalised.passed.filtered.vcf.gz \
##        -out $S2_VARIANTS_OXOG/${PATIENT_ID}T1-strelka2.AD.annotated.oxog.normalised.passed.filtered.vcf.gz
##else
##    echo "Strelka file for ${PATIENT_ID}T1 already annotated with allelic depth field"
##fi

#### PURPLE
## Needs VariantAnnotation, cowplot, and CIRCOS dependencies pre-installed.

if [ ! -d $OUTPUT_PURPLE/${PATIENT_ID}T1 ]
then
    echo "Running PURPLE for ${PATIENT_ID}T1"
    java $JVM_OPTS $JVM_TMP_DIR -jar $PURPLE_JAR \
       -reference ${PATIENT_ID}N \
       -tumor ${PATIENT_ID}T1 \
       -output_dir $OUTPUT_PURPLE/${PATIENT_ID}T1 \
       -amber $OUTPUT_AMBER/${PATIENT_ID}T1 \
       -cobalt $OUTPUT_COBALT/${PATIENT_ID}T1 \
       -gc_profile $GC_PROFILE \
       -ref_genome $REFERENCE \
       -somatic_vcf $ENSEMBLE_VCF \
       -circos $CIRCOS \
       -structural_vcf $GRIDSS_FINAL_FILTERED 
       -sv_recovery_vcf $GRIDSS_PON_FILTERED
else
    echo "PURPLE output already generated for ${PATIENT_ID}T1"
fi







    
    
    
   