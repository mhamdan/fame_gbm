#!/bin/bash

# To run this script, do 
# qsub -t n submit_jabba_stuart.sh CONFIG IDS
#
#$ -N jabba
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=16G
#$ -pe sharedmem 16
#$ -l h_rt=200:00:00

## Based on mskilab's Cell paper on genome graph reconstruction of structural variants.
## Dependencies and installation instructions: https://github.com/mskilab/JaBbA
## Decision made to use COBALT's coverage file due to difficulties in using fragCounter/DryClean: https://github.com/mskilab/JaBbA/issues/37
## Field column for coverage file is left as ratio and generated in R and saved in rds format.
## Ratio in this case is TumorGCRatio/ReferenceGCDiploidRatio: https://github.com/hartwigmedical/hmftools/issues/142
## Can't run multiple files at the same time- CPLEX engine goes on overdrive.
## IDS file contains PURITY and PLOIDY calls from PURPLE - 1 sample per line. PATIENT_ID PURITY PLOIDY

CONFIG=$1
IDS=$2

unset MODULEPATH
. /etc/profile.d/modules.sh

source $CONFIG
source activate Jabba

SAMPLE_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 1` 
PURITY=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 2`
PLOIDY=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 3`
PURPLE_SV=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 4`
JABBA_COV=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 5` 

JABBA_PATH=$(Rscript -e 'cat(paste0(installed.packages()["JaBbA", "LibPath"], "/JaBbA/extdata/"))')
export PATH=${PATH}:${JABBA_PATH}
export CPLEX_DIR=/exports/igmm/eddie/Glioblastoma-WGS/scripts/cplex
export DEFAULT_BSGENOME=/exports/igmm/eddie/Glioblastoma-WGS/resources/hg38.chrom.sizes
JABBA_DIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/sv/jabba/results/${SAMPLE_ID}

## Create sample specific result directory
if [ ! -d $JABBA_DIR ]
then 
    mkdir -p $JABBA_DIR && cd $JABBA_DIR
else
    cd $JABBA_DIR
fi

## Run JBA, let it determine purity and ploidy (but compare this to PURPLE results after)
## Also let JaBbA do its own segmentation (and compare with PURPLE)
echo "Running JaBbA for ${SAMPLE_ID} using purity ${PURITY} and ploidy ${PLOIDY}..."
jba $PURPLE_SV $JABBA_COV --purity=${PURITY} --ploidy=${PLOIDY}  

    













