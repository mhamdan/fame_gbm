#!/bin/bash

# To run this script, do 
# qsub -t 1-n submit_fragCounter.sh CONFIG IDS BATCH TYPE
#
#$ -N fragCounter
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=8G
#$ -pe sharedmem 8
#$ -l h_rt=10:00:00

CONFIG=$1
IDS=$2
BATCH=$3
TYPE=$4

PATIENT_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1`

source $CONFIG

## Run fragCounter
## Based on https://github.com/mskilab/fragCounter 
## Outputs need to be in different directories for dryclean analysis (PON creation, put in batch specific directories)

source activate Jabba

JABBA_PATH=$(Rscript -e 'cat(paste0(installed.packages()["JaBbA", "LibPath"], "/JaBbA/extdata/"))')
export PATH=${PATH}:${JABBA_PATH}
export PATH=${PATH}:$(Rscript -e 'cat(paste0(installed.packages()["fragCounter", "LibPath"], "/fragCounter/extdata/"))')

echo "Correcting for GC and mappability bias for ${TYPE} bam file..."

if [ $TYPE == Tumour ]
then
mkdir -p ${FRAG_RESULT}/${TYPE}/${BATCH}/${PATIENT_ID}
frag -b $ALIGNED_BAM_FILE_TUMOR -d $GCMAP -w 1000 -o ${FRAG_RESULT}/${TYPE}/${BATCH}/${PATIENT_ID}
fi

if [ $TYPE == Normal ]
then
mkdir -p ${FRAG_RESULT}/${TYPE}/${BATCH}/${PATIENT_ID}
frag -b $ALIGNED_BAM_FILE_NORMAL -d $GCMAP -w 1000 -o ${FRAG_RESULT}/${TYPE}/${BATCH}/${PATIENT_ID}
fi








