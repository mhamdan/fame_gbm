#!/bin/bash

# To run this script, do 
# qsub -t 1-n submit_permutationTest.sh VARIANTS REGION PERMUTATION_NUMBER CORES
#
#$ -N circPerm
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=32G
#$ -l h_rt=72:00:00

VARIANTS=$1
REGION=$2
PERMUTATION_NUMBER=$3
CORES=$4

PERMUTATION=/exports/igmm/eddie/Glioblastoma-WGS/scripts/fame_gbm/R/submit_permutation.R

export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/r_env/bin:$PATH

Rscript $PERMUTATION \
    --input $VARIANTS \
    --region $REGION \
    --ntimes $PERMUTATION_NUMBER \
    --cores $CORES
    
