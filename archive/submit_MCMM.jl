#!/usr/bin/env julia

## Based on https://github.com/shahcompbio/MultiModalMuSig.jl
## To run locally, use /Applications/Julia-1.6.app/Contents/Resources/julia/bin/julia submit_MCMM.jl

println("#### Running MCMM program ####")
println("Loading required libraries...")
using MultiModalMuSig
using CSV
using DataFrames
using VegaLite
using Random

Random.seed!(999)

println("Setting working directory...")
cd("/Users/alhafidzhamdan1/PhD/Data/Mutational_signatures")

println("Loading mutational counts...")
snv_counts = CSV.read("snv_counts.txt", DataFrame, delim='\t')
dnv_counts = CSV.read("dnv_counts.txt", DataFrame, delim='\t')
indel_counts = CSV.read("indel_counts.txt", DataFrame, delim='\t')
sv_counts = CSV.read("sv_counts.txt", DataFrame, delim='\t')

println("Formatting counts data...")
samples = [c for c in propertynames(snv_counts) if c != :term] # names of columns with counts
X = format_counts_mmctm([snv_counts, dnv_counts, indel_counts, sv_counts], samples)

println("Calculating mutational probabilities for each modality...")
model = MMCTM([2, 2, 2, 2], [0.1, 0.1, 0.1, 0.1], X)
fit!(model, tol=1e-5)

println("Calculating average log-likelihood for this K...")
MultiModalMuSig.calculate_loglikelihoods(model)

## Do this for K=2-13 and plot an elbow plot to determine which K is optimum.
## Arrived at SNV = 7, Indel = 6, SV = 9 (DNV did not reach plateau)

println("Collecting signatures...")
snv_signatures = DataFrame(hcat(model.ϕ[1]...), :auto)
indel_signatures = DataFrame(hcat(model.ϕ[2]...), :auto)
sv_signatures = DataFrame(hcat(model.ϕ[3]...), :auto)

println("Appending terms to signatures...")
snv_signatures[!,:term] = snv_counts[!,:term]
indel_signatures[!,:term] = indel_counts[!,:term]
sv_signatures[!,:term] = sv_counts[!,:term]

## To plot:
snv_signatures = stack(
    snv_signatures, Not(:term), variable_name=:signature, value_name=:probability
)
indel_signatures = stack(
    indel_signatures, Not(:term), variable_name=:signature, value_name=:probability
)
sv_signatures = stack(
    sv_signatures, Not(:term), variable_name=:signature, value_name=:probability
)

snv_signatures |> @vlplot(
           :bar, x={:term, sort=:null}, y=:probability, row=:signature,
           resolve={scale={y=:independent}}
       )
indel_signatures |> @vlplot(
           :bar, x={:term, sort=:null}, y=:probability, row=:signature,
           resolve={scale={y=:independent}}
       )

sv_signatures |> @vlplot(
           :bar, x={:term, sort=:null}, y=:probability, row=:signature,
           resolve={scale={y=:independent}}
       )


println("Collecting signatures per sample...")
snv_props = hcat(
	[model.props[i][1] for i in 1:length(model.props)]...
)
indel_props = hcat(
	[model.props[i][2] for i in 1:length(model.props)]...
)
sv_props = hcat(
	[model.props[i][3] for i in 1:length(model.props)]...
)

println("Attaching sample labels...")
snv_props = rename!(DataFrame(snv_props,:auto), samples)
indel_props = rename!(DataFrame(indel_props,:auto), samples)
sv_props = rename!(DataFrame(sv_props,:auto), samples)





