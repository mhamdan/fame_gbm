#!/bin/bash
# To run this script, do 
# qsub -t 1-n submit_PURPLE-LINX.sh CONFIG IDS
#
#$ -N PURPLE-LINX
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=16G
#$ -pe sharedmem 12
#$ -l h_rt=72:00:00

#### This script will perform PURPLE analysis
#### Based on https://github.com/hartwigmedical/hmftools
#### July '22: Updated PURPLE and LINX to 3.5 and 1.2 respectively

##Define SGE parameters

CONFIG=$1
IDS=$2
STAGE=$3
TYPE=$4

##Define files/paths
PATIENT_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1`

source $CONFIG

export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/purple/bin:$PATH
export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/linx/bin:$PATH

## Annotate ssvs (if not done):
if [[ ! -f $ENSEMBLE_DIR/${PATIENT_ID}${TYPE}.ssv.snpeff.vcf.gz ]]; then
    java -Xmx4G -jar $SNPEFF_JAR -i vcf -o vcf GRCh38.99 $ENSEMBLE_VCF > $ENSEMBLE_DIR/${PATIENT_ID}${TYPE}.ssv.snpeff.vcf
    ##gzip $ENSEMBLE_DIR/${PATIENT_ID}${TYPE}.ssv.snpeff.vcf

    ## Edit sample name:
    sed "s/${PATIENT_ID}T/${PATIENT_ID}T2/" $ENSEMBLE_DIR/${PATIENT_ID}${TYPE}.ssv.snpeff.vcf | sed "s/${PATIENT_ID}T22/${PATIENT_ID}T2/" | bgzip > $ENSEMBLE_DIR/${PATIENT_ID}${TYPE}.ssv.snpeff.vcf.gz
fi

## Need copynumber package installed in R (done through this conda env)
## Needs VariantAnnotation, cowplot, and CIRCOS dependencies pre-installed.
## Also needs a lot of perl modules installed to run circos

echo "Running PURPLE for ${PATIENT_ID}${TYPE}"
java $JVM_OPTS $JVM_TMP_DIR -jar $PURPLE_JAR \
       -reference ${PATIENT_ID}N \
       -tumor ${PATIENT_ID}${TYPE} \
       -output_dir $OUTPUT_PURPLE/${PATIENT_ID}${TYPE} \
       -amber $OUTPUT_AMBER/${PATIENT_ID}${TYPE} \
       -cobalt $OUTPUT_COBALT/${PATIENT_ID}${TYPE} \
       -gc_profile $GC_PROFILE \
       -ref_genome $REFERENCE \
       -somatic_vcf $PURPLE_SNV_INPUT \
       -structural_vcf $GRIDSS_FINAL_FILTERED_RM \
       -sv_recovery_vcf $GRIDSS_PON_FILTERED_RM \
       -driver_catalog \
       -hotspots $HOTSPOTS \
       -driver_gene_panel $GENE_PANEL \
       -circos $CIRCOS

## PURPLE will generate files needed to construct circos files 
## This is redundant as LINX will also produce circos files, and those are much more comperehensive/finalised.

mkdir -p $OUTPUT_LINX/${PATIENT_ID}${TYPE}

echo "Running LINX for ${PATIENT_ID}${TYPE}..."
java $JVM_OPTS $JVM_TMP_DIR -jar $LINX_JAR \
    -sample ${PATIENT_ID}${TYPE} \
    -sv_vcf $PURPLE_SV_OUTPUT \
    -purple_dir $OUTPUT_PURPLE/${PATIENT_ID}${TYPE} \
    -output_dir $OUTPUT_LINX/${PATIENT_ID}${TYPE} \
    -ref_genome_version HG38 \
    -fragile_site_file $FRAGILE_SITES \
    -line_element_file $LINE_ELEMENTS \
    -replication_origins_file $HELI_REP_ORIGIN \
    -viral_hosts_file $VIRAL_HOST_REF \
    -gene_transcripts_dir $HMF_ENSEMBLE \
    -check_fusions \
    -known_fusion_file $HMF_FUSION \
    -check_drivers \
    -log_debug \
    -write_vis_data

## Need to make sure all perl modules are installed- try manually first and check which modules not installed.
## Make sure to install perl modules first, and then the R modules otherwise install.packages("magick") would not work as could not find library.
## Will need several R dependencies installed as per https://github.com/hartwigmedical/hmftools/blob/master/sv-linx/README_VIS.md#dependencies

mkdir -p $OUTPUT_LINX/${PATIENT_ID}${TYPE}/plot
mkdir -p $OUTPUT_LINX/${PATIENT_ID}${TYPE}/data

echo "Visualising LINX for ${PATIENT_ID}${TYPE}..."
java $JVM_OPTS $JVM_TMP_DIR -cp $LINX_JAR com.hartwig.hmftools.linx.visualiser.SvVisualiser \
    -sample ${PATIENT_ID}${TYPE} \
    -gene_transcripts_dir $HMF_ENSEMBLE \
    -plot_out $OUTPUT_LINX/${PATIENT_ID}${TYPE}/plot \
    -data_out $OUTPUT_LINX/${PATIENT_ID}${TYPE}/data \
    -segment $OUTPUT_LINX/${PATIENT_ID}${TYPE}/${PATIENT_ID}${TYPE}.linx.vis_segments.tsv \
    -link $OUTPUT_LINX/${PATIENT_ID}${TYPE}/${PATIENT_ID}${TYPE}.linx.vis_sv_data.tsv \
    -exon $OUTPUT_LINX/${PATIENT_ID}${TYPE}/${PATIENT_ID}${TYPE}.linx.vis_gene_exon.tsv \
    -cna $OUTPUT_LINX/${PATIENT_ID}${TYPE}/${PATIENT_ID}${TYPE}.linx.vis_copy_number.tsv \
    -protein_domain $OUTPUT_LINX/${PATIENT_ID}${TYPE}/${PATIENT_ID}${TYPE}.linx.vis_protein_domain.tsv \
    -fusion $OUTPUT_LINX/${PATIENT_ID}${TYPE}/${PATIENT_ID}${TYPE}.linx.vis_fusion.tsv \
    -circos $CIRCOS \
    -threads 16


