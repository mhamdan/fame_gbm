#!/bin/bash

### Forked from Alison Meynert 
### Based snp-pileup code from facets repository (git clone).

# To run this script, do 
# qsub -t 1-n submit_facets_snp_pileup.sh <params.txt> <outdir>
#
# column 1 - sample id
# column 2 - path to normal BAM file
# column 3 - path to tumor BAM file
#
#$ -N snp_pileup
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_rt=48:00:00
#$ -l h_vmem=4G

unset MODULEPATH
. /etc/profile.d/modules.sh

module load igmm/compilers/gcc/5.5.0
module load igmm/libs/htslib/1.6

IDS=$1

PATIENT_ID=`head -n $SGE_TASK_ID $PARAMS | tail -n 1 | cut -f 1`

WGS=/exports/igmm/eddie/Glioblastoma-WGS/WGS
ALIGNMENTS=$WGS/alignments
TUMOR_DIR=$ALIGNMENTS/${PATIENT_ID}T/${PATIENT_ID}T
NORMAL_DIR=$ALIGNMENTS/${PATIENT_ID}N/${PATIENT_ID}N
ALIGNED_BAM_FILE_TUMOR=$TUMOR_DIR/${PATIENT_ID}T-ready.bam
ALIGNED_BAM_FILE_NORMAL=$NORMAL_DIR/${PATIENT_ID}N-ready.bam

DBSNP=/exports/igmm/eddie/NextGenResources/bcbio-1.1.5/genomes/Hsapiens/hg38/variation/dbsnp-151.vcf.gz
SNP_PILEUP=/exports/igmm/eddie/Glioblastoma-WGS/scripts/facets/inst/extcode/snp-pileup

OUTDIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/cnv/facets/pileup_snp/${PATIENT_ID}-pileup.csv.gz

### Compile the snp-pileup script (done once)

export PATH=/exports/igmm/software/src/htslib/htslib:$PATH

cd /exports/igmm/eddie/Glioblastoma-WGS/scripts/facets/inst/extcode
g++ -std=c++11 snp-pileup.cpp -lhts -o snp-pileup

$SNP_PILEUP \
    --gzip --pseudo-snps=100 \
    --min-map-quality=30 \
    --min-base-quality=30 \
    --min-read-counts=10,0 \
    $DBSNP \
    $OUTDIR \
    $ALIGNED_BAM_FILE_NORMAL \
    $ALIGNED_BAM_FILE_TUMOR
    
    
    
    
    
    
    
    



