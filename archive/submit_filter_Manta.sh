#!/bin/bash

# To run this script, do 
# qsub -t 1-n submit_filter_Manta.sh <CONFIG> <IDS>
#
#$ -N FilterManta
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=8G
#$ -pe sharedmem 8
#$ -l h_rt=22:00:00

CONFIG=$1
IDS=$2

PATIENT_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1`
INTERM_HIGH_CONF_MANTA=/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/sv/manta/${PATIENT_ID}-manta.high.conf.vcf
INTERM_LOW_CONF_MANTA=/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/sv/manta/${PATIENT_ID}-manta.low.conf.vcf

source $CONFIG

zcat $MANTA_UNFILTERED_VCF | grep -E "PASS|#" > $INTERM_HIGH_CONF_MANTA
bgzip $INTERM_HIGH_CONF_MANTA
tabix $MANTA_HIGH_CONF_VCF

zcat $MANTA_UNFILTERED_VCF | grep -vE "PASS" > $INTERM_LOW_CONF_MANTA
bgzip $INTERM_LOW_CONF_MANTA
tabix $MANTA_LOW_CONF_VCF


