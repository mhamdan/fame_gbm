#!/bin/bash

# To run this script, do 
# qsub -t 1-n submit_sequenza_complete.sh <PARAM>
#
#$ -N seqz_to_done
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=16G
#$ -pe sharedmem 4
#$ -l h_rt=90:00:00

## Sequenza is installed in the py365 environment
export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/py365/bin:$PATH

PARAM=$1

## Define files/directories
PATIENT_ID=`head -n $SGE_TASK_ID $PARAM | tail -n 1`

WGS=/exports/igmm/eddie/Glioblastoma-WGS/WGS
ALIGNMENTS=$WGS/alignments
TUMOR_DIR=$ALIGNMENTS/${PATIENT_ID}T/${PATIENT_ID}T
NORMAL_DIR=$ALIGNMENTS/${PATIENT_ID}N/${PATIENT_ID}N
ALIGNED_BAM_FILE_TUMOR=$TUMOR_DIR/${PATIENT_ID}T-ready.bam
ALIGNED_BAM_FILE_NORMAL=$NORMAL_DIR/${PATIENT_ID}N-ready.bam

RESOURCES=/exports/igmm/eddie/Glioblastoma-WGS/resources
REFERENCE=$RESOURCES/refgenome38/hg38.fa

UTIL_DIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/sequenza/prepocess

## Use sequenza utils as based here https://sequenza-utils.readthedocs.io/en/latest/install.html
## Generate a wiggle file for gc content of the reference used. For downstream use. Just run once.
## sequenza-utils gc_wiggle -f $REFERENCE -o $OUTPUT_DIR/hg38.gc50Base.wig.gz −w 50

## Generate seqz file
sequenza-utils bam2seqz -n $ALIGNED_BAM_FILE_NORMAL \
    -t $ALIGNED_BAM_FILE_TUMOR \
    -F $REFERENCE \
    -gc $RESOURCES/hg38.gc50Base.wig.gz \
    -o $UTIL_DIR/${PATIENT_ID}.sequenza.gz

## Binning to reduce memory
sequenza-utils seqz_binning \
    --seqz $UTIL_DIR/${PATIENT_ID}.sequenza.gz \
    -w 50 \
    -o $UTIL_DIR/${PATIENT_ID}_bin50.seqz.gz

###Post-process bin50 results in R

module load roslin/gcc/7.3.0 
module load igmm/apps/R/3.6.3

##Export libraries - loaded before, as above
#export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/py365/bin:$PATH

##Define paths to sequenza R script and bin50
SEQUENZA_R=/exports/igmm/eddie/Glioblastoma-WGS/scripts/fame_gbm/R/sequenza.R
SEQZ_FILE=$UTIL_DIR/${PATIENT_ID}_bin50_edited.seqz.gz
RESULTS_DIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/sequenza/results/${PATIENT_ID}

##Edit sequenza file to feed into sequenza R script
cd $UTIL_DIR
gunzip ${PATIENT_ID}_bin50.seqz.gz
sed 's/chr//' ${PATIENT_ID}_bin50.seqz | sed 's/omosome/chromosome/' > ${PATIENT_ID}_bin50_edited.seqz
gzip ${PATIENT_ID}_bin50_edited.seqz
rm ${PATIENT_ID}_bin50_edited.seqz
rm ${PATIENT_ID}_bin50.seqz*
rm ${PATIENT_ID}.sequenza*

mkdir -p $RESULTS_DIR

##Run the script
Rscript $SEQUENZA_R \
    --seqz_file $SEQZ_FILE \
    --sample_name ${PATIENT_ID} \
    --output_dir $RESULTS_DIR
    



