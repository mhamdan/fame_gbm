#!/bin/bash

# To run this script, do 
# qsub -t 1-n submit_sequenza.sh CONFIG IDS
#
#$ -N sequenza
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=16G
#$ -pe sharedmem 8
#$ -l h_rt=72:00:00

##Define SGE parameters

unset MODULEPATH
. /etc/profile.d/modules.sh

module load roslin/gcc/7.3.0 
module load igmm/apps/R/3.6.3

##Export libraries
export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/py365/bin:$PATH

IDS=$1

##Define files/paths

PATIENT_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1`
SEQUENZA_DIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/sequenza
SEQUENZA_R=/exports/igmm/eddie/Glioblastoma-WGS/scripts/fame_gbm/R/sequenza.R

##Edit sequenza file to feed into sequenza R script
#cd $SEQUENZA_DIR
#gunzip ${PATIENT_ID}_bin50.seqz.gz
#sed 's/chr//' ${PATIENT_ID}_bin50.seqz | sed 's/omosome/chromosome/' > ${PATIENT_ID}_bin50_edited.seqz
#gzip ${PATIENT_ID}_bin50_edited.seqz
#rm ${PATIENT_ID}_bin50_edited.seqz
#rm ${PATIENT_ID}_bin50.seqz

SEQZ_FILE=$SEQUENZA_DIR/preprocess/${PATIENT_ID}_bin50_edited.seqz.gz
OUTPUT_DIR=$SEQUENZA_DIR/results/$PATIENT_ID

mkdir -p $OUTPUT_DIR

##Run the script
Rscript $SEQUENZA_R \
    --seqz_file $SEQZ_FILE \
    --sample_name ${PATIENT_ID} \
    --output_dir $OUTPUT_DIR
    



