#!/bin/bash

#$ -N trimmomatic
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=8G
#$ -pe sharedmem 8
#$ -l h_rt=120:00:00

unset MODULEPATH
. /etc/profile.d/modules.sh

###IDS=$1
###INPUT=`head -n $SGE_TASK_ID $IDS | tail -n 1`

INPUT=/exports/igmm/eddie/Glioblastoma-WGS/scmultiome/fastqs/atac/test/LEB01_E31/E31_S13_L001_R2_001.fastq.gz
OUTPUT=/exports/igmm/eddie/Glioblastoma-WGS/scmultiome/fastqs/atac/trimmed/LEB01_E31/E31_S13_L001_R2_001.fastq.gz 
JAR=/exports/igmm/software/pkg/el7/apps/trimmomatic/0.36/trimmomatic-0.36.jar

## specific for 10x scATACseq data trimming (R2 reads), restricting to 24bp reads

java -jar $JAR SE $INPUT $OUTPUT CROP:24











