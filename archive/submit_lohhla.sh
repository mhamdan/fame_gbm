#!/bin/bash

## This script runs edited version of LOHHLA to cater for hg38.
## We used externally sliced bam files previously used for LILAC

#$ -N LOHHLA_HG38
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=12G
#$ -pe sharedmem 12
#$ -l h_rt=10:00:00


CONFIG=$1
IDS=$2
MIN_COV=$3

## Define files/directories
PATIENT_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 1`
TYPE=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 2`
NEW_TYPE=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 3`
STAGE=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 4`
SAMPLE_ID=${PATIENT_ID}${TYPE}

HLA_HAPLOTYPE=
PURITY_PLOIDY=

source $CONFIG
export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/lohhla/bin:$PATH

## Guides:
### hlaPath => txt file containing hla haplotypes for the sample
### copyNumLoc => purity and ploidy of that sample

### Need novoalign licence from NovoCraft:
### /exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/lohhla/bin/novoalign-license-register /exports/igmm/eddie/Glioblastoma-WGS/scripts/lohhla/novoalign.lic
### Here output_dir is workdir

LOHHLA_EDITED=/exports/igmm/eddie/Glioblastoma-WGS/scripts/fame_gbm/R/LOHHLAscript_edited.R

Rscript $LOHHLA_EDITED \
    -i $PATIENT_ID \
    -o $OUTPUT_DIR \
    -n $SLICED_BAM_NORMAL_REALIGNED \
    -b $SLICED_BAM_DIR \
    --hlaPath $HLA_HAPLOTYPE \
    --HLAfastaLoc $HLA_FASTA \
    --CopyNumLoc $PURITY_PLOIDY \
    --minCoverageFilter $MIN_COV \
    --novoDir $NOVOALIGN_BIN \
    --gatkDir $GATK4_BIN \
    --HLAexonLoc $HLA_DAT 









