#!/bin/bash

# To run this script, do 
# qsub -t n submit_SVclone.sh CONFIG IDS
#
#$ -N SVclone
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=8G
#$ -pe sharedmem 8
#$ -l h_rt=24:00:00

CONFIG=$1
IDS=$2
BATCH=$3
STAGE=$4
TYPE=$5

## Define files/directories
PATIENT_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1`
SAMPLE_ID=${PATIENT_ID}${TYPE}
echo $SAMPLE_ID

source $CONFIG
source activate svclone

## Based on https://github.com/mcmero/SVclone
## Read length changed to a constant, rather than dynamically detected by the script (as samples have mixtures of read lengths due to trimming). See MultiQC.
## For cluster step, need dplyr installed in R

cd $SVCLONE_OUTPUT

## Annotate
if [[ ! -f $SVCLONE_ANN ]]; then
echo "Annotating breakpoints for ${SAMPLE_ID}..."
svclone annotate -i $PURPLE_SV_OUTPUT -b $ALIGNED_BAM_FILE_TUMOR -s $SAMPLE_ID -cfg $SVCLONE_CONFIG --sv_format vcf -o $SAMPLE_ID
fi

## Count
if [[ ! -f $SVCLONE_COUNT ]]; then
echo "Counting breakpoint reads for ${SAMPLE_ID}..."
svclone count -i $SVCLONE_ANN -b $ALIGNED_BAM_FILE_TUMOR -s $SAMPLE_ID -cfg $SVCLONE_CONFIG -o $SAMPLE_ID
fi

## Filter
echo "Filtering out low-confidence breaks in ${SAMPLE_ID}..."
svclone filter -s $SAMPLE_ID -i $SVCLONE_COUNT --snvs $PURPLE_SNV_OUTPUT -c $SVCLONE_CNV \
    --snv_format mutect -p $SVCLONE_PLOIDY_PURITY -cfg $SVCLONE_CONFIG -o $SAMPLE_ID

## Cluster
echo "Clustering breakpoints in ${SAMPLE_ID}..."
svclone cluster -s ${SAMPLE_ID} -p $SVCLONE_PLOIDY_PURITY -cfg $SVCLONE_CONFIG --ss_seeds 9999

if [ -f $SVCLONE_RESULTS/${SAMPLE_ID}/ccube_out/${SAMPLE_ID}_ccube_sv_results.RData ]; then
    echo "Successfully ran SVCLONE for ${SAMPLE_ID} to completion!"
fi






