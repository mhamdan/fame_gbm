#!/bin/bash

#$ -N svaba_local
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=8G
#$ -pe sharedmem 8
#$ -l h_rt=16:00:00

unset MODULEPATH
. /etc/profile.d/modules.sh

CONFIG=$1
IDS=$2

source $CONFIG
export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/bin:$PATH

SAMPLE=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 1`
BED=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 2`
ALIGNED_BAM_FILE_TUMOR=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 3`
ALIGNED_BAM_FILE_NORMAL=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 4`
TREES_ASSEMBLY_DIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/sv/svaba/trees_assembly
REFERENCE=/exports/igmm/eddie/Glioblastoma-WGS/resources/refgenome38/svaba/hg38.fa

cd $TREES_ASSEMBLY_DIR
echo "Running svaba for sample $SAMPLE and bed $BED"
echo "Tumor file: $ALIGNED_BAM_FILE_TUMOR"
echo "Normal file: $ALIGNED_BAM_FILE_NORMAL"

svaba run -t $ALIGNED_BAM_FILE_TUMOR -n $ALIGNED_BAM_FILE_NORMAL -p 12 -k $BED -G $REFERENCE -a ${SAMPLE}_$BED --write-asqg

## Index contig.bam files:
if [ -f ${SAMPLE}_${BED}.contigs.bam ]; then
    echo "File ${SAMPLE}_${BED}.contigs.bam exists, sorting and indexing..."
    samtools sort -m 8G -o ${SAMPLE}_${BED}.contigs.sorted.bam ${SAMPLE}_${BED}.contigs.bam 
    samtools index ${SAMPLE}_${BED}.contigs.sorted.bam  

else
    echo "File ${SAMPLE}_${BED}.contigs.bam does not exist."
    exit 1
fi




     



