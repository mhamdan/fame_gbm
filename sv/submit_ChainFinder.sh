#!/bin/bash
    
#$ -N ChainFinder
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=8G
#$ -pe sharedmem 8
#$ -l h_rt=84:00:00

CONFIG=$1
IDS=$2

CHAINFINDER_SAMPLE_DIR=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 1` 

source $CONFIG

## Append paths:
LD_LIBRARY_PATH=/exports/igmm/eddie/Glioblastoma-WGS/scripts/MCR/v81/runtime/glnxa64:/exports/igmm/eddie/Glioblastoma-WGS/scripts/MCR/v81/bin/glnxa64:/exports/igmm/eddie/Glioblastoma-WGS/scripts/MCR/v81/sys/os/glnxa64:/exports/igmm/eddie/Glioblastoma-WGS/scripts/MCR/v81/sys/java/jre/glnxa64/jre/lib/amd64/native_threads:/exports/igmm/eddie/Glioblastoma-WGS/scripts/MCR/v81/sys/java/jre/glnxa64/jre/lib/amd64/server:/exports/igmm/eddie/Glioblastoma-WGS/scripts/MCR/v81/sys/java/jre/glnxa64/jre/lib/amd64

XAPPLRESDIR=/exports/igmm/eddie/Glioblastoma-WGS/scripts/MCR/v81/X11/app-defaults

## Run ChainFinder on individual samples:
cd $CHAINFINDER_SAMPLE_DIR && $CHAINFINDER $MCR_ENV

######### INSTALLATION STEPS ############

#1) MATLAB Runtime Environment
## Installation of MATLAB Runtime environment based on https://uk.mathworks.com/help/compiler/install-the-matlab-runtime.html
## Download the appropriate version, depending on ChainFinder version used, as per https://software.broadinstitute.org/cancer/cga/chainfinder_download
## Download it here https://uk.mathworks.com/products/compiler/matlab-runtime.html, and unzip it.
## To install, use this command:
##  ./install -mode silent -agreeToLicense yes -destinationFolder <ABSOLUTE_PATH_TO_FOLDER>

## At the end of the installation, it asks you to some paths to PATH

#2) ChainFinder
### Download ChainFinder 1.01 here https://software.broadinstitute.org/cancer/cga/chainfinder_download
### Unzip it.

## Troubleshoot with test data:
### cd into unzipped chainfinder folder and run `./run_ChainFinder.sh <path_to_MCR_v81>`
### Ran successfully


## TODO
## May need to exclude SVs and samples as per https://www.biorxiv.org/content/biorxiv/early/2022/05/12/2022.01.09.475586.full.pdf
## In particular, may need to exclude regions identified as chromothripsis by ShatterSeek

## Troubleshooting logs:

### Try switching off summarising genes as it uses hg19 (summarizeGenes to "No", rather than "false"; default is "true")
### Worked, results produced without gene annotation.

## Own data:
### Prepare parameters.txt file as per https://software.broadinstitute.org/cancer/cga/chainfinder_run
### Use PURPLE outputs to prepare rr and CN segment files 

### Error that looked like probes_hg19. Get hg38 probes from https://gdc.cancer.gov/about-data/gdc-data-processing/gdc-reference-files and re-format within MatLab

### Still error, converted hg38 to hg19 => nope
### Tried running on individual samples => nope
### Finally able to run using ShatterSeek's processed CN and SV data, with colnames ordered exactly like these:
### SV = 
### CNV = 
### Parameter file with all parameters used
### Create a probe data using hg38 from GDC, and re-format in Matlab (even though this is not going to be used, as "seq" type is used, but just to be safe, since we can't see the .exe ChainFinder source code...) 
### Create a gene_table file using gencode v38 following the format gene, chr, start, end.

## IMPORTANT = Need to use all SVs in the cohort as background 







