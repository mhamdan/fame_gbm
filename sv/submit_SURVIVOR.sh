#!/bin/bash

# To run this script, do 
# qsub -t n submit_SURVIVOR.sh.sh IDS
#
#$ -N SURV
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=8G
#$ -pe sharedmem 8
#$ -l h_rt=1:00:00

IDS=$1

SAMPLE_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1`

VCF_DIR=/exports/igmm/eddie/Glioblastoma-WGS/scripts/CADD-SV/vcfs
INPUT_VCF=${SAMPLE_ID}.purple.sv.vcf
OUTPUT_BEDPE=${SAMPLE_ID}.purple.sv.bedpe

OUTPUT_BED_DIR=/exports/igmm/eddie/Glioblastoma-WGS/scripts/CADD-SV/input
OUTPUT_BED=id_${SAMPLE_ID}.bed

cd $VCF_DIR

export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/prepBED/bin:$PATH

SURVIVOR vcftobed $INPUT_VCF 0 -1 $OUTPUT_BEDPE
cut -f1,2,6,11 $OUTPUT_BEDPE > $OUTPUT_BED_DIR/$OUTPUT_BED







