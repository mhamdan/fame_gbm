#!/bin/bash

# To run this script, do 
# qsub -t 1-n submit_svaba.sh IDS
#
#$ -N svaba
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=16G
#$ -pe sharedmem 8
#$ -l h_rt=96:00:00

unset MODULEPATH
. /etc/profile.d/modules.sh

IDS=$1

CONFIG=/exports/igmm/eddie/Glioblastoma-WGS/scripts/fame_gbm/config.sh

source $CONFIG

PATIENT_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1`
TUMOR_DIR=$ALIGNMENTS/${PATIENT_ID}T/${PATIENT_ID}T
NORMAL_DIR=$ALIGNMENTS/${PATIENT_ID}N/${PATIENT_ID}N
ALIGNED_BAM_FILE_TUMOR=$TUMOR_DIR/${PATIENT_ID}T-ready.bam
ALIGNED_BAM_FILE_NORMAL=$NORMAL_DIR/${PATIENT_ID}N-ready.bam

## Svaba -> based https://github.com/walaj/svaba#filtering-and-refiltering
## Tips from https://gavinhalab.org/projects/SV_10X_analysis/ and https://github.com/walaj/svaba#gh-md-toc
## Example -> svaba run -t $TUM_BAM -n $NORM_BAM -p $CORES -D $DBSNP -a somatic_run -G $REF

export PATH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/bin:$PATH

#svaba run \
#    -t $ALIGNED_BAM_FILE_TUMOR \
#    -n $ALIGNED_BAM_FILE_NORMAL \
#    -D $KNOWN_SITES \
#    -p 8 \
#    -G $REFERENCE \
#    -a ${PATIENT_ID}

#mv files to svaba directory
SVABA_DIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/variants/sv/svaba

samtools sort -m 8G -o $SVABA_DIR/${PATIENT_ID}.contigs.sorted.bam $SVABA_DIR/${PATIENT_ID}.contigs.bam 
samtools index $SVABA_DIR/${PATIENT_ID}.contigs.sorted.bam   
     
     



